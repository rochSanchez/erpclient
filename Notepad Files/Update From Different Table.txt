UPDATE kempriseerp.quotationdetail quoteDetail 
INNER JOIN kempriseerp.quotationquotationdetail quoteQuoteDetail
ON quoteDetail.quotationdetail_id = quoteQuoteDetail.QuotationDetailsQuotationDetailId
SET quoteDetail.quotation_id = quoteQuoteDetail.QuotationsQuotationId