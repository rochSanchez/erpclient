﻿using Kemprise.Infrastructure.Controllers;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Events.Security;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Regions;
using Kemprise.PLM.WorkArea;
using Prism.Events;
using Prism.Regions;
using Unity;
using Unity.Lifetime;

namespace Kemprise.PLM.Controller
{
    public class PLMController : ControllerBase
    {
        public PLMController(IEventAggregator eventAggregator, IRegionManager regionManager,
            IUnityContainer container) :
            base(eventAggregator, regionManager, container)
        {
            eventAggregator.GetEvent<OpenModuleEvent>().Subscribe(OnOpenModule);
            eventAggregator.GetEvent<LogoutUserEvent>().Subscribe(OnLogoutUserEvent);
        }

        private void OnLogoutUserEvent()
        {
            Container.RegisterType<object, PLMWorkAreaView>(typeof(PLMWorkAreaView).FullName,
                new ContainerControlledLifetimeManager());
        }

        private void OnOpenModule(OpenModuleEventPayload obj)
        {
            if (obj.ErpModule == ErpModules.PLM)
            {
                obj.QuickAccess.IsSelected = true;
            }
            else
            {
                EventAggregator.GetEvent<DeSelectModuleEvent<PLMController>>().Publish();
                return;
            }

            RegionManager.RequestNavigate(RegionNames.MainWorkAreaRegion, typeof(PLMWorkAreaView).FullName);
        }
    }
}