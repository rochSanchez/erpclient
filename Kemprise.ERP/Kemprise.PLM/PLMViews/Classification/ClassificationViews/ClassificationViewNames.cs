﻿namespace Kemprise.PLM.PLMViews.Classification.ClassificationViews
{
    public class ClassificationViewNames
    {
        public const string ClassificationMainView = "Classification Main View";
        public const string ClassificationEditView = "Classification Edit View";
        public const string ClassificationDisplayView = "Classification Display View";
    }
}