﻿using ERP.WebApi.DTO.DTOs.PLM.Classification;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.PLM.PLMViews.Classification.ClassificationViews;
using Kemprise.PLM.WorkArea;
using Prism.Commands;
using System.Windows;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.PLM.PLMViews.Classification
{
    public sealed class ClassificationWorkAreaViewModel : PLMViewModelBase
    {
        private ClassificationEntity _entity;

        public ClassificationWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, PLMTypes.Classification.ToString())
        {
            SetView(ClassificationViewNames.ClassificationMainView);

            ClearCommand = new DelegateCommand(ClearExecute);
            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            EditCommand = new DelegateCommand(EditExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSaveExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshClassificationsExecute);

            Methods["ClassificationGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        private void WhenGetRecDone()
        {
            _entity.ClassificationDto.PropertyChanged += (s, e) => { SaveCommand.RaiseCanExecuteChanged(); };
            SaveCommand.RaiseCanExecuteChanged();
        }

        private void ClearExecute()
        {
            RadWindow.Confirm("This will clear everything. Continue?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.ClassificationDto.ClearValues();
            });
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new ClassificationEntity());
            _entity = Entity.GetDto<ClassificationEntity>();

            await OpenItemExecute(SelectedItem.Id);

            SetView(ClassificationViewNames.ClassificationDisplayView);
        }

        private async void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new ClassificationEntity());

            _entity = Entity.GetDto<ClassificationEntity>();
            _entity.IsNew = true;

            await OpenItemExecute("");

            IsClearVisible = true;
            SetView(ClassificationViewNames.ClassificationEditView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<PLMWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Part Classifications"));
        }

        private void RefreshClassificationsExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        private async void EditExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select a Classification",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            Entity = new DynamicNotifyPropertyChangedProxy(new ClassificationEntity());
            _entity = Entity.GetDto<ClassificationEntity>();

            await OpenItemExecute(SelectedItem.Id);

            IsClearVisible = false;
            SetView(ClassificationViewNames.ClassificationEditView);
        }

        private void BackExecute()
        {
            SetView(ClassificationViewNames.ClassificationMainView);
            RefreshExecute();
        }

        private async void SaveExecute()
        {
            var taskToPerform = _entity.IsNew ? "ClassificationAddRec" : "ClassificationModiRec";
            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSaveExecute()
        {
            var classificationDto = _entity.ClassificationDto.GetDto<ClassificationDto>();
            return !string.IsNullOrEmpty(classificationDto.Name);
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<PlmLookupAggregate>(ApiControllers.Lookup, "PlmGetLookups");
        }
    }
}