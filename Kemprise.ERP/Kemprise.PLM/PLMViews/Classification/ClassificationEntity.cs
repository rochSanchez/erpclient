﻿using ERP.WebApi.DTO.DTOs.PLM.Classification;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.PLM.PLMViews.Classification
{
    public class ClassificationEntity : EntityBase
    {
        public ClassificationEntity()
        {
            ClassificationDto = new DynamicNotifyPropertyChangedProxy(new ClassificationDto());
        }

        public DynamicNotifyPropertyChangedProxy ClassificationDto { get; set; }
    }
}