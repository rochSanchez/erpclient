﻿using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Classification;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.Classification.Methods
{
    public class ClassificationGetRec : GetMethodCallBase
    {
        public ClassificationGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "ClassificationGetRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override string Category => PLMTypes.Classification.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Classification;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var ClassificationAggregate =
                await GetRecordAsync<ClassificationAggregate>(MethodName, new object[] {MethodParameter});
            if (ClassificationAggregate == null) return;

            if (ClassificationAggregate.ClassificationDto != null)
                entity.SetProperty("ClassificationDto",
                    new DynamicNotifyPropertyChangedProxy(ClassificationAggregate.ClassificationDto));
        }
    }
}