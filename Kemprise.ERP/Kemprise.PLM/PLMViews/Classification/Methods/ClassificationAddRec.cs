﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Classification;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.Classification.Methods
{
    public class ClassificationAddRec : ErpSaveMethodCallBase
    {
        public ClassificationAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "ClassificationAddRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override ApiControllers Controller => ApiControllers.Classification;
        public override string Category => PLMTypes.Classification.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var ClassificationEntity = entity.As<ClassificationEntity>();

            var attributeAggregate = new ClassificationAggregate
            {
                ClassificationDto = ClassificationEntity.ClassificationDto.GetDto<ClassificationDto>()
            };

            var dataToSave = new Dictionary<string, object>
            {
                {nameof(ClassificationAggregate), attributeAggregate}
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}