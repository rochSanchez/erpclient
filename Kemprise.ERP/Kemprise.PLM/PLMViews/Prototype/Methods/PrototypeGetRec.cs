﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.Prototype.Methods
{
    public class PrototypeGetRec : GetMethodCallBase
    {
        public PrototypeGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override ApiControllers Controller => ApiControllers.PLM;
        public override string MethodName => "PrototypeGetRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override string Category => PLMTypes.Prototype.ToString();
        public override bool RunAtStartup => true;
        public override bool UseDefaultParameter => true;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<EcnAggregate>(MethodName, new object[] {MethodParameter});
            if (aggregate == null) return;

            if (aggregate.EcnDto != null)
                entity.SetProperty("EcnDto", new DynamicNotifyPropertyChangedProxy(aggregate.EcnDto));

            if (aggregate.StateDto != null)
                entity.SetProperty("StateDto", aggregate.StateDto);

            if (aggregate.EcnDocuments != null)
            {
                var dtos = new ObservableCollection<EcnDocumentDto>();
                aggregate.EcnDocuments.ForEach(item => dtos.Add(item));
                entity.SetProperty("EcnDocuments", dtos);
            }

            if (aggregate.EcnParts != null)
            {
                var dtos = new ObservableCollection<EcnPartDto>();
                aggregate.EcnParts.ForEach(item => dtos.Add(item));
                entity.SetProperty("EcnParts", dtos);
            }

            if (aggregate.EcnDrawings != null)
            {
                var dtos = new ObservableCollection<EcnDrawingDto>();
                aggregate.EcnDrawings.ForEach(item => dtos.Add(item));
                entity.SetProperty("EcnDrawings", dtos);
            }
        }
    }
}