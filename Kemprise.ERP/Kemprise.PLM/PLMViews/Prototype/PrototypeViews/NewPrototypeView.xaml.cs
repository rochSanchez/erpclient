﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.Prototype.PrototypeViews
{
    /// <summary>
    /// Interaction logic for NewDocumentView.xaml
    /// </summary>
    public partial class NewPrototypeView : UserControl
    {
        public NewPrototypeView()
        {
            InitializeComponent();
        }
    }
}