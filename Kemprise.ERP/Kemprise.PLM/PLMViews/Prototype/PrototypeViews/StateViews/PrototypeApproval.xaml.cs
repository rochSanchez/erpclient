﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.Prototype.PrototypeViews.StateViews
{
    /// <summary>
    /// Interaction logic for NewReleasedApproval.xaml
    /// </summary>
    public partial class PrototypeApproval : UserControl
    {
        public PrototypeApproval()
        {
            InitializeComponent();
        }
    }
}