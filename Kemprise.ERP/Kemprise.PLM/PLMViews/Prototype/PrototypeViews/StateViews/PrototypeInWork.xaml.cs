﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.Prototype.PrototypeViews.StateViews
{
    /// <summary>
    /// Interaction logic for NewReleasedInWork.xaml
    /// </summary>
    public partial class PrototypeInWork : UserControl
    {
        public PrototypeInWork()
        {
            InitializeComponent();
        }
    }
}