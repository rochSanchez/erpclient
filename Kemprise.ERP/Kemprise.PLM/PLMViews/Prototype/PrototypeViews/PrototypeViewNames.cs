﻿namespace Kemprise.PLM.PLMViews.Prototype.PrototypeViews
{
    public class PrototypeViewNames
    {
        public const string PrototypeMainView = "Prototype Main";
        public const string NewPrototypeView = "New Prototype View";
        public const string InWorkView = "In Work View";
        public const string ApprovalView = "Approval View";
    }
}