﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.ReviseToObsolete.ReviseToObsoleteViews
{
    /// <summary>
    /// Interaction logic for NewDocumentView.xaml
    /// </summary>
    public partial class NewReviseToObsoleteView : UserControl
    {
        public NewReviseToObsoleteView()
        {
            InitializeComponent();
        }
    }
}