﻿namespace Kemprise.PLM.PLMViews.ReviseToObsolete.ReviseToObsoleteViews
{
    public class ReviseToObsoleteViewNames
    {
        public const string ReviseToObsoleteMainView = "ReviseToObsolete Main";
        public const string NewReviseToObsoleteView = "New ReviseToObsolete View";
        public const string InWorkView = "In Work View";
        public const string ApprovalView = "Approval View";
        public const string ReviseToObsoleteApprover = "ReviseToObsolete Approver";
    }
}