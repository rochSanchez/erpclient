﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.ReviseToObsolete.ReviseToObsoleteViews.StateViews
{
    /// <summary>
    /// Interaction logic for NewReleasedApproval.xaml
    /// </summary>
    public partial class ReviseToObsoleteApproval : UserControl
    {
        public ReviseToObsoleteApproval()
        {
            InitializeComponent();
        }
    }
}