﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.ReviseToObsolete.ReviseToObsoleteViews.StateViews
{
    /// <summary>
    /// Interaction logic for NewReleasedInWork.xaml
    /// </summary>
    public partial class ReviseToObsoleteApprover : UserControl
    {
        public ReviseToObsoleteApprover()
        {
            InitializeComponent();
        }
    }
}