﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.ReviseToObsolete.Methods
{
    public class ReviseToObsoleteGetReleasedPrototypeParts : GetMethodCallBase
    {
        public ReviseToObsoleteGetReleasedPrototypeParts(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override ApiControllers Controller => ApiControllers.PLM;
        public override string MethodName => "GetAvailableParts";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override string Category => PLMTypes.ReviseToObsolete.ToString();
        public override bool RunAtStartup => false;
        public override bool UseDefaultParameter => false;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var partDtos = await GetRecordsAsync<PartDto>(MethodName, new object[] {MethodParameter});
            if (partDtos == null) return;
            var dtos = new ObservableCollection<PartDto>();
            partDtos.ForEach(item => dtos.Add(item));
            entity.SetProperty("AvailableParts", dtos);
        }
    }
}