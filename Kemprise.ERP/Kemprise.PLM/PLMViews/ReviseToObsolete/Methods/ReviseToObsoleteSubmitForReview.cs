﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.ReviseToObsolete.Methods
{
    public class ReviseToObsoleteSubmitForReview : ErpSaveMethodCallBase
    {
        public ReviseToObsoleteSubmitForReview(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "ReviseToObsoleteSubmitForReview";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override ApiControllers Controller => ApiControllers.PLM;
        public override string Category => PLMTypes.ReviseToObsolete.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var newReleasedEntity = entity.As<ReviseToObsoleteEntity>();

            var aggregate = new EcnAggregate
            {
                EcnDto = newReleasedEntity.EcnDto.GetDto<EcnDto>(),
                EcnParts = newReleasedEntity.EcnParts.ToList(),
                EcnDrawings = newReleasedEntity.EcnDrawings.ToList(),
                EcnDocuments = newReleasedEntity.EcnDocuments.ToList()
            };

            var dataToSave = new Dictionary<string, object>
            {
                {nameof(EcnAggregate), aggregate}
            };

            await SaveNewRecordAsync<EcnAggregate>(MethodName, dataToSave);
        }
    }
}