﻿using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Threading.Tasks;

namespace Kemprise.PLM.PLMViews.Document.Methods
{
    public class DocumentGetRec : GetMethodCallBase
    {
        public DocumentGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override ApiControllers Controller => ApiControllers.PLM;
        public override string MethodName => "DocumentGetRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override string Category => PLMTypes.Documents.ToString();
        public override bool RunAtStartup => true;
        public override bool UseDefaultParameter => true;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<DocumentDrawingAggregate>(MethodName, new object[] {MethodParameter});
            if (aggregate == null) return;

            if (aggregate.DocumentDto != null)
                entity.SetProperty("DocumentDto", new DynamicNotifyPropertyChangedProxy(aggregate.DocumentDto));

            if (aggregate.DocumentState != null)
                entity.SetProperty("StateDto", aggregate.DocumentState);
        }
    }
}