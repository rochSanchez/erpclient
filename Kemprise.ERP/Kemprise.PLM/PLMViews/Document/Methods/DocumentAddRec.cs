﻿using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.PLM.PLMViews.Document.Methods
{
    public class DocumentAddRec : ErpSaveMethodCallBase
    {
        public DocumentAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "DocumentAddRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override ApiControllers Controller => ApiControllers.PLM;
        public override string Category => PLMTypes.Documents.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var documentEntity = entity.As<DocumentEntity>();

            var aggregate = new DocumentDrawingAggregate
            {
                DocumentDto = documentEntity.DocumentDto.GetDto<DocumentDto>()
            };

            var dataToSave = new Dictionary<string, object>
            {
                {nameof(DocumentDrawingAggregate), aggregate}
            };

            await SaveNewRecordAsync<DocumentDrawingAggregate>(MethodName, dataToSave);
        }
    }
}