﻿using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.PLM.PLMViews.Document
{
    public class DocumentEntity : EntityBase
    {
        public DocumentEntity()
        {
            DocumentDto = new DynamicNotifyPropertyChangedProxy(new DocumentDto());
            StateDto = new StateDto();
        }

        public DynamicNotifyPropertyChangedProxy DocumentDto { get; set; }
        public StateDto StateDto { get; set; }
    }
}