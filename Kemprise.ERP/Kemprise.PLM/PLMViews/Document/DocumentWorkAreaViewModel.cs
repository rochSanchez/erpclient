﻿using ERP.WebApi.DTO.DTOs.PLM.Part;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using ERP.WebApi.DTO.Lookups.LookupItems;
using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.FileViewer;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.PLM.PLMViews.Document.DocumentViews;
using Kemprise.PLM.WorkArea;
using Microsoft.Win32;
using Prism.Commands;
using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.PLM.PLMViews.Document
{
    public sealed class DocumentWorkAreaViewModel : PLMViewModelBase
    {
        private readonly IFileService _fileService;
        private DocumentEntity _entity;
        private AddDocumentWindow _addDocumentWindow;
        private PdfViewer _pdfViewer;

        public DocumentWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, PLMTypes.Documents.ToString())
        {
            _fileService = fileService;
            SetView(DocumentViewNames.DocumentMainView);

            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            CancelCommand = new DelegateCommand(CancelExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshDocumentsExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            UploadDocumentCommand = new DelegateCommand(UploadDocumentExecute, UploadDocumentCanExecute);
            SelectDocumentCommand = new DelegateCommand(SelectDocumentExecute);
            UpdateDocumentCommand = new DelegateCommand(UpdateDocumentExecute);
        }

        private async void UpdateDocumentExecute()
        {
            if (SelectedItem == null) return;
            if (SelectedItem.As<DocumentListItem>().StateDto.Name != "In Work" &&
                SelectedItem.As<DocumentListItem>().StateDto.Name != "Rework")
            {
                EventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload("The document is locked for editing."));
                return;
            }

            Entity = new DynamicNotifyPropertyChangedProxy(new DocumentEntity());
            _entity = Entity.GetDto<DocumentEntity>();
            await OpenItemExecute(SelectedItem.Id, true);
            _entity.DocumentDto.PropertyChanged += (s, e) => UploadDocumentCommand.RaiseCanExecuteChanged();
            UploadDocumentCommand.RaiseCanExecuteChanged();

            _addDocumentWindow = new AddDocumentWindow { DataContext = this };
            _addDocumentWindow.ShowDialog();
        }

        private void SelectDocumentExecute()
        {
            var dialog = new OpenFileDialog
            {
                Filter = "PDF | *.pdf; "
            };

            dialog.ShowDialog();

            if (!dialog.FileNames.Any()) return;

            var noLargeFiles = dialog.FileNames.All(fileName =>
            {
                var fileInfo = new FileInfo(fileName);
                return fileInfo.Length <= 2097152;
            });

            if (!noLargeFiles)
            {
                EventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload("Large Files Found. Please Limit File Size to 2MB"));
                return;
            }

            _entity.DocumentDto.SetProperty("Name",
                Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(dialog.FileName)));
            _entity.DocumentDto.SetProperty("FileData", File.ReadAllBytes(dialog.FileName));
        }

        private bool UploadDocumentCanExecute()
        {
            if (_entity.DocumentDto.GetDto<DocumentDto>().FileData == null) return false;
            if (_entity.DocumentDto.GetDto<DocumentDto>().Name == string.Empty) return false;
            return true;
        }

        private async void UploadDocumentExecute()
        {
            if (_entity.IsNew)
            {
                var result = await SaveRecordAsync("DocumentAddRec");
                if (result.OperationResult == OperationResult.Success)
                {
                    _addDocumentWindow.Close();
                    RefreshExecute();
                }
            }
            else
            {
                var result = await SaveRecordAsync("DocumentModiRec");
                if (result.OperationResult == OperationResult.Success)
                {
                    _addDocumentWindow.Close();
                    RefreshExecute();
                }
            }
        }

        private void ClearExecute()
        {
            RadWindow.Confirm("This will clear everything. Continue?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                Entity.SetProperty("DocumentDto", new DynamicNotifyPropertyChangedProxy(new DocumentDto()));
            });
        }

        private async void SaveChangesExecute()
        {
            var result = await SaveRecordAsync("NewPartModiRec");
            if (result.OperationResult == OperationResult.Success) BackExecute();
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;
            try
            {
                Entity = new DynamicNotifyPropertyChangedProxy(new DocumentEntity());
                _entity = Entity.GetDto<DocumentEntity>();
                await OpenItemExecute(SelectedItem.Id, true);

                _pdfViewer = new PdfViewer { Owner = Application.Current.MainWindow, DataContext = this };
                _pdfViewer.SetDocument(_entity.DocumentDto.GetDto<DocumentDto>());
                _pdfViewer.ShowDialog();
            }
            catch (Exception e)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload(e.Message));
            }
        }

        private void RefreshDocumentsExecute(object obj)
        {
            RefreshExecute();
        }

        private void CancelExecute()
        {
            _addDocumentWindow?.Close();
        }

        private void BackExecute()
        {
            RefreshExecute();
            SetView(DocumentViewNames.DocumentMainView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<PLMWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Documents"));
        }

        private void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new DocumentEntity());
            _entity = Entity.GetDto<DocumentEntity>();
            _entity.DocumentDto.PropertyChanged += (s, e) => UploadDocumentCommand.RaiseCanExecuteChanged();
            UploadDocumentCommand.RaiseCanExecuteChanged();

            _entity.IsNew = true;
            _addDocumentWindow = new AddDocumentWindow { DataContext = this };
            _addDocumentWindow.ShowDialog();
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<PlmLookupAggregate>(ApiControllers.Lookup, "PlmGetLookups");
        }

        public override async void CreateFilters()
        {
            RegisterLookups();
            await GetLookupsAsync();

            var view = new CollectionViewSource { Source = EntityListItems };
            CollectionView = view;

            SelectedPlmFilter = Lookups["States"].FirstOrDefault(x => x.Text == "All").As<StateLookup>();
            SelectedItem = null;
        }

        private void FilterItems(string filterValue)
        {
            switch (filterValue)
            {
                case null:
                    return;

                case "All":
                    {
                        var view = new CollectionViewSource { Source = EntityListItems };
                        CollectionView = view;
                        break;
                    }
                default:
                    {
                        var filteredEntities = EntityListItems.Where(x => x.As<DocumentListItem>().StateDto.Name == filterValue);
                        var view = new CollectionViewSource { Source = filteredEntities };
                        CollectionView = view;
                        break;
                    }
            }
        }

        public StateLookup SelectedPlmFilter
        {
            get { return GetValue(() => SelectedPlmFilter); }
            set
            {
                SetValue(() => SelectedPlmFilter, value);
                if (value != null) FilterItems(value.Text);
                SelectedItem = null;
            }
        }

        public DelegateCommand SelectDocumentCommand { get; set; }
        public DelegateCommand UploadDocumentCommand { get; set; }
        public DelegateCommand UpdateDocumentCommand { get; set; }
    }
}