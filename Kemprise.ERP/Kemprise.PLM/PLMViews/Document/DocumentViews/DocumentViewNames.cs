﻿namespace Kemprise.PLM.PLMViews.Document.DocumentViews
{
    public class DocumentViewNames
    {
        public const string DocumentMainView = "Document Main";
        public const string NewDocumentView = "New Document View";
        public const string EditDocumentView = "Edit Document View";
    }
}