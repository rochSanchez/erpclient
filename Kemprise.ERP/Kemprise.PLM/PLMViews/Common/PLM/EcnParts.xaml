﻿<UserControl x:Class="Kemprise.PLM.PLMViews.Common.PLM.EcnParts"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:controls="clr-namespace:Kemprise.Infrastructure.Controls;assembly=Kemprise.Infrastructure"
             xmlns:telerik="http://schemas.telerik.com/2008/xaml/presentation">

    <UserControl.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary Source="/Kemprise.Infrastructure;component/Controls/GeneralStyles.xaml" />
            </ResourceDictionary.MergedDictionaries>

            <Style BasedOn="{StaticResource GenTextBlock}" TargetType="{x:Type TextBlock}">
                <Setter Property="Width" Value="100" />
            </Style>
            <Style BasedOn="{StaticResource GenRadMaskedTextInput}" TargetType="{x:Type telerik:RadMaskedTextInput}">
                <Setter Property="MaxWidth" Value="190" />
                <Setter Property="MinWidth" Value="190" />
            </Style>
            <Style BasedOn="{StaticResource GenCheckBox}" TargetType="{x:Type CheckBox}" />
        </ResourceDictionary>
    </UserControl.Resources>

    <Grid Margin="10">
        <Grid>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="Auto" />
            </Grid.ColumnDefinitions>
        </Grid>
        <StackPanel Margin="10">
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="*" />
                    <ColumnDefinition Width="Auto" />
                </Grid.ColumnDefinitions>

                <StackPanel Orientation="Horizontal">
                    <Image Width="35" Source="/Kemprise.Client;component/Images/PurchaseRequestIcon.png" />
                    <Separator Margin="15,5,15,5" Style="{StaticResource {x:Static ToolBar.SeparatorStyleKey}}" />
                    <TextBlock Style="{StaticResource CategoryTxtBlockStyle}" Text="Available Documents" />
                </StackPanel>
            </Grid>
            <Separator Margin="5,10,5,0" Background="{StaticResource NameTxtBlockColor}" />
        </StackPanel>
        <StackPanel>
            <telerik:RadGridView MinWidth="1580"
                                 MinHeight="480"
                                 MaxWidth="1580"
                                 MaxHeight="480"
                                 AutoExpandGroups="True"
                                 AutoGenerateColumns="False"
                                 CanUserDeleteRows="False"
                                 CanUserInsertRows="False"
                                 CanUserReorderColumns="False"
                                 CanUserResizeColumns="False"
                                 GroupRenderMode="Flat"
                                 IsReadOnly="True"
                                 IsTabStop="True"
                                 ItemsSource="{Binding Entity.EcnParts}"
                                 NewRowPosition="None"
                                 RowIndicatorVisibility="Collapsed"
                                 SelectedItem="{Binding SelectedEcnPart}">

                <telerik:EventToCommandBehavior.EventBindings>
                    <telerik:EventBinding Command="{Binding EcnViewPartCommand}"
                                          EventName="MouseDoubleClick"
                                          PassEventArgsToCommand="False" />
                    <telerik:EventBinding Command="{Binding SortedCommand}"
                                          EventName="Sorted"
                                          PassEventArgsToCommand="False" />
                    <telerik:EventBinding Command="{Binding GroupedCommand}"
                                          EventName="Grouped"
                                          PassEventArgsToCommand="False" />
                    <telerik:EventBinding Command="{Binding FilteredCommand}"
                                          EventName="FieldFilterEditorCreated"
                                          PassEventArgsToCommand="False" />
                </telerik:EventToCommandBehavior.EventBindings>

                <telerik:RadGridView.Resources>
                    <Style TargetType="TextBlock">
                        <Setter Property="Width" Value="Auto" />
                    </Style>
                    <Style BasedOn="{StaticResource GenRadMaskedTextInput}" TargetType="{x:Type telerik:RadMaskedTextInput}">
                        <Setter Property="Width" Value="Auto" />
                    </Style>
                    <Style BasedOn="{StaticResource GenRadMaskedCurrencyInput}" TargetType="{x:Type telerik:RadMaskedCurrencyInput}">
                        <Setter Property="Width" Value="Auto" />
                    </Style>
                </telerik:RadGridView.Resources>

                <telerik:RadGridView.ControlPanelItems>
                    <telerik:ControlPanelItem ButtonTooltip="Select Visible Columns">
                        <telerik:ControlPanelItem.Content>
                            <ListBox BorderThickness="0" ItemsSource="{Binding Columns}">
                                <ListBox.ItemTemplate>
                                    <DataTemplate>
                                        <CheckBox Content="{Binding Header, Mode=OneWay}" IsChecked="{Binding IsVisible, Mode=TwoWay}" />
                                    </DataTemplate>
                                </ListBox.ItemTemplate>
                            </ListBox>
                        </telerik:ControlPanelItem.Content>
                    </telerik:ControlPanelItem>
                </telerik:RadGridView.ControlPanelItems>

                <telerik:RadGridView.Columns>
                    <telerik:GridViewDataColumn Width="150"
                                                CellStyle="{StaticResource BoldCellStyle}"
                                                DataMemberBinding="{Binding Part.PartNumber, Mode=TwoWay}"
                                                Header="Part Number"
                                                HeaderTextAlignment="Center"
                                                HeaderTextWrapping="WrapWithOverflow"
                                                TextAlignment="Center" />
                    <telerik:GridViewDataColumn Width="500"
                                                DataMemberBinding="{Binding Part.Name, Mode=TwoWay}"
                                                Header="Name"
                                                HeaderTextAlignment="Center"
                                                HeaderTextWrapping="WrapWithOverflow"
                                                TextWrapping="WrapWithOverflow" />
                    <telerik:GridViewComboBoxColumn MinWidth="150"
                                                    DataMemberBinding="{Binding Part.ClassificationId, Mode=TwoWay}"
                                                    DisplayMemberPath="Text"
                                                    Header="Classification"
                                                    HeaderTextAlignment="Center"
                                                    HeaderTextWrapping="WrapWithOverflow"
                                                    ItemsSource="{Binding Path=DataContext.Lookups[Classifications], RelativeSource={RelativeSource AncestorType={x:Type UserControl}}}"
                                                    SelectedValueMemberPath="Id" />
                    <telerik:GridViewDataColumn Width="110"
                                                DataMemberBinding="{Binding Part.Version, Mode=TwoWay}"
                                                Header="Revision"
                                                HeaderTextAlignment="Center"
                                                HeaderTextWrapping="WrapWithOverflow"
                                                TextWrapping="WrapWithOverflow" />
                    <telerik:GridViewDataColumn Width="110"
                                                DataMemberBinding="{Binding Part.Iteration, Mode=TwoWay}"
                                                Header="Iteration"
                                                HeaderTextAlignment="Center"
                                                HeaderTextWrapping="WrapWithOverflow"
                                                TextWrapping="WrapWithOverflow" />
                    <telerik:GridViewDataColumn MinWidth="150"
                                                DataFormatString="dd MMM yyyy"
                                                DataMemberBinding="{Binding Part.CreatedAt, Mode=TwoWay}"
                                                Header="Date Created"
                                                HeaderTextAlignment="Center"
                                                HeaderTextWrapping="WrapWithOverflow" />
                    <telerik:GridViewDataColumn MinWidth="150"
                                                DataMemberBinding="{Binding Part.CreatedByFullName, Mode=TwoWay}"
                                                Header="Created By"
                                                HeaderTextAlignment="Center"
                                                HeaderTextWrapping="WrapWithOverflow" />
                    <telerik:GridViewComboBoxColumn MinWidth="150"
                                                    CellStyle="{StaticResource BoldCellStyle}"
                                                    DataMemberBinding="{Binding Part.StateId, Mode=TwoWay}"
                                                    DisplayMemberPath="Text"
                                                    Header="State"
                                                    HeaderTextAlignment="Center"
                                                    HeaderTextWrapping="WrapWithOverflow"
                                                    ItemsSource="{Binding Path=DataContext.Lookups[States], RelativeSource={RelativeSource AncestorType={x:Type UserControl}}}"
                                                    SelectedValueMemberPath="Id"
                                                    TextWrapping="WrapWithOverflow" />
                </telerik:RadGridView.Columns>
            </telerik:RadGridView>

            <StackPanel Margin="0,10,0,0"
                        Orientation="Horizontal"
                        Visibility="{Binding StateVisibility}">
                <controls:ErpButton Margin="0,0,5,0"
                                    Command="{Binding SelectPartsCommand}"
                                    Content="Add Part" />
                <controls:ErpButton Command="{Binding DeletePartCommand}" Content="Delete Part" />
            </StackPanel>
        </StackPanel>
    </Grid>
</UserControl>