﻿using System.Collections.ObjectModel;
using ERP.WebApi.DTO.DTOs.PLM.Classification;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using ERP.WebApi.DTO.DTOs.PLM.PartAttribute;
using Kemprise.Infrastructure.MethodCalls;

namespace Kemprise.PLM.PLMViews.PartsViewer
{
    public class PartsViewerEntity : EntityBase
    {
        public PartsViewerEntity()
        {
            BillOfMaterials = new ObservableCollection<BillOfMaterialDto>();
            PartAttributes = new ObservableCollection<PartPartAttributeDto>();
            PartDocuments = new ObservableCollection<PartDocumentDto>();
            PartDrawings = new ObservableCollection<PartDrawingDto>();
        }

        public PartDto NewPart { get; set; }
        public ClassificationDto ClassificationDto { get; set; }
        public ObservableCollection<BillOfMaterialDto> BillOfMaterials { get; set; }
        public ObservableCollection<PartPartAttributeDto> PartAttributes { get; set; }
        public ObservableCollection<PartDocumentDto> PartDocuments { get; set; }
        public ObservableCollection<PartDrawingDto> PartDrawings { get; set; }
        public DrawingDto DrawingDto { get; set; }
        public EcnDto OriginalEcn { get; set; }
        public EcnDto LatestEcn { get; set; }
    }
}