﻿using ERP.WebApi.DTO.Lookups.LookupAggregates;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.FileViewer;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.PLM.PLMViews.Common.PLM;
using Kemprise.PLM.PLMViews.PartsViewer.PartsViewerViews;
using Kemprise.PLM.WorkArea;
using Prism.Commands;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.PLM.PLMViews.PartsViewer
{
    public sealed class PartsViewerWorkAreaViewModel : PLMViewModelBase
    {
        private PartsViewerEntity _entity;
        private PartViewerWindow _partViewerWindow;
        private PdfViewer _pdfViewer;

        public Visibility StateVisibility
        {
            get { return GetValue(() => StateVisibility); }
            set { SetValue(() => StateVisibility, value); }
        }

        public PartsViewerWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, PLMTypes.PartsViewer.ToString())
        {
            SetView(PartsViewerViewNames.PartsViewerMainView);
            OpenCommand = new DelegateCommand(ViewPartExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshPartsViewersExecute);
            CancelCommand = new DelegateCommand(CloseViewerExecute);
            StateVisibility = Visibility.Collapsed;
            PrintPartCommand = new DelegateCommand(PrintNewReleasedPartExecute);
        }

        private async void PrintNewReleasedPartExecute()
        {
            if (_entity.PartDrawings.Any())
            {
                Methods["DrawingGetRec"].MethodParameter = _entity.PartDrawings.First().DrawingId.ToString();
                var result = await GetRecordAsync("DrawingGetRec");
            }

            var data = BuildPartDocument("PartViewerReportDetails");

            if (_entity.PartDrawings.Any())
            {
                _pdfViewer = new PdfViewer { Owner = Application.Current.MainWindow, DataContext = this };
                _pdfViewer.BuildPartDocumentPdf(data, _entity.DrawingDto.FileData);
                _pdfViewer.ShowDialog();
            }
            else
            {
                _pdfViewer = new PdfViewer { Owner = Application.Current.MainWindow, DataContext = this };
                _pdfViewer.BuildPartDocumentPdf(data);
                _pdfViewer.ShowDialog();
            }
        }

        private void CloseViewerExecute()
        {
            _partViewerWindow?.Close();
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<PLMWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Parts Viewer"));
        }

        private async void ViewPartExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new PartsViewerEntity());
            _entity = Entity.GetDto<PartsViewerEntity>();

            Methods["NewPartGetRec"].MethodParameter = SelectedItem.Id;
            _entity.BillOfMaterials.Clear();

            var result = await GetRecordAsync("NewPartGetRec");
            if (result.OperationResult != OperationResult.Success) return;

            _partViewerWindow = new PartViewerWindow { DataContext = this, Owner = Application.Current.MainWindow };
            _partViewerWindow.ShowDialog();
        }

        private void RefreshPartsViewersExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        public override async void CreateFilters()
        {
            RegisterLookups();
            await GetLookupsAsync();

            var view = new CollectionViewSource { Source = EntityListItems };
            CollectionView = view;

            SelectedItem = null;
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<PlmLookupAggregate>(ApiControllers.Lookup, "PlmGetLookups");
        }
    }
}