﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.NewPart.NewPartViews
{
    /// <summary>
    /// Interaction logic for EditView.xaml
    /// </summary>
    public partial class EditView : UserControl
    {
        public EditView()
        {
            InitializeComponent();
        }
    }
}