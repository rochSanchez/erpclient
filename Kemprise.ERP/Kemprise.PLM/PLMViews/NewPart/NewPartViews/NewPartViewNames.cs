﻿namespace Kemprise.PLM.PLMViews.NewPart.NewPartViews
{
    public class NewPartViewNames
    {
        public const string NewPartMainView = "New Part Main";
        public const string NewPartView = "New Part View";
        public const string EditNewPartView = "Edit New Part View";
    }
}