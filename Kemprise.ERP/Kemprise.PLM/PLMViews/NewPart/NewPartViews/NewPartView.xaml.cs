﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.NewPart.NewPartViews
{
    /// <summary>
    /// Interaction logic for NewProductView.xaml
    /// </summary>
    public partial class NewPartView : UserControl
    {
        public NewPartView()
        {
            InitializeComponent();
        }
    }
}