﻿using ERP.WebApi.DTO.DTOs.PLM.Part;
using ERP.WebApi.DTO.DTOs.PLM.PartAttribute;
using ERP.WebApi.DTO.Lookups;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using ERP.WebApi.DTO.Lookups.LookupItems;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.FileViewer;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.PLM.PLMViews.Common.PLM;
using Kemprise.PLM.PLMViews.NewPart.NewPartViews;
using Kemprise.PLM.WorkArea;
using Prism.Commands;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.PLM.PLMViews.NewPart
{
    public sealed class NewPartWorkAreaViewModel : PLMViewModelBase
    {
        private readonly IFileService _fileService;
        private NewPartEntity _entity;
        private AddAttributeWindow _addAttributeWindow;
        private PdfViewer _pdfViewer;
        private AddPartsWindow _addPartsWindow;
        private AddDrawingsWindow _addDrawingsWindow;
        private AddDocumentsWindow _addDocumentsWindow;

        public NewPartWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, PLMTypes.NewProductRequest.ToString())
        {
            _fileService = fileService;
            SetView(NewPartViewNames.NewPartMainView);

            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            ClearCommand = new DelegateCommand(ClearExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshNewProductsExecute);
            BackCommand = new DelegateCommand(BackExecute);
            CancelCommand = new DelegateCommand(CancelExecute);
            SaveCommand = new DelegateCommand(SaveExecute, SaveCanExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            SaveChangesCommand = new DelegateCommand(SaveChangesExecute);
            PrintPartCommand = new DelegateCommand(PrintPartExecute);

            //Attributes
            ClassificationChangedCommand = new DelegateCommand<int?>(ClassificationChangedExecute);
            EditAttributesCommand = new DelegateCommand(() => SaveCommand.RaiseCanExecuteChanged());

            //bom
            AddNewPartsCommand = new DelegateCommand(AddNewPartsExecute);
            AddPartCommand = new DelegateCommand(AddSelectedPartExecute, AddSelectedPartCanExecute);
            SelectSelectedPartCommand = new DelegateCommand(SelectSelectedPartExecute);
            SelectAvailablePartCommand = new DelegateCommand(SelectAvailablePartExecute);
            RefreshAvailablePartsCommand = new DelegateCommand(RefreshAvailablePartsExecute);
            DeletePartCommand = new DelegateCommand(DeletePartExecute, () => SelectedPartToDelete != null);

            //Drawings
            AddDrawingsCommand = new DelegateCommand(AddDrawingsExecute);
            AddSelectedDrawingsCommand = new DelegateCommand(AddSelectedDrawingsExecute, AddSelectedDrawingCanExecute);
            SelectSelectedDrawingCommand = new DelegateCommand(SelectSelectedDrawingExecute);
            SelectAvailableDrawingCommand = new DelegateCommand(SelectAvailableDrawingExecute);
            RefreshAvailableDrawingsCommand = new DelegateCommand(RefreshAvailableDrawingsExecute);
            DeleteDrawingCommand = new DelegateCommand(DeleteDrawingExecute, () => SelectedDrawingToDelete != null);

            //Documents
            AddDocumentsCommand = new DelegateCommand(AddDocumentsExecute);
            AddSelectedDocumentsCommand =
                new DelegateCommand(AddSelectedDocumentsExecute, AddSelectedDocumentsCanExecute);
            SelectSelectedDocumentCommand = new DelegateCommand(SelectSelectedDocumentExecute);
            SelectAvailableDocumentCommand = new DelegateCommand(SelectAvailableDocumentExecute);
            RefreshAvailableDocumentsCommand = new DelegateCommand(RefreshAvailableDocumentsExecute);
            DeleteDocumentCommand = new DelegateCommand(DeleteDocumentsExecute, () => SelectedDocumentToDelete != null);

            Methods["NewPartGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
            StateVisibility = Visibility.Visible;
        }

        private async void PrintPartExecute()
        {
            var data = BuildPartDocument("NewPartDetails");

            if (_entity.PartDrawings.Any())
            {
                Methods["DrawingGetRec"].MethodParameter = _entity.PartDrawings.First().DrawingId.ToString();
                var result = await GetRecordAsync("DrawingGetRec");
                _pdfViewer = new PdfViewer { Owner = Application.Current.MainWindow, DataContext = this };
                _pdfViewer.BuildPartDocumentPdf(data, _entity.DrawingDto.FileData);
                _pdfViewer.ShowDialog();
            }
            else
            {
                _pdfViewer = new PdfViewer { Owner = Application.Current.MainWindow, DataContext = this };
                _pdfViewer.BuildPartDocumentPdf(data);
                _pdfViewer.ShowDialog();
            }
        }

        private async void ClassificationChangedExecute(int? classificationId)
        {
            if (classificationId == null) return;
            var parameter = Methods["GetClassificationAttributes"].As<IErpGetMethodCall>().MethodParameter;
            if (parameter == classificationId.ToString()) return;
            Methods["GetClassificationAttributes"].As<IErpGetMethodCall>().MethodParameter =
                classificationId.Value.ToString();
            await GetRecordAsync("GetClassificationAttributes");
            SaveCommand.RaiseCanExecuteChanged();
        }

        //Documents
        private void DeleteDocumentsExecute()
        {
            var itemToRemove =
                _entity.PartDocuments.FirstOrDefault(x =>
                    x.PartDocumentId == SelectedDocumentToDelete.PartDocumentId);

            _entity.PartDocuments.Remove(itemToRemove);
            if (itemToRemove != null) _entity.AvailableDocuments.Add(itemToRemove.Document);
        }

        private async void RefreshAvailableDocumentsExecute()
        {
            _entity.AvailableDocuments.Clear();
            _entity.SelectedDocuments.Clear();
            await GetRecordAsync("GetAvailableDocuments");

            _entity.PartDocuments.ForEach(item =>
            {
                var itemToRemove =
                    _entity.AvailableDocuments.FirstOrDefault(x =>
                        x.DocumentId == item.Document.DocumentId);
                _entity.AvailableDocuments.Remove(itemToRemove);
            });
        }

        private void SelectAvailableDocumentExecute()
        {
            if (_entity.SelectedDocuments.Contains(SelectedAvailableDocument)) return;

            _entity.SelectedDocuments.Add(SelectedAvailableDocument);
            _entity.AvailableDocuments.Remove(SelectedAvailableDocument);
        }

        private void SelectSelectedDocumentExecute()
        {
            if (_entity.AvailableDocuments.Contains(SelectedDocumentToAdd)) return;

            _entity.AvailableDocuments.Add(SelectedDocumentToAdd);
            _entity.SelectedDocuments.Remove(SelectedDocumentToAdd);
        }

        private bool AddSelectedDocumentsCanExecute()
        {
            return _entity.SelectedDocuments.Any();
        }

        private void AddSelectedDocumentsExecute()
        {
            _entity.SelectedDocuments.ForEach(item =>
            {
                _entity.PartDocuments.Add(new PartDocumentDto() { Document = item });
            });

            _entity.SelectedDocuments.Clear();
            _addDocumentsWindow.Close();
        }

        private async void AddDocumentsExecute()
        {
            await GetRecordAsync("GetAvailableDocuments");
            _entity.SelectedDocuments.CollectionChanged += SelectedDocumentsOnCollectionChanged;
            _entity.PartDocuments.ForEach(item =>
            {
                var itemToRemove =
                    _entity.AvailableDocuments.FirstOrDefault(x =>
                        x.DocumentId == item.Document.DocumentId);
                if (itemToRemove != null) _entity.AvailableDocuments.Remove(itemToRemove);
            });

            _addDocumentsWindow = new AddDocumentsWindow() { DataContext = this };
            _addDocumentsWindow.ShowDialog();
        }

        private void SelectedDocumentsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            AddSelectedDocumentsCommand.RaiseCanExecuteChanged();
        }

        //Drawings
        private bool AddSelectedDrawingCanExecute()
        {
            return _entity.SelectedDrawings.Any();
        }

        private void DeleteDrawingExecute()
        {
            var itemToRemove =
                _entity.PartDrawings.FirstOrDefault(x =>
                    x.PartDrawingId == SelectedDrawingToDelete.PartDrawingId);

            _entity.PartDrawings.Remove(itemToRemove);
            if (itemToRemove != null) _entity.AvailableDrawings.Add(itemToRemove.Drawing);
        }

        private void SelectAvailableDrawingExecute()
        {
            if (_entity.SelectedDrawings.Contains(SelectedAvailableDrawing)) return;

            _entity.SelectedDrawings.Add(SelectedAvailableDrawing);
            _entity.AvailableDrawings.Remove(SelectedAvailableDrawing);
        }

        private void SelectSelectedDrawingExecute()
        {
            if (_entity.AvailableDrawings.Contains(SelectedDrawingToAdd)) return;

            _entity.AvailableDrawings.Add(SelectedDrawingToAdd);
            _entity.SelectedDrawings.Remove(SelectedDrawingToAdd);
        }

        private void AddSelectedDrawingsExecute()
        {
            _entity.SelectedDrawings.ForEach(item =>
            {
                _entity.PartDrawings.Add(new PartDrawingDto() { Drawing = item });
            });

            _entity.SelectedDrawings.Clear();
            _addDrawingsWindow.Close();
        }

        private async void RefreshAvailableDrawingsExecute()
        {
            _entity.AvailableDrawings.Clear();
            _entity.SelectedDrawings.Clear();
            await GetRecordAsync("GetAvailableDrawings");

            _entity.PartDrawings.ForEach(item =>
            {
                var itemToRemove =
                    _entity.AvailableDrawings.FirstOrDefault(x =>
                        x.DrawingId == item.Drawing.DrawingId);
                _entity.AvailableDrawings.Remove(itemToRemove);
            });

            _entity.SelectedDrawings.CollectionChanged += SelectedDrawingOnCollectionChanged;
        }

        private async void AddDrawingsExecute()
        {
            await GetRecordAsync("GetAvailableDrawings");
            _entity.SelectedDrawings.CollectionChanged += SelectedDrawingOnCollectionChanged;
            _entity.PartDrawings.ForEach(item =>
            {
                var itemToRemove =
                    _entity.AvailableDrawings.FirstOrDefault(x =>
                        x.DrawingId == item.Drawing.DrawingId);
                if (itemToRemove != null) _entity.AvailableDrawings.Remove(itemToRemove);
            });

            _addDrawingsWindow = new AddDrawingsWindow { DataContext = this };
            _addDrawingsWindow.ShowDialog();
        }

        private void SelectedDrawingOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            AddSelectedDrawingsCommand.RaiseCanExecuteChanged();
        }

        //BOM
        private void DeletePartExecute()
        {
            var itemToRemove =
                _entity.BillOfMaterials.FirstOrDefault(x =>
                    x.BillOfMaterialId == SelectedPartToDelete.BillOfMaterialId);

            _entity.BillOfMaterials.Remove(itemToRemove);
            if (itemToRemove != null) _entity.AvailableParts.Add(itemToRemove.Part);
        }

        private bool AddSelectedPartCanExecute()
        {
            return _entity.SelectedParts.Any();
        }

        private void AddSelectedPartExecute()
        {
            _entity.SelectedParts.ForEach(item =>
            {
                int maxNum = 0;
                if (_entity.BillOfMaterials.Any()) maxNum = _entity.BillOfMaterials.Select(x => x.ItemNumber).Max();
                var newBillOfMaterial = new BillOfMaterialDto { Part = item, ItemNumber = maxNum == 0 ? 1 : maxNum + 1 };
                _entity.BillOfMaterials.Add(newBillOfMaterial);
            });

            _entity.SelectedParts.Clear();
            _addPartsWindow.Close();
        }

        private void SelectSelectedPartExecute()
        {
            if (_entity.AvailableParts.Contains(SelectedPartToAdd)) return;

            _entity.AvailableParts.Add(SelectedPartToAdd);
            _entity.SelectedParts.Remove(SelectedPartToAdd);
        }

        private void SelectAvailablePartExecute()
        {
            if (_entity.SelectedParts.Contains(SelectedAvailablePart)) return;

            _entity.SelectedParts.Add(SelectedAvailablePart);
            _entity.AvailableParts.Remove(SelectedAvailablePart);
        }

        private async void AddNewPartsExecute()
        {
            await GetRecordAsync("GetAvailableParts");
            _entity.SelectedParts.CollectionChanged += SelectedPartOnCollectionChanged;
            _entity.BillOfMaterials.ForEach(item =>
            {
                var itemToRemove =
                    _entity.AvailableParts.FirstOrDefault(x =>
                        x.PartId == item.PartId);
                if (itemToRemove != null) _entity.AvailableParts.Remove(itemToRemove);
            });

            var currentItem = _entity.AvailableParts.FirstOrDefault(x =>
                x.PartId == _entity.NewPart.GetDto<PartDto>().PartId);
            _entity.AvailableParts.Remove(currentItem);

            _addPartsWindow = new AddPartsWindow { DataContext = this };
            _addPartsWindow.ShowDialog();
        }

        private async void RefreshAvailablePartsExecute()
        {
            _entity.AvailableParts.Clear();
            _entity.SelectedParts.Clear();
            await GetRecordAsync("GetAvailableParts");

            _entity.BillOfMaterials.ForEach(item =>
            {
                var itemToRemove =
                    _entity.AvailableParts.FirstOrDefault(x =>
                        x.PartId == item.PartId);
                _entity.AvailableParts.Remove(itemToRemove);
            });

            var currentItem = _entity.AvailableParts.FirstOrDefault(x =>
                x.PartId == _entity.NewPart.GetDto<PartDto>().PartId);
            _entity.AvailableParts.Remove(currentItem);

            _entity.SelectedParts.CollectionChanged += SelectedPartOnCollectionChanged;
        }

        private void SelectedPartOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            AddPartCommand.RaiseCanExecuteChanged();
        }

        //Attributes
        // private void SelectAvailablePartAttributeExecute()
        // {
        //     if (_entity.SelectedPartAttributes.Contains(SelectedAvailablePartAttribute)) return;
        //
        //     _entity.SelectedPartAttributes.Add(SelectedAvailablePartAttribute);
        //     _entity.AvailablePartAttributes.Remove(SelectedAvailablePartAttribute);
        // }
        //
        // private void DeleteAttributeExecute()
        // {
        //     var itemToRemove =
        //         _entity.PartAttributes.FirstOrDefault(x =>
        //             x.PartAttributeId == SelectedAttributeToDelete.PartAttributeId);
        //
        //     _entity.PartAttributes.Remove(itemToRemove);
        //     _entity.AvailablePartAttributes.Add(itemToRemove);
        // }
        //
        // private bool SaveNewAttributesCanExecute()
        // {
        //     return _entity.SelectedPartAttributes.Any();
        // }
        //
        // private void SaveNewAttributesExecute()
        // {
        //     _entity.SelectedPartAttributes.ForEach(item =>
        //     {
        //         if(_entity.PartAttributes.Contains(item)) return;
        //         _entity.PartAttributes.Add(item);
        //     });
        //
        //     _entity.SelectedPartAttributes.Clear();
        //     _addAttributeWindow.Close();
        // }
        //
        // private async void RefreshAttributesExecute()
        // {
        //     _entity.AvailablePartAttributes.Clear();
        //     _entity.SelectedPartAttributes.Clear();
        //     await GetRecordAsync("AvailableAttributeGetRecList");
        //     _entity.PartAttributes.ForEach(item =>
        //     {
        //         var itemToRemove =
        //             _entity.AvailablePartAttributes.FirstOrDefault(x =>
        //                 x.PartAttributeId == item.PartAttributeId);
        //         _entity.AvailablePartAttributes.Remove(itemToRemove);
        //     });
        //     _entity.SelectedPartAttributes.CollectionChanged += SelectedPartAttributesOnCollectionChanged;
        // }
        //
        // private void SelectedPartAttributesOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        // {
        //     AddNewAttributesCommand.RaiseCanExecuteChanged();
        // }
        //
        // private void SelectPartAttributeExecute()
        // {
        //     if (_entity.AvailablePartAttributes.Contains(SelectedPartAttribute)) return;
        //
        //     _entity.AvailablePartAttributes.Add(SelectedPartAttribute);
        //     _entity.SelectedPartAttributes.Remove(SelectedPartAttribute);
        // }
        //
        // private async void AddAttributeExecute()
        // {
        //     if (_entity.AvailablePartAttributes.Count == 0)
        //     {
        //         await GetRecordAsync("AvailableAttributeGetRecList");
        //         _entity.SelectedPartAttributes.CollectionChanged += SelectedPartAttributesOnCollectionChanged;
        //     }
        //     _entity.PartAttributes.ForEach(item =>
        //     {
        //         var itemToRemove =
        //             _entity.AvailablePartAttributes.FirstOrDefault(x =>
        //                 x.PartAttributeId == item.PartAttributeId);
        //         _entity.AvailablePartAttributes.Remove(itemToRemove);
        //     });
        //     _addAttributeWindow = new AddAttributeWindow { DataContext = this };
        //     _addAttributeWindow.ShowDialog();
        // }

        private void ClearExecute()
        {
            RadWindow.Confirm("This will clear everything. Continue?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                Entity.SetProperty("NewPart", new DynamicNotifyPropertyChangedProxy(new PartDto()));
            });
        }

        private async void SaveChangesExecute()
        {
            var result = await SaveRecordAsync("NewPartModiRec");
            if (result.OperationResult == OperationResult.Success) BackExecute();
        }

        private void WhenGetRecDone()
        {
            if (!_entity.IsNew)
            {
            }
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new NewPartEntity());
            _entity = Entity.GetDto<NewPartEntity>();
            await OpenItemExecute(SelectedItem.Id, true);
            if (_entity.StateDto?.Name != "In Work" && _entity.StateDto?.Name != "Rework")
                StateVisibility = Visibility.Collapsed;

            SetView(NewPartViewNames.EditNewPartView);
        }

        private void RefreshNewProductsExecute(object obj)
        {
            RefreshExecute();
        }

        private bool SaveCanExecute()
        {
            if (_entity.NewPart.GetDto<PartDto>().UnitOfMeasurementId == 0) return false;
            if (string.IsNullOrEmpty(_entity.NewPart.GetDto<PartDto>().Name)) return false;
            if (string.IsNullOrEmpty(_entity.NewPart.GetDto<PartDto>().Source)) return false;
            if (string.IsNullOrEmpty(_entity.NewPart.GetDto<PartDto>().AssemblyMode)) return false;
            if (_entity.NewPart.GetDto<PartDto>().ClassificationId == 0) return false;
            if (_entity.PartAttributes.Any())
                if (_entity.PartAttributes.Any(x => string.IsNullOrEmpty(x.Value)))
                    return false;
            return true;
        }

        private async void SaveExecute()
        {
            if (_entity.IsNew)
            {
                var result = await SaveRecordAsync("NewPartAddRec");
                if (result.OperationResult == OperationResult.Success) BackExecute();
            }
            else
            {
                var result = await SaveRecordAsync("NewPartModiRec");
                if (result.OperationResult == OperationResult.Success)
                    await GetRecordAsync("NewPartGetRec");
            }
        }

        private void CancelExecute()
        {
            if (_addAttributeWindow != null)
            {
                _entity.SelectedPartAttributes.ForEach(item => _entity.AvailablePartAttributes.Add(item));
                _entity.SelectedPartAttributes.Clear();
                _addAttributeWindow?.Close();
            }

            if (_addPartsWindow != null)
            {
                _entity.SelectedParts.ForEach(item => _entity.AvailableParts.Add(item));
                _entity.SelectedParts.Clear();
                _addPartsWindow?.Close();
            }

            if (_addDrawingsWindow != null)
            {
                _entity.SelectedDrawings.ForEach(item => _entity.AvailableDrawings.Add(item));
                _entity.SelectedDrawings.Clear();
                _addDrawingsWindow?.Close();
            }

            if (_addDocumentsWindow != null)
            {
                _entity.SelectedDocuments.ForEach(item => _entity.AvailableDocuments.Add(item));
                _entity.SelectedDocuments.Clear();
                _addDocumentsWindow?.Close();
            }
        }

        private void BackExecute()
        {
            StateVisibility = Visibility.Visible;
            RefreshExecute();
            SetView(NewPartViewNames.NewPartMainView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<PLMWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Parts"));
        }

        private void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new NewPartEntity());
            _entity = Entity.GetDto<NewPartEntity>();
            _entity.NewPart.GetDto<PartDto>().UnitOfMeasurementId = 32;

            _entity.NewPart.PropertyChanged += (s, e) => SaveCommand.RaiseCanExecuteChanged();
            _entity.PartAttributes.CollectionChanged += (s, e) => SaveCommand.RaiseCanExecuteChanged();
            SaveCommand.RaiseCanExecuteChanged();

            CommandName = "Save";
            _entity.IsNew = true;
            SetView(NewPartViewNames.NewPartView);
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<PlmLookupAggregate>(ApiControllers.Lookup, "PlmGetLookups");
        }

        public override async void CreateFilters()
        {
            RegisterLookups();
            await GetLookupsAsync();

            var view = new CollectionViewSource { Source = EntityListItems };
            CollectionView = view;

            SelectedPlmFilter = Lookups["States"].FirstOrDefault(x => x.Text == "All").As<StateLookup>();
            SelectedItem = null;
        }

        private void FilterItems(string filterValue)
        {
            switch (filterValue)
            {
                case null:
                    return;

                case "All":
                    {
                        var view = new CollectionViewSource { Source = EntityListItems };
                        CollectionView = view;
                        break;
                    }
                default:
                    {
                        var filteredEntities = EntityListItems.Where(x => x.As<NewPartListItem>().StateDto.Name == filterValue);
                        var view = new CollectionViewSource { Source = filteredEntities };
                        CollectionView = view;
                        break;
                    }
            }
        }

        public StateLookup SelectedPlmFilter
        {
            get { return GetValue(() => SelectedPlmFilter); }
            set
            {
                SetValue(() => SelectedPlmFilter, value);
                if (value != null) FilterItems(value.Text);
                SelectedItem = null;
            }
        }

        public string CommandName
        {
            get { return GetValue(() => CommandName); }
            set { SetValue(() => CommandName, value); }
        }

        public LookupEntity SelectedProductType
        {
            get { return GetValue(() => SelectedProductType); }
            set { SetValue(() => SelectedProductType, value, SetProductTypeCommand.RaiseCanExecuteChanged); }
        }

        public PartDto SelectedPartToAdd
        {
            get { return GetValue(() => SelectedPartToAdd); }
            set { SetValue(() => SelectedPartToAdd, value); }
        }

        public PartDto SelectedAvailablePart
        {
            get { return GetValue(() => SelectedAvailablePart); }
            set { SetValue(() => SelectedAvailablePart, value); }
        }

        public PartAttributeDto SelectedAvailablePartAttribute
        {
            get { return GetValue(() => SelectedAvailablePartAttribute); }
            set { SetValue(() => SelectedAvailablePartAttribute, value); }
        }

        public PartAttributeDto SelectedPartAttribute
        {
            get { return GetValue(() => SelectedPartAttribute); }
            set { SetValue(() => SelectedPartAttribute, value); }
        }

        public PartAttributeDto SelectedAttributeToDelete
        {
            get { return GetValue(() => SelectedAttributeToDelete); }
            set { SetValue(() => SelectedAttributeToDelete, value, DeleteAttributeCommand.RaiseCanExecuteChanged); }
        }

        public BillOfMaterialDto SelectedPartToDelete
        {
            get { return GetValue(() => SelectedPartToDelete); }
            set { SetValue(() => SelectedPartToDelete, value, DeletePartCommand.RaiseCanExecuteChanged); }
        }

        public PartDrawingDto SelectedDrawingToDelete
        {
            get { return GetValue(() => SelectedDrawingToDelete); }
            set { SetValue(() => SelectedDrawingToDelete, value, DeleteDrawingCommand.RaiseCanExecuteChanged); }
        }

        public PartDocumentDto SelectedDocumentToDelete
        {
            get { return GetValue(() => SelectedDocumentToDelete); }
            set { SetValue(() => SelectedDocumentToDelete, value, DeleteDocumentCommand.RaiseCanExecuteChanged); }
        }

        public SimpleDocumentDto SelectedDocumentToAdd
        {
            get { return GetValue(() => SelectedDocumentToAdd); }
            set { SetValue(() => SelectedDocumentToAdd, value); }
        }

        public SimpleDocumentDto SelectedAvailableDocument
        {
            get { return GetValue(() => SelectedAvailableDocument); }
            set { SetValue(() => SelectedAvailableDocument, value); }
        }

        public SimpleDrawingDto SelectedDrawingToAdd
        {
            get { return GetValue(() => SelectedDrawingToAdd); }
            set { SetValue(() => SelectedDrawingToAdd, value); }
        }

        public SimpleDrawingDto SelectedAvailableDrawing
        {
            get { return GetValue(() => SelectedAvailableDrawing); }
            set { SetValue(() => SelectedAvailableDrawing, value); }
        }

        public Visibility StateVisibility
        {
            get { return GetValue(() => StateVisibility); }
            set { SetValue(() => StateVisibility, value); }
        }

        public DelegateCommand SetProductTypeCommand { get; set; }
        public DelegateCommand SaveChangesCommand { get; set; }
        public DelegateCommand AddNewAttributesCommand { get; set; }
        public DelegateCommand DeleteAttributeCommand { get; set; }

        //attributes
        public DelegateCommand AddAttributeCommand { get; set; }

        public DelegateCommand SelectAvailablePartAttributeCommand { get; set; }
        public DelegateCommand SelectPartAttributeCommand { get; set; }
        public DelegateCommand RefreshAttributesCommand { get; set; }
        public DelegateCommand<int?> ClassificationChangedCommand { get; set; }
        public DelegateCommand EditAttributesCommand { get; set; }

        //bom
        public DelegateCommand AddNewPartsCommand { get; set; }

        public DelegateCommand SelectSelectedPartCommand { get; set; }
        public DelegateCommand SelectAvailablePartCommand { get; set; }
        public DelegateCommand RefreshAvailablePartsCommand { get; set; }
        public DelegateCommand DeletePartCommand { get; set; }

        //drawings
        public DelegateCommand DeleteDrawingCommand { get; set; }

        public DelegateCommand RefreshAvailableDrawingsCommand { get; set; }
        public DelegateCommand SelectAvailableDrawingCommand { get; set; }
        public DelegateCommand SelectSelectedDrawingCommand { get; set; }
        public DelegateCommand AddSelectedDrawingsCommand { get; set; }
        public DelegateCommand AddDrawingsCommand { get; set; }

        //documents
        public DelegateCommand DeleteDocumentCommand { get; set; }

        public DelegateCommand RefreshAvailableDocumentsCommand { get; set; }
        public DelegateCommand SelectAvailableDocumentCommand { get; set; }
        public DelegateCommand SelectSelectedDocumentCommand { get; set; }
        public DelegateCommand AddSelectedDocumentsCommand { get; set; }
        public DelegateCommand AddDocumentsCommand { get; set; }
    }
}