﻿using System.Collections.ObjectModel;
using System.Security.Cryptography.X509Certificates;
using ERP.WebApi.DTO.DTOs.PLM.Classification;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using ERP.WebApi.DTO.DTOs.PLM.PartAttribute;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.PLM.PLMViews.NewPart
{
    public class NewPartEntity : EntityBase
    {
        public NewPartEntity()
        {
            NewPart = new DynamicNotifyPropertyChangedProxy(new PartDto());
            BillOfMaterials = new ObservableCollection<BillOfMaterialDto>();
            Drawings = new ObservableCollection<SimpleDrawingDto>();

            PartAttributes = new ObservableCollection<PartPartAttributeDto>();
            SelectedPartAttributes = new ObservableCollection<PartAttributeDto>();
            AvailablePartAttributes = new ObservableCollection<PartAttributeDto>();

            SelectedParts = new ObservableCollection<PartDto>();
            AvailableParts = new ObservableCollection<PartDto>();

            SelectedDrawings = new ObservableCollection<SimpleDrawingDto>();
            AvailableDrawings = new ObservableCollection<SimpleDrawingDto>();

            SelectedDocuments = new ObservableCollection<SimpleDocumentDto>();
            AvailableDocuments = new ObservableCollection<SimpleDocumentDto>();

            PartDrawings = new ObservableCollection<PartDrawingDto>();
            PartDocuments = new ObservableCollection<PartDocumentDto>();

            WhereUsedDtos = new ObservableCollection<WhereUsedDto>();
        }

        public ObservableCollection<PartDocumentDto> PartDocuments { get; set; }

        public ObservableCollection<PartDrawingDto> PartDrawings { get; set; }

        public ObservableCollection<SimpleDrawingDto> Drawings { get; set; }

        public DynamicNotifyPropertyChangedProxy NewPart { get; set; }

        public ObservableCollection<BillOfMaterialDto> BillOfMaterials { get; set; }

        public ObservableCollection<PartPartAttributeDto> PartAttributes { get; set; }

        public StateDto StateDto { get; set; }

        public ClassificationDto ClassificationDto { get; set; }

        public ObservableCollection<PartAttributeDto> SelectedPartAttributes { get; set; }

        public ObservableCollection<PartAttributeDto> AvailablePartAttributes { get; set; }

        public ObservableCollection<PartDto> SelectedParts { get; set; }

        public ObservableCollection<PartDto> AvailableParts { get; set; }

        public ObservableCollection<SimpleDrawingDto> SelectedDrawings { get; set; }

        public ObservableCollection<SimpleDrawingDto> AvailableDrawings { get; set; }

        public ObservableCollection<SimpleDocumentDto> SelectedDocuments { get; set; }

        public ObservableCollection<SimpleDocumentDto> AvailableDocuments { get; set; }

        public ObservableCollection<WhereUsedDto> WhereUsedDtos { get; set; }

        public DrawingDto DrawingDto { get; set; }
    }
}