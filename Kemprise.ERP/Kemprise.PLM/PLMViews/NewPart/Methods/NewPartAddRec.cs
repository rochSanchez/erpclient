﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.NewPart.Methods
{
    public class NewPartAddRec : ErpSaveMethodCallBase
    {
        public NewPartAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "NewPartAddRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override ApiControllers Controller => ApiControllers.PLM;
        public override string Category => PLMTypes.NewProductRequest.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var newPartEntity = entity.As<NewPartEntity>();

            var productRequestAggregate = new NewPartAggregate
            {
                NewPart = newPartEntity.NewPart.GetDto<PartDto>(),
                PartAttributes = newPartEntity.PartAttributes.ToList(),
                PartDrawings = newPartEntity.PartDrawings.ToList(),
                PartDocuments = newPartEntity.PartDocuments.ToList()
            };

            var dataToSave = new Dictionary<string, object>
            {
                {nameof(NewPartAggregate), productRequestAggregate}
            };

            await SaveNewRecordAsync<NewPartAggregate>(MethodName, dataToSave);
        }
    }
}