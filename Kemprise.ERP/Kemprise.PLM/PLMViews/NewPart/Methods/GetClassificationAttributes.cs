﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.PartAttribute;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.NewPart.Methods
{
    public class GetClassificationAttributes : GetMethodCallBase
    {
        public GetClassificationAttributes(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override ApiControllers Controller => ApiControllers.PLM;
        public override string MethodName => "GetClassificationAttributes";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override string Category => PLMTypes.NewProductRequest.ToString();
        public override bool RunAtStartup => false;
        public override bool UseDefaultParameter => false;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var partAttributes = await GetRecordsAsync<PartAttributeDto>(MethodName, new object[] {MethodParameter});
            if (partAttributes == null) return;
            var dtos = new ObservableCollection<PartPartAttributeDto>();
            partAttributes.ForEach(item => dtos.Add(new PartPartAttributeDto
                {PartAttributeId = item.PartAttributeId, Name = item.Name, ClassificationId = item.ClassificationId}));
            entity.SetProperty("PartAttributes", dtos);
        }
    }
}