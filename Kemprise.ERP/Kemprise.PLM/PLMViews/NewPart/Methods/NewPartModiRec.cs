﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.NewPart.Methods
{
    public class NewPartModiRec : ErpSaveMethodCallBase
    {
        public NewPartModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "NewPartModiRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override ApiControllers Controller => ApiControllers.PLM;
        public override string Category => PLMTypes.NewProductRequest.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var newPartEntity = entity.As<NewPartEntity>();

            var partAggregate = new NewPartAggregate()
            {
                NewPart = newPartEntity.NewPart.GetDto<PartDto>(),
                PartAttributes = newPartEntity.PartAttributes.ToList(),
                BillOfMaterials = newPartEntity.BillOfMaterials.ToList(),
                PartDrawings = newPartEntity.PartDrawings.ToList(),
                PartDocuments = newPartEntity.PartDocuments.ToList()
            };

            var dataToSave = new Dictionary<string, object>
            {
                {nameof(NewPartAggregate), partAggregate}
            };

            await SaveNewRecordAsync<InquiryAggregate>(MethodName, dataToSave);
        }
    }
}