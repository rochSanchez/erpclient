﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using ERP.WebApi.DTO.DTOs.PLM.PartAttribute;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.NewPart.Methods
{
    public class NewPartGetRec : GetMethodCallBase
    {
        public NewPartGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override ApiControllers Controller => ApiControllers.PLM;
        public override string MethodName => "NewPartGetRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override string Category => PLMTypes.NewProductRequest.ToString();
        public override bool RunAtStartup => true;
        public override bool UseDefaultParameter => true;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<NewPartAggregate>(MethodName, new object[] {MethodParameter});
            if (aggregate == null) return;

            if (aggregate.NewPart != null)
                entity.SetProperty("NewPart", new DynamicNotifyPropertyChangedProxy(aggregate.NewPart));

            if (aggregate.StateDto != null)
                entity.SetProperty("StateDto", aggregate.StateDto);

            if (aggregate.ClassificationDto != null)
                entity.SetProperty("ClassificationDto", aggregate.ClassificationDto);

            if (aggregate.PartAttributes != null)
            {
                var partAttributes = new ObservableCollection<PartPartAttributeDto>();
                aggregate.PartAttributes.ForEach(item => partAttributes.Add(item));
                entity.SetProperty("PartAttributes", partAttributes);
            }

            if (aggregate.BillOfMaterials != null)
            {
                var billOfMaterialDtos = new ObservableCollection<BillOfMaterialDto>();
                aggregate.BillOfMaterials.ForEach(item => billOfMaterialDtos.Add(item));
                entity.SetProperty("BillOfMaterials", billOfMaterialDtos);
            }

            if (aggregate.PartDrawings != null)
            {
                var partDrawingDtos = new ObservableCollection<PartDrawingDto>();
                aggregate.PartDrawings.ForEach(item => partDrawingDtos.Add(item));
                entity.SetProperty("PartDrawings", partDrawingDtos);
            }

            if (aggregate.PartDocuments != null)
            {
                var documentDtos = new ObservableCollection<PartDocumentDto>();
                aggregate.PartDocuments.ForEach(item => documentDtos.Add(item));
                entity.SetProperty("PartDocuments", documentDtos);
            }

            if (aggregate.WhereUsedDtos != null)
            {
                var whereUsedDtos = new ObservableCollection<WhereUsedDto>();
                aggregate.WhereUsedDtos.ForEach(item => whereUsedDtos.Add(item));
                entity.SetProperty("WhereUsedDtos", whereUsedDtos);
            }
        }
    }
}