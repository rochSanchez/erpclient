﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.PartAttribute;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.NewPart.Methods
{
    public class AvailableAttributeGetRecList : GetMethodCallBase
    {
        public AvailableAttributeGetRecList(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override ApiControllers Controller => ApiControllers.PLM;
        public override string MethodName => "AvailableAttributeGetRecList";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override string Category => PLMTypes.NewProductRequest.ToString();
        public override bool RunAtStartup => false;
        public override bool UseDefaultParameter => false;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var attributeListItem = await GetRecordsAsync<PartAttributeDto>(MethodName, new object[] {MethodParameter});
            if (attributeListItem == null) return;
            var attributeListItems = new ObservableCollection<PartAttributeDto>();
            attributeListItem.ForEach(item => attributeListItems.Add(item));
            entity.SetProperty("AvailablePartAttributes", attributeListItems);
        }
    }
}