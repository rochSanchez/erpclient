﻿using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.NewPart.Methods
{
    public class NewPartDrawingGetRec : GetMethodCallBase
    {
        public NewPartDrawingGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override ApiControllers Controller => ApiControllers.PLM;
        public override string MethodName => "DrawingGetRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override string Category => PLMTypes.NewProductRequest.ToString();
        public override bool RunAtStartup => false;
        public override bool UseDefaultParameter => false;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<DocumentDrawingAggregate>(MethodName, new object[] { MethodParameter });
            if (aggregate == null) return;

            if (aggregate.DrawingDto != null)
                entity.SetProperty("DrawingDto", aggregate.DrawingDto);
        }
    }
}