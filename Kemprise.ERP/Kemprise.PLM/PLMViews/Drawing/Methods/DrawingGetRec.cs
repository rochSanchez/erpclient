﻿using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.Drawing.Methods
{
    public class DrawingGetRec : GetMethodCallBase
    {
        public DrawingGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override ApiControllers Controller => ApiControllers.PLM;
        public override string MethodName => "DrawingGetRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override string Category => PLMTypes.Drawings.ToString();
        public override bool RunAtStartup => true;
        public override bool UseDefaultParameter => true;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<DocumentDrawingAggregate>(MethodName, new object[] {MethodParameter});
            if (aggregate == null) return;

            if (aggregate.DrawingDto != null)
                entity.SetProperty("DrawingDto", new DynamicNotifyPropertyChangedProxy(aggregate.DrawingDto));

            if (aggregate.DrawingState != null)
                entity.SetProperty("StateDto", aggregate.DocumentState);
        }
    }
}