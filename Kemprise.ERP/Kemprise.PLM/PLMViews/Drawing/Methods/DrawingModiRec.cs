﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.Drawing.Methods
{
    public class DrawingModiRec : ErpSaveMethodCallBase
    {
        public DrawingModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "DrawingModiRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override ApiControllers Controller => ApiControllers.PLM;
        public override string Category => PLMTypes.Drawings.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var documentEntity = entity.As<DrawingEntity>();

            var aggregate = new DocumentDrawingAggregate
            {
                DrawingDto = documentEntity.DrawingDto.GetDto<DrawingDto>()
            };

            var dataToSave = new Dictionary<string, object>
            {
                {nameof(DocumentDrawingAggregate), aggregate}
            };

            await SaveNewRecordAsync<DocumentDrawingAggregate>(MethodName, dataToSave);
        }
    }
}