﻿using ERP.WebApi.DTO.DTOs.PLM.Part;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using ERP.WebApi.DTO.Lookups.LookupItems;
using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.FileViewer;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.PLM.PLMViews.Drawing.DrawingViews;
using Kemprise.PLM.WorkArea;
using Microsoft.Win32;
using Prism.Commands;
using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.PLM.PLMViews.Drawing
{
    public sealed class DrawingWorkAreaViewModel : PLMViewModelBase
    {
        private readonly IFileService _fileService;
        private DrawingEntity _entity;
        private AddDrawingWindow _addDrawingWindow;
        private PdfViewer _pdfViewer;

        public DrawingWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, PLMTypes.Drawings.ToString())
        {
            _fileService = fileService;
            SetView(DrawingViewNames.DrawingMainView);

            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            CancelCommand = new DelegateCommand(CancelExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshDrawingsExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            UploadDrawingCommand = new DelegateCommand(UploadDrawingExecute, UploadDrawingCanExecute);
            SelectDrawingCommand = new DelegateCommand(SelectDrawingExecute);
            UpdateDrawingCommand = new DelegateCommand(UpdateDrawingExecute);
        }

        private async void UpdateDrawingExecute()
        {
            if (SelectedItem == null) return;
            if (SelectedItem.As<DrawingListItem>().StateDto.Name != "In Work" &&
                SelectedItem.As<DrawingListItem>().StateDto.Name != "Rework")
            {
                EventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload("The document is locked for editing."));
                return;
            }

            ;

            Entity = new DynamicNotifyPropertyChangedProxy(new DrawingEntity());
            _entity = Entity.GetDto<DrawingEntity>();
            await OpenItemExecute(SelectedItem.Id, true);
            _entity.DrawingDto.PropertyChanged += (s, e) => UploadDrawingCommand.RaiseCanExecuteChanged();
            UploadDrawingCommand.RaiseCanExecuteChanged();

            _addDrawingWindow = new AddDrawingWindow { DataContext = this };
            _addDrawingWindow.ShowDialog();
        }

        private void SelectDrawingExecute()
        {
            var dialog = new OpenFileDialog
            {
                Filter = "PDF | *.pdf; "
            };

            dialog.ShowDialog();

            if (!dialog.FileNames.Any()) return;

            var noLargeFiles = dialog.FileNames.All(fileName =>
            {
                var fileInfo = new FileInfo(fileName);
                return fileInfo.Length <= 2097152;
            });

            if (!noLargeFiles)
            {
                EventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload("Large Files Found. Please Limit File Size to 2MB"));
                return;
            }

            _entity.DrawingDto.SetProperty("Name",
                Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(dialog.FileName)));
            _entity.DrawingDto.SetProperty("FileData", File.ReadAllBytes(dialog.FileName));
        }

        private bool UploadDrawingCanExecute()
        {
            if (_entity.DrawingDto.GetDto<DrawingDto>().FileData == null) return false;
            if (_entity.DrawingDto.GetDto<DrawingDto>().Name == string.Empty) return false;
            return true;
        }

        private async void UploadDrawingExecute()
        {
            if (_entity.IsNew)
            {
                var result = await SaveRecordAsync("DrawingAddRec");
                if (result.OperationResult == OperationResult.Success)
                {
                    _addDrawingWindow.Close();
                    RefreshExecute();
                }
            }
            else
            {
                var result = await SaveRecordAsync("DrawingModiRec");
                if (result.OperationResult == OperationResult.Success)
                {
                    _addDrawingWindow.Close();
                    RefreshExecute();
                }
            }
        }

        private void ClearExecute()
        {
            RadWindow.Confirm("This will clear everything. Continue?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                Entity.SetProperty("DrawingDto", new DynamicNotifyPropertyChangedProxy(new DrawingDto()));
            });
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;
            try
            {
                Entity = new DynamicNotifyPropertyChangedProxy(new DrawingEntity());
                _entity = Entity.GetDto<DrawingEntity>();
                await OpenItemExecute(SelectedItem.Id, true);

                _pdfViewer = new PdfViewer { Owner = Application.Current.MainWindow, DataContext = this };
                _pdfViewer.SetDrawing(_entity.DrawingDto.GetDto<DrawingDto>());
                _pdfViewer.ShowDialog();
            }
            catch (Exception e)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload(e.Message));
            }
        }

        private void RefreshDrawingsExecute(object obj)
        {
            RefreshExecute();
        }

        private void CancelExecute()
        {
            _addDrawingWindow?.Close();
        }

        private void BackExecute()
        {
            RefreshExecute();
            SetView(DrawingViewNames.DrawingMainView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<PLMWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Drawings"));
        }

        private void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new DrawingEntity());
            _entity = Entity.GetDto<DrawingEntity>();
            _entity.DrawingDto.PropertyChanged += (s, e) => UploadDrawingCommand.RaiseCanExecuteChanged();
            UploadDrawingCommand.RaiseCanExecuteChanged();

            _entity.IsNew = true;
            _addDrawingWindow = new AddDrawingWindow { DataContext = this, Owner = Application.Current.MainWindow };
            _addDrawingWindow.ShowDialog();
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<PlmLookupAggregate>(ApiControllers.Lookup, "PlmGetLookups");
        }

        public override async void CreateFilters()
        {
            RegisterLookups();
            await GetLookupsAsync();

            var view = new CollectionViewSource { Source = EntityListItems };
            CollectionView = view;

            SelectedPlmFilter = Lookups["States"].FirstOrDefault(x => x.Text == "All").As<StateLookup>();
            SelectedItem = null;
        }

        private void FilterItems(string filterValue)
        {
            switch (filterValue)
            {
                case null:
                    return;

                case "All":
                    {
                        var view = new CollectionViewSource { Source = EntityListItems };
                        CollectionView = view;
                        break;
                    }
                default:
                    {
                        var filteredEntities = EntityListItems.Where(x => x.As<DrawingListItem>().StateDto.Name == filterValue);
                        var view = new CollectionViewSource { Source = filteredEntities };
                        CollectionView = view;
                        break;
                    }
            }
        }

        public StateLookup SelectedPlmFilter
        {
            get { return GetValue(() => SelectedPlmFilter); }
            set
            {
                SetValue(() => SelectedPlmFilter, value);
                if (value != null) FilterItems(value.Text);
                SelectedItem = null;
            }
        }

        public DelegateCommand SelectDrawingCommand { get; set; }
        public DelegateCommand UploadDrawingCommand { get; set; }
        public DelegateCommand UpdateDrawingCommand { get; set; }
    }
}