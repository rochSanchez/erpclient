﻿using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.PLM.PLMViews.Drawing
{
    public class DrawingEntity : EntityBase
    {
        public DrawingEntity()
        {
            DrawingDto = new DynamicNotifyPropertyChangedProxy(new DrawingDto());
            StateDto = new StateDto();
        }

        public DynamicNotifyPropertyChangedProxy DrawingDto { get; set; }
        public StateDto StateDto { get; set; }
    }
}