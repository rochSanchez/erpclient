﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.Drawing.DrawingViews
{
    /// <summary>
    /// Interaction logic for NewDocumentView.xaml
    /// </summary>
    public partial class NewDrawingView : UserControl
    {
        public NewDrawingView()
        {
            InitializeComponent();
        }
    }
}