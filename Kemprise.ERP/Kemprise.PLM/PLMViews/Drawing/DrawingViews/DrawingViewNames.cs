﻿namespace Kemprise.PLM.PLMViews.Drawing.DrawingViews
{
    public class DrawingViewNames
    {
        public const string DrawingMainView = "Drawing Main";
        public const string NewDrawingView = "New Drawing View";
        public const string EditDrawingView = "Edit Drawing View";
    }
}