﻿namespace Kemprise.PLM.PLMViews
{
    public enum PLMTypes
    {
        NewProductRequest,
        Documents,
        Drawings,
        NewReleased,
        Prototype,
        ReviseToReleased,
        ReviseToObsolete,
        ReviseToInactive,
        Iterate,
        PartAttribute,
        Classification,
        PartsViewer
    }
}