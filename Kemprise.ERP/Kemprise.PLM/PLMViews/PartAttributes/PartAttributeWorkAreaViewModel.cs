﻿using ERP.WebApi.DTO.DTOs.PLM.PartAttribute;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.PLM.PLMViews.PartAttributes.PartAttributeViews;
using Kemprise.PLM.WorkArea;
using Prism.Commands;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.PLM.PLMViews.PartAttributes
{
    public sealed class PartAttributeWorkAreaViewModel : PLMViewModelBase
    {
        private PartAttributeEntity _entity;

        public PartAttributeWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, PLMTypes.PartAttribute.ToString())
        {
            SetView(PartAttributeViewNames.PartAttributeMainView);

            ClearCommand = new DelegateCommand(ClearExecute);
            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            EditCommand = new DelegateCommand(EditExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSaveExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshPartAttributesExecute);

            Methods["PartAttributeGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        private void WhenGetRecDone()
        {
            _entity.PartAttributeDto.PropertyChanged += (s, e) => { SaveCommand.RaiseCanExecuteChanged(); };
            SaveCommand.RaiseCanExecuteChanged();
        }

        private void ClearExecute()
        {
            RadWindow.Confirm("This will clear everything. Continue?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.PartAttributeDto.ClearValues();
            });
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new PartAttributeEntity());
            _entity = Entity.GetDto<PartAttributeEntity>();

            await OpenItemExecute(SelectedItem.Id);

            SetView(PartAttributeViewNames.PartAttributeDisplayView);
        }

        private async void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new PartAttributeEntity());

            _entity = Entity.GetDto<PartAttributeEntity>();
            _entity.IsNew = true;

            await OpenItemExecute("");

            IsClearVisible = true;
            SetView(PartAttributeViewNames.PartAttributeEditView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<PLMWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Part Attributes"));
        }

        private void RefreshPartAttributesExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        private async void EditExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select a Part Attribute",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            Entity = new DynamicNotifyPropertyChangedProxy(new PartAttributeEntity());
            _entity = Entity.GetDto<PartAttributeEntity>();

            await OpenItemExecute(SelectedItem.Id);

            IsClearVisible = false;
            SetView(PartAttributeViewNames.PartAttributeEditView);
        }

        private void BackExecute()
        {
            SetView(PartAttributeViewNames.PartAttributeMainView);
            RefreshExecute();
        }

        private async void SaveExecute()
        {
            var taskToPerform = _entity.IsNew ? "PartAttributeAddRec" : "PartAttributeModiRec";
            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSaveExecute()
        {
            var partAttributeDto = _entity.PartAttributeDto.GetDto<PartAttributeDto>();
            return !string.IsNullOrEmpty(partAttributeDto.Name) && partAttributeDto.ClassificationId != 0;
        }

        public override async void CreateFilters()
        {
            RegisterLookups();
            await GetLookupsAsync();

            var view = new CollectionViewSource { Source = EntityListItems };
            CollectionView = view;

            SelectedItem = null;
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<PlmLookupAggregate>(ApiControllers.Lookup, "PlmGetLookups");
        }
    }
}