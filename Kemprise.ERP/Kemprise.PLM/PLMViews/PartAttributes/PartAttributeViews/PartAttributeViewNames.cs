﻿namespace Kemprise.PLM.PLMViews.PartAttributes.PartAttributeViews
{
    public class PartAttributeViewNames
    {
        public const string PartAttributeMainView = "Part Attribute Main View";
        public const string PartAttributeEditView = "Part Attribute Edit View";
        public const string PartAttributeDisplayView = "Part Attribute Display View";
    }
}