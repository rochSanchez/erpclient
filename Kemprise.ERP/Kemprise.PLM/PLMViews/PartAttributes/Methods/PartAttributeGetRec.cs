﻿using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.PartAttribute;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.PartAttributes.Methods
{
    public class PartAttributeGetRec : GetMethodCallBase
    {
        public PartAttributeGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "PartAttributeGetRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override string Category => PLMTypes.PartAttribute.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.PartAttribute;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var partAttributeAggregate =
                await GetRecordAsync<PartAttributeAggregate>(MethodName, new object[] {MethodParameter});
            if (partAttributeAggregate == null) return;

            if (partAttributeAggregate.PartAttributeDto != null)
                entity.SetProperty("PartAttributeDto",
                    new DynamicNotifyPropertyChangedProxy(partAttributeAggregate.PartAttributeDto));
        }
    }
}