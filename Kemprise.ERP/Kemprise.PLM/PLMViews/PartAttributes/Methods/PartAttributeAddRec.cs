﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.PartAttribute;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.PartAttributes.Methods
{
    public class PartAttributeAddRec : ErpSaveMethodCallBase
    {
        public PartAttributeAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "PartAttributeAddRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override ApiControllers Controller => ApiControllers.PartAttribute;
        public override string Category => PLMTypes.PartAttribute.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var partAttributeEntity = entity.As<PartAttributeEntity>();

            var attributeAggregate = new PartAttributeAggregate()
            {
                PartAttributeDto = partAttributeEntity.PartAttributeDto.GetDto<PartAttributeDto>()
            };

            var dataToSave = new Dictionary<string, object>
            {
                {nameof(PartAttributeAggregate), attributeAggregate}
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}