﻿using ERP.WebApi.DTO.DTOs.PLM.PartAttribute;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.PLM.PLMViews.PartAttributes
{
    public class PartAttributeEntity : EntityBase
    {
        public PartAttributeEntity()
        {
            PartAttributeDto = new DynamicNotifyPropertyChangedProxy(new PartAttributeDto());
        }

        public DynamicNotifyPropertyChangedProxy PartAttributeDto { get; set; }
    }
}