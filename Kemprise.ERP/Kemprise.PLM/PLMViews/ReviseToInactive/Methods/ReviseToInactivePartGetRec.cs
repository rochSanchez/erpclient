﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using ERP.WebApi.DTO.DTOs.PLM.PartAttribute;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.ReviseToInactive.Methods
{
    public class ReviseToInactivePartGetRec : GetMethodCallBase
    {
        public ReviseToInactivePartGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override ApiControllers Controller => ApiControllers.PLM;
        public override string MethodName => "NewPartGetRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override string Category => PLMTypes.ReviseToInactive.ToString();
        public override bool RunAtStartup => false;
        public override bool UseDefaultParameter => false;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<NewPartAggregate>(MethodName, new object[] {MethodParameter});
            if (aggregate == null) return;


            if (aggregate.NewPart != null)
                entity.SetProperty("NewPart", aggregate.NewPart);

            if (aggregate.ClassificationDto != null)
                entity.SetProperty("ClassificationDto", aggregate.ClassificationDto);

            if (aggregate.PartAttributes != null)
            {
                var partAttributes = new ObservableCollection<PartPartAttributeDto>();
                aggregate.PartAttributes.ForEach(item => partAttributes.Add(item));
                entity.SetProperty("PartAttributes", partAttributes);
            }

            if (aggregate.BillOfMaterials != null)
            {
                var billOfMaterialDtos = new ObservableCollection<BillOfMaterialDto>();
                aggregate.BillOfMaterials.ForEach(item => billOfMaterialDtos.Add(item));
                entity.SetProperty("BillOfMaterials", billOfMaterialDtos);
            }

            if (aggregate.PartDrawings != null)
            {
                var partDrawingDtos = new ObservableCollection<PartDrawingDto>();
                aggregate.PartDrawings.ForEach(item => partDrawingDtos.Add(item));
                entity.SetProperty("PartDrawings", partDrawingDtos);
            }

            if (aggregate.PartDocuments != null)
            {
                var documentDtos = new ObservableCollection<PartDocumentDto>();
                aggregate.PartDocuments.ForEach(item => documentDtos.Add(item));
                entity.SetProperty("PartDocuments", documentDtos);
            }

            if (aggregate.LatestEcn != null)
                entity.SetProperty("LatestEcn", aggregate.LatestEcn);
            if (aggregate.LatestEcn != null)
                entity.SetProperty("OriginalEcn", aggregate.OriginalEcn);
        }
    }
}