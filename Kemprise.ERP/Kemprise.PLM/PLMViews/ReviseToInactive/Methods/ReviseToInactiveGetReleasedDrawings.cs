﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.ReviseToInactive.Methods
{
    public class ReviseToInactiveGetReleasedDrawings : GetMethodCallBase
    {
        public ReviseToInactiveGetReleasedDrawings(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override ApiControllers Controller => ApiControllers.PLM;
        public override string MethodName => "GetReleasedPrototypeDrawings";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override string Category => PLMTypes.ReviseToInactive.ToString();
        public override bool RunAtStartup => false;
        public override bool UseDefaultParameter => false;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var drawingDtos = await GetRecordsAsync<SimpleDrawingDto>(MethodName, new object[] {MethodParameter});
            if (drawingDtos == null) return;
            var dtos = new ObservableCollection<SimpleDrawingDto>();
            drawingDtos.ForEach(item => dtos.Add(item));
            entity.SetProperty("AvailableDrawings", dtos);
        }
    }
}