﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.ReviseToInactive.Methods
{
    public class ReviseToInactiveGetReleasedPrototypeDocuments : GetMethodCallBase
    {
        public ReviseToInactiveGetReleasedPrototypeDocuments(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override ApiControllers Controller => ApiControllers.PLM;
        public override string MethodName => "GetReleasedPrototypeDocuments";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override string Category => PLMTypes.ReviseToInactive.ToString();
        public override bool RunAtStartup => false;
        public override bool UseDefaultParameter => false;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var documentDtos = await GetRecordsAsync<SimpleDocumentDto>(MethodName, new object[] {MethodParameter});
            if (documentDtos == null) return;
            var dtos = new ObservableCollection<SimpleDocumentDto>();
            documentDtos.ForEach(item => dtos.Add(item));
            entity.SetProperty("AvailableDocuments", dtos);
        }
    }
}