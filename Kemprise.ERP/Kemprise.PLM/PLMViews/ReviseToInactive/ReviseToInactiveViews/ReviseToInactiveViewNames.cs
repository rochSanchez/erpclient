﻿namespace Kemprise.PLM.PLMViews.ReviseToInactive.ReviseToInactiveViews
{
    public class ReviseToInactiveViewNames
    {
        public const string ReviseToInactiveMainView = "ReviseToInactive Main";
        public const string NewReviseToInactiveView = "New ReviseToInactive View";
        public const string InWorkView = "In Work View";
        public const string ApprovalView = "Approval View";
        public const string ReviseToInactiveApprover = "ReviseToInactive Approver";
    }
}