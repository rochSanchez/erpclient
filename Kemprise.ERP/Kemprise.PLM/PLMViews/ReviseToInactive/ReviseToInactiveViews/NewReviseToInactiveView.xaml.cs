﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.ReviseToInactive.ReviseToInactiveViews
{
    /// <summary>
    /// Interaction logic for NewDocumentView.xaml
    /// </summary>
    public partial class NewReviseToInactiveView : UserControl
    {
        public NewReviseToInactiveView()
        {
            InitializeComponent();
        }
    }
}