﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.ReviseToInactive.ReviseToInactiveViews.StateViews
{
    /// <summary>
    /// Interaction logic for NewReleasedInWork.xaml
    /// </summary>
    public partial class ReviseToInactiveApprover : UserControl
    {
        public ReviseToInactiveApprover()
        {
            InitializeComponent();
        }
    }
}