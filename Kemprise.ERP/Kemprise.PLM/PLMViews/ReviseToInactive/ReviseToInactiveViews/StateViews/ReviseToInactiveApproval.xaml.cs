﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.ReviseToInactive.ReviseToInactiveViews.StateViews
{
    /// <summary>
    /// Interaction logic for NewReleasedApproval.xaml
    /// </summary>
    public partial class ReviseToInactiveApproval : UserControl
    {
        public ReviseToInactiveApproval()
        {
            InitializeComponent();
        }
    }
}