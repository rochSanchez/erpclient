﻿namespace Kemprise.PLM.PLMViews.ReviseToRelease.ReviseToReleasedViews
{
    public class ReviseToReleasedViewNames
    {
        public const string ReviseToReleasedMainView = "ReviseToReleased Main";
        public const string NewReviseToReleasedView = "New ReviseToReleased View";
        public const string InWorkView = "In Work View";
        public const string ApprovalView = "Approval View";
        public const string ReviseToReleasedApprover = "ReviseToReleased Approver";
    }
}