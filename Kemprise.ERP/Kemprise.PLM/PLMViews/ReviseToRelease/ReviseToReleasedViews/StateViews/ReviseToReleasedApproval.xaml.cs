﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.ReviseToRelease.ReviseToReleasedViews.StateViews
{
    /// <summary>
    /// Interaction logic for NewReleasedApproval.xaml
    /// </summary>
    public partial class ReviseToReleasedApproval : UserControl
    {
        public ReviseToReleasedApproval()
        {
            InitializeComponent();
        }
    }
}