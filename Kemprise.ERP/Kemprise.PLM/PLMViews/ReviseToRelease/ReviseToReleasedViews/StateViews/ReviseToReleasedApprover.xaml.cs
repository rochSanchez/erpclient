﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.ReviseToRelease.ReviseToReleasedViews.StateViews
{
    /// <summary>
    /// Interaction logic for NewReleasedInWork.xaml
    /// </summary>
    public partial class ReviseToReleasedApprover : UserControl
    {
        public ReviseToReleasedApprover()
        {
            InitializeComponent();
        }
    }
}