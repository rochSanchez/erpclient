﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.ReviseToRelease.ReviseToReleasedViews
{
    /// <summary>
    /// Interaction logic for NewDocumentView.xaml
    /// </summary>
    public partial class NewReviseToReleasedView : UserControl
    {
        public NewReviseToReleasedView()
        {
            InitializeComponent();
        }
    }
}