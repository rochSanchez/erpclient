﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.PLM.PLMViews.ReviseToRelease.Methods
{
    public class ReviseToReleasedModiRec : ErpSaveMethodCallBase
    {
        public ReviseToReleasedModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "ReviseToReleasedModiRec";
        public override ErpModules ModuleName => ErpModules.PLM;
        public override ApiControllers Controller => ApiControllers.PLM;
        public override string Category => PLMTypes.ReviseToReleased.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var newReleasedEntity = entity.As<ReviseToReleasedEntity>();

            var aggregate = new EcnAggregate
            {
                EcnDto = newReleasedEntity.EcnDto.GetDto<EcnDto>(),
                EcnParts = newReleasedEntity.EcnParts.ToList(),
                EcnDrawings = newReleasedEntity.EcnDrawings.ToList(),
                EcnDocuments = newReleasedEntity.EcnDocuments.ToList()
            };

            var dataToSave = new Dictionary<string, object>
            {
                {nameof(EcnAggregate), aggregate}
            };

            await SaveNewRecordAsync<EcnAggregate>(MethodName, dataToSave);
        }
    }
}