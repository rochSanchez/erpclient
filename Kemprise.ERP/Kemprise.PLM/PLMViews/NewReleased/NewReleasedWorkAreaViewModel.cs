﻿using ERP.WebApi.DTO.DTOs.PLM.Part;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using ERP.WebApi.DTO.Lookups.LookupItems;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.FileViewer;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.PLM.PLMViews.Common.PLM;
using Kemprise.PLM.PLMViews.NewReleased.NewReleasedViews;
using Kemprise.PLM.WorkArea;
using Prism.Commands;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Unity;

namespace Kemprise.PLM.PLMViews.NewReleased
{
    public sealed class NewReleasedWorkAreaViewModel : PLMViewModelBase
    {
        private readonly IFileService _fileService;
        private NewReleasedEntity _entity;
        private PdfViewer _pdfViewer;
        private AddPartsWindow _addPartsWindow;
        private AddDrawingsWindow _addDrawingsWindow;
        private AddDocumentsWindow _addDocumentsWindow;
        private PartViewerWindow _partViewerWindow;

        public NewReleasedWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, PLMTypes.NewReleased.ToString())
        {
            _fileService = fileService;
            SetView(NewReleasedViewNames.NewReleasedMainView);

            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            CancelCommand = new DelegateCommand(CancelExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveNewEcnCommand = new DelegateCommand(SaveNewEcnExecute, SaveNewEcnCanExecute);
            SubmitForReviewCommand = new DelegateCommand(SubmitForReviewExecute);
            SubmitEcnChangesCommand = new DelegateCommand(SubmitChangesExecute);
            ReworkCommand = new DelegateCommand(ReworkExecute);
            SubmitApprovalCommand = new DelegateCommand(SubmitApprovalExecute);
            PrintPartCommand = new DelegateCommand(PrintNewReleasedPartExecute);

            EcnViewPartCommand = new DelegateCommand(ViewPartExecute);
            EcnViewDocumentCommand = new DelegateCommand(ViewDocumentExecute);
            EcnViewDrawingCommand = new DelegateCommand(ViewDrawingExecute);

            //parts
            SelectPartsCommand = new DelegateCommand(AddNewPartsExecute);
            AddPartCommand = new DelegateCommand(AddSelectedPartExecute, AddSelectedPartCanExecute);
            RefreshAvailablePartsCommand = new DelegateCommand(RefreshAvailablePartsExecute);
            SelectSelectedPartCommand = new DelegateCommand(SelectSelectedPartExecute);
            SelectAvailablePartCommand = new DelegateCommand(SelectAvailablePartExecute);
            DeletePartCommand = new DelegateCommand(DeletePartExecute, () => SelectedEcnPart != null);

            //drawings
            SelectDrawingsCommand = new DelegateCommand(AddNewDrawingsExecute);
            AddSelectedDrawingsCommand = new DelegateCommand(AddSelectedDrawingExecute, AddSelectedDrawingCanExecute);
            RefreshAvailableDrawingsCommand = new DelegateCommand(RefreshAvailableDrawingsExecute);
            SelectSelectedDrawingCommand = new DelegateCommand(SelectSelectedDrawingExecute);
            SelectAvailableDrawingCommand = new DelegateCommand(SelectAvailableDrawingExecute);
            DeleteDrawingCommand = new DelegateCommand(DeleteDrawingExecute, () => SelectedEcnDrawing != null);

            //documents
            SelectDocumentsCommand = new DelegateCommand(AddNewDocumentsExecute);
            AddSelectedDocumentsCommand =
                new DelegateCommand(AddSelectedDocumentExecute, AddSelectedDocumentCanExecute);
            RefreshAvailableDocumentsCommand = new DelegateCommand(RefreshAvailableDocumentsExecute);
            SelectSelectedDocumentCommand = new DelegateCommand(SelectSelectedDocumentExecute);
            SelectAvailableDocumentCommand = new DelegateCommand(SelectAvailableDocumentExecute);
            DeleteDocumentCommand = new DelegateCommand(DeleteDocumentExecute, () => SelectedEcnDocument != null);

            StateVisibility = Visibility.Visible;
            ReleasedVisibility = Visibility.Visible;
        }

        private async void PrintNewReleasedPartExecute()
        {
            //todo refactor
            if (_entity.PartDrawings.Any())
            {
                Methods["DrawingGetRec"].MethodParameter = _entity.PartDrawings.First().DrawingId.ToString();
                var result = await GetRecordAsync("DrawingGetRec");
            }

            var data = BuildPartDocument("NewReleasePartDetails");

            if (_entity.PartDrawings.Any())
            {
                _pdfViewer = new PdfViewer { Owner = Application.Current.MainWindow, DataContext = this };
                _pdfViewer.BuildPartDocumentPdf(data, _entity.DrawingDto.FileData);
                _pdfViewer.ShowDialog();
            }
            else
            {
                _pdfViewer = new PdfViewer { Owner = Application.Current.MainWindow, DataContext = this };
                _pdfViewer.BuildPartDocumentPdf(data);
                _pdfViewer.ShowDialog();
            }
        }

        private async void ViewDrawingExecute()
        {
            Methods["DrawingGetRec"].MethodParameter = SelectedEcnDrawing.Drawing.DrawingId.ToString();
            var result = await GetRecordAsync("DrawingGetRec");

            _pdfViewer = new PdfViewer { Owner = Application.Current.MainWindow, DataContext = this };
            _pdfViewer.SetDrawing(_entity.DrawingDto);
            _pdfViewer.ShowDialog();
        }

        private async void ViewDocumentExecute()
        {
            Methods["DocumentGetRec"].MethodParameter = SelectedEcnDocument.Document.DocumentId.ToString();
            var result = await GetRecordAsync("DocumentGetRec");

            _pdfViewer = new PdfViewer { Owner = Application.Current.MainWindow, DataContext = this };
            _pdfViewer.SetDocument(_entity.DocumentDto);
            _pdfViewer.ShowDialog();
        }

        private async void ViewPartExecute()
        {
            _entity.BillOfMaterials.Clear();
            Methods["NewPartGetRec"].MethodParameter = SelectedEcnPart.Part.PartId.ToString();
            _entity.BillOfMaterials.Clear();
            var result = await GetRecordAsync("NewPartGetRec");
            if (result.OperationResult == OperationResult.Success)
            {
                StateVisibility = Visibility.Collapsed;
                _partViewerWindow = new PartViewerWindow { DataContext = this, Owner = Application.Current.MainWindow };
                _partViewerWindow.ShowDialog();
            }
        }

        private async void SubmitApprovalExecute()
        {
            if (_entity.StateDto.Name == "Under Review")
            {
                var result = await SaveRecordAsync("NewReleasedCheckersApproval");
                if (result.OperationResult == OperationResult.Success) BackExecute();
            }
            else
            {
                var result = await SaveRecordAsync("NewReleasedPromotersApproval");
                if (result.OperationResult == OperationResult.Success) BackExecute();
            }
        }

        private async void SubmitChangesExecute()
        {
            var result = await SaveRecordAsync("NewReleasedModiRec");
            if (result.OperationResult == OperationResult.Success) BackExecute();
        }

        //route
        private async void SubmitForReviewExecute()
        {
            var result = await SaveRecordAsync("SubmitForReview");
            if (result.OperationResult == OperationResult.Success) BackExecute();
        }

        private async void ReworkExecute()
        {
            var result = await SaveRecordAsync("ReworkNewReleased");
            if (result.OperationResult == OperationResult.Success) BackExecute();
        }

        //Documents
        private void DeleteDocumentExecute()
        {
            var itemToRemove =
                _entity.EcnDocuments.FirstOrDefault(x =>
                    x.Document.DocumentId == SelectedEcnDocument.Document.DocumentId);

            _entity.EcnDocuments.Remove(itemToRemove);
            if (itemToRemove != null) _entity.AvailableDocuments.Add(itemToRemove.Document);
        }

        private void SelectSelectedDocumentExecute()
        {
            if (_entity.AvailableDocuments.Contains(SelectedDocumentToAdd)) return;

            _entity.AvailableDocuments.Add(SelectedDocumentToAdd);
            _entity.SelectedDocuments.Remove(SelectedDocumentToAdd);
        }

        private void SelectAvailableDocumentExecute()
        {
            if (_entity.SelectedDocuments.Contains(SelectedAvailableDocument)) return;

            _entity.SelectedDocuments.Add(SelectedAvailableDocument);
            _entity.AvailableDocuments.Remove(SelectedAvailableDocument);
        }

        private bool AddSelectedDocumentCanExecute()
        {
            return _entity.SelectedDocuments.Any();
        }

        private void AddSelectedDocumentExecute()
        {
            _entity.SelectedDocuments.ForEach(item =>
            {
                _entity.EcnDocuments.Add(new EcnDocumentDto { Document = item });
            });

            _entity.SelectedDocuments.Clear();
            _addDocumentsWindow.Close();
        }

        private async void RefreshAvailableDocumentsExecute()
        {
            _entity.AvailableDocuments.Clear();
            _entity.SelectedDocuments.Clear();
            await GetRecordAsync("GetInWorkDocuments");

            _entity.EcnDocuments.ForEach(item =>
            {
                var itemToRemove =
                    _entity.AvailableDocuments.FirstOrDefault(x =>
                        x.DocumentId == item.DocumentId);
                _entity.AvailableDocuments.Remove(itemToRemove);
            });
        }

        private async void AddNewDocumentsExecute()
        {
            {
                await GetRecordAsync("GetInWorkDocuments");
                _entity.SelectedDocuments.CollectionChanged += SelectedDocumentOnCollectionChanged;
            }
            _entity.EcnDocuments.ForEach(item =>
            {
                var itemToRemove =
                    _entity.AvailableDocuments.FirstOrDefault(x =>
                        x.DocumentId == item.Document.DocumentId);
                if (itemToRemove != null) _entity.AvailableDocuments.Remove(itemToRemove);
            });

            _addDocumentsWindow = new AddDocumentsWindow() { DataContext = this };
            _addDocumentsWindow.ShowDialog();
        }

        private void SelectedDocumentOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            AddSelectedDocumentsCommand.RaiseCanExecuteChanged();
        }

        //Drawings
        private void DeleteDrawingExecute()
        {
            var itemToRemove =
                _entity.EcnDrawings.FirstOrDefault(x =>
                    x.Drawing.DrawingId == SelectedEcnDrawing.Drawing.DrawingId);

            _entity.EcnDrawings.Remove(itemToRemove);
            if (itemToRemove != null) _entity.AvailableDrawings.Add(itemToRemove.Drawing);
        }

        private void SelectSelectedDrawingExecute()
        {
            if (_entity.AvailableDrawings.Contains(SelectedDrawingToAdd)) return;

            _entity.AvailableDrawings.Add(SelectedDrawingToAdd);
            _entity.SelectedDrawings.Remove(SelectedDrawingToAdd);
        }

        private void SelectAvailableDrawingExecute()
        {
            if (_entity.SelectedDrawings.Contains(SelectedAvailableDrawing)) return;

            _entity.SelectedDrawings.Add(SelectedAvailableDrawing);
            _entity.AvailableDrawings.Remove(SelectedAvailableDrawing);
        }

        private bool AddSelectedDrawingCanExecute()
        {
            return _entity.SelectedDrawings.Any();
        }

        private void AddSelectedDrawingExecute()
        {
            _entity.SelectedDrawings.ForEach(item =>
            {
                _entity.EcnDrawings.Add(new EcnDrawingDto() { Drawing = item });
            });

            _entity.SelectedDrawings.Clear();
            _addDrawingsWindow.Close();
        }

        private async void RefreshAvailableDrawingsExecute()
        {
            _entity.AvailableDrawings.Clear();
            _entity.SelectedDrawings.Clear();
            await GetRecordAsync("GetInWorkDrawings");

            _entity.EcnDrawings.ForEach(item =>
            {
                var itemToRemove =
                    _entity.AvailableDrawings.FirstOrDefault(x =>
                        x.DrawingId == item.DrawingId);
                _entity.AvailableDrawings.Remove(itemToRemove);
            });
        }

        private async void AddNewDrawingsExecute()
        {
            {
                await GetRecordAsync("GetInWorkDrawings");
                _entity.SelectedDrawings.CollectionChanged += SelectedDrawingOnCollectionChanged;
            }
            _entity.EcnDrawings.ForEach(item =>
            {
                var itemToRemove =
                    _entity.AvailableDrawings.FirstOrDefault(x =>
                        x.DrawingId == item.Drawing.DrawingId);
                if (itemToRemove != null) _entity.AvailableDrawings.Remove(itemToRemove);
            });

            _addDrawingsWindow = new AddDrawingsWindow() { DataContext = this };
            _addDrawingsWindow.ShowDialog();
        }

        private void SelectedDrawingOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            AddSelectedDrawingsCommand.RaiseCanExecuteChanged();
        }

        //parts
        private void DeletePartExecute()
        {
            var itemToRemove =
                _entity.EcnParts.FirstOrDefault(x =>
                    x.Part.PartId == SelectedEcnPart.Part.PartId);

            _entity.EcnParts.Remove(itemToRemove);
            if (itemToRemove != null) _entity.AvailableParts.Add(itemToRemove.Part);
        }

        private void SelectSelectedPartExecute()
        {
            if (_entity.AvailableParts.Contains(SelectedPartToAdd)) return;

            _entity.AvailableParts.Add(SelectedPartToAdd);
            _entity.SelectedParts.Remove(SelectedPartToAdd);
        }

        private void SelectAvailablePartExecute()
        {
            if (_entity.SelectedParts.Contains(SelectedAvailablePart)) return;

            _entity.SelectedParts.Add(SelectedAvailablePart);
            _entity.AvailableParts.Remove(SelectedAvailablePart);
        }

        private bool AddSelectedPartCanExecute()
        {
            return _entity.SelectedParts.Any();
        }

        private void AddSelectedPartExecute()
        {
            _entity.SelectedParts.ForEach(item => { _entity.EcnParts.Add(new EcnPartDto { Part = item }); });

            _entity.SelectedParts.Clear();
            _addPartsWindow.Close();
        }

        private async void RefreshAvailablePartsExecute()
        {
            _entity.AvailableParts.Clear();
            _entity.SelectedParts.Clear();
            await GetRecordAsync("GetInWorkParts");

            _entity.EcnParts.ForEach(item =>
            {
                var itemToRemove =
                    _entity.AvailableParts.FirstOrDefault(x =>
                        x.PartId == item.PartId);
                _entity.AvailableParts.Remove(itemToRemove);
            });
        }

        private async void AddNewPartsExecute()
        {
            await GetRecordAsync("GetInWorkParts");
            _entity.SelectedParts.CollectionChanged += SelectedPartOnCollectionChanged;
            _entity.EcnParts.ForEach(item =>
            {
                var itemToRemove =
                    _entity.AvailableParts.FirstOrDefault(x =>
                        x.PartId == item.Part.PartId);
                if (itemToRemove != null) _entity.AvailableParts.Remove(itemToRemove);
            });

            _addPartsWindow = new AddPartsWindow { DataContext = this };
            _addPartsWindow.ShowDialog();
        }

        private void SelectedPartOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            AddPartCommand.RaiseCanExecuteChanged();
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new NewReleasedEntity());
            _entity = Entity.GetDto<NewReleasedEntity>();
            await OpenItemExecute(SelectedItem.Id, true);
            DeleteDocumentCommand.RaiseCanExecuteChanged();
            DeleteDrawingCommand.RaiseCanExecuteChanged();
            DeletePartCommand.CanExecute();
            if (_entity.StateDto.Name == "In Work" || _entity.StateDto.Name == "Rework")
            {
                SetView(NewReleasedViewNames.InWorkView);
            }
            else
            {
                SetView(NewReleasedViewNames.ApprovalView);
                StateVisibility = Visibility.Collapsed;
                if (_entity.StateDto.Name == "Released") ReleasedVisibility = Visibility.Collapsed;
            }
        }

        private void RefreshExecute(object obj)
        {
            RefreshExecute();
        }

        private void CancelExecute()
        {
            if (_addPartsWindow != null)
            {
                _entity.SelectedParts.ForEach(item => _entity.AvailableParts.Add(item));
                _entity.SelectedParts.Clear();
                _addPartsWindow?.Close();
            }

            if (_addDocumentsWindow != null)
            {
                _entity.SelectedDocuments.ForEach(item => _entity.AvailableDocuments.Add(item));
                _entity.SelectedDocuments.Clear();
                _addDocumentsWindow?.Close();
            }

            if (_addDrawingsWindow != null)
            {
                _entity.SelectedDrawings.ForEach(item => _entity.AvailableDrawings.Add(item));
                _entity.SelectedDrawings.Clear();
                _addDrawingsWindow?.Close();
            }

            if (_partViewerWindow != null)
            {
                _partViewerWindow.Close();
                if (_entity.StateDto.Name == "In Work" || _entity.StateDto.Name == "Rework")
                    StateVisibility = Visibility.Visible;
            }
        }

        private bool SaveNewEcnCanExecute()
        {
            if (string.IsNullOrEmpty(_entity.EcnDto.GetDto<EcnDto>().Description)) return false;
            if (string.IsNullOrEmpty(_entity.EcnDto.GetDto<EcnDto>().Comments)) return false;
            return true;
        }

        private async void SaveNewEcnExecute()
        {
            var result = await SaveRecordAsync("NewReleasedAddRec");
            if (result.OperationResult == OperationResult.Success) BackExecute();
        }

        private void BackExecute()
        {
            StateVisibility = Visibility.Visible;
            ReleasedVisibility = Visibility.Visible;
            RefreshExecute();
            SetView(NewReleasedViewNames.NewReleasedMainView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<PLMWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "New Released"));
        }

        private void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new NewReleasedEntity());
            _entity = Entity.GetDto<NewReleasedEntity>();
            _entity.EcnDto.PropertyChanged += EntityOnPropertyChanged;
            _entity.EcnDto.SetProperty("ChangeLevel", "No Change");
            _entity.EcnDto.SetProperty("Disposition", "New Released");

            _entity.IsNew = true;
            SetView(NewReleasedViewNames.NewNewReleasedView);
        }

        private void EntityOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            SaveNewEcnCommand.RaiseCanExecuteChanged();
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<PlmLookupAggregate>(ApiControllers.Lookup, "PlmGetLookups");
        }

        public override async void CreateFilters()
        {
            RegisterLookups();
            await GetLookupsAsync();

            var view = new CollectionViewSource { Source = EntityListItems };
            CollectionView = view;

            SelectedPlmFilter = Lookups["States"].FirstOrDefault(x => x.Text == "All").As<StateLookup>();
            SelectedItem = null;
        }

        private void FilterItems(string filterValue)
        {
            switch (filterValue)
            {
                case null:
                    return;

                case "All":
                    {
                        var view = new CollectionViewSource { Source = EntityListItems };
                        CollectionView = view;
                        break;
                    }
                default:
                    {
                        var filteredEntities = EntityListItems.Where(x => x.As<EcnListItem>().StateDto.Name == filterValue);
                        var view = new CollectionViewSource { Source = filteredEntities };
                        CollectionView = view;
                        break;
                    }
            }
        }

        public StateLookup SelectedPlmFilter
        {
            get { return GetValue(() => SelectedPlmFilter); }
            set
            {
                SetValue(() => SelectedPlmFilter, value);
                if (value != null) FilterItems(value.Text);
                SelectedItem = null;
            }
        }

        public DelegateCommand SaveNewEcnCommand { get; set; }
        public DelegateCommand SubmitForReviewCommand { get; set; }
        public DelegateCommand ReworkCommand { get; set; }
        public DelegateCommand SubmitEcnChangesCommand { get; set; }
        public DelegateCommand SubmitApprovalCommand { get; set; }

        //parts commands
        public DelegateCommand DeletePartCommand { get; set; }

        public DelegateCommand SelectAvailablePartCommand { get; set; }
        public DelegateCommand SelectSelectedPartCommand { get; set; }
        public DelegateCommand RefreshAvailablePartsCommand { get; set; }
        public DelegateCommand SelectPartsCommand { get; set; }

        //drawings commands
        public DelegateCommand DeleteDrawingCommand { get; set; }

        public DelegateCommand SelectAvailableDrawingCommand { get; set; }
        public DelegateCommand SelectSelectedDrawingCommand { get; set; }
        public DelegateCommand RefreshAvailableDrawingsCommand { get; set; }
        public DelegateCommand SelectDrawingsCommand { get; set; }
        public DelegateCommand AddSelectedDrawingsCommand { get; set; }

        //documents commands
        public DelegateCommand DeleteDocumentCommand { get; set; }

        public DelegateCommand SelectAvailableDocumentCommand { get; set; }
        public DelegateCommand SelectSelectedDocumentCommand { get; set; }
        public DelegateCommand RefreshAvailableDocumentsCommand { get; set; }
        public DelegateCommand SelectDocumentsCommand { get; set; }
        public DelegateCommand AddSelectedDocumentsCommand { get; set; }

        //parts prop
        public EcnPartDto SelectedEcnPart
        {
            get { return GetValue(() => SelectedEcnPart); }
            set { SetValue(() => SelectedEcnPart, value, DeletePartCommand.RaiseCanExecuteChanged); }
        }

        public PartDto SelectedPartToAdd
        {
            get { return GetValue(() => SelectedPartToAdd); }
            set { SetValue(() => SelectedPartToAdd, value); }
        }

        public PartDto SelectedAvailablePart
        {
            get { return GetValue(() => SelectedAvailablePart); }
            set { SetValue(() => SelectedAvailablePart, value); }
        }

        //drawings prop
        public EcnDrawingDto SelectedEcnDrawing
        {
            get { return GetValue(() => SelectedEcnDrawing); }
            set { SetValue(() => SelectedEcnDrawing, value, DeleteDrawingCommand.RaiseCanExecuteChanged); }
        }

        public SimpleDrawingDto SelectedDrawingToAdd
        {
            get { return GetValue(() => SelectedDrawingToAdd); }
            set { SetValue(() => SelectedDrawingToAdd, value); }
        }

        public SimpleDrawingDto SelectedAvailableDrawing
        {
            get { return GetValue(() => SelectedAvailableDrawing); }
            set { SetValue(() => SelectedAvailableDrawing, value); }
        }

        //documents prop
        public EcnDocumentDto SelectedEcnDocument
        {
            get { return GetValue(() => SelectedEcnDocument); }
            set { SetValue(() => SelectedEcnDocument, value, DeleteDocumentCommand.RaiseCanExecuteChanged); }
        }

        public SimpleDocumentDto SelectedDocumentToAdd
        {
            get { return GetValue(() => SelectedDocumentToAdd); }
            set { SetValue(() => SelectedDocumentToAdd, value); }
        }

        public SimpleDocumentDto SelectedAvailableDocument
        {
            get { return GetValue(() => SelectedAvailableDocument); }
            set { SetValue(() => SelectedAvailableDocument, value); }
        }

        public Visibility ReleasedVisibility
        {
            get { return GetValue(() => ReleasedVisibility); }
            set { SetValue(() => ReleasedVisibility, value); }
        }

        public Visibility StateVisibility
        {
            get { return GetValue(() => StateVisibility); }
            set { SetValue(() => StateVisibility, value); }
        }
    }
}