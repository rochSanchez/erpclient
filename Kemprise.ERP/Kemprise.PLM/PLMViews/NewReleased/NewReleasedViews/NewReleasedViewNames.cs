﻿namespace Kemprise.PLM.PLMViews.NewReleased.NewReleasedViews
{
    public class NewReleasedViewNames
    {
        public const string NewReleasedMainView = "New Released Main";
        public const string NewNewReleasedView = "New New Released View";
        public const string InWorkView = "In Work View";
        public const string ApprovalView = "Approval View";
    }
}