﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.NewReleased.NewReleasedViews
{
    /// <summary>
    /// Interaction logic for NewDocumentView.xaml
    /// </summary>
    public partial class NewNewReleasedView : UserControl
    {
        public NewNewReleasedView()
        {
            InitializeComponent();
        }
    }
}