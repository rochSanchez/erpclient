﻿using System.Collections.ObjectModel;
using DocumentFormat.OpenXml.Presentation;
using ERP.WebApi.DTO.DTOs.PLM.Classification;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using ERP.WebApi.DTO.DTOs.PLM.PartAttribute;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.PLM.PLMViews.NewReleased
{
    public class NewReleasedEntity : EntityBase
    {
        public NewReleasedEntity()
        {
            EcnDto = new DynamicNotifyPropertyChangedProxy(new EcnDto());

            SelectedParts = new ObservableCollection<PartDto>();
            AvailableParts = new ObservableCollection<PartDto>();
            EcnParts = new ObservableCollection<EcnPartDto>();

            SelectedDrawings = new ObservableCollection<SimpleDrawingDto>();
            AvailableDrawings = new ObservableCollection<SimpleDrawingDto>();
            EcnDrawings = new ObservableCollection<EcnDrawingDto>();

            SelectedDocuments = new ObservableCollection<SimpleDocumentDto>();
            AvailableDocuments = new ObservableCollection<SimpleDocumentDto>();
            EcnDocuments = new ObservableCollection<EcnDocumentDto>();

            BillOfMaterials = new ObservableCollection<BillOfMaterialDto>();

            OriginalEcn = new EcnDto();

            StateDto = new StateDto();
        }

        public DynamicNotifyPropertyChangedProxy EcnDto { get; set; }
        public StateDto StateDto { get; set; }

        //parts
        public ObservableCollection<EcnPartDto> EcnParts { get; set; }

        public ObservableCollection<PartDto> SelectedParts { get; set; }

        public ObservableCollection<PartDto> AvailableParts { get; set; }

        public PartDto NewPart { get; set; }

        //part details
        public ObservableCollection<BillOfMaterialDto> BillOfMaterials { get; set; }

        public ObservableCollection<PartPartAttributeDto> PartAttributes { get; set; }

        public ObservableCollection<PartDocumentDto> PartDocuments { get; set; }

        public ObservableCollection<PartDrawingDto> PartDrawings { get; set; }

        public ClassificationDto ClassificationDto { get; set; }

        //drawings
        public ObservableCollection<EcnDrawingDto> EcnDrawings { get; set; }

        public ObservableCollection<SimpleDrawingDto> SelectedDrawings { get; set; }

        public ObservableCollection<SimpleDrawingDto> AvailableDrawings { get; set; }

        public DrawingDto DrawingDto { get; set; }


        //documents
        public ObservableCollection<EcnDocumentDto> EcnDocuments { get; set; }

        public ObservableCollection<SimpleDocumentDto> SelectedDocuments { get; set; }

        public ObservableCollection<SimpleDocumentDto> AvailableDocuments { get; set; }

        public DocumentDto DocumentDto { get; set; }

        public EcnDto OriginalEcn { get; set; }

        public EcnDto LatestEcn { get; set; }
    }
}