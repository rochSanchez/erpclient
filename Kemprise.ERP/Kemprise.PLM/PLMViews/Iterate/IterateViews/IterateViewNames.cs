﻿namespace Kemprise.PLM.PLMViews.Iterate.IterateViews
{
    public class IterateViewNames
    {
        public const string IterateMainView = "Iterate Main";
        public const string NewIterateView = "New Iterate View";
        public const string InWorkView = "In Work View";
        public const string ApprovalView = "Approval View";
        public const string IterateApprover = "Iterate Approver";
    }
}