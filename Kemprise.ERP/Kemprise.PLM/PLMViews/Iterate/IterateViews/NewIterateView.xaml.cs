﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.Iterate.IterateViews
{
    /// <summary>
    /// Interaction logic for NewDocumentView.xaml
    /// </summary>
    public partial class NewIterateView : UserControl
    {
        public NewIterateView()
        {
            InitializeComponent();
        }
    }
}