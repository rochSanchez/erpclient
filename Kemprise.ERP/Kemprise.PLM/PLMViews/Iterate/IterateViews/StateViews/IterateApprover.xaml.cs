﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.Iterate.IterateViews.StateViews
{
    /// <summary>
    /// Interaction logic for NewReleasedInWork.xaml
    /// </summary>
    public partial class IterateApprover : UserControl
    {
        public IterateApprover()
        {
            InitializeComponent();
        }
    }
}