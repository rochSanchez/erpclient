﻿using System.Windows.Controls;

namespace Kemprise.PLM.PLMViews.Iterate.IterateViews.StateViews
{
    /// <summary>
    /// Interaction logic for NewReleasedApproval.xaml
    /// </summary>
    public partial class IterateApproval : UserControl
    {
        public IterateApproval()
        {
            InitializeComponent();
        }
    }
}