﻿using System.Linq;
using System.Reflection;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Controllers;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.PLM.Controller;
using Kemprise.PLM.WorkArea;
using Prism.Ioc;
using Prism.Unity;
using Unity;
using Unity.Lifetime;
using Unity.RegistrationByConvention;


namespace Kemprise.PLM
{
    public class PlmModule : ErpModule
    {
        public PlmModule(IUnityContainer container)
        {
            container.RegisterType(typeof(IControllerBase), typeof(PLMController),
                nameof(PLMController), new ContainerControlledLifetimeManager());
        }

        protected override Assembly GetBusinessAssembly()
        {
            return Assembly.GetAssembly(typeof(EntityListItem));
        }

        protected override Assembly GetModuleAssembly()
        {
            return Assembly.GetAssembly(typeof(PlmModule));
        }

        public override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.GetContainer().RegisterType<object, PLMWorkAreaView>(typeof(PLMWorkAreaView).FullName,
                new ContainerControlledLifetimeManager());
            containerRegistry.GetContainer()
                .RegisterTypes(
                    AllClasses.FromLoadedAssemblies().Where(x => typeof(IChildNavigationItem).IsAssignableFrom(x)),
                    WithMappings.FromAllInterfaces, WithName.TypeName, WithLifetime.ContainerControlled);
            containerRegistry.Register<IGenericFileService<OperationItemFileDto>, GenericFileService<OperationItemFileDto>>();
        }
    }
}