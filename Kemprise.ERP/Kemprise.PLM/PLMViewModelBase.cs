﻿using System.Collections.Generic;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.ViewModel;
using Prism.Commands;
using Unity;

namespace Kemprise.PLM
{
    public class PLMViewModelBase : ErpViewModelBase
    {
        protected PLMViewModelBase(IUnityContainer container, string category) :
            base(container, ErpModules.PLM, category)

        {
            ProductSource = new Dictionary<string, string> { { "Make", "Make" }, { "Buy", "Buy" } };
            AssemblyModes = new Dictionary<string, string>
                {{"Separable", "Separable"}, {"Inseparable", "Inseparable"}, {"Component", "Component"}};
        }

        protected override string ModuleReportAssembly => "Kemprise.PLM";
        protected override string ModuleReportFilePath => @"Reports\ReportFiles";

        public DelegateCommand EcnViewPartCommand { get; set; }
        public DelegateCommand EcnViewDocumentCommand { get; set; }
        public DelegateCommand EcnViewDrawingCommand { get; set; }
        public DelegateCommand PrintPartCommand { get; set; }

        public Dictionary<string, string> AssemblyModes { get; set; }
        public Dictionary<string, string> ProductSource { get; set; }
    }
}