﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.PLM.PLMViews.NewReleased;
using Kemprise.PLM.PLMViews.ReviseToObsolete;
using Kemprise.PLM.PLMViews.ReviseToRelease;
using Unity;

namespace Kemprise.PLM.NavigationItems
{
    public class EcnReviseToReleased : ChildNavigationItemBase, IChildNavigationItem
    {
        private ReviseToReleasedWorkAreaView _workAreaView;

        public EcnReviseToReleased(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<ReviseToReleasedWorkAreaView>();
        }

        public override string Name => "Revise to Released";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Version.png", UriKind.Relative);

        public override Type Parent => typeof(EcnNavItem);

        public override ErpModules Module => ErpModules.PLM;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<EcnListItem>(ApiControllers.PLM, "ReviseToReleasedGetRecList");
        }
    }
}