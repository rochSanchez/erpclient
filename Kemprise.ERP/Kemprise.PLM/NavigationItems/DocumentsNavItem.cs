﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.PLM.PLMViews.Document;
using Unity;

namespace Kemprise.PLM.NavigationItems
{
    public class DocumentsNavItem : ParentNavigationItemBase
    {
        private readonly DocumentWorkAreaView _workAreaView;

        public DocumentsNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<DocumentWorkAreaView>();
        }

        public override string Name => "Documents";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Doc.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.PLM;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<DocumentListItem>(ApiControllers.PLM, "DocumentGetRecList");
        }
    }
}