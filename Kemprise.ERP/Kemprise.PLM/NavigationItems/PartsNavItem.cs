﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.PLM.PLMViews.NewPart;
using Unity;

namespace Kemprise.PLM.NavigationItems
{
    public class PartsNavItem : ParentNavigationItemBase
    {
        private readonly NewPartWorkAreaView _workAreaView;

        public PartsNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<NewPartWorkAreaView>();
        }

        public override string Name => "Parts";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Part.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.PLM;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<NewPartListItem>(ApiControllers.PLM, "NewPartGetRecList");
        }
    }
}