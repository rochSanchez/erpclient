﻿using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.PLM.PLMViews.Prototype;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.PLM.NavigationItems
{
    public class EcnPrototype : ChildNavigationItemBase, IChildNavigationItem
    {
        private readonly PrototypeWorkAreaView _workAreaView;

        public EcnPrototype(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<PrototypeWorkAreaView>();
        }

        public override string Name => "Prototype";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Version.png", UriKind.Relative);

        public override Type Parent => typeof(EcnNavItem);

        public override object View => _workAreaView;

        public override ErpModules Module => ErpModules.PLM;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<EcnListItem>(ApiControllers.PLM, "PrototypeGetRecList");
        }
    }
}