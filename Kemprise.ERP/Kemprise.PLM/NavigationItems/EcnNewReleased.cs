﻿using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.PLM.PLMViews.NewReleased;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.PLM.NavigationItems
{
    public class EcnNewReleased : ChildNavigationItemBase, IChildNavigationItem
    {
        private NewReleasedWorkAreaView _workAreaView;

        public EcnNewReleased(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<NewReleasedWorkAreaView>();
        }

        public override string Name => "New Released";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Version.png", UriKind.Relative);

        public override Type Parent => typeof(EcnNavItem);

        public override ErpModules Module => ErpModules.PLM;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<EcnListItem>(ApiControllers.PLM, "NewReleasedGetRecList");
        }
    }
}