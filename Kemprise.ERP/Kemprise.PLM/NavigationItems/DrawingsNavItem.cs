﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.PLM.PLMViews.Drawing;
using Unity;

namespace Kemprise.PLM.NavigationItems
{
    public class DrawingsNavItem : ParentNavigationItemBase
    {
        private readonly DrawingWorkAreaView _workAreaView;

        public DrawingsNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<DrawingWorkAreaView>();
        }

        public override string Name => "Drawings";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Draw.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.PLM;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<DrawingListItem>(ApiControllers.PLM, "DrawingGetRecList");
        }
    }
}