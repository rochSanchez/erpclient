﻿using System;
using System.Linq;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Unity;

namespace Kemprise.PLM.NavigationItems
{
    public class EcnNavItem : ParentNavigationItemBase
    {
        public EcnNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            Children = container.ResolveAll<IChildNavigationItem>()
                .Where(item => item.Parent == typeof(EcnNavItem));
        }

        public override string Name => "ECN";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Version.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.PLM;
    }
}