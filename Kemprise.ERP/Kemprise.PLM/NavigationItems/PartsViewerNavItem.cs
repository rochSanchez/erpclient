﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.PLM.PLMViews.PartsViewer;
using Unity;

namespace Kemprise.PLM.NavigationItems
{
    public class PartsViewerNavItem : ParentNavigationItemBase
    {
        private readonly PartsViewerWorkAreaView _workAreaView;

        public PartsViewerNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<PartsViewerWorkAreaView>();
        }

        public override string Name => "Parts Viewer";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Part.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.PLM;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<NewPartListItem>(ApiControllers.PLM, "ReleasedPartsGetRecList");
        }
    }
}