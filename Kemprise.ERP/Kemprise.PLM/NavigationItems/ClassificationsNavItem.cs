﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.PLM.Classification;
using ERP.WebApi.DTO.DTOs.PLM.PartAttribute;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.PLM.PLMViews.Classification;
using Kemprise.PLM.PLMViews.PartAttributes;
using Unity;

namespace Kemprise.PLM.NavigationItems
{
    public class ClassificationsNavItem : ParentNavigationItemBase
    {
        private readonly ClassificationWorkAreaView _workAreaView;

        public ClassificationsNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<ClassificationWorkAreaView>();

            IsVisible = ApiSession.HasRoles(ErpRoles.PlmRoles);
        }

        public override string Name => "Part Classifications";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Part.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.PLM;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<ClassificationListItem>(ApiControllers.Classification,
                "ClassificationGetRecList");
        }
    }
}