﻿using ERP.WebApi.DTO.Constants;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.PLM.Controller;
using Prism.Commands;
using Prism.Events;
using Unity;

namespace Kemprise.PLM.QuickAccess
{
    public class PLMQuickAccessViewModel : NotifyPropertyChangedBase, IQuickAccess
    {
        private readonly IEventAggregator _eventAggregator;

        public PLMQuickAccessViewModel(IEventAggregator eventAggregator, IUnityContainer container)
        {
            _eventAggregator = eventAggregator;
            eventAggregator.GetEvent<DeSelectModuleEvent<PLMController>>().Subscribe(OnDeSelectModule);

            OpenModuleCommand = new DelegateCommand(OpenModuleExecute);

            IsVisible = ApiSession.HasRoles(ErpRoles.PlmRoles);
        }

        public DelegateCommand OpenModuleCommand { get; set; }

        private void OpenModuleExecute()
        {
            _eventAggregator.GetEvent<OpenModuleEvent>().Publish(new OpenModuleEventPayload(ErpModules.PLM, this));
        }

        private void OnDeSelectModule()
        {
            IsSelected = false;
        }

        public bool IsSelected
        {
            get { return GetValue(() => IsSelected); }
            set { SetValue(() => IsSelected, value); }
        }

        public bool IsVisible
        {
            get { return GetValue(() => IsVisible); }
            set { SetValue(() => IsVisible, value); }
        }
    }
}