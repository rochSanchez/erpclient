﻿using Kemprise.Infrastructure.Modules;

namespace Kemprise.PLM.QuickAccess
{
    /// <summary>
    /// Interaction logic for ManagementQuickAccessView.xaml
    /// </summary>
    public partial class PLMQuickAccessView : IQuickAccessView
    {
        public PLMQuickAccessView()
        {
            InitializeComponent();
        }

        public int Order => 4;
    }
}