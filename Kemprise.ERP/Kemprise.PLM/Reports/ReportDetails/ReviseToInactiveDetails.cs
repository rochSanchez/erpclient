﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using ERP.WebApi.DTO.Lookups;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Reporting;
using Kemprise.PLM.PLMViews.ReviseToInactive;

namespace Kemprise.PLM.Reports.ReportDetails
{
    public class ReviseToInactiveDetails : IModuleReportDetail
    {
        public string ModuleName => ErpModules.PLM.ToString();

        public string ReportTitle => "Part Details";

        public string ReportFileName => "partdetails";

        public DataSet ConfigureReportData(EntityBase entity,
            Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            var ecnEntity = entity.As<ReviseToInactiveEntity>();
            var part = ecnEntity.NewPart;
            var ecn = ecnEntity.EcnDto.GetDto<EcnDto>();
            var boms = ecnEntity.BillOfMaterials;
            var attributes = ecnEntity.PartAttributes;
            var classification = lookups["Classifications"].First(x => x.Id == part.ClassificationId.ToString());
            var uom = lookups["Uoms"].First(x => x.Id == part.UnitOfMeasurementId.ToString());
            var state = lookups["States"].First(x => x.Id == part.StateId.ToString());
            var drawing = ecnEntity.DrawingDto;
            var latest = ecnEntity.LatestEcn;
            var original = ecnEntity.OriginalEcn;

            var reportData = new PartReportData()
            {
                Name = part.Name,
                Source = part.Source,
                Description = part.Description,
                AssemblyMode = part.AssemblyMode,
                Classification = classification.Text,
                PartNumber = part.PartNumber,
                Version = $"{part.Version}.{part.Iteration}",
                UnitOfMeasurement = uom.Text,
                State = state.Text,
                EcnNumber = $"{latest.EcnNumber} - {latest.Description}",
                CheckerFullName = $"{latest.CheckerFullName}:{latest.DateApprovedByChecker.Value.ToShortDateString()}",
                PromoterFullName = $"{latest.PromoterFullName}:{latest.DateApprovedByPromoter.Value.ToShortDateString()}",
                Submitter = $"{latest.CreatedByFullName}:{latest.CreatedAt.ToShortDateString()}",
                ApproverFullName = latest.ApproverFullName,
                DrawingNumber = drawing?.DrawingNumber,
                DrawingVersion = $"{drawing?.Version}.{drawing?.Iteration}",
                OriginalCheckerFullName = $"{original.CheckerFullName}:{original.DateApprovedByChecker.Value.ToShortDateString()}",
                OriginalPromoterFullName = $"{original.PromoterFullName}:{original.DateApprovedByPromoter.Value.ToShortDateString()}",
                OriginalApproverFullName = original.ApproverFullName,
                OriginalSubmitter = $"{original.CreatedByFullName}:{original.CreatedAt.ToShortDateString()}"
            };

            var bomReportData = new List<BomReportData>();
            boms.ForEach(item =>
            {
                var partUom = lookups["Uoms"].First(x => x.Id == item.Part.UnitOfMeasurementId.ToString());
                bomReportData.Add(new BomReportData
                {
                    Name = item.Part.Name,
                    PartNumber = item.Part.PartNumber,
                    UnitOfMeasurement = partUom.Text,
                    ItemNumber = item.ItemNumber.ToString(),
                    Quantity = item.Quantity.ToString()
                });
            });

            var parAttributesReportData = new List<AttributesReportData>();
            attributes.ForEach(item =>
            {
                parAttributesReportData.Add(new AttributesReportData
                {
                    Name = item.Name,
                    Value = item.Value
                });
            });

            var dataSet = new DataSet();

            dataSet.Tables.Add(reportData.ToDataTable());
            dataSet.Tables.Add(bomReportData.ToDataTable());
            ;
            dataSet.Tables.Add(parAttributesReportData.ToDataTable());
            ;
            return dataSet;
        }

        public ReportParameters GetReportParameters(EntityBase entity,
            Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            return new ReportParameters();
        }
    }
}