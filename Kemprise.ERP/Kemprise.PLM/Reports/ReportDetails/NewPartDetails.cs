﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using ERP.WebApi.DTO.Lookups;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Reporting;
using Kemprise.PLM.PLMViews.NewPart;

namespace Kemprise.PLM.Reports.ReportDetails
{
    public class NewPartDetails : IModuleReportDetail
    {
        public string ModuleName => ErpModules.PLM.ToString();

        public string ReportTitle => "Part Details";

        public string ReportFileName => "partdetails";

        public DataSet ConfigureReportData(EntityBase entity,
            Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            var partEntity = entity.As<NewPartEntity>();
            var part = partEntity.NewPart.GetDto<PartDto>();
            var boms = partEntity.BillOfMaterials;
            var attributes = partEntity.PartAttributes;
            var classification = lookups["Classifications"].First(x => x.Id == part.ClassificationId.ToString());
            var uom = lookups["Uoms"].First(x => x.Id == part.UnitOfMeasurementId.ToString());
            var state = lookups["States"].First(x => x.Id == part.StateId.ToString());
            var drawing = partEntity.DrawingDto;

            var reportData = new PartReportData
            {
                Name = part.Name,
                Source = part.Source,
                Description = part.Description,
                AssemblyMode = part.AssemblyMode,
                Classification = classification.Text,
                PartNumber = part.PartNumber,
                Version = $"{part.Version}.{part.Iteration}",
                UnitOfMeasurement = uom.Text,
                State = state.Text,
                DrawingNumber = drawing?.DrawingNumber,
                DrawingVersion = drawing?.Version
            };

            var bomReportData = new List<BomReportData>();
            boms.ForEach(item =>
            {
                var partUom = lookups["Uoms"].First(x => x.Id == item.Part.UnitOfMeasurementId.ToString());
                bomReportData.Add(new BomReportData
                {
                    Name = item.Part.Name,
                    PartNumber = item.Part.PartNumber,
                    UnitOfMeasurement = partUom.Text,
                    ItemNumber = item.ItemNumber.ToString(),
                    Quantity = item.Quantity.ToString()
                });
            });

            var parAttributesReportData = new List<AttributesReportData>();
            attributes.ForEach(item =>
            {
                parAttributesReportData.Add(new AttributesReportData
                {
                    Name = item.Name, Value = item.Value
                });
            });

            var dataSet = new DataSet();

            dataSet.Tables.Add(reportData.ToDataTable());
            dataSet.Tables.Add(bomReportData.ToDataTable());
            ;
            dataSet.Tables.Add(parAttributesReportData.ToDataTable());
            ;
            return dataSet;
        }

        public ReportParameters GetReportParameters(EntityBase entity,
            Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            return new ReportParameters();
        }
    }
}