﻿namespace Kemprise.PLM.Reports.ReportDetails
{
    public class AttributesReportData
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}