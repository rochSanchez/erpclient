﻿namespace Kemprise.PLM.Reports.ReportDetails
{
    public class BomReportData
    {
        public string Quantity { get; set; }

        public string ItemNumber { get; set; }

        public string PartNumber { get; set; }

        public string Name { get; set; }

        public string UnitOfMeasurement { get; set; }
    }
}