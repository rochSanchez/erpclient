﻿namespace Kemprise.PLM.Reports.ReportDetails
{
    public class PartReportData
    {
        public string PartNumber { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Version { get; set; }

        public string Source { get; set; }

        public string AssemblyMode { get; set; }

        public string UnitOfMeasurement { get; set; }

        public string Classification { get; set; }

        public string State { get; set; }

        //ecn
        public string Submitter { get; set; }

        public string EcnNumber { get; set; }

        public string ApproverFullName { get; set; }

        public string CheckerFullName { get; set; }

        public string PromoterFullName { get; set; }

        public string OriginalSubmitter { get; set; }

        public string OriginalApproverFullName { get; set; }

        public string OriginalCheckerFullName { get; set; }

        public string OriginalPromoterFullName { get; set; }

        //drawing
        public string DrawingNumber { get; set; }

        public string DrawingVersion { get; set; }
    }
}