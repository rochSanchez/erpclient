﻿using ERP.WebApi.DTO;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.WorkArea;
using Prism.Events;
using System.Collections.Generic;
using System.Linq;
using Unity;

namespace Kemprise.PLM.WorkArea
{
    public class PLMWorkAreaViewModel : ErpWorkAreaViewModelBase
    {
        public PLMWorkAreaViewModel(IUnityContainer container, IEventAggregator eventAggregator) :
            base(container, eventAggregator)
        {
            ItemsListDictionary = new Dictionary<string, IEnumerable<EntityListItem>>();

            NavigationItems = container.ResolveAll<IParentNavigationItem>()
                .Where(item => item.Module == ErpModules.PLM)
                .OrderBy(item => item.Name);

            eventAggregator.GetEvent<RefreshItemListEvent<PLMWorkAreaViewModel>>().Subscribe(OnRefreshItemList);
        }
    }
}