﻿namespace Kemprise.Infrastructure.Modules
{
    public interface IQuickAccessView
    {
        int Order { get; }
    }
}