﻿namespace Kemprise.Infrastructure.Modules
{
    public enum ErpModules
    {
        Home,
        Inquiry,
        Sales,
        Management,
        Account,
        Inventory,
        Procurement,
        PLM
    }
}