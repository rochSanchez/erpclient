﻿namespace Kemprise.Infrastructure.Modules
{
    public interface IQuickAccess
    {
        bool IsSelected { get; set; }
        bool IsVisible { get; set; }
    }
}