﻿using System.Collections.Generic;

namespace Kemprise.Infrastructure.Modules.Navigation
{
    public interface IParentNavigationItem : INavigationItem
    {
        IEnumerable<INavigationItem> Children { get; set; }
        bool IsVisible { get; }
        int Order { get; }
    }
}