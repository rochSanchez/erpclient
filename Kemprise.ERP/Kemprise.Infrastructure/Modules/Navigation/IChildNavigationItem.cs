﻿using System;

namespace Kemprise.Infrastructure.Modules.Navigation
{
    public interface IChildNavigationItem : INavigationItem
    {
        public Type Parent { get; }
    }
}