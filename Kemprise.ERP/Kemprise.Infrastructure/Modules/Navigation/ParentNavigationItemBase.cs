﻿using ERP.WebApi.DTO;
using Kemprise.Infrastructure.Services.APICommon;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.Infrastructure.Modules.Navigation
{
    public abstract class ParentNavigationItemBase : IParentNavigationItem

    {
        private readonly IUnityContainer _container;
        protected readonly IApiRequestManager RequestManager;

        protected ParentNavigationItemBase(IUnityContainer container, IApiRequestManager requestManager)
        {
            _container = container;
            RequestManager = requestManager;

            Children = new ObservableCollection<INavigationItem>();
            IsVisible = true;
        }

        public IEnumerable<INavigationItem> Children { get; set; }
        public bool IsVisible { get; set; }
        public virtual int Order { get; set; }

        public abstract string Name { get; }
        public virtual object View => null;
        public abstract Uri Icon { get; }
        public abstract ErpModules Module { get; }

        public virtual async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await Task.Run(() => new List<EntityListItem>());
        }
    }
}