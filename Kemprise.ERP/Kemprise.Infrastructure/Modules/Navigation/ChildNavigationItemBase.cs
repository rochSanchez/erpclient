﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;
using Kemprise.Infrastructure.Services.APICommon;
using Unity;

namespace Kemprise.Infrastructure.Modules.Navigation
{
    public abstract class ChildNavigationItemBase : IChildNavigationItem

    {
        public IApiRequestManager RequestManager { get; }
        public bool IsVisible { get; set; }
        private readonly IUnityContainer _container;

        protected ChildNavigationItemBase(IUnityContainer container, IApiRequestManager requestManager)
        {
            RequestManager = requestManager;
            _container = container;
            IsVisible = true;
        }

        public abstract string Name { get; }
        public virtual object View => null;
        public abstract Uri Icon { get; }
        public abstract Type Parent { get; }
        public abstract ErpModules Module { get; }

        public virtual async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await Task.Run(() => new List<EntityListItem>());
        }
    }
}