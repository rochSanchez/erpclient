﻿namespace Kemprise.Infrastructure.Modules.Navigation
{
    public enum NavItemModule
    {
        Management, Sales
    }
}