﻿using Kemprise.Infrastructure.Modules;
using Prism.Events;

namespace Kemprise.Infrastructure.Events.Regions
{
    public class CloseDocumentPaneEvent : PubSubEvent<CloseDocumentPanePayload>
    {
    }

    public class CloseDocumentPanePayload
    {
        public CloseDocumentPanePayload(string id, ErpModules moduleName)
        {
            Id = id;
            ModuleName = moduleName;
        }

        public string Id { get; }
        public ErpModules ModuleName { get; }
    }
}