﻿using Prism.Events;

namespace Kemprise.Infrastructure.Events.Errors
{
    public class ErrorEvent : PubSubEvent<ErrorEventPayload>
    {
    }

    public class ErrorEventPayload
    {
        public ErrorEventPayload(string message = null)
        {
            Message = message;
        }

        public ErrorEventPayload(DuplicateKeyException duplicateKeyException)
        {
            Message = duplicateKeyException.Message;
        }

        public string Message { get; }
    }

    public class DuplicateKeyException
    {
        public string Message { get; set; }

        public DuplicateKeyException(string message)
        {
            Message = message;
        }
    }
}