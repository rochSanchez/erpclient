﻿using Kemprise.Infrastructure.WorkArea;
using Prism.Events;

namespace Kemprise.Infrastructure.Events.Modules
{
    public class RefreshItemListEvent<T> : PubSubEvent<RefreshItemListEventPayload> where T : ErpWorkAreaViewModelBase
    {
    }

    public class RefreshItemListEventPayload
    {
        public RefreshItemListEventPayload(object viewModel, string typeName)
        {
            ViewModel = viewModel;
            TypeName = typeName;
        }

        public object ViewModel { get; }
        public string TypeName { get; }
    }
}