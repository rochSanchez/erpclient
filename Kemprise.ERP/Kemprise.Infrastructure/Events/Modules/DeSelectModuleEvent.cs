﻿using Kemprise.Infrastructure.Controllers;
using Prism.Events;

namespace Kemprise.Infrastructure.Events.Modules
{
    public class DeSelectModuleEvent<T> : PubSubEvent
    {
    }
}