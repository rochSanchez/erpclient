﻿using Prism.Events;

namespace Kemprise.Infrastructure.Events.Security
{
    public class LoginSuccessfulEvent : PubSubEvent<LoginSuccessfulEventPayload>
    {
    }

    public class LoginSuccessfulEventPayload
    {
        public bool IsSuccessful { get; set; }
    }
}