﻿using Prism.Events;

namespace Kemprise.Infrastructure.Events.Security
{
    public class LogoutUserEvent : PubSubEvent
    {
    }
}