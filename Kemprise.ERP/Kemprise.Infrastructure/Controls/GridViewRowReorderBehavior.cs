﻿using Microsoft.Xaml.Behaviors;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.GridView;

namespace Kemprise.Infrastructure.Controls
{
    public class GridViewRowReorderBehavior : Behavior<RadGridView>
    {
        private Popup DropIndicator { get; set; }
        private Grid DragCue { get; set; }

        public Brush DropIndicatorBrush
        {
            get => (Brush)GetValue(DropIndicatorBrushProperty);
            set => SetValue(DropIndicatorBrushProperty, value);
        }

        public Brush DropImpossibleBackground
        {
            get => (Brush)GetValue(DropImpossibleBackgroundProperty);
            set => SetValue(DropImpossibleBackgroundProperty, value);
        }

        public Brush DropPossibleBackground
        {
            get => (Brush)GetValue(DropPossibleBackgroundProperty);
            set => SetValue(DropPossibleBackgroundProperty, value);
        }

        public static readonly DependencyProperty DropIndicatorBrushProperty =
            DependencyProperty.Register("DropIndicatorBrush", typeof(Brush), typeof(GridViewRowReorderBehavior),
                new PropertyMetadata((SolidColorBrush)new BrushConverter().ConvertFrom("#00AC4C")));

        public static readonly DependencyProperty DropImpossibleBackgroundProperty =
            DependencyProperty.Register("DropImpossibleBackground", typeof(Brush), typeof(GridViewRowReorderBehavior),
                new PropertyMetadata(new SolidColorBrush(Colors.Transparent)));

        public static readonly DependencyProperty DropPossibleBackgroundProperty =
            DependencyProperty.Register("DropPossibleBackground", typeof(Brush), typeof(GridViewRowReorderBehavior),
                new PropertyMetadata(new SolidColorBrush(Colors.Transparent)));

        public bool ItemsSourceSupportNotifications => AssociatedObject.ItemsSource is INotifyCollectionChanged;

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.RowLoaded += AssociatedObject_RowLoaded;
            DropIndicator = new Popup
            {
                AllowsTransparency = true,
                Focusable = false,
                IsManipulationEnabled = false,
                Child = new Rectangle
                {
                    Fill = DropIndicatorBrush,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    IsHitTestVisible = false,
                }
            };
        }

        [Obsolete("Obsolete")]
        private void AssociatedObject_RowLoaded(object sender, RowLoadedEventArgs e)
        {
            if (!(e.Row is GridViewRow row) || RadDragAndDropManager.GetAllowDrag(row)) return;
            RadDragAndDropManager.SetAllowDrag(row, true);
            RadDragAndDropManager.SetAllowDrop(row, true);
            RadDragAndDropManager.AddDragQueryHandler(row, OnDragQuery);
            RadDragAndDropManager.AddDragInfoHandler(row, OnDragInfo);
            RadDragAndDropManager.AddDropQueryHandler(row, OnDropQuery);
            RadDragAndDropManager.AddDragInfoHandler(row, OnDropInfo);
        }

        [Obsolete("Obsolete")]
        public void OnDragQuery(object sender, DragDropQueryEventArgs e)
        {
            if (!(sender is GridViewRow row)) return;

            e.QueryResult = RadDragAndDropManager.GetAllowDrag(row) &&
                (e.Options.Destination == null || e.Options.Destination is GridViewRow);
            if (!(bool)e.QueryResult || e.Options.Status != DragStatus.DragQuery) return;
            e.Options.Payload = row.DataContext;

            DragCue = new Grid
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch
            };

            DragCue.Children.Add(row.ToImage());
            e.Options.DragCue = DragCue;

            if (DropIndicator.Child is Rectangle child)
            {
                child.Height = row.ActualHeight;
                child.Width = 10;
            }

            DropIndicator.Height = row.ActualHeight;
            DropIndicator.IsHitTestVisible = false;
            DropIndicator.Opacity = 0;
            DropIndicator.IsOpen = true;
            DropIndicator.PlacementTarget = Application.Current.MainWindow;
            DropIndicator.Placement = System.Windows.Controls.Primitives.PlacementMode.Top;
        }

        public void OnDragInfo(object sender, DragDropEventArgs e)
        {
            if (e.Options.Status == DragStatus.DropImpossible && DragCue != null)
            {
                DragCue.Background = DropImpossibleBackground;
                DropIndicator.Opacity = 0;
            }
            else if (DragCue != null)
            {
                DragCue.Background = DropPossibleBackground;
                DropIndicator.HorizontalOffset = e.Options.CurrentDragPoint.X - e.Options.RelativeDragPoint.X;
                DropIndicator.VerticalOffset = e.Options.CurrentDragPoint.Y - e.Options.RelativeDragPoint.Y + DropIndicator.Height;
                System.Diagnostics.Debug.WriteLine(e.Options.CurrentDragPoint);
                DropIndicator.Opacity = 100;
            }
        }

        public void OnDropQuery(object sender, DragDropQueryEventArgs e)
        {
            if (e.Options.Destination is GridViewRow targetRow)
                e.QueryResult = targetRow.GridViewDataControl == AssociatedObject;
            else
                e.QueryResult = false;
        }

        public void OnDropInfo(object sender, DragDropEventArgs e)
        {
            var item = e.Options.Payload;

            if (e.Options.Source is GridViewRow
                && e.Options.Destination is GridViewRow destination
                && AssociatedObject.Items.Contains(item)
                && e.Options.Status == DragStatus.DragComplete)
            {
                var index = AssociatedObject.ItemContainerGenerator.IndexFromContainer(destination);

                ((IList)AssociatedObject.ItemsSource).Remove(item);
                ((IList)AssociatedObject.ItemsSource).Insert(index, item);

                if (!ItemsSourceSupportNotifications)
                {
                    AssociatedObject.Rebind();
                }

                DropIndicator.IsOpen = false;
            }

            if (e.Options.Status == DragStatus.DragCancel)
            {
                DropIndicator.IsOpen = false;
            }
        }
    }

    public static class FrameworkElementHelper
    {
        public static Image ToImage(this FrameworkElement element)
        {
            var sourceBrush = new VisualBrush(element) { Stretch = Stretch.None };
            var drawingVisual = new DrawingVisual();

            var drawingContext = drawingVisual.RenderOpen();
            using (drawingContext)
            {
                drawingContext.DrawRectangle(sourceBrush, null, new Rect(new Point(0, 0), new Size(element.ActualWidth, element.ActualHeight)));
            }

            var bitmap = new RenderTargetBitmap(1200, 1200, 96, 96, PixelFormats.Pbgra32);
            bitmap.Render(drawingVisual);

            return new Image { Source = bitmap };
        }
    }
}