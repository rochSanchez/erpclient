﻿using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.Extensions;
using System.Windows;
using System.Windows.Controls;

namespace Kemprise.Infrastructure.Controls
{
    public class QuotationReceivedTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var rfqDto = item.As<RfqDto>();

            return rfqDto?.Status == RfqStatus.AwaitingQuotation
                ? AwaitingQuotationTemplate
                : QuotationReceivedTemplate;
        }

        public DataTemplate AwaitingQuotationTemplate { get; set; }
        public DataTemplate QuotationReceivedTemplate { get; set; }
    }
}