﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Kemprise.Infrastructure.Controls
{
    public class ImageMenuButton : Button
    {
        static ImageMenuButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ImageMenuButton), new FrameworkPropertyMetadata(typeof(ImageMenuButton)));
            ImageSourceProperty = DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(ImageMenuButton));
            ButtonForegroundProperty = DependencyProperty.Register("ButtonForeground", typeof(SolidColorBrush), typeof(ImageMenuButton));
        }

        public static readonly DependencyProperty ImageSourceProperty;
        public static readonly DependencyProperty ButtonForegroundProperty;

        public ImageSource ImageSource
        {
            get => (ImageSource)GetValue(ImageSourceProperty);
            set => SetValue(ImageSourceProperty, value);
        }

        public SolidColorBrush ButtonForeground
        {
            get => (SolidColorBrush)GetValue(ButtonForegroundProperty);
            set => SetValue(ButtonForegroundProperty, value);
        }
    }
}