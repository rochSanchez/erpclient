﻿using ERP.WebApi.DTO.DTOs.Procurement.NewPurchaseRequest;
using Kemprise.Infrastructure.Extensions;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Kemprise.Infrastructure.Controls
{
    public class PrSupplierTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var prSupplierAggregate = item.As<PrSupplierAggregate>();

            if (prSupplierAggregate?.PrQuotations == null) return null;
            return prSupplierAggregate.PrQuotations.Any() ? PrQuotationsTemplate : null;
        }

        public DataTemplate PrQuotationsTemplate { get; set; }
    }
}