﻿using System.Globalization;
using System.Windows.Data;

namespace Kemprise.Infrastructure.Controls
{
    public class CultureAwareBinding : Binding
    {
        public CultureAwareBinding()
        {
            ConverterCulture = CultureInfo.CurrentCulture;
        }
    }
}