﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Kemprise.Infrastructure.Tools.ExcelReader
{
    public class TableToObjectMapper<T> where T : class, new()
    {
        public IEnumerable<T> Map(DataTable table)
        {
            var properties = typeof(T).GetProperties()
                .Where(x => x.GetCustomAttributes(typeof(MappingNamesAttribute), true).Any())
                .ToList();

            var entities = new List<T>();
            foreach (var row in table.AsEnumerable())
            {
                var entity = new T();
                foreach (var prop in properties) Map(typeof(T), row, prop, entity);
                entities.Add(entity);
            }

            return entities;
        }

        private void Map(Type type, DataRow row, PropertyInfo prop, object entity)
        {
            var columnNames = GetDataNames(type, prop.Name);

            foreach (var columnName in columnNames)
            {
                if (string.IsNullOrWhiteSpace(columnName) || !row.Table.Columns.Contains(columnName)) continue;
                var propertyValue = row[columnName];
                if (propertyValue == DBNull.Value) continue;
                Parse(prop, entity, row[columnName]);
                break;
            }
        }

        private IEnumerable<string> GetDataNames(Type type, string propertyName)
        {
            var property = type.GetProperty(propertyName)?.GetCustomAttributes(false)
                .FirstOrDefault(x => x.GetType().Name == "MappingNamesAttribute");
            return property != null ? ((MappingNamesAttribute)property).MappingNames : new List<string>();
        }

        private static void Parse(PropertyInfo prop, object entity, object value)
        {
            if (prop.PropertyType == typeof(string))
                prop.SetValue(entity, value.ToString().Trim(), null);
            if (prop.PropertyType == typeof(int))
            {
                if (value == null)
                    prop.SetValue(entity, null, null);
                else
                    prop.SetValue(entity, int.Parse(value.ToString()), null);
            }

            if (prop.PropertyType != typeof(decimal)) return;
            if (value == null)
                prop.SetValue(entity, null, null);
            else
                prop.SetValue(entity, decimal.Parse(value.ToString()), null);
        }
    }
}