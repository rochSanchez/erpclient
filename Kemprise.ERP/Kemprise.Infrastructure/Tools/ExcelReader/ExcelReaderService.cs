﻿using ClosedXML.Excel;
using ExcelDataReader;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace Kemprise.Infrastructure.Tools.ExcelReader
{
    public class ExcelReaderService : IExcelReaderService
    {
        public IEnumerable<T> Import<T>(string fileName) where T : class, new()
        {
            using var stream = File.Open(fileName, FileMode.Open, FileAccess.Read);
            using var reader = ExcelReaderFactory.CreateReader(stream);
            var dataSet = reader.AsDataSet(new ExcelDataSetConfiguration
            { ConfigureDataTable = tableReader => new ExcelDataTableConfiguration { FilterRow = FilterRow } });
            var mapper = new TableToObjectMapper<T>();
            return mapper.Map(dataSet.Tables[0]);
        }

        public void Export(DataTable dt, string filename)
        {
            using var wb = new XLWorkbook();

            wb.Worksheets.Add(dt, "Sheet 1");
            var ws = wb.Worksheets.FirstOrDefault();
            ws?.AutoFilter.Clear();
            ws?.SetAutoFilter(false);
            ws?.Columns().AdjustToContents();

            wb.SaveAs(filename);
        }

        private bool FilterRow(IExcelDataReader arg)
        {
            return arg.Depth != 0;
        }
    }
}