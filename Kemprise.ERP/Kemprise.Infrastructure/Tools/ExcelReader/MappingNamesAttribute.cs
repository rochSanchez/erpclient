﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kemprise.Infrastructure.Tools.ExcelReader
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MappingNamesAttribute : Attribute
    {
        public MappingNamesAttribute()
        {
            MappingNames = new List<string>();
        }

        public MappingNamesAttribute(params string[] valueNames)
        {
            MappingNames = valueNames.ToList();
        }

        public List<string> MappingNames { get; set; }
    }
}