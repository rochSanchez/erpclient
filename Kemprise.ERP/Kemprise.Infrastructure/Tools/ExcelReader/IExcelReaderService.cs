﻿using System.Collections.Generic;
using System.Data;

namespace Kemprise.Infrastructure.Tools.ExcelReader
{
    public interface IExcelReaderService
    {
        IEnumerable<T> Import<T>(string fileName) where T : class, new();

        void Export(DataTable dt, string filename);
    }
}