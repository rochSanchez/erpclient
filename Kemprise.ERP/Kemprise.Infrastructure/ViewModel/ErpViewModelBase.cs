﻿using ERP.WebApi.DTO;
using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.Lookups;
using ERP.WebApi.DTO.Lookups.LookupItems;
using Flurl.Http;
using Kemprise.Infrastructure.Constants;
using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Reporting;
using Kemprise.Infrastructure.Reporting.ReportViewerView;
using Kemprise.Infrastructure.Reporting.Service;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Lookup;
using Kemprise.Infrastructure.Services.Notifications;
using Prism.Commands;
using Prism.Events;
using Prism.Ioc;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Infrastructure.ViewModel
{
    public class ErpViewModelBase : NotifyPropertyChangedBase, IErpViewModelBase
    {
        private readonly string _category;
        private readonly IDesktopNotificationService _desktopNotificationService;
        private readonly ErpModules _moduleName;
        private readonly IReportLoaderService _reportLoaderService;
        private Dictionary<string, IModuleReportDetail> _moduleReportDetails;

        protected ErpViewModelBase(IUnityContainer container, ErpModules moduleName, string category)
        {
            _moduleName = moduleName;
            _category = category;
            Container = container;

            _reportLoaderService = container.Resolve<IReportLoaderService>();
            _desktopNotificationService = container.Resolve<IDesktopNotificationService>();
            EventAggregator = container.Resolve<IEventAggregator>();
            LookupService = container.Resolve<ILookupService>();
            WorldService = container.Resolve<IWorldService>();

            Lookups = new Dictionary<string, ObservableCollection<LookupEntity>>();
            LookupService.InitiateLookups(Lookups);
            Methods = new Dictionary<string, IErpMethodCall>();

            DocumentPanes = new ObservableCollection<RadDocumentPane>();

            SortedCommand = new DelegateCommand(() => SelectedItem = null);
            GroupedCommand = new DelegateCommand(() => SelectedItem = null);
            FilteredCommand = new DelegateCommand(() => SelectedItem = null);
            SortedPartCommand = new DelegateCommand(() => SelectedPart = null);
            FilteredPartCommand = new DelegateCommand(() => SelectedPart = null);

            var dateInfo = new DateTimeFormatInfo { ShortDatePattern = "dd/MMM/yyyy" };
            DateCultureInfo = new CultureInfo("en-US") { DateTimeFormat = dateInfo };

            IsEditVisible = ApiSession.HasRole(ErpRoles.SuperUser) || ApiSession.IsManager();

            var countries = WorldService.GetAllCountries();
            countries.ForEach(x => Countries.Add(x, x));

            ConfigureMethodCalls(moduleName, category);
            GetModuleReports();
        }

        public Dictionary<string, IErpMethodCall> Methods { get; set; }
        public Dictionary<string, string> Countries { get; set; } = new Dictionary<string, string>();

        public DelegateCommand SortedCommand { get; set; }
        public DelegateCommand GroupedCommand { get; set; }
        public DelegateCommand FilteredCommand { get; set; }
        public DelegateCommand ClearCommand { get; set; }
        public DelegateCommand AddNewRecordCommand { get; set; }
        public DelegateCommand OpenCommand { get; set; }
        public DelegateCommand EditCommand { get; set; }
        public DelegateCommand EditAsCommand { get; set; }
        public DelegateCommand BackCommand { get; set; }
        public DelegateCommand SaveAsDraftCommand { get; set; }
        public DelegateCommand PrintDraftCommand { get; set; }
        public DelegateCommand DiscardDraftCommand { get; set; }
        public DelegateCommand SaveForLaterCommand { get; set; }
        public DelegateCommand SaveCommand { get; set; }
        public DelegateCommand CancelCommand { get; set; }
        public DelegateCommand AddExistingItemCommand { get; set; }
        public DelegateCommand AddBlankItemCommand { get; set; }
        public DelegateCommand AddPartCommand { get; set; }
        public DelegateCommand SortedPartCommand { get; set; }
        public DelegateCommand FilteredPartCommand { get; set; }
        public DelegateCommand CloseEntityCommand { get; set; }
        public DelegateCommand AskForRevisionCommand { get; set; }
        public DelegateCommand ViewReasonForRevisionCommand { get; set; }
        public DelegateCommand ViewClosingDetailsCommand { get; set; }
        public DelegateCommand ModifyCommand { get; set; }
        public DelegateCommand<object> RefreshCommand { get; set; }
        public DelegateCommand<object> RadioButtonIsCheckedCommand { get; set; }

        public IUnityContainer Container { get; }
        public IEventAggregator EventAggregator { get; }
        public ILookupService LookupService { get; set; }
        public IWorldService WorldService { get; set; }

        public ObservableCollection<RadDocumentPane> DocumentPanes { get; set; }

        public DynamicNotifyPropertyChangedProxy Entity { get; set; }

        protected virtual string ModuleReportAssembly { get; set; }
        protected virtual string ModuleReportFilePath { get; set; }
        public string Id { get; set; }

        public string BusyContent
        {
            get { return GetValue(() => BusyContent); }
            set { SetValue(() => BusyContent, value); }
        }

        public bool IsBusy
        {
            get { return GetValue(() => IsBusy); }
            set { SetValue(() => IsBusy, value); }
        }

        public CollectionViewSource CollectionView
        {
            get { return GetValue(() => CollectionView); }
            set { SetValue(() => CollectionView, value); }
        }

        public IEnumerable<EntityListItem> EntityListItems
        {
            get { return GetValue(() => EntityListItems); }
            set { SetValue(() => EntityListItems, value); }
        }

        public EntityListItem SelectedItem
        {
            get { return GetValue(() => SelectedItem); }
            set { SetValue(() => SelectedItem, value); }
        }

        public bool IsEditVisible
        {
            get { return GetValue(() => IsEditVisible); }
            set { SetValue(() => IsEditVisible, value); }
        }

        public bool IsClearVisible
        {
            get { return GetValue(() => IsClearVisible); }
            set { SetValue(() => IsClearVisible, value); }
        }

        public bool IsPrintDraftVisible
        {
            get { return GetValue(() => IsPrintDraftVisible); }
            set { SetValue(() => IsPrintDraftVisible, value); }
        }

        public bool IsSaveForLaterVisible
        {
            get { return GetValue(() => IsSaveForLaterVisible); }
            set { SetValue(() => IsSaveForLaterVisible, value); }
        }

        public bool IsViewReasonVisible
        {
            get { return GetValue(() => IsViewReasonVisible); }
            set { SetValue(() => IsViewReasonVisible, value); }
        }

        public Dictionary<string, ObservableCollection<LookupEntity>> Lookups
        {
            get { return GetValue(() => Lookups); }
            set { SetValue(() => Lookups, value); }
        }

        public string SearchFor
        {
            get { return GetValue(() => SearchFor); }
            set
            {
                SetValue(() => SearchFor, value, () =>
                {
                    if (value == null) return;
                    var view = CollectionView == null
                        ? CollectionViewSource.GetDefaultView(EntityListItems)
                        : CollectionView.View;
                    if (view == null) return;

                    view.Filter = obj =>
                    {
                        if (obj == null) return false;
                        return obj.ToString().IndexOf(value, StringComparison.OrdinalIgnoreCase) > -1;
                    };
                });
            }
        }

        public string ViewName
        {
            get { return GetValue(() => ViewName); }
            set { SetValue(() => ViewName, value); }
        }

        public CultureInfo DateCultureInfo
        {
            get { return GetValue(() => DateCultureInfo); }
            set { SetValue(() => DateCultureInfo, value); }
        }

        public CultureInfo CurrencyCultureInfo
        {
            get { return GetValue(() => CurrencyCultureInfo); }
            set { SetValue(() => CurrencyCultureInfo, value); }
        }

        public RadWindow PartWindow
        {
            get { return GetValue(() => PartWindow); }
            set { SetValue(() => PartWindow, value); }
        }

        public CollectionViewSource PartCollectionView
        {
            get { return GetValue(() => PartCollectionView); }
            set { SetValue(() => PartCollectionView, value); }
        }

        public string SearchForPartNumber
        {
            get { return GetValue(() => SearchForPartNumber); }
            set
            {
                SetValue(() => SearchForPartNumber, value, () =>
                {
                    if (value == null) return;
                    var view = PartCollectionView == null
                        ? CollectionViewSource.GetDefaultView(Lookups["Parts"])
                        : PartCollectionView.View;
                    if (view == null) return;

                    view.Filter = obj =>
                    {
                        if (obj == null) return false;
                        return obj.As<PartLookup>().PartNumber.IndexOf(value, StringComparison.OrdinalIgnoreCase) > -1;
                    };
                });
            }
        }

        public string SearchForDescription
        {
            get { return GetValue(() => SearchForDescription); }
            set
            {
                SetValue(() => SearchForDescription, value, () =>
                {
                    if (value == null) return;
                    var view = PartCollectionView == null
                        ? CollectionViewSource.GetDefaultView(Lookups["Parts"])
                        : PartCollectionView.View;
                    if (view == null) return;

                    view.Filter = obj =>
                    {
                        if (obj == null) return false;
                        return obj.As<PartLookup>().Text.IndexOf(value, StringComparison.OrdinalIgnoreCase) > -1;
                    };
                });
            }
        }

        public PartLookup SelectedPart
        {
            get { return GetValue(() => SelectedPart); }
            set { SetValue(() => SelectedPart, value); }
        }

        protected virtual void SetView(string viewName = "")
        {
            ViewName = viewName;
        }

        public virtual void CreateFilters()
        {
            var view = new CollectionViewSource { Source = EntityListItems };
            CollectionView = view;

            var hasStatus = EntityListItems.OfType<IHaveStatus>();
            if (hasStatus.Any())
                FilterItems(SelectedFilter.Value);

            SelectedItem = null;
        }

        public KeyValuePair<string, string> SelectedFilter
        {
            get { return GetValue(() => SelectedFilter); }
            set
            {
                SetValue(() => SelectedFilter, value);
                FilterItems(value.Value);
                SelectedItem = null;
            }
        }

        private void FilterItems(string filterValue)
        {
            switch (filterValue)
            {
                case null:
                    return;

                case "ALL":
                    {
                        var view = new CollectionViewSource { Source = EntityListItems };
                        CollectionView = view;
                        break;
                    }
                default:
                    {
                        var filteredEntities = EntityListItems.Where(x => x.As<IHaveStatus>().Status == filterValue);
                        var view = new CollectionViewSource { Source = filteredEntities };
                        CollectionView = view;
                        break;
                    }
            }
        }

        protected async Task<OperationResponse> OpenItemExecute(string parameter, object view, string header)
        {
            var viewModel = view.As<FrameworkElement>().GetViewModel().As<ErpViewModelBase>();
            try
            {
                viewModel.IsBusy = true;
                viewModel.Id = parameter;

                CreatePane(view, header);

                var result = await viewModel.RunStartupMethodsAsync(parameter);
                var operationResponses = result as IList<OperationResponse> ?? result.ToList();

                if (operationResponses.Any(x => x.OperationResult == OperationResult.Failed))
                {
                    var errorResponse = operationResponses.First(x => x.OperationResult == OperationResult.Failed);
                    EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload(errorResponse.ErrorMessage));
                    return errorResponse;
                }

                return new OperationResponse(OperationResult.Success);
            }
            catch (Exception e)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload(e.Message));
                return new OperationResponse(OperationResult.Failed);
            }
            finally
            {
                viewModel.IsBusy = false;
            }
        }

        protected async Task<OperationResponse> OpenItemExecute(string parameter, bool runStartUp = false)
        {
            try
            {
                IsBusy = true;
                Id = parameter;

                var result = await RunStartupMethodsAsync(parameter, runStartUp);
                var operationResponses = result as IList<OperationResponse> ?? result.ToList();

                if (operationResponses.Any(x => x.OperationResult == OperationResult.Failed))
                {
                    var errorResponse = operationResponses.First(x => x.OperationResult == OperationResult.Failed);
                    EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload(errorResponse.ErrorMessage));
                    return errorResponse;
                }

                return new OperationResponse(OperationResult.Success);
            }
            catch (Exception e)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload(e.Message));
                return new OperationResponse(OperationResult.Failed);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void CreatePane(object view, string header)
        {
            var content = new ScrollViewer
            {
                Content = view,
                VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                HorizontalScrollBarVisibility = ScrollBarVisibility.Auto
            };

            var pane = new RadDocumentPane
            {
                Content = content,
                Header = header,
                Tag = Guid.NewGuid()
            };

            DocumentPanes.Add(pane);
        }

        public async Task<OperationResponse> GetRecordAsync(string methodName)
        {
            OperationResponse result = null;

            try
            {
                IsBusy = true;
                if (Methods.ContainsKey(methodName))
                {
                    var method = Methods[methodName].As<GetMethodCallBase>();
                    BusyContent = method.BusyContent;
                    await method.RunAsync(Entity).ContinueWith(task =>
                    {
                        if (task.Status == TaskStatus.RanToCompletion && task.Exception != null)
                        {
                            result = new OperationResponse(methodName, OperationResult.Failed);
                            throw new Exception(task.Exception.Message);
                        }

                        if (task.Status == TaskStatus.Faulted)
                        {
                            result = new OperationResponse(methodName, OperationResult.Failed, task.Exception?.Message);
                            throw new Exception(task.Exception?.Message);
                        }

                        result = new OperationResponse(methodName, OperationResult.Success);
                    });
                    if (method.WhenDone != null)
                        await Task.Run(() => method.WhenDone()).ContinueWith(task =>
                        {
                            var responses = new List<OperationResponse>();
                            AddOperationResponse(task, responses, string.Concat(method.MethodName, "-whenDone"));
                            result = responses.FirstOrDefault();
                        });
                }
            }
            catch (Exception e)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload(e.Message));
                return new OperationResponse(methodName, OperationResult.Failed, e.Message);
            }
            finally
            {
                IsBusy = false;
            }

            return result;
        }

        public async Task<IEnumerable<OperationResponse>> RunStartupMethodsAsync(string parameter, bool runStartUp = false)
        {
            var result = new List<OperationResponse>();
            try
            {
                IsBusy = true;

                RegisterLookups();
                await GetLookupsAsync();

                var startupMethods = Methods.Values
                    .OfType<IErpGetMethodCall>()
                    .Where(getMethodCall => getMethodCall.RunAtStartup)
                    .ToList();
                SetDefaultParameter(parameter);

                foreach (var method in startupMethods)
                {
                    if (!Entity.GetDto<EntityBase>().IsNew || runStartUp)
                    {
                        await method.RunAsync(Entity).ContinueWith(task =>
                        {
                            AddOperationResponse(task, result, method.MethodName);
                        });
                    }

                    if (result.Any(response => response.OperationResult == OperationResult.Failed)) break;

                    if (method.WhenDone != null)
                        await Task.Run(() => method.WhenDone()).ContinueWith(task =>
                        {
                            AddOperationResponse(task, result, string.Concat(method.MethodName, "-whenDone"));
                        });
                }

                return result;
            }
            catch (Exception e)
            {
                result.Add(new OperationResponse("", OperationResult.Failed, e.Message));
                return result;
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task<OperationResponse> SaveRecordAsync(string methodName)
        {
            OperationResponse result = null;
            try
            {
                IsBusy = true;
                if (Methods.ContainsKey(methodName))
                {
                    var method = Methods[methodName].As<ErpSaveMethodCallBase>();
                    BusyContent = method.BusyContent;
                    await method.SaveRecordAsync(Entity.GetDto<EntityBase>()).ContinueWith(async task =>
                    {
                        var errorMessage = "";

                        if (task.Exception != null)
                        {
                            if (task.Exception.GetBaseException() is FlurlHttpException flurlHttpException)
                            {
                                var statusCode = flurlHttpException.Call.Response.StatusCode;
                                if (statusCode == 403)
                                    errorMessage = "Access Restricted. Please Contact Administrator.";
                                else
                                {
                                    errorMessage = await flurlHttpException.GetResponseStringAsync();
                                    if (string.IsNullOrEmpty(errorMessage))
                                        errorMessage = task.Exception.InnerExceptions.Any()
                                            ? task.Exception.InnerExceptions.First().Message
                                            : task.Exception.Message;
                                }
                            }
                            else
                                errorMessage = task.Exception.Message;
                        }

                        result = task.Status == TaskStatus.RanToCompletion && task.Exception == null
                            ? new OperationResponse(methodName, OperationResult.Success)
                            : new OperationResponse(methodName, OperationResult.Failed, errorMessage);
                    });

                    if (result.OperationResult == OperationResult.Success)
                    {
                        if (method.ShowNotification)
                            DisplayNotification();
                    }
                    else
                    {
                        if (result.ErrorMessage != null && result.ErrorMessage.Contains("Duplicate"))
                        {
                            var message = "Duplicate Found in " + _category;
                            EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload(new DuplicateKeyException(message)));
                        }
                        else
                            EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload(result.ErrorMessage));
                    }
                }
            }
            catch (Exception e)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload(e.Message));
                return new OperationResponse(methodName, OperationResult.Failed, e.Message);
            }
            finally
            {
                IsBusy = false;
            }

            return result;
        }

        protected static async void AddOperationResponse(Task task, ICollection<OperationResponse> list, string methodName)
        {
            var methodResult = task.Status == TaskStatus.RanToCompletion
                ? OperationResult.Success
                : OperationResult.Failed;
            var errorMessage = "";

            if (task.Exception != null)
            {
                if (task.Exception.GetBaseException() is FlurlHttpException flurlHttpException)
                {
                    var statusCode = flurlHttpException.Call.Response.StatusCode;
                    if (statusCode == 403)
                        errorMessage = "Access Restricted. Please Contact Administrator.";
                    else
                    {
                        errorMessage = await flurlHttpException.GetResponseStringAsync();
                        if (string.IsNullOrEmpty(errorMessage))
                            errorMessage = task.Exception.InnerExceptions.Any()
                                ? task.Exception.InnerExceptions.First().Message
                                : task.Exception.Message;
                    }
                }
                else
                    errorMessage = task.Exception.InnerExceptions.Any()
                        ? task.Exception.InnerExceptions.First().Message
                        : task.Exception.Message;
            }

            list.Add(new OperationResponse(methodName, methodResult, errorMessage));
        }

        protected void ConfigureMethodCalls(ErpModules moduleName, string category)
        {
            var methods = Container.ResolveAll<IErpMethodCall>()
                .Where(methodCall => methodCall.ModuleName == moduleName)
                .Where(call => call.Category == category);
            methods.ForEach(methodCall => Methods.Add(methodCall.MethodName, methodCall));
        }

        protected async Task GetLookupsAsync()
        {
            var lookups = await LookupService.GetLookupsAsync();
            lookups.ForEach(lookupDictionary =>
            {
                var lookup = Lookups.FirstOrDefault(x => x.Key == lookupDictionary.Key);
                if (lookup.Key == null) return;
                lookup.Value.Clear();
                lookupDictionary.Value.ForEach(lookup.Value.Add);
            });
        }

        // protected async void RefreshLookup<T>(ApiControllers controller, string method, string lookup) where T : Lookups
        // {
        //     IsBusy = true;
        //     var lookupData = await LookupService.RefreshLookupAsync<T>(controller, method);
        //     Lookups[lookup] = lookupData[lookup];
        //     IsBusy = false;
        // }

        protected virtual void RefreshView()
        {
        }

        protected virtual void RegisterLookups()
        {
        }

        private void DisplayNotification()
        {
            _desktopNotificationService.NotifyUser(new SaveSuccessfulAlert(new NotificationInfo
            { ModuleName = _moduleName.ToString().ToUpper() }));
        }

        private async Task<Dictionary<string, IEnumerable<LookupEntity>>> RefreshLookups<T>(ApiControllers controller, string method) where T : Lookups
        {
            return await LookupService.RefreshLookupAsync<T>(controller, method);
        }

        private void SetDefaultParameter(string parameter)
        {
            Methods.Values.Where(x => x.UseDefaultParameter)
                .ForEach(x => x.MethodParameter = parameter);
        }

        protected void GetModuleReports()
        {
            _moduleReportDetails = ReportHelper.GetReports<IModuleReportDetail>(_moduleName.ToString());
        }

        protected void RunReport(string key)
        {
            var moduleReport = _moduleReportDetails[key];

            var reportData = moduleReport.ConfigureReportData(Entity.GetDto<EntityBase>(), Lookups);
            var reportParameters = moduleReport.GetReportParameters(Entity.GetDto<EntityBase>(), Lookups);

            var reportLoaderInfo = new ReportLoaderInfo(reportData, reportParameters, moduleReport.ReportTitle,
                moduleReport.ReportFileName, ModuleReportAssembly, ModuleReportFilePath);

            var reportViewer = _reportLoaderService.Load(reportLoaderInfo);
            var viewerWindow = ContainerLocator.Current.Resolve<ModuleReportViewerView>();
            if (viewerWindow == null) return;
            if (viewerWindow.DataContext is ReportViewerViewModelBase viewModel)
                viewModel.ViewReport(reportLoaderInfo, reportViewer, viewerWindow);

            viewerWindow.Show();
            var window = viewerWindow.ParentOfType<Window>();
            window.ShowInTaskbar = true;
        }

        protected byte[] BuildPartDocument(string key)
        {
            var moduleReport = _moduleReportDetails[key];
            var reportData = moduleReport.ConfigureReportData(Entity.GetDto<EntityBase>(), Lookups);
            var reportParameters = moduleReport.GetReportParameters(Entity.GetDto<EntityBase>(), Lookups);
            var reportLoaderInfo = new ReportLoaderInfo(reportData, reportParameters, moduleReport.ReportTitle,
                moduleReport.ReportFileName, ModuleReportAssembly, ModuleReportFilePath);
            var reportViewer = _reportLoaderService.Load(reportLoaderInfo);
            var bytes = reportViewer.LocalReport.Render("PDF");
            return bytes;
        }
    }
}