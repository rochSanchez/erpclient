﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Kemprise.Infrastructure.Converters
{
    public class ExpirationDateToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is DateTime dateTimeValue)) return false;
            return (DateTime?)dateTimeValue < DateTime.Now;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is DateTime dateTimeValue)) return false;
            return (DateTime?)dateTimeValue < DateTime.Now;
        }
    }
}