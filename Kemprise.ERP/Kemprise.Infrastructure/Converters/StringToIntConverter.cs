﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Kemprise.Infrastructure.Converters
{
    public class StringToIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var returnValue = System.Convert.ToInt32(value);
            return returnValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return Binding.DoNothing;
            var returnValue = System.Convert.ToInt32(value);
            return returnValue;
        }
    }
}