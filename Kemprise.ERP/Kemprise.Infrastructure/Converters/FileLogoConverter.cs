﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Kemprise.Infrastructure.Converters
{
    public class FileLogoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value == DependencyProperty.UnsetValue)
                return DefaultGeometry();
            if (value.ToString() == ".jpg" || value.ToString() == ".jpeg")
                return JpegGeometry();
            if (value.ToString() == ".pdf")
                return PdfGeometry();
            if (value.ToString() == ".docx" || value.ToString() == ".doc")
                return WordGeometry();
            return value.ToString() == ".xlsx" || value.ToString() == ".xls" ? ExcelGeometry() : DefaultGeometry();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        private static Geometry DefaultGeometry()
        {
            return Geometry.Parse("M14,2H6A2,2 0 0,0 4,4V20A2,2 0 0,0 6,22H18A2,2 0 0,0 20,20V8L14,2M18,20H6V4H13V9H18V20Z");
        }

        private static Geometry JpegGeometry()
        {
            return Geometry.Parse("M19,19H5V5H19M19,3H5A2,2 0 0,0 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5A2,2 0 0,0 19,3M13.96," +
                                  "12.29L11.21,15.83L9.25,13.47L6.5,17H17.5L13.96,12.29Z");
        }

        private static Geometry PdfGeometry()
        {
            return Geometry.Parse("M12,10.5H13V13.5H12V10.5M7,11.5H8V10.5H7V11.5M20,6V18A2,2 0 0,1 18,20H6A2,2 0 0,1 4,18V6A2,2 0 0,1 6," +
                                  "4H18A2,2 0 0,1 20,6M9.5,10.5A1.5,1.5 0 0,0 8,9H5.5V15H7V13H8A1.5,1.5 0 0,0 9.5,11.5V10.5M14.5,10.5A1.5,1.5" +
                                  " 0 0,0 13,9H10.5V15H13A1.5,1.5 0 0,0 14.5,13.5V10.5M18.5,9H15.5V15H17V13H18.5V11.5H17V10.5H18.5V9Z");
        }

        private static Geometry WordGeometry()
        {
            return Geometry.Parse("M5 3C3.89 3 3 3.89 3 5V19C3 20.11 3.89 21 5 21H19C20.11 21 21 20.11 21 19V5C21 3.89 20.11 3 19 3H5M5 5H19V19H5V5M17.9 " +
                                  "7L15.5 17H14L12 9.5L10 17H8.5L6.1 7H7.8L9.34 14.5L11.3 7H12.7L14.67 14.5L16.2 7H17.9Z");
        }

        private static Geometry ExcelGeometry()
        {
            return Geometry.Parse("M5 3C3.89 3 3 3.89 3 5V19C3 20.11 3.89 21 5 21H19C20.11 21 21 20.11 21 19V5C21 3.89 20.11 3 19 3H5M5 5H19V19H5V5M13 12L16.2 " +
                                  "17H14.2L12 13.2L9.8 17H7.8L11 12L7.8 7H9.8L12 10.8L14.2 7H16.2L13 12Z");
        }
    }
}