﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Kemprise.Infrastructure.Converters
{
    public class ExceededInvoiceConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue) return false;
            if (values[1] == DependencyProperty.UnsetValue) return false;

            if (values[0] == null) return false;
            if (values[1] == null) return false;

            var pending = System.Convert.ToDecimal(values[0]);
            var quantity = System.Convert.ToDecimal(values[1]);

            return quantity > pending;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new[] { Binding.DoNothing };
        }
    }
}