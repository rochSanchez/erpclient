﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Kemprise.Infrastructure.Converters
{
    public class PartIsReadOnlyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return false;
            var partId = System.Convert.ToInt32(value);
            return partId != 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return false;
            var partId = System.Convert.ToInt32(value);
            return partId != 0;
        }
    }
}