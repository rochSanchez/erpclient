﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.Infrastructure.Tools;
using Prism.Ioc;

namespace Kemprise.Infrastructure.Converters
{
    public class UserPhotoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value == null) return new BitmapImage(new Uri(@"/Kemprise.Client;component/Images/DefaultUserImage.png", UriKind.Relative));
            var fileService =  ContainerLocator.Current.Resolve<IFileService>();
            return fileService.ByteArrayToImage(value as byte[]);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}