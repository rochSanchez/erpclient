﻿using ERP.WebApi.DTO;
using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.ViewModel;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Unity;

namespace Kemprise.Infrastructure.WorkArea
{
    public class ErpWorkAreaViewModelBase : NotifyPropertyChangedBase
    {
        protected readonly IEventAggregator EventAggregator;
        protected readonly IUnityContainer Container;

        protected Dictionary<string, IEnumerable<EntityListItem>> ItemsListDictionary;

        public ErpWorkAreaViewModelBase(IUnityContainer container, IEventAggregator eventAggregator)
        {
            Container = container;
            EventAggregator = eventAggregator;
        }

        public IEnumerable<INavigationItem> NavigationItems
        {
            get { return GetValue(() => NavigationItems); }
            set { SetValue(() => NavigationItems, value); }
        }

        public INavigationItem SelectedNavItem
        {
            get { return GetValue(() => SelectedNavItem); }
            set
            {
                SetValue(() => SelectedNavItem, value, () =>
                {
                    if (value?.View == null) return;
                    var viewModel = value.View.As<FrameworkElement>().GetViewModel();
                    RunStartup(viewModel, value.Name);
                });
            }
        }

        private async void RunStartup(object viewModel, string typeName)
        {
            var viewModelBase = viewModel.As<ErpViewModelBase>();

            try
            {
                viewModelBase.IsBusy = true;
                IEnumerable<EntityListItem> entityListItems;

                if (ItemsListDictionary.ContainsKey(typeName))
                {
                    entityListItems = ItemsListDictionary[typeName].ToList();
                }
                else
                {
                    var result = await GetItemListAsync();
                    entityListItems = result as IList<EntityListItem> ?? result.ToList();
                    ItemsListDictionary.Add(typeName, entityListItems);
                }

                viewModelBase.EntityListItems = entityListItems;
                viewModelBase.CreateFilters();
            }
            catch (Exception e)
            {
                EventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload($"{e.Message}"));
            }
            finally
            {
                viewModelBase.IsBusy = false;
            }
        }

        protected async void OnRefreshItemList(RefreshItemListEventPayload obj)
        {
            var viewModelBase = obj.ViewModel.As<ErpViewModelBase>();

            try
            {
                viewModelBase.IsBusy = true;
                var result = await GetItemListAsync();
                var entityListItems = result as IList<EntityListItem> ?? result.ToList();
                viewModelBase.EntityListItems = entityListItems;
                viewModelBase.CreateFilters();

                if (ItemsListDictionary.ContainsKey(obj.TypeName))
                    ItemsListDictionary[obj.TypeName] = entityListItems;
                else
                    ItemsListDictionary.Add(obj.TypeName, entityListItems);
            }
            catch (Exception e)
            {
                EventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload($"{e.Message}"));
            }
            finally
            {
                viewModelBase.IsBusy = false;
            }
        }

        private Task<IEnumerable<EntityListItem>> GetItemListAsync()
        {
            return SelectedNavItem?.GetRecordsAsync();
        }
    }
}