﻿using System.Collections;
using System.Collections.Generic;

namespace Kemprise.Infrastructure.Reporting
{
    public class ReportParameters : IEnumerable<ReportDetailParameter>
    {
        private readonly List<ReportDetailParameter> _parameters;

        public ReportParameters()
        {
            _parameters = new List<ReportDetailParameter>();
        }

        public static IEnumerable<ReportDetailParameter> Empty => new List<ReportDetailParameter>();

        public IEnumerator<ReportDetailParameter> GetEnumerator()
        {
            return _parameters.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public ReportParameters Add(string name, object value)
        {
            _parameters.Add(new ReportDetailParameter(name, value));
            return this;
        }
    }
}