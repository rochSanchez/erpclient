﻿using System.Data;

namespace Kemprise.Infrastructure.Reporting
{
    public class ReportLoaderInfo
    {
        public ReportLoaderInfo(DataSet data, ReportParameters parameters, string title, string reportFileName, string assembly, string reportFilePath)
        {
            Data = data;
            Parameters = parameters;
            Title = title;
            ReportFileName = reportFileName;
            Assembly = assembly;
            ReportFilePath = reportFilePath;
        }

        public string ReportFilePath { get; }
        public DataSet Data { get; }

        public ReportParameters Parameters { get; }
        public string Title { get; }
        public string Assembly { get; }
        public string ReportFileName { get; }
    }
}