﻿using System.IO;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Reporting.ReportViewerView;
using Microsoft.Reporting.WinForms;
using Prism.Events;
using Prism.Ioc;

namespace Kemprise.Infrastructure.Reporting
{
    public class ReportViewerViewModelBase : NotifyPropertyChangedBase
    {
        private readonly IEventAggregator _eventAggregator;
        private ReportLoaderInfo _info;
        private ModuleReportViewerView _view;

        public ReportViewerViewModelBase()
        {
            _eventAggregator = ContainerLocator.Current.Resolve<IEventAggregator>();
        }

        public Stream Source
        {
            get { return GetValue(() => Source); }
            set { SetValue(() => Source, value); }
        }

        public bool IsBusy
        {
            get { return GetValue(() => IsBusy); }
            set { SetValue(() => IsBusy, value); }
        }

        public string BusyContent
        {
            get { return GetValue(() => BusyContent); }
            set { SetValue(() => BusyContent, value); }
        }

        public string ReportTitle
        {
            get { return GetValue(() => ReportTitle); }
            set { SetValue(() => ReportTitle, value); }
        }

        public void ViewReport(ReportLoaderInfo info, ReportViewer viewer, ModuleReportViewerView viewerWindow)
        {
            _info = info;
            _view = viewerWindow;
            ReportTitle = info.Title;
            _view.SetViewer(viewer);
            viewer.SetDisplayMode(DisplayMode.PrintLayout);
            viewer.RefreshReport();
        }
    }
}