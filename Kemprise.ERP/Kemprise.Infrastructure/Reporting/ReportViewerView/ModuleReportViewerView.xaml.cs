﻿using System.Windows.Forms;
using Telerik.Windows.Controls.Navigation;
using Application = System.Windows.Application;

namespace Kemprise.Infrastructure.Reporting.ReportViewerView
{
    /// <summary>
    ///     Interaction logic for ModuleReportViewerView.xaml
    /// </summary>
    public partial class ModuleReportViewerView
    {
        private object _viewer;

        public ModuleReportViewerView()
        {
            InitializeComponent();
            RadWindowInteropHelper.SetAllowTransparency(this, false);
            Owner = Application.Current.MainWindow;
        }

        public void SetViewer(object reportViewer)
        {
            _viewer = reportViewer;
            WindowsFormsHost.Child = (Control)_viewer;
        }
    }
}