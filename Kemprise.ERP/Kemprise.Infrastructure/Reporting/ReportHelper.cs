﻿using Prism.Ioc;
using System.Collections.Generic;
using System.Linq;

namespace Kemprise.Infrastructure.Reporting
{
    public static class ReportHelper
    {
        public static Dictionary<string, T> GetReports<T>(string moduleName) where T : IModuleReportDetail
        {
            var reportDetails = ContainerLocator.Current.Resolve<IEnumerable<T>>().Where(x => x.ModuleName == moduleName).ToList();
            return reportDetails.Any() ? reportDetails.ToDictionary(reportDetail => reportDetail.GetType().Name) : new Dictionary<string, T>();
        }
    }
}