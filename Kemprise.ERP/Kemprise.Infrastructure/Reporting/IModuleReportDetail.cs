﻿using ERP.WebApi.DTO.Lookups;
using Kemprise.Infrastructure.MethodCalls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace Kemprise.Infrastructure.Reporting
{
    public interface IModuleReportDetail
    {
        string ModuleName { get; }

        string ReportTitle { get; }

        string ReportFileName { get; }

        DataSet ConfigureReportData(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups);

        ReportParameters GetReportParameters(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups);
    }
}