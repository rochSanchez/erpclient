﻿using System.Data;

namespace Kemprise.Infrastructure.Reporting
{
    public interface IReportLoaderInfo
    {
        string Assembly { get; }
        string ReportFilePath { get; }
        DataSet Data { get; }

        ReportParameters Parameters { get; }
        string Title { get; }
        string ReportFileName { get; }
    }
}