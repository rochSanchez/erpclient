﻿namespace Kemprise.Infrastructure.Reporting
{
    public class ReportDetailParameter
    {
        public ReportDetailParameter(string name, object value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; }
        public object Value { get; }
    }
}