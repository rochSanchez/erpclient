﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Kemprise.Infrastructure.Events.Errors;
using Microsoft.Reporting.WinForms;
using Prism.Events;
using Prism.Ioc;

namespace Kemprise.Infrastructure.Reporting.Service
{
    public interface IReportLoaderService
    {
        ReportViewer Load(ReportLoaderInfo info);
    }

    public class ReportLoaderService : IReportLoaderService
    {
        private readonly IEventAggregator _eventAggregator;

        public ReportLoaderService()
        {
            _eventAggregator = ContainerLocator.Current.Resolve<IEventAggregator>();
        }

        public ReportViewer Load(ReportLoaderInfo info)
        {
            var viewer = new ReportViewer();
            try
            {
                viewer.Reset();
                SetReport(viewer, info.ReportFileName, info.ReportFilePath, info.Assembly);
                SetParameters(viewer, info.Parameters);
                SetData(viewer, info.Data);
            }
            catch (Exception ex)
            {
                _eventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload(ex.Message));
                return null;
            }

            return viewer;
        }

        private void SetParameters(ReportViewer viewer, ReportParameters reportParameters)
        {
            var localReport = viewer.LocalReport;
            var parameters = reportParameters.Select(p => new ReportParameter(p.Name, p.Value.ToString())).ToList();
            if (parameters.Count > 0) localReport.SetParameters(parameters);
        }

        public void SetReport(ReportViewer viewer, string fileName, string filePath, string assembly)
        {
            var resourcePath = $"{assembly}.Reports.ReportFiles.{fileName}.rdlc";
            var stream = Assembly.Load(assembly).GetManifestResourceStream(resourcePath);
            viewer.LocalReport.LoadReportDefinition(stream);
        }

        private void SetData(ReportViewer viewer, DataSet dataSet)
        {
            var dataSources = new List<ReportDataSource>();
            foreach (DataTable dataSetTable in dataSet.Tables)
            {
                var dataSource = new ReportDataSource(dataSetTable.TableName) { Value = dataSetTable };
                viewer.LocalReport.DataSources.Add(dataSource);
            }
        }
    }
}