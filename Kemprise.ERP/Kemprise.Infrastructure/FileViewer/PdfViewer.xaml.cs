﻿using System;
using System.IO;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using Telerik.Windows.Documents.Fixed.FormatProviders;
using Telerik.Windows.Documents.Fixed.FormatProviders.Pdf;

namespace Kemprise.Infrastructure.FileViewer
{
    /// <summary>
    /// Interaction logic for PdfViewer.xaml
    /// </summary>
    public partial class PdfViewer 
    {
        public PdfViewer()
        {
            InitializeComponent();
        }

        public void SetDocument(DocumentDto document)
        {
            var path = Path.GetTempPath() + $"{document.Name}-{document.DocumentNumber}.pdf";
            File.WriteAllBytes(path, document.FileData);
            var stream = new MemoryStream();
            using (var input = File.OpenRead(path))
                input.CopyTo(stream);
            var settings = new FormatProviderSettings(ReadingMode.OnDemand);
            var provider = new PdfFormatProvider(stream, settings);
            var doc = provider.Import();
            RadPdfViewer.Document = doc;
        }

        public void SetDrawing(DrawingDto drawing)
        {
            var path = Path.GetTempPath() + $"{drawing.Name}-{drawing.DrawingNumber}.pdf";
            File.WriteAllBytes(path, drawing.FileData);
            var stream = new MemoryStream();
            using (var input = File.OpenRead(path))
                input.CopyTo(stream);
            var settings = new FormatProviderSettings(ReadingMode.OnDemand);
            var provider = new PdfFormatProvider(stream, settings);
            var doc = provider.Import();
            RadPdfViewer.Document = doc;
        }

        public void BuildPartDocumentPdf(byte[] part1, byte[] part2 = null)
        {
            try
            {
                var outputDocument = new PdfDocument();
                var detailStream = new MemoryStream(part1);
                var detailsDocument = PdfReader.Open(detailStream, PdfDocumentOpenMode.Import);
                foreach (var page in detailsDocument.Pages) outputDocument.AddPage(page);
                if (part2 != null)
                {
                    var drawingStream = new MemoryStream(part2);
                    var drawingDocument = PdfReader.Open(drawingStream, PdfDocumentOpenMode.Import);
                    foreach (var page in drawingDocument.Pages) outputDocument.AddPage(page);
                }

                var document = outputDocument;
                var combinedStream = new MemoryStream();
                document.Save(combinedStream, true);
                var data = combinedStream.ToArray();

                var path = Path.GetTempPath() + $"{"test"}-{"test"}.pdf";
                File.WriteAllBytes(path, data);
                var stream = new MemoryStream();
                using (var input = File.OpenRead(path)) input.CopyTo(stream);

                var settings = new FormatProviderSettings(ReadingMode.OnDemand);
                var provider = new PdfFormatProvider(stream, settings);
                var doc = provider.Import();
                RadPdfViewer.Document = doc;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
