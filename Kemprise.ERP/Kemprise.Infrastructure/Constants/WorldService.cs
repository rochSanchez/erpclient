﻿using Kemprise.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Kemprise.Infrastructure.Constants
{
    public interface IWorldService
    {
        IReadOnlyList<WorldCurrency> GetAllCurrencies();

        IReadOnlyList<string> GetAllCountries();
    }

    public class WorldService : IWorldService
    {
        public IReadOnlyList<WorldCurrency> GetAllCurrencies()
        {
            return _currencies.OrderBy(x => x.Name).ToList();
        }

        public IReadOnlyList<string> GetAllCountries()
        {
            var countriesList = new List<string>();
            var cultureInfo = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

            cultureInfo.ForEach(culture =>
            {
                var regionInfo = new RegionInfo(culture.LCID);
                if (!countriesList.Contains(regionInfo.EnglishName))
                    countriesList.Add(regionInfo.EnglishName);
            });

            countriesList.Sort();
            return countriesList;
        }

        private readonly List<WorldCurrency> _currencies = new List<WorldCurrency>
        {
            new WorldCurrency {  Code = "AED", Name = "United Arab Emirates Dirham" },
            new WorldCurrency {  Code = "AFN", Name = "Afghanistan Afghani" },
            new WorldCurrency {  Code = "ALL", Name = "Albania Lek" },
            new WorldCurrency {  Code = "AMD", Name = "Armenia Dram" },
            new WorldCurrency {  Code = "ANG", Name = "Netherlands Antilles Guilder" },
            new WorldCurrency {  Code = "AOA", Name = "Angola Kwanza" },
            new WorldCurrency {  Code = "ARS", Name = "Argentina Peso" },
            new WorldCurrency {  Code = "AUD", Name = "Australia Dollar" },
            new WorldCurrency {  Code = "AWG", Name = "Aruba Guilder" },
            new WorldCurrency {  Code = "AZN", Name = "Azerbaijan New Manat" },
            new WorldCurrency {  Code = "BAM", Name = "Bosnia and Herzegovina Convertible Marka" },
            new WorldCurrency {  Code = "BBD", Name = "Barbados Dollar" },
            new WorldCurrency {  Code = "BDT", Name = "Bangladesh Taka" },
            new WorldCurrency {  Code = "BGN", Name = "Bulgaria Lev" },
            new WorldCurrency {  Code = "BHD", Name = "Bahrain Dinar" },
            new WorldCurrency {  Code = "BIF", Name = "Burundi Franc" },
            new WorldCurrency {  Code = "BMD", Name = "Bermuda Dollar" },
            new WorldCurrency {  Code = "BND", Name = "Brunei Darussalam Dollar" },
            new WorldCurrency {  Code = "BOB", Name = "Bolivia Boliviano" },
            new WorldCurrency {  Code = "BRL", Name = "Brazil Real" },
            new WorldCurrency {  Code = "BSD", Name = "Bahamas Dollar" },
            new WorldCurrency {  Code = "BTN", Name = "Bhutan Ngultrum" },
            new WorldCurrency {  Code = "BWP", Name = "Botswana Pula" },
            new WorldCurrency {  Code = "BYR", Name = "Belarus Ruble" },
            new WorldCurrency {  Code = "BZD", Name = "Belize Dollar" },
            new WorldCurrency {  Code = "CAD", Name = "Canada Dollar" },
            new WorldCurrency {  Code = "CDF", Name = "Congo/Kinshasa Franc" },
            new WorldCurrency {  Code = "CHF", Name = "Switzerland Franc" },
            new WorldCurrency {  Code = "CLP", Name = "Chile Peso" },
            new WorldCurrency {  Code = "CNY", Name = "China Yuan Renminbi" },
            new WorldCurrency {  Code = "COP", Name = "Colombia Peso" },
            new WorldCurrency {  Code = "CRC", Name = "Costa Rica Colon" },
            new WorldCurrency {  Code = "CUC", Name = "Cuba Convertible Peso" },
            new WorldCurrency {  Code = "CUP", Name = "Cuba Peso" },
            new WorldCurrency {  Code = "CVE", Name = "Cape Verde Escudo" },
            new WorldCurrency {  Code = "CZK", Name = "Czech Republic Koruna" },
            new WorldCurrency {  Code = "DJF", Name = "Djibouti Franc" },
            new WorldCurrency {  Code = "DKK", Name = "Denmark Krone" },
            new WorldCurrency {  Code = "DOP", Name = "Dominican Republic Peso" },
            new WorldCurrency {  Code = "DZD", Name = "Algeria Dinar" },
            new WorldCurrency {  Code = "EGP", Name = "Egypt Pound" },
            new WorldCurrency {  Code = "ERN", Name = "Eritrea Nakfa" },
            new WorldCurrency {  Code = "ETB", Name = "Ethiopia Birr" },
            new WorldCurrency {  Code = "EUR", Name = "Euro Member Countries" },
            new WorldCurrency {  Code = "FJD", Name = "Fiji Dollar" },
            new WorldCurrency {  Code = "FKP", Name = "Falkland Islands (Malvinas) Pound" },
            new WorldCurrency {  Code = "GBP", Name = "United Kingdom Pound" },
            new WorldCurrency {  Code = "GEL", Name = "Georgia Lari" },
            new WorldCurrency {  Code = "GGP", Name = "Guernsey Pound" },
            new WorldCurrency {  Code = "GHS", Name = "Ghana Cedi" },
            new WorldCurrency {  Code = "GIP", Name = "Gibraltar Pound" },
            new WorldCurrency {  Code = "GMD", Name = "Gambia Dalasi" },
            new WorldCurrency {  Code = "GNF", Name = "Guinea Franc" },
            new WorldCurrency {  Code = "GTQ", Name = "Guatemala Quetzal" },
            new WorldCurrency {  Code = "GYD", Name = "Guyana Dollar" },
            new WorldCurrency {  Code = "HKD", Name = "Hong Kong Dollar" },
            new WorldCurrency {  Code = "HNL", Name = "Honduras Lempira" },
            new WorldCurrency {  Code = "HRK", Name = "Croatia Kuna" },
            new WorldCurrency {  Code = "HTG", Name = "Haiti Gourde" },
            new WorldCurrency {  Code = "HUF", Name = "Hungary Forint" },
            new WorldCurrency {  Code = "IDR", Name = "Indonesia Rupiah" },
            new WorldCurrency {  Code = "ILS", Name = "Israel Shekel" },
            new WorldCurrency {  Code = "IMP", Name = "Isle of Man Pound" },
            new WorldCurrency {  Code = "INR", Name = "India Rupee" },
            new WorldCurrency {  Code = "IQD", Name = "Iraq Dinar" },
            new WorldCurrency {  Code = "IRR", Name = "Iran Rial" },
            new WorldCurrency {  Code = "ISK", Name = "Iceland Krona" },
            new WorldCurrency {  Code = "JEP", Name = "Jersey Pound" },
            new WorldCurrency {  Code = "JMD", Name = "Jamaica Dollar" },
            new WorldCurrency {  Code = "JOD", Name = "Jordan Dinar" },
            new WorldCurrency {  Code = "JPY", Name = "Japan Yen" },
            new WorldCurrency {  Code = "KES", Name = "Kenya Shilling" },
            new WorldCurrency {  Code = "KGS", Name = "Kyrgyzstan Som" },
            new WorldCurrency {  Code = "KHR", Name = "Cambodia Riel" },
            new WorldCurrency {  Code = "KMF", Name = "Comoros Franc" },
            new WorldCurrency {  Code = "KPW", Name = "Korea (North) Won" },
            new WorldCurrency {  Code = "KRW", Name = "Korea (South) Won" },
            new WorldCurrency {  Code = "KWD", Name = "Kuwait Dinar" },
            new WorldCurrency {  Code = "KYD", Name = "Cayman Islands Dollar" },
            new WorldCurrency {  Code = "KZT", Name = "Kazakhstan Tenge" },
            new WorldCurrency {  Code = "LAK", Name = "Laos Kip" },
            new WorldCurrency {  Code = "LBP", Name = "Lebanon Pound" },
            new WorldCurrency {  Code = "LKR", Name = "Sri Lanka Rupee" },
            new WorldCurrency {  Code = "LRD", Name = "Liberia Dollar" },
            new WorldCurrency {  Code = "LSL", Name = "Lesotho Loti" },
            new WorldCurrency {  Code = "LYD", Name = "Libya Dinar" },
            new WorldCurrency {  Code = "MAD", Name = "Morocco Dirham" },
            new WorldCurrency {  Code = "MDL", Name = "Moldova Leu" },
            new WorldCurrency {  Code = "MGA", Name = "Madagascar Ariary" },
            new WorldCurrency {  Code = "MKD", Name = "Macedonia Denar" },
            new WorldCurrency {  Code = "MMK", Name = "Myanmar (Burma) Kyat" },
            new WorldCurrency {  Code = "MNT", Name = "Mongolia Tughrik" },
            new WorldCurrency {  Code = "MOP", Name = "Macau Pataca" },
            new WorldCurrency {  Code = "MRO", Name = "Mauritania Ouguiya" },
            new WorldCurrency {  Code = "MUR", Name = "Mauritius Rupee" },
            new WorldCurrency {  Code = "MVR", Name = "Maldives (Maldive Islands) Rufiyaa" },
            new WorldCurrency {  Code = "MWK", Name = "Malawi Kwacha" },
            new WorldCurrency {  Code = "MXN", Name = "Mexico Peso" },
            new WorldCurrency {  Code = "MYR", Name = "Malaysia Ringgit" },
            new WorldCurrency {  Code = "MZN", Name = "Mozambique Metical" },
            new WorldCurrency {  Code = "NAD", Name = "Namibia Dollar" },
            new WorldCurrency {  Code = "NGN", Name = "Nigeria Naira" },
            new WorldCurrency {  Code = "NIO", Name = "Nicaragua Cordoba" },
            new WorldCurrency {  Code = "NOK", Name = "Norway Krone" },
            new WorldCurrency {  Code = "NPR", Name = "Nepal Rupee" },
            new WorldCurrency {  Code = "NZD", Name = "New Zealand Dollar" },
            new WorldCurrency {  Code = "OMR", Name = "Oman Rial" },
            new WorldCurrency {  Code = "PAB", Name = "Panama Balboa" },
            new WorldCurrency {  Code = "PEN", Name = "Peru Nuevo Sol" },
            new WorldCurrency {  Code = "PGK", Name = "Papua New Guinea Kina" },
            new WorldCurrency {  Code = "PHP", Name = "Philippines Peso" },
            new WorldCurrency {  Code = "PKR", Name = "Pakistan Rupee" },
            new WorldCurrency {  Code = "PLN", Name = "Poland Zloty" },
            new WorldCurrency {  Code = "PYG", Name = "Paraguay Guarani" },
            new WorldCurrency {  Code = "QAR", Name = "Qatar Riyal" },
            new WorldCurrency {  Code = "RON", Name = "Romania New Leu" },
            new WorldCurrency {  Code = "RSD", Name = "Serbia Dinar" },
            new WorldCurrency {  Code = "RUB", Name = "Russia Ruble" },
            new WorldCurrency {  Code = "RWF", Name = "Rwanda Franc" },
            new WorldCurrency {  Code = "SAR", Name = "Saudi Arabia Riyal" },
            new WorldCurrency {  Code = "SBD", Name = "Solomon Islands Dollar" },
            new WorldCurrency {  Code = "SCR", Name = "Seychelles Rupee" },
            new WorldCurrency {  Code = "SDG", Name = "Sudan Pound" },
            new WorldCurrency {  Code = "SEK", Name = "Sweden Krona" },
            new WorldCurrency {  Code = "SGD", Name = "Singapore Dollar" },
            new WorldCurrency {  Code = "SHP", Name = "Saint Helena Pound" },
            new WorldCurrency {  Code = "SLL", Name = "Sierra Leone Leone" },
            new WorldCurrency {  Code = "SOS", Name = "Somalia Shilling" },
            new WorldCurrency {  Code = "SPL", Name = "Seborga Luigino" },
            new WorldCurrency {  Code = "SRD", Name = "Suriname Dollar" },
            new WorldCurrency {  Code = "STD", Name = "São Tomé and Príncipe Dobra" },
            new WorldCurrency {  Code = "SVC", Name = "El Salvador Colon" },
            new WorldCurrency {  Code = "SYP", Name = "Syria Pound" },
            new WorldCurrency {  Code = "SZL", Name = "Swaziland Lilangeni" },
            new WorldCurrency {  Code = "THB", Name = "Thailand Baht" },
            new WorldCurrency {  Code = "TJS", Name = "Tajikistan Somoni" },
            new WorldCurrency {  Code = "TMT", Name = "Turkmenistan Manat" },
            new WorldCurrency {  Code = "TND", Name = "Tunisia Dinar" },
            new WorldCurrency {  Code = "TOP", Name = "Tonga Pa'anga" },
            new WorldCurrency {  Code = "TRY", Name = "Turkey Lira" },
            new WorldCurrency {  Code = "TTD", Name = "Trinidad and Tobago Dollar" },
            new WorldCurrency {  Code = "TVD", Name = "Tuvalu Dollar" },
            new WorldCurrency {  Code = "TWD", Name = "Taiwan New Dollar" },
            new WorldCurrency {  Code = "TZS", Name = "Tanzania Shilling" },
            new WorldCurrency {  Code = "UAH", Name = "Ukraine Hryvnia" },
            new WorldCurrency {  Code = "UGX", Name = "Uganda Shilling" },
            new WorldCurrency {  Code = "USD", Name = "United States Dollar" },
            new WorldCurrency {  Code = "UYU", Name = "Uruguay Peso" },
            new WorldCurrency {  Code = "UZS", Name = "Uzbekistan Som" },
            new WorldCurrency {  Code = "VEF", Name = "Venezuela Bolivar" },
            new WorldCurrency {  Code = "VND", Name = "Viet Nam Dong" },
            new WorldCurrency {  Code = "VUV", Name = "Vanuatu Vatu" },
            new WorldCurrency {  Code = "WST", Name = "Samoa Tala" },
            new WorldCurrency {  Code = "XAF", Name = "Communauté Financière Africaine (BEAC) CFA Franc BEAC" },
            new WorldCurrency {  Code = "XCD", Name = "East Caribbean Dollar" },
            new WorldCurrency {  Code = "XDR", Name = "International Monetary Fund (IMF) Special Drawing Rights" },
            new WorldCurrency {  Code = "XOF", Name = "Communauté Financière Africaine (BCEAO) Franc" },
            new WorldCurrency {  Code = "XPF", Name = "Comptoirs Français du Pacifique (CFP) Franc" },
            new WorldCurrency {  Code = "YER", Name = "Yemen Rial" },
            new WorldCurrency {  Code = "ZAR", Name = "South Africa Rand" },
            new WorldCurrency {  Code = "ZMW", Name = "Zambia Kwacha" },
            new WorldCurrency {  Code = "ZWD", Name = "Zimbabwe Dollar" }
        };
    }

    public class WorldCurrency
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }
}