﻿using System;
using System.Threading.Tasks;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.Infrastructure.MethodCalls
{
    public interface IErpGetMethodCall : IErpMethodCall
    {
        Action WhenDone { get; set; }

        bool RunAtStartup { get; }

        Task RunAsync(DynamicNotifyPropertyChangedProxy entity);
    }
}