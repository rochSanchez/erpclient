﻿using System.Threading.Tasks;

namespace Kemprise.Infrastructure.MethodCalls
{
    public interface IErpSaveMethodCall : IErpMethodCall
    {
        Task SaveRecordAsync(EntityBase entity);
    }
}