﻿namespace Kemprise.Infrastructure.MethodCalls
{
    public class OperationResponse
    {
        public OperationResponse(OperationResult operationResult)
        {
            OperationResult = operationResult;
        }

        public OperationResponse(string methodName, OperationResult operationResult, string errorMessage = "")
        {
            MethodName = methodName;
            OperationResult = operationResult;
            ErrorMessage = errorMessage;
        }

        public string MethodName { get; }
        public OperationResult OperationResult { get; }
        public string ErrorMessage { get; }
    }
}