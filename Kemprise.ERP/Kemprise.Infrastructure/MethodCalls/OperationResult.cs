﻿namespace Kemprise.Infrastructure.MethodCalls
{
    public enum OperationResult
    {
        Success,
        Failed
    }
}