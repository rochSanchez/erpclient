﻿using ERP.WebApi.DTO.Lookups;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.Services.APICommon;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using ERP.WebApi.DTO.Lookups.LookupItems;

namespace Kemprise.Infrastructure.Services.Lookup
{
    public class LookupService : ILookupService
    {
        public static Dictionary<string, IEnumerable<LookupEntity>> LookupRepository =
            new Dictionary<string, IEnumerable<LookupEntity>>();

        private readonly IApiRequestManager _requestManager;
        private readonly Dictionary<string, Func<Task<Lookups>>> _setLookup;

        public LookupService(IApiRequestManager requestManager)
        {
            _requestManager = requestManager;
            _setLookup = new Dictionary<string, Func<Task<Lookups>>>();
        }

        public bool HasError { get; private set; }

        public void RegisterLookup<T>(ApiControllers apiController, string method) where T : Lookups
        {
            if (_setLookup.All(x => x.Key != method))
                _setLookup.Add(method, async () => await _requestManager.GetLookups<T>(apiController, method));
        }

        public async Task<Dictionary<string, IEnumerable<LookupEntity>>> RefreshLookupAsync<T>(
            ApiControllers apiController, string method) where T : Lookups
        {
            var lookups = new Dictionary<string, IEnumerable<LookupEntity>>();
            var result = await _requestManager.GetLookups<T>(apiController, method);

            var propertyInfos = result.GetType().GetProperties();
            propertyInfos.ForEach(info =>
                lookups.Add(info.Name, info.GetValue(result, null).As<IEnumerable<LookupEntity>>()));

            lookups.ForEach(lookup =>
            {
                var data = lookup.Value.OrderBy(item => item.Text).ToList();

                if (LookupRepository.ContainsKey(lookup.Key))
                    LookupRepository[lookup.Key] = data;
                else
                    LookupRepository.Add(lookup.Key, data);
            });

            return LookupRepository;
        }

        public async Task<Dictionary<string, IEnumerable<LookupEntity>>> GetLookupsAsync()
        {
            var lookups = new Dictionary<string, IEnumerable<LookupEntity>>();

            var lookupActions = new List<Func<Task<Lookups>>>();
            _setLookup.ForEach(x => lookupActions.Add(x.Value));

            foreach (var action in lookupActions)
            {
                try
                {
                    var result = await action.Invoke();
                    if (result == null) continue;

                    var propertyInfos = result.GetType().GetProperties();
                    propertyInfos.ForEach(info =>
                        lookups.Add(info.Name, info.GetValue(result, null).As<IEnumerable<LookupEntity>>()));

                    lookups.ForEach(lookup =>
                    {
                        var data = lookup.Value.OrderBy(item => item.Text).ToList();

                        if (LookupRepository.ContainsKey(lookup.Key))
                            LookupRepository[lookup.Key] = data;
                        else
                            LookupRepository.Add(lookup.Key, data);
                    });
                }
                catch (Exception)
                {
                    HasError = true;
                    throw;
                }
            }

            return LookupRepository;
        }

        public void InitiateLookups(Dictionary<string, ObservableCollection<LookupEntity>> lookupDictionary)
        {
            var lookups = new Dictionary<string, ObservableCollection<LookupEntity>>
            {
                {"Customers", new ObservableCollection<LookupEntity>()},
                {"Currencies", new ObservableCollection<LookupEntity>()},
                {"Reasons", new ObservableCollection<LookupEntity>()},
                {"Categories", new ObservableCollection<LookupEntity>()},
                {"Parts", new ObservableCollection<LookupEntity>()},
                {"SalesPersons", new ObservableCollection<LookupEntity>()},
                {"Suppliers", new ObservableCollection<LookupEntity>()},
                {"UnitOfMeasurements", new ObservableCollection<LookupEntity>()},
                {"Classifications", new ObservableCollection<LookupEntity>()},
                {"States", new ObservableCollection<LookupEntity>()},
                {"DistinctStates", new ObservableCollection<LookupEntity>()},
                {"Uoms", new ObservableCollection<LookupEntity>()},
                {"PartAttributeCategories", new ObservableCollection<LookupEntity>()},
                {"Warehouses", new ObservableCollection<LookupEntity>()},
            };

            lookups.ForEach(lookup =>
            {
                if (lookupDictionary.All(lookupDict => lookupDict.Key != lookup.Key))
                    lookupDictionary.Add(lookup.Key, lookup.Value);
            });
        }
    }
}