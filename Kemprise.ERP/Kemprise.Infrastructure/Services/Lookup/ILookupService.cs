﻿using ERP.WebApi.DTO.Lookups;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Kemprise.Infrastructure.Services.Lookup
{
    public interface ILookupService
    {
        Task<Dictionary<string, IEnumerable<LookupEntity>>> RefreshLookupAsync<T>(ApiControllers apiController,
            string method) where T : Lookups;

        void RegisterLookup<T>(ApiControllers apiController, string method) where T : Lookups;

        Task<Dictionary<string, IEnumerable<LookupEntity>>> GetLookupsAsync();

        void InitiateLookups(Dictionary<string, ObservableCollection<LookupEntity>> lookups);
    }
}