﻿using ERP.WebApi.DTO.DTOs.Procurement.NewPurchaseRequest;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseOrder;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using ERP.WebApi.DTO.Lookups;
using ERP.WebApi.DTO.Lookups.LookupItems;
using Kemprise.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Kemprise.Infrastructure.Services.Procurement
{
    public static class ProcurementService
    {
        public static PurchaseRequestDto ToNewPurchaseRequestDto(this PurchaseRequestDto purchaseRequestDto)
        {
            return new PurchaseRequestDto
            {
                Purpose = purchaseRequestDto.Purpose,
                DateCreated = DateTime.Now
            };
        }

        public static PrQuotationDetailDto ToPrQuotationDetail(this PurchaseRequestDetailDto prDetail)
        {
            return new PrQuotationDetailDto
            {
                OrderNumber = prDetail.OrderNumber,
                Description = prDetail.Description,
                UnitOfMeasurement = prDetail.UnitOfMeasurement,
                Quantity = prDetail.Quantity,
                PurchaseRequestDetailId = prDetail.PurchaseRequestDetailId,
                PartId = prDetail.PartId,
                PartNumber = prDetail.PartNumber
            };
        }

        public static PurchaseRequestDetailDto ToPurchaseRequestDetail(this PrQuotationDetailDto prQuoteDetail)
        {
            return new PurchaseRequestDetailDto
            {
                PurchaseRequestDetailId = prQuoteDetail.PurchaseRequestDetailId,
                Description = prQuoteDetail.Description,
                Mismatched = prQuoteDetail.Mismatched,
                MismatchedDescription = prQuoteDetail.Mismatched ? prQuoteDetail.Description : "",
                UnitOfMeasurement = prQuoteDetail.UnitOfMeasurement,
                Quantity = prQuoteDetail.Quantity,
                UnitPrice = prQuoteDetail.UnitPrice,
                AcceptedSupplier = prQuoteDetail.SupplierName,
                PartId = prQuoteDetail.PartId,
                PartNumber = prQuoteDetail.PartNumber,
                PrQuotationDetailId = prQuoteDetail.PrQuotationDetailId
            };
        }

        public static PurchaseOrderDto ToPurchaseOrder(this PurchaseRequestDto prDto, PrSupplierDto prSupplierDto, PrQuotationDto prQuotationDto)
        {
            return new PurchaseOrderDto
            {
                PurchaseRequestId = prDto.PurchaseRequestId,
                Description = prDto.Purpose,
                DateCreated = DateTime.Now,
                DateRequired = prDto.DateRequired,
                SupplierId = prSupplierDto.SupplierId,
                SupplierName = prSupplierDto.SupplierName,
                SupplierAddress = prSupplierDto.SupplierAddress,
                OfficeNumber = prSupplierDto.OfficeNumber,
                Email = prSupplierDto.Email,
                CurrencyId = prQuotationDto.CurrencyId ?? 0,
                CurrencyName = prQuotationDto.CurrencyName,
                ShipTo = "Kemprise Development and Trading FZE",
                ShippingAddress = "S18-4, RAKEZ Technology Park Al Hamra Free Zone RAK, United Arab Emirates"
            };
        }

        public static PurchaseOrderDto ToPurchaseOrder(this RfqDto rfqDto, string description = "")
        {
            return new PurchaseOrderDto
            {
                PurchaseRequestId = rfqDto.PurchaseRequestId,
                Description = description,
                DateCreated = DateTime.Now,
                DateRequired = rfqDto.DateRequired,
                SupplierId = rfqDto.SupplierId,
                SupplierName = rfqDto.SupplierName,
                SupplierAddress = rfqDto.SupplierAddress,
                OfficeNumber = rfqDto.OfficeNumber,
                Email = rfqDto.Email,
                CurrencyId = rfqDto.CurrencyId ?? 0,
                CurrencyName = rfqDto.CurrencyName,
                ShipTo = "Kemprise Development and Trading FZE",
                ShippingAddress = "S18-4, RAKEZ Technology Park Al Hamra Free Zone RAK, United Arab Emirates"
            };
        }

        public static ICollection<PurchaseOrderDetailDto> ToPurchaseOrderDetails(this ICollection<PurchaseRequestDetailDto> requestDetails)
        {
            var details = new List<PurchaseOrderDetailDto>();

            requestDetails.ForEach(x =>
            {
                details.Add(new PurchaseOrderDetailDto
                {
                    PurchaseRequestDetailId = x.PurchaseRequestDetailId,
                    Description = x.Mismatched ? x.MismatchedDescription : x.Description,
                    UnitOfMeasurement = x.UnitOfMeasurement,
                    Quantity = x.Quantity,
                    PartId = x.PartId,
                    PartNumber = x.PartNumber,
                    PrQuotationDetailId = x.PrQuotationDetailId,
                    Price = x.UnitPrice,
                    Total = x.UnitPrice * x.Quantity
                });
            });

            return details;
        }

        public static PurchaseOrderDto ToNewPurchaseOrderDto(this PurchaseOrderDto purchaseOrderDto)
        {
            return new PurchaseOrderDto
            {
                Description = purchaseOrderDto.Description,
                DateCreated = DateTime.Now,
                PaymentTerms = purchaseOrderDto.PaymentTerms,
                TaxPercentage = purchaseOrderDto.TaxPercentage,
                ShipTo = purchaseOrderDto.ShipTo,
                ShippingAddress = purchaseOrderDto.ShippingAddress,
                SupplierId = purchaseOrderDto.SupplierId,
                SupplierName = purchaseOrderDto.SupplierName,
                SupplierAddress = purchaseOrderDto.SupplierAddress,
                Country = purchaseOrderDto.Country,
                OfficeNumber = purchaseOrderDto.OfficeNumber,
                CurrencyId = purchaseOrderDto.CurrencyId,
                CurrencyName = purchaseOrderDto.CurrencyName
            };
        }

        public static decimal GetLatestPrice(this PurchaseOrderDetailDto purchaseOrderDetail, ObservableCollection<LookupEntity> lookUp)
        {
            var partLookUp = lookUp.OfType<PartLookup>().FirstOrDefault(x => x.Id == purchaseOrderDetail.PartId.ToString());
            return partLookUp?.Price ?? 0;
        }
    }
}