﻿using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using ERP.WebApi.DTO.DTOs.Sales.Invoicing;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using Kemprise.Infrastructure.Extensions;
using System;
using System.Collections.Generic;

namespace Kemprise.Infrastructure.Services.Sales
{
    public static class SalesService
    {
        public static InquiryDto ToNewInquiry(this InquiryDto inquiryDto)
        {
            return new InquiryDto
            {
                ProjectName = inquiryDto.ProjectName,
                Requester = inquiryDto.Requester,
                DateCreated = DateTime.Now,
                PaymentTerms = inquiryDto.PaymentTerms,
                BillingAddress = inquiryDto.BillingAddress,
                BillingCountry = inquiryDto.BillingCountry,
                IsSameAsBillingAddress = inquiryDto.IsSameAsBillingAddress,
                ShipTo = inquiryDto.ShipTo,
                ShippingAddress = inquiryDto.ShippingAddress,
                ShippingCountry = inquiryDto.ShippingCountry,
                CustomerId = inquiryDto.CustomerId,
                CustomerName = inquiryDto.CustomerName,
                CustomerType = inquiryDto.CustomerType,
                CustomerAddress = inquiryDto.CustomerAddress,
                CurrencyId = inquiryDto.CurrencyId,
                CurrencyName = inquiryDto.CurrencyName,
                SalesPersonId = inquiryDto.SalesPersonId,
                SalesPersonName = inquiryDto.SalesPersonName
            };
        }

        public static QuotationDto ToQuotation(this InquiryDto inquiryDto)
        {
            return new QuotationDto
            {
                InquiryId = inquiryDto.InquiryId,
                QuotationNumber = inquiryDto.InquiryNumber,
                ProjectName = inquiryDto.ProjectName,
                Requester = inquiryDto.Requester,
                Delivery = "upon receipt of purchase order.",
                DateCreated = inquiryDto.DateCreated,
                ExpirationDate = inquiryDto.ExpirationDate,
                PaymentTerms = inquiryDto.PaymentTerms,
                IsSameAsBillingAddress = inquiryDto.IsSameAsBillingAddress,
                ShipTo = inquiryDto.ShipTo,
                ShippingAddress = inquiryDto.ShippingAddress,
                ShippingCountry = inquiryDto.ShippingCountry,
                CustomerId = inquiryDto.CustomerId,
                CustomerType = inquiryDto.CustomerType,
                CustomerName = inquiryDto.CustomerName,
                CustomerAddress = inquiryDto.CustomerAddress,
                BillingAddress = inquiryDto.BillingAddress,
                BillingCountry = inquiryDto.BillingCountry,
                CurrencyId = inquiryDto.CurrencyId,
                CurrencyName = inquiryDto.CurrencyName,
                SalesPersonId = inquiryDto.SalesPersonId,
                InvoiceStatus = InvoiceStatus.NotYetInvoiced
            };
        }

        public static ICollection<QuotationDetailDto> ToQuotationDetails(this ICollection<InquiryDetailDto> inquiryDetails)
        {
            var quotationDetails = new List<QuotationDetailDto>();

            inquiryDetails.ForEach(iDetail =>
            {
                quotationDetails.Add(new QuotationDetailDto
                {
                    OrderNumber = iDetail.OrderNumber,
                    Description = iDetail.Description,
                    Category = iDetail.Category,
                    UnitOfMeasurement = iDetail.UnitOfMeasurement,
                    Quantity = iDetail.Quantity,
                    PartNumber = iDetail.PartNumber,
                    Price = iDetail.Price,
                    PartId = iDetail.PartId,
                    Total = iDetail.Quantity * iDetail.Price
                });
            });

            return quotationDetails;
        }

        public static InquiryDto ToInquiry(this QuotationDto quotationDto)
        {
            return new InquiryDto
            {
                ProjectName = quotationDto.ProjectName,
                Requester = quotationDto.Requester,
                DateCreated = DateTime.Now,
                PaymentTerms = quotationDto.PaymentTerms,
                Status = InquiryStatus.Draft,
                BillingAddress = quotationDto.BillingAddress,
                BillingCountry = quotationDto.BillingCountry,
                IsSameAsBillingAddress = quotationDto.IsSameAsBillingAddress,
                ShipTo = quotationDto.ShipTo,
                ShippingAddress = quotationDto.ShippingAddress,
                ShippingCountry = quotationDto.ShippingCountry,
                CustomerId = quotationDto.CustomerId,
                CustomerName = quotationDto.CustomerName,
                CustomerType = quotationDto.CustomerType,
                CustomerAddress = quotationDto.CustomerAddress,
                CurrencyId = quotationDto.CurrencyId,
                Prefix = quotationDto.Prefix,
                CurrencyName = quotationDto.CurrencyName,
                SalesPersonId = quotationDto.SalesPersonId,
                SalesPersonName = quotationDto.SalesPersonName
            };
        }

        public static ICollection<InquiryDetailDto> ToInquiryDetails(this ICollection<QuotationDetailDto> quotationDetails)
        {
            var inquiryDetails = new List<InquiryDetailDto>();

            quotationDetails.ForEach(detail =>
            {
                inquiryDetails.Add(new InquiryDetailDto
                {
                    OrderNumber = detail.OrderNumber,
                    Description = detail.Description,
                    Category = detail.Category,
                    UnitOfMeasurement = detail.UnitOfMeasurement,
                    Quantity = detail.Quantity,
                    Price = detail.Price,
                    PartNumber = detail.PartNumber,
                    PartId = detail.PartId
                });
            });

            return inquiryDetails;
        }

        public static InvoiceDto ToInvoiceDto(this QuotationDto quotationDto)
        {
            return new InvoiceDto
            {
                DateCreated = DateTime.Now,
                PurchaseOrderNumber = quotationDto.PurchaseOrderNumber,
                Status = InvoiceStatus.Valid,
                CustomerName = quotationDto.CustomerName,
                BillingAddress = quotationDto.BillingAddress,
                ShippingAddress = quotationDto.ShippingAddress,
                QuotationId = quotationDto.QuotationId,
                CustomerId = quotationDto.CustomerId,
                CurrencyId = quotationDto.CurrencyId,
                Prefix = quotationDto.Prefix,
                CurrencyName = quotationDto.CurrencyName
            };
        }

        public static ICollection<InvoiceDetailDto> ToInvoiceDetails(this ICollection<QuotationDetailDto> quotationDetails)
        {
            var invoiceDetails = new List<InvoiceDetailDto>();

            quotationDetails.ForEach(detail =>
            {
                invoiceDetails.Add(new InvoiceDetailDto
                {
                    OrderNumber = detail.OrderNumber,
                    Description = detail.Description,
                    Category = detail.Category,
                    UnitOfMeasurement = detail.UnitOfMeasurement,
                    Pending = detail.Pending,
                    Price = detail.Price,
                    PartId = detail.PartId,
                    PartNumber = detail.PartNumber,
                    QuotationDetailId = detail.QuotationDetailId
                });
            });

            return invoiceDetails;
        }
    }
}