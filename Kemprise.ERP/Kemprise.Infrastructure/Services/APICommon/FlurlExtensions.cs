﻿using Flurl;

namespace Kemprise.Infrastructure.Services.APICommon
{
    public static class FlurlExtensions
    {
        public static Url AppendCallParameters(this Url url, params object[] parameters)
        {
            if (parameters == null) return url;
            foreach (var parameter in parameters)
            {
                if(parameter == null) continue;
                url.AppendPathSegment(parameter);
            }
            return url;
        }
    }
}