﻿namespace Kemprise.Infrastructure.Services.APICommon
{
    public enum ApiControllers
    {
        Lookup,
        Account,
        Customer,
        Employee,
        Supplier,
        Currency,
        Inquiry,
        Company,
        Quotation,
        PurchaseRequest,
        NewPurchaseRequest,
        PurchaseOrder,
        PLM,
        PartAttribute,
        Classification,
        Import,
        Inventory,
        SalesOrder,
        Invoice
    }
}