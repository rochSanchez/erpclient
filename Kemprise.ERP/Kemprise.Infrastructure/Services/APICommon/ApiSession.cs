﻿using Kemprise.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;

namespace Kemprise.Infrastructure.Services.APICommon
{
    public static class ApiSession
    {
        private static readonly SecureString Id = new SecureString();
        private static readonly SecureString UserName = new SecureString();
        private static readonly SecureString Email = new SecureString();
        private static readonly SecureString Token = new SecureString();
        private static readonly SecureString LocationId = new SecureString();
        private static readonly List<string> _roles = new List<string>();
        private static byte[] _userPhoto;

        public static string GetId()
        {
            return Decode(Id);
        }

        public static void SetId(string id)
        {
            Id.Clear();
            foreach (var c in id) Id.AppendChar(c);
        }

        public static string GetToken()
        {
            return Decode(Token);
        }

        public static void SetToken(string token)
        {
            Token.Clear();
            foreach (var c in token) Token.AppendChar(c);
        }

        public static string GetUserName()
        {
            return Decode(UserName);
        }

        public static void SetUserName(string userName)
        {
            UserName.Clear();
            foreach (var c in userName) UserName.AppendChar(c);
        }

        public static string GetEmail()
        {
            return Decode(Email);
        }

        public static void SetEmail(string password)
        {
            Email.Clear();
            foreach (var c in password) Email.AppendChar(c);
        }

        private static string Decode(SecureString secure)
        {
            var unmanagedPassword = Marshal.SecureStringToBSTR(secure);
            var decoded = Marshal.PtrToStringUni(unmanagedPassword);
            Marshal.ZeroFreeBSTR(unmanagedPassword);

            return decoded;
        }

        public static void SetRoles(IEnumerable<string> responseRoles)
        {
            _roles.Clear();
            responseRoles.ForEach(item => _roles.Add(item));
        }

        public static bool HasRole(string role)
        {
            return _roles.Contains(role);
        }

        public static bool HasRoles(string[] roles)
        {
            return roles.Any(role => _roles.Contains(role));
        }

        public static bool IsManager()
        {
            return _roles.Any(role => role.Contains("MANAGER"));
        }

        public static void SetUserPhoto(byte[] userPhoto)
        {
            _userPhoto = userPhoto;
        }

        public static byte[] GetUserPhoto()
        {
            return _userPhoto;
        }
    }
}