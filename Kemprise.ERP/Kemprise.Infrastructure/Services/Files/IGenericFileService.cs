﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.Services.Notifications;
using Microsoft.Win32;
using Prism.Events;
using Telerik.Windows.Controls;

namespace Kemprise.Infrastructure.Services.Files
{
    public interface IGenericFileService<T> where T : IFile
    {
        void GetAnyFile(ObservableCollection<T> fileCollection);

        IEnumerable<T> GetImageFile();

        IEnumerable<T> GetPdfFile();

        void SaveFile(T fileDto);

        BitmapImage ByteArrayToImage(byte[] array);

        BitmapImage BitmapToImageSource(Bitmap bitmap);

        byte[] ImageToBytes();
    }

    public class GenericFileService<T> : IGenericFileService<T> where T : IFile, new()
    {
        private readonly IDesktopNotificationService _desktopNotificationService;
        private readonly IEventAggregator _eventAggregator;

        public GenericFileService(IDesktopNotificationService desktopNotificationService, IEventAggregator eventAggregator)
        {
            _desktopNotificationService = desktopNotificationService;
            _eventAggregator = eventAggregator;
        }

        public void GetAnyFile(ObservableCollection<T> fileCollection)
        {
            var files = ShowDialog("Files | *.jpg; *.jpeg; *.pdf; *.docx; *.doc; *.xlsx; *.xls; ");
            if (files == null) return;

            var fileList = files.ToList();
            var fileNames = fileList.Select(x => x.FileName).ToList();
            var duplicates = fileCollection.Where(x => fileNames.Contains(x.FileName)).ToList();

            if (duplicates.Any())
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Duplicate File Names Found",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            fileList.ForEach(fileCollection.Add);
        }

        public IEnumerable<T> GetImageFile()
        {
            return ShowDialog("JPEG | *.jpg; *.jpeg; ");
        }

        public byte[] ImageToBytes()
        {
            var dialog = new OpenFileDialog
            {
                Filter = "JPEG | *.jpg; *.jpeg; "
            };
            dialog.ShowDialog();
            if (!dialog.FileNames.Any())
                return null;

            var noLargeFiles = dialog.FileNames.All(fileName =>
            {
                var fileInfo = new FileInfo(fileName);
                return fileInfo.Length <= 2097152;
            });

            if (noLargeFiles) return File.ReadAllBytes(dialog.FileName);
            _eventAggregator.GetEvent<ErrorEvent>()
                .Publish(new ErrorEventPayload("Large File Found. Please Limit File Size to 2MB"));
            return null;
        }

        public IEnumerable<T> GetPdfFile()
        {
            return ShowDialog("PDF | *.pdf; ");
        }

        private IEnumerable<T> ShowDialog(string fileFilters)
        {
            var dialog = new OpenFileDialog
            {
                Filter = fileFilters,
                Multiselect = true
            };
            dialog.ShowDialog();

            if (!dialog.FileNames.Any())
                return null;

            var noLargeFiles = dialog.FileNames.All(fileName =>
            {
                var fileInfo = new FileInfo(fileName);
                return fileInfo.Length <= 2097152;
            });

            if (!noLargeFiles)
            {
                _eventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload("Large Files Found. Please Limit File Size to 2MB"));
                return null;
            }

            var files = new List<T>();
            dialog.FileNames.ForEach(file =>
            {
                files.Add(new T
                {
                    FileName = Path.GetFileNameWithoutExtension(file),
                    FileFormat = Path.GetExtension(file).ToLower(),
                    FileData = File.ReadAllBytes(file)
                });
            });

            return files;
        }

        public void SaveFile(T fileDto)
        {
            var saveDialog = new SaveFileDialog
            {
                OverwritePrompt = true,
                Filter = SaveAsType(fileDto.FileFormat),
                FileName = fileDto.FileName
            };

            if (saveDialog.ShowDialog() == false) return;
            if (string.IsNullOrEmpty(saveDialog.FileName)) return;

            File.WriteAllBytes(saveDialog.FileName, fileDto.FileData);

            var notificationInfo = new NotificationInfo
            {
                ModuleName = "FILE NOTIFICATION",
                Message = "File Successfully Saved"
            };
            var successfulAlert = new SaveSuccessfulAlert(notificationInfo);
            _desktopNotificationService.NotifyUser(successfulAlert);
        }

        private static string SaveAsType(string format)
        {
            return format switch
            {
                ".docx" => $"Document |*{format}",
                ".doc" => $"Document |*{format}",
                ".pdf" => $"PDF |*{format}",
                ".xls" => $"Excel |*{format}",
                ".xlsx" => $"Excel |*{format}",
                ".jpg" => $"JPEG |*{format}",
                ".jpeg" => $"JPEG |*{format}",
                _ => $"FILES |*{format}"
            };
        }

        public BitmapImage ByteArrayToImage(byte[] array)
        {
            using var memoryStream = new MemoryStream(array);
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImage.StreamSource = memoryStream;
            bitmapImage.EndInit();
            return bitmapImage;
        }

        public BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using var memoryStream = new MemoryStream();
            bitmap.Save(memoryStream, ImageFormat.Bmp);
            memoryStream.Position = 0L;
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = memoryStream;
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImage.EndInit();
            return bitmapImage;
        }
    }
}