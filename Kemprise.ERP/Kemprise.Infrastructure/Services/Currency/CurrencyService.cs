﻿using System.Globalization;
using System.Linq;

namespace Kemprise.Infrastructure.Services.Currency
{
    public static class CurrencyService
    {
        public static string ToCurrency(this decimal valueToConvert, string currencyCode, string prefix)
        {
            var cultureInfoList = CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                .Where(x => new RegionInfo(x.LCID).ISOCurrencySymbol == currencyCode)
                .ToList();
            var englishCulture = cultureInfoList.FirstOrDefault(x => x.DisplayName.Contains("English"));
            var cultureInfo = englishCulture ?? cultureInfoList.FirstOrDefault();

            return !string.IsNullOrEmpty(prefix)
                ? $"{prefix} {string.Format(cultureInfo, "{0:c}", valueToConvert)}"
                : string.Format(cultureInfo, "{0:c}", valueToConvert);
        }
    }
}