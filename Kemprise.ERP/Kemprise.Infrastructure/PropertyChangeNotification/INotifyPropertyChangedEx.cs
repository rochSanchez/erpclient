﻿using System.ComponentModel;

namespace Kemprise.Infrastructure.PropertyChangeNotification
{
    public interface INotifyPropertyChangedEx : INotifyPropertyChanged
    {
        bool IsNotifying { get; set; }

        void NotifyOfPropertyChange(string propertyName);

        void Refresh();
    }
}