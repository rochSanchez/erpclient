﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Kemprise.Infrastructure.Extensions
{
    public static class ObjectExtensions
    {
        public static void IfNotNullThen<T>(this T o, Action<T> action) where T : class
        {
            if ((object)o == null)
                return;
            action(o);
        }

        public static bool IsTypeOf<T>(this object o) => o.GetType() == typeof(T);

        public static T As<T>(this object o) where T : class => o as T;

        public static void AddTo<T>(this T item, IList<T> list) => list.Add(item);

        public static U Select<T, U>(this T item, Func<T, U> func) => func(item);

        public static object GetDefault(this Type type) => type.IsValueType ? Activator.CreateInstance(type) : (object)null;

        public static R IfNotNull<T, R>(this T o, Func<T, R> returnFunc, R elseVal) => ((object)o).IsNull() ? elseVal : returnFunc(o);

        public static R IfNotNull<T, R>(this T o, Func<T, R> returnFunc) => o.IfNotNull<T, R>(returnFunc, default(R));

        public static void IfIsOfType<T>(this object o, Action<T> action) where T : class
        {
            if (!(o is T obj))
                return;
            action(obj);
        }

        public static bool IsNull(this object obj) => obj == null;

        public static bool IsNotNull(this object obj) => obj != null;

        public static VALUE TryGetValue<KEY, VALUE>(this IDictionary<KEY, VALUE> dict, KEY key) => dict.ContainsKey(key) ? dict[key] : default(VALUE);

        public static IEnumerable<T> WrapInEnumerable<T>(this T obj)
        {
            if (((object)obj).IsNull())
                return (IEnumerable<T>)new T[0];
            return (IEnumerable<T>)new T[1] { obj };
        }

        public static bool NullSafeEquals(this object x, object y) => x.IsNull() && y.IsNull() || x.IsNotNull() && x.Equals(y) || y.Equals(x);

        public static R Swallow<R>(this Func<R> func, params Type[] exceptions)
        {
            try
            {
                return func();
            }
            catch (Exception ex)
            {
                if (((IEnumerable<Type>)exceptions).Any<Type>((Func<Type, bool>)(x => x.IsAssignableFrom(ex.GetType()))))
                    return default(R);
                throw;
            }
        }

        public static void SetProperty(this object o, string propertyName, object value)
        {
            o.GetType().GetProperty(propertyName);
            o.GetType().GetProperty(propertyName).SetValue(o, value, (object[])null);
        }

        public static object GetPropertyValue(this object o, string propertyName) => o.GetType().GetProperty(propertyName).GetValue(o, (object[])null);

        public static MemberInfo GetMemberInfo(this Expression expression)
        {
            LambdaExpression lambdaExpression = (LambdaExpression)expression;
            return (!(lambdaExpression.Body is UnaryExpression) ? (MemberExpression)lambdaExpression.Body : (MemberExpression)((UnaryExpression)lambdaExpression.Body).Operand).Member;
        }

        public static DataTable ToDataTable(this object dataObject)
        {
            if (dataObject == null)
                return null;

            DataTable table;

            if (dataObject.GetType().Name.Contains("AnonymousType"))
            {
                var type = dataObject.GetType();
                table = new DataTable("Data");
                table.GenerateColumns(type.GetProperties());
                table.ExtractSingleObjectToRow(dataObject, table.NewRow());
            }
            else
            {
                var type = dataObject.GetType().IsGenericType ? dataObject.GetType().GetGenericArguments().First() : dataObject.GetType();
                table = new DataTable(type.Name);

                table.GenerateColumns(type.GetProperties());
                if (dataObject.GetType().IsGenericType)
                    table.ExtractMultipleObjectToRows(dataObject);
                else
                    table.ExtractSingleObjectToRow(dataObject, table.NewRow());
            }
            return table;
        }

        public static void GenerateColumns(this DataTable dataTable, PropertyInfo[] propertyInfos)
        {
            propertyInfos
                .ForEach(p =>
                {
                    var customColumnName = p.GetCustomColumnAttribute();

                    dataTable.Columns.Add(p.PropertyType == typeof(DateTime?)
                        ? new DataColumn(customColumnName != null ? customColumnName.ColumnName : p.Name, typeof(DateTime))
                        : new DataColumn(customColumnName != null ? customColumnName.ColumnName : p.Name, p.PropertyType));
                });
        }

        public static CustomColumnNameAttribute GetCustomColumnAttribute(this PropertyInfo propertyInfo)
        {
            var attribute = propertyInfo.GetCustomAttributes(typeof(CustomColumnNameAttribute), false)
                .Cast<CustomColumnNameAttribute>()
                .FirstOrDefault();

            return attribute;
        }

        public static void ExtractSingleObjectToRow(this DataTable dataTable, object objectToExtract, DataRow row)
        {
            if (objectToExtract.GetType().Name.Contains("AnonymousType"))
            {
                var properties = objectToExtract.GetType().GetProperties();
                properties.ForEach(property => SetRowValue(property, row, objectToExtract));
            }
            else
            {
                objectToExtract
                    .GetType()
                    .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly)
                    .ForEach(property => SetRowValue(property, row, objectToExtract));
            }

            dataTable.Rows.Add(row);
        }

        public static void SetRowValue(PropertyInfo property, DataRow row, object objectToExtract)
        {
            var customColumnName = property.GetCustomColumnAttribute();
            var rowName = customColumnName != null ? customColumnName.ColumnName : property.Name;

            if (property.PropertyType == typeof(DateTime?))
            {
                var value = property.GetValue(objectToExtract, null);
                row[rowName] = value ?? DateTime.MinValue;
            }
            else
                row[rowName] = property.GetValue(objectToExtract, null);
        }

        public static void ExtractMultipleObjectToRows(this DataTable dataTable, object dataToExtract)
        {
            var list = dataToExtract.As<IEnumerable<object>>();
            list?.ForEach(o => dataTable.ExtractSingleObjectToRow(o, dataTable.NewRow()));
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class CustomColumnNameAttribute : Attribute
    {
        public string ColumnName { get; set; }

        public CustomColumnNameAttribute(string name)
        {
            ColumnName = name;
        }
    }
}