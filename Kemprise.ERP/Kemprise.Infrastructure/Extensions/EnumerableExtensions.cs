﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kemprise.Infrastructure.Extensions
{
    public static class EnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            if (items == null) return;
            foreach (var obj in items)
                action(obj);
        }

        public static IEnumerable<T> Remove<T>(this IEnumerable<T> items, Func<T, bool> func)
        {
            var list = items.ToList();
            list.Where(func).ToList().ForEach(i => list.Remove(i));
            return list;
        }
    }
}