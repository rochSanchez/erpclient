﻿using ERP.WebApi.DTO.Lookups;
using ERP.WebApi.DTO.Lookups.LookupItems;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Prism.Commands;
using System;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;

namespace Kemprise.Infrastructure.Windows.CloseEntity
{
    public class CloseEntityWindowViewModel : NotifyPropertyChangedBase
    {
        public CloseEntityWindowViewModel()
        {
            CloseCommand = new DelegateCommand<object>(CloseExecute);
            OkCommand = new DelegateCommand<object>(OkExecute, CanOk);
        }

        public DelegateCommand<object> CloseCommand { get; set; }
        public DelegateCommand<object> OkCommand { get; set; }

        public Action SetPropertyToDto { get; set; }
        public Action CompletedAction { get; set; }

        private void CloseExecute(object obj)
        {
            var alertWindow = obj.As<RadWindow>();
            alertWindow?.Close();
        }

        private void OkExecute(object obj)
        {
            var alertWindow = obj.As<RadWindow>();
            alertWindow?.Close();
            CompletedAction();
        }

        private bool CanOk(object arg)
        {
            return !string.IsNullOrEmpty(SelectedReasonText) &&
                   !string.IsNullOrEmpty(DetailedReasonForClosing);
        }

        public ObservableCollection<LookupEntity> Reasons
        {
            get { return GetValue(() => Reasons); }
            set { SetValue(() => Reasons, value); }
        }

        public ReasonLookup SelectedReason
        {
            get { return GetValue(() => SelectedReason); }
            set
            {
                SetValue(() => SelectedReason, value);
                if (value == null) return;
                SelectedReasonText = value.Text;
            }
        }

        public string SelectedReasonText
        {
            get { return GetValue(() => SelectedReasonText); }
            set
            {
                SetValue(() => SelectedReasonText, value);
                OkCommand.RaiseCanExecuteChanged();
                SetPropertyToDto();
            }
        }

        public string DetailedReasonForClosing
        {
            get { return GetValue(() => DetailedReasonForClosing); }
            set
            {
                SetValue(() => DetailedReasonForClosing, value);
                OkCommand.RaiseCanExecuteChanged();
                SetPropertyToDto();
            }
        }
    }
}