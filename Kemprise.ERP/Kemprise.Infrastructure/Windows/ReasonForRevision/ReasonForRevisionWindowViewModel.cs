﻿using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Prism.Commands;
using Telerik.Windows.Controls;

namespace Kemprise.Infrastructure.Windows.ReasonForRevision
{
    public class ReasonForRevisionWindowViewModel : NotifyPropertyChangedBase
    {
        public ReasonForRevisionWindowViewModel()
        {
            OkCommand = new DelegateCommand<object>(OkExecute);
        }

        public DelegateCommand<object> OkCommand { get; set; }

        private void OkExecute(object obj)
        {
            var alertWindow = obj.As<RadWindow>();
            alertWindow?.Close();
        }

        public string RevisionNotes
        {
            get { return GetValue(() => RevisionNotes); }
            set { SetValue(() => RevisionNotes, value); }
        }
    }
}