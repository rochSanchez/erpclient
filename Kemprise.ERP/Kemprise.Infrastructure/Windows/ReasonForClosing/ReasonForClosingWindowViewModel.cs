﻿using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Prism.Commands;
using Telerik.Windows.Controls;

namespace Kemprise.Infrastructure.Windows.ReasonForClosing
{
    public class ReasonForClosingWindowViewModel : NotifyPropertyChangedBase
    {
        public ReasonForClosingWindowViewModel()
        {
            OkCommand = new DelegateCommand<object>(OkExecute);
        }

        public DelegateCommand<object> OkCommand { get; set; }

        private void OkExecute(object obj)
        {
            var alertWindow = obj.As<RadWindow>();
            alertWindow?.Close();
        }

        public string ReasonForClosing
        {
            get { return GetValue(() => ReasonForClosing); }
            set { SetValue(() => ReasonForClosing, value); }
        }

        public string DetailedReasonForClosing
        {
            get { return GetValue(() => DetailedReasonForClosing); }
            set { SetValue(() => DetailedReasonForClosing, value); }
        }
    }
}