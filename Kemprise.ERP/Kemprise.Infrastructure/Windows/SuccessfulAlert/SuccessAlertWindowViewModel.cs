﻿using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Prism.Commands;
using Telerik.Windows.Controls;

namespace Kemprise.Infrastructure.Windows.SuccessfulAlert
{
    public class SuccessAlertWindowViewModel : NotifyPropertyChangedBase
    {
        public SuccessAlertWindowViewModel()
        {
            OkCommand = new DelegateCommand<object>(OkExecute);
        }

        private static void OkExecute(object obj)
        {
            var alertWindow = obj.As<RadWindow>();
            alertWindow?.Close();
        }

        public DelegateCommand<object> OkCommand { get; set; }

        public string Content
        {
            get { return GetValue(() => Content); }
            set { SetValue(() => Content, value); }
        }

        public string BoldContent
        {
            get { return GetValue(() => BoldContent); }
            set { SetValue(() => BoldContent, value); }
        }
    }
}