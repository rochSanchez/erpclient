﻿using ERP.WebApi.DTO;
using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseOrder;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Currency;
using Kemprise.Procurement.ProcurementViews.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.Procurement.NavigationItems
{
    public class PurchaseOrderNavItem : ParentNavigationItemBase
    {
        private readonly PurchaseOrderWorkAreaView _workAreaView;

        public PurchaseOrderNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<PurchaseOrderWorkAreaView>();
            IsVisible = ApiSession.HasRoles(ErpRoles.CreatePurchaseOrder);
            Order = 3;
        }

        public override string Name => "Purchase Orders";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/PurchaseOrder.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Procurement;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            var itemList = await RequestManager.GetRecordsAsync<PurchaseOrderListItem>(ApiControllers.PurchaseOrder, "PurchaseOrderGetRecList");
            itemList = itemList.ToList();

            itemList.ForEach(x => x.StringTotal = x.Total.ToCurrency(x.CurrencyCode, x.Prefix));
            return itemList;
        }
    }
}