﻿using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Procurement.ProcurementViews.PurchaseRequest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.Procurement.NavigationItems
{
    public class PurchaseRequestNavItem : ParentNavigationItemBase
    {
        private readonly PurchaseRequestWorkAreaView _workAreaView;

        public PurchaseRequestNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<PurchaseRequestWorkAreaView>();
            Order = 1;

            IsVisible = false;
        }

        public override string Name => "Old Purchase Requests";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/PurchaseRequest.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Procurement;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<PurchaseRequestListItem>(ApiControllers.PurchaseRequest, "PurchaseRequestGetRecList");
        }
    }
}