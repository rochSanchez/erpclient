﻿using Kemprise.Infrastructure.Modules;

namespace Kemprise.Procurement.QuickAccess
{
    /// <summary>
    /// Interaction logic for ManagementQuickAccessView.xaml
    /// </summary>
    public partial class ProcurementQuickAccessView : IQuickAccessView
    {
        public ProcurementQuickAccessView()
        {
            InitializeComponent();
        }

        public int Order => 3;
    }
}