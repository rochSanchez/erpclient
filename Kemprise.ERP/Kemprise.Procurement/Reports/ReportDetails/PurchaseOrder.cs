﻿using ERP.WebApi.DTO.DTOs.Procurement.PurchaseOrder;
using ERP.WebApi.DTO.Lookups;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Reporting;
using Kemprise.Infrastructure.Services.Currency;
using Kemprise.Procurement.ProcurementViews.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace Kemprise.Procurement.Reports.ReportDetails
{
    public class PurchaseOrder : IModuleReportDetail
    {
        public string ModuleName => ErpModules.Procurement.ToString();
        public string ReportTitle => "Purchase Order";
        public string ReportFileName => "purchaseorder";

        public DataSet ConfigureReportData(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            var purchaseOrderEntity = entity.As<PurchaseOrderEntity>();
            var poDto = purchaseOrderEntity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();
            var total = poDto.Total.ToCurrency(poDto.CurrencyName, poDto.Prefix);

            var purchaseOrderData = new PurchaseOrderData
            {
                PurchaseOrderNumber = poDto.PurchaseOrderNumber,
                DateCreated = poDto.DateCreated == null
                    ? ""
                    : poDto.DateCreated.Value.ToString("dd/MMM/yyyy"),
                DateRequired = poDto.DateRequired == null
                    ? ""
                    : poDto.DateRequired.Value.ToString("dd/MMM/yyyy"),
                PaymentTerms = poDto.PaymentTerms,
                SubTotal = $"{poDto.SubTotal:n}",
                Tax = $"{poDto.TaxAmount:n}",
                Discount = $"{poDto.DiscountAmount:n}",
                Total = total,
                SupplierName = poDto.SupplierName,
                SupplierAddress = $"{poDto.SupplierAddress}{Environment.NewLine}{poDto.Country}"
            };
            var purchaseOrderDataTable = purchaseOrderData.ToDataTable();

            var details = new List<RequestForQuotationDetailsData>();
            purchaseOrderEntity.PurchaseOrderDetails.ForEach(detail =>
            {
                var detailDto = detail.GetDto<PurchaseOrderDetailDto>();
                details.Add(new RequestForQuotationDetailsData
                {
                    Serial = detailDto.OrderNumber.ToString(),
                    PartNumber = detailDto.PartNumber,
                    Description = detailDto.Description.Trim(),
                    UnitOfMeasurement = detailDto.UnitOfMeasurement,
                    Quantity = detailDto.Quantity.ToString("G29"),
                    Price = $"{detailDto.Price:n}",
                    Total = $"{detailDto.Total:n}"
                });
            });
            var detailsTable = details.ToDataTable();

            var notes = new List<RequestForQuotationNotesData>();
            purchaseOrderEntity.PurchaseOrderNotes.ForEach(note =>
            {
                notes.Add(new RequestForQuotationNotesData
                {
                    Item = note.Item.ToString("N0"),
                    Name = note.Name
                });
            });
            var notesTable = notes.ToDataTable();

            var dataSet = new DataSet();
            dataSet.Tables.Add(purchaseOrderDataTable);
            dataSet.Tables.Add(detailsTable);
            dataSet.Tables.Add(notesTable);

            return dataSet;
        }

        public ReportParameters GetReportParameters(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            return new ReportParameters();
        }
    }
}