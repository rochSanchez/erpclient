﻿using ERP.WebApi.DTO.DTOs.Procurement.NewPurchaseRequest;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using ERP.WebApi.DTO.Lookups;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Reporting;
using Kemprise.Procurement.ProcurementViews.NewPurchaseRequest;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace Kemprise.Procurement.Reports.ReportDetails
{
    public class RfqDraft : IModuleReportDetail
    {
        public string ModuleName => ErpModules.Procurement.ToString();
        public string ReportTitle => "Request For Quotation";
        public string ReportFileName => "requestforquotationdraft";

        public DataSet ConfigureReportData(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            var newPrEntity = entity.As<NewPrEntity>();
            var prSupplierDto = newPrEntity.PrSupplierDto.GetDto<PrSupplierDto>();

            var rfqData = new RequestForQuotationData
            {
                SupplierName = prSupplierDto.SupplierName,
                SupplierAddress = $"{prSupplierDto.SupplierAddress}{Environment.NewLine}{prSupplierDto.Country}",
                Date = prSupplierDto.DateCreated == null
                    ? ""
                    : prSupplierDto.DateCreated.Value.ToString("dd/MMM/yyyy"),
                DateRequired = prSupplierDto.DateRequired == null
                    ? ""
                    : prSupplierDto.DateRequired.Value.ToString("dd/MMM/yyyy"),
                PurchaseRequestNumber = prSupplierDto.PurchaseRequestNumber
            };
            var rfqDataTable = rfqData.ToDataTable();

            var details = new List<RequestForQuotationDetailsData>();
            newPrEntity.PurchaseRequestDetails.ForEach(x =>
            {
                var detail = x.GetDto<PurchaseRequestDetailDto>();

                details.Add(new RequestForQuotationDetailsData
                {
                    Serial = detail.OrderNumber.ToString(),
                    Description = detail.Description.Trim(),
                    UnitOfMeasurement = detail.UnitOfMeasurement,
                    Quantity = detail.Quantity.ToString("G29")
                });
            });
            var detailsTable = details.ToDataTable();

            var notes = new List<RequestForQuotationNotesData>();
            newPrEntity.PurchaseRequestNotes.ForEach(note =>
            {
                notes.Add(new RequestForQuotationNotesData
                {
                    Item = note.Item.ToString("N0"),
                    Name = note.Name
                });
            });
            var notesTable = notes.ToDataTable();

            var dataSet = new DataSet();
            dataSet.Tables.Add(rfqDataTable);
            dataSet.Tables.Add(detailsTable);
            dataSet.Tables.Add(notesTable);

            return dataSet;
        }

        public ReportParameters GetReportParameters(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            return new ReportParameters();
        }
    }
}