﻿namespace Kemprise.Procurement.Reports.ReportDetails
{
    public class RequestForQuotationData
    {
        public string SupplierName { get; set; }
        public string SupplierAddress { get; set; }
        public string Date { get; set; }
        public string DateRequired { get; set; }
        public string PurchaseRequestNumber { get; set; }
    }

    public class RequestForQuotationDetailsData
    {
        public string Serial { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string UnitOfMeasurement { get; set; }
        public string Quantity { get; set; }
        public string Price { get; set; }
        public string Total { get; set; }
    }

    public class RequestForQuotationNotesData
    {
        public string Item { get; set; }
        public string Name { get; set; }
    }

    public class PurchaseOrderData
    {
        public string PurchaseOrderNumber { get; set; }
        public string DateCreated { get; set; }
        public string DateRequired { get; set; }
        public string PaymentTerms { get; set; }
        public string SubTotal { get; set; }
        public string Tax { get; set; }
        public string Discount { get; set; }
        public string Total { get; set; }
        public string SupplierName { get; set; }
        public string SupplierAddress { get; set; }
    }
}