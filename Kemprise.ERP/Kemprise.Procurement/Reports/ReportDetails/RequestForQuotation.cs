﻿using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using ERP.WebApi.DTO.Lookups;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Reporting;
using Kemprise.Procurement.ProcurementViews.PurchaseRequest;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace Kemprise.Procurement.Reports.ReportDetails
{
    public class RequestForQuotation : IModuleReportDetail
    {
        public string ModuleName => ErpModules.Procurement.ToString();
        public string ReportTitle => "Request For Quotation";
        public string ReportFileName => "requestforquotation";

        public DataSet ConfigureReportData(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            var purchaseRequestEntity = entity.As<PurchaseRequestEntity>();
            var rfqDto = purchaseRequestEntity.RfqDto.GetDto<RfqDto>();

            var rfqData = new RequestForQuotationData
            {
                SupplierName = rfqDto.SupplierName,
                SupplierAddress = $"{rfqDto.SupplierAddress}{Environment.NewLine}{rfqDto.Country}",
                Date = rfqDto.DateCreated == null
                    ? ""
                    : rfqDto.DateCreated.Value.ToString("dd/MMM/yyyy"),
                DateRequired = rfqDto.DateRequired == null
                    ? ""
                    : rfqDto.DateRequired.Value.ToString("dd/MMM/yyyy"),
                PurchaseRequestNumber = rfqDto.PurchaseRequestNumber
            };
            var rfqDataTable = rfqData.ToDataTable();

            var details = new List<RequestForQuotationDetailsData>();
            purchaseRequestEntity.PurchaseRequestDetails.ForEach(detail =>
            {
                details.Add(new RequestForQuotationDetailsData
                {
                    Serial = detail.OrderNumber.ToString(),
                    Description = detail.Description.Trim(),
                    UnitOfMeasurement = detail.UnitOfMeasurement,
                    Quantity = detail.Quantity.ToString("G29")
                });
            });
            var detailsTable = details.ToDataTable();

            var notes = new List<RequestForQuotationNotesData>();
            purchaseRequestEntity.PurchaseRequestNotes.ForEach(note =>
            {
                notes.Add(new RequestForQuotationNotesData
                {
                    Item = note.Item.ToString("N0"),
                    Name = note.Name
                });
            });
            var notesTable = notes.ToDataTable();

            var dataSet = new DataSet();
            dataSet.Tables.Add(rfqDataTable);
            dataSet.Tables.Add(detailsTable);
            dataSet.Tables.Add(notesTable);

            return dataSet;
        }

        public ReportParameters GetReportParameters(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            return new ReportParameters();
        }
    }
}