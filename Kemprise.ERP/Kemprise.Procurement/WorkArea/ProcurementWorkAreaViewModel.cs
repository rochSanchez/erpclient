﻿using ERP.WebApi.DTO;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.WorkArea;
using Prism.Events;
using System.Collections.Generic;
using System.Linq;
using Unity;

namespace Kemprise.Procurement.WorkArea
{
    public class ProcurementWorkAreaViewModel : ErpWorkAreaViewModelBase
    {
        public ProcurementWorkAreaViewModel(IUnityContainer container, IEventAggregator eventAggregator) :
            base(container, eventAggregator)
        {
            ItemsListDictionary = new Dictionary<string, IEnumerable<EntityListItem>>();

            NavigationItems = container.ResolveAll<IParentNavigationItem>()
                .Where(item => item.Module == ErpModules.Procurement)
                .OrderBy(item => item.Order);

            eventAggregator.GetEvent<RefreshItemListEvent<ProcurementWorkAreaViewModel>>().Subscribe(OnRefreshItemList);
        }
    }
}