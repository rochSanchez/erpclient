﻿using ERP.WebApi.DTO;
using Kemprise.Infrastructure.Controllers;
using Kemprise.Infrastructure.Modules;
using Kemprise.Procurement.Controller;
using Kemprise.Procurement.WorkArea;
using Prism.Ioc;
using Prism.Unity;
using System.Reflection;
using Unity;
using Unity.Lifetime;

namespace Kemprise.Procurement
{
    public class ProcurementModule : ErpModule
    {
        public ProcurementModule(IUnityContainer container)
        {
            container.RegisterType(typeof(IControllerBase), typeof(ProcurementController),
                nameof(ProcurementController), new ContainerControlledLifetimeManager());
        }

        protected override Assembly GetBusinessAssembly()
        {
            return Assembly.GetAssembly(typeof(EntityListItem));
        }

        protected override Assembly GetModuleAssembly()
        {
            return Assembly.GetAssembly(typeof(ProcurementModule));
        }

        public override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.GetContainer().RegisterType<object, ProcurementWorkAreaView>(typeof(ProcurementWorkAreaView).FullName, new ContainerControlledLifetimeManager());
        }
    }
}