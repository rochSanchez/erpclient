﻿using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.ViewModel;
using Unity;

namespace Kemprise.Procurement
{
    public class ProcurementViewModelBase : ErpViewModelBase
    {
        protected ProcurementViewModelBase(IUnityContainer container, string category) :
            base(container, ErpModules.Procurement, category)
        {
        }

        protected override string ModuleReportAssembly => "Kemprise.Procurement";
        protected override string ModuleReportFilePath => @"Reports\ReportFiles";
    }
}