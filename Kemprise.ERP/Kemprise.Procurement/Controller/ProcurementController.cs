﻿using Kemprise.Infrastructure.Controllers;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Events.Security;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Regions;
using Kemprise.Procurement.WorkArea;
using Prism.Events;
using Prism.Regions;
using Unity;
using Unity.Lifetime;

namespace Kemprise.Procurement.Controller
{
    public class ProcurementController : ControllerBase
    {
        public ProcurementController(IEventAggregator eventAggregator, IRegionManager regionManager, IUnityContainer container) :
            base(eventAggregator, regionManager, container)
        {
            eventAggregator.GetEvent<OpenModuleEvent>().Subscribe(OnOpenModule);
            eventAggregator.GetEvent<LogoutUserEvent>().Subscribe(OnLogoutUserEvent);
        }

        private void OnLogoutUserEvent()
        {
            Container.RegisterType<object, ProcurementWorkAreaView>(typeof(ProcurementWorkAreaView).FullName,
                new ContainerControlledLifetimeManager());
        }

        private void OnOpenModule(OpenModuleEventPayload obj)
        {
            if (obj.ErpModule == ErpModules.Procurement)
                obj.QuickAccess.IsSelected = true;
            else
            {
                EventAggregator.GetEvent<DeSelectModuleEvent<ProcurementController>>().Publish();
                return;
            }

            RegionManager.RequestNavigate(RegionNames.MainWorkAreaRegion, typeof(ProcurementWorkAreaView).FullName);
        }
    }
}