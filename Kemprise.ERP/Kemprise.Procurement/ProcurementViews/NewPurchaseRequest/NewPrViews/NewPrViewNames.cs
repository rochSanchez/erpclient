﻿namespace Kemprise.Procurement.ProcurementViews.NewPurchaseRequest.NewPrViews
{
    public class NewPrViewNames
    {
        public const string NewPrMainView = "New Purchase Request Main View";
        public const string NewPrEditView = "New Purchase Request Edit View";
        public const string NewPrDisplayView = "New Purchase Request Display View";
        public const string NewPrConvertToPoView = "New Purchase Request Convert To Purchase Order View";
    }
}