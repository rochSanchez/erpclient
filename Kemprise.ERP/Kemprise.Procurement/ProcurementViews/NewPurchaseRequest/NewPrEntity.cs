﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Procurement.NewPurchaseRequest;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using System.Collections.ObjectModel;

namespace Kemprise.Procurement.ProcurementViews.NewPurchaseRequest
{
    public class NewPrEntity : EntityBase
    {
        public NewPrEntity()
        {
            PurchaseRequestDto = new DynamicNotifyPropertyChangedProxy(new PurchaseRequestDto());
            PurchaseRequestDetails = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            PurchaseRequestFiles = new ObservableCollection<FileDto>();
            PurchaseRequestNotes = new ObservableCollection<NoteDto>();
            PrSuppliers = new ObservableCollection<PrSupplierAggregate>();

            PrSupplierDto = new DynamicNotifyPropertyChangedProxy(new PrSupplierDto());
            PrQuotationDto = new DynamicNotifyPropertyChangedProxy(new PrQuotationDto());
            PrQuotationDetails = new ObservableCollection<PrQuotationDetailDto>();
            PrQuotationFiles = new ObservableCollection<FileDto>();

            QuotesReceived = new ObservableCollection<PrQuotationDetailDto>();
            PrDetailsToConvert = new ObservableCollection<PurchaseRequestDetailDto>();
            PrDetailToSave = new PurchaseRequestDetailDto();
        }

        public DynamicNotifyPropertyChangedProxy PurchaseRequestDto { get; set; }
        public ObservableCollection<DynamicNotifyPropertyChangedProxy> PurchaseRequestDetails { get; set; }
        public ObservableCollection<FileDto> PurchaseRequestFiles { get; set; }
        public ObservableCollection<NoteDto> PurchaseRequestNotes { get; set; }
        public ObservableCollection<PrSupplierAggregate> PrSuppliers { get; set; }

        public DynamicNotifyPropertyChangedProxy PrSupplierDto { get; set; }
        public DynamicNotifyPropertyChangedProxy PrQuotationDto { get; set; }
        public ObservableCollection<PrQuotationDetailDto> PrQuotationDetails { get; set; }
        public ObservableCollection<FileDto> PrQuotationFiles { get; set; }

        public ObservableCollection<PrQuotationDetailDto> QuotesReceived { get; set; }
        public ObservableCollection<PurchaseRequestDetailDto> PrDetailsToConvert { get; set; }
        public PurchaseRequestDetailDto PrDetailToSave { get; set; }
    }
}