﻿using ERP.WebApi.DTO.DTOs.Procurement.NewPurchaseRequest;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.NewPurchaseRequest.Methods
{
    public class PrQuotationAddRec : ErpSaveMethodCallBase
    {
        public PrQuotationAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "PrQuotationAddRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override ApiControllers Controller => ApiControllers.NewPurchaseRequest;
        public override string Category => ProcurementTypes.NewPurchaseRequest.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var newPrEntity = entity.As<NewPrEntity>();
            var prQuotationDto = newPrEntity.PrQuotationDto.GetDto<PrQuotationDto>();

            var aggregate = new PrQuotationAggregate
            {
                PrQuotationDto = prQuotationDto,
                PrQuotationDetails = newPrEntity.PrQuotationDetails,
                PrQuotationFiles = newPrEntity.PrQuotationFiles
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(PrQuotationAggregate), aggregate }
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}