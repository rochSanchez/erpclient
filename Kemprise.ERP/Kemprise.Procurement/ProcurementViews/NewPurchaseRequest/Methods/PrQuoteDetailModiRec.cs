﻿using ERP.WebApi.DTO.DTOs.Procurement.NewPurchaseRequest;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.NewPurchaseRequest.Methods
{
    public class PrQuoteDetailModiRec : ErpSaveMethodCallBase
    {
        public PrQuoteDetailModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "PrQuoteDetailModiRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override ApiControllers Controller => ApiControllers.NewPurchaseRequest;
        public override string Category => ProcurementTypes.NewPurchaseRequest.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var newPrEntity = entity.As<NewPrEntity>();

            var aggregate = new PrQuotationAggregate
            {
                PrQuotationDetails = newPrEntity.QuotesReceived
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(PrQuotationAggregate), aggregate }
            };

            await UpdateRecordAsync(MethodName, dataToSave);
        }
    }
}