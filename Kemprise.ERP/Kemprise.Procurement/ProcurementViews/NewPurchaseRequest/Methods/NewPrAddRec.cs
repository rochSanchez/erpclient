﻿using ERP.WebApi.DTO.DTOs.Procurement.NewPurchaseRequest;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.NewPurchaseRequest.Methods
{
    public class NewPrAddRec : ErpSaveMethodCallBase
    {
        public NewPrAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "NewPrAddRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override ApiControllers Controller => ApiControllers.NewPurchaseRequest;
        public override string Category => ProcurementTypes.NewPurchaseRequest.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var purchaseRequestEntity = entity.As<NewPrEntity>();
            var purchaseRequestDetails = new List<PurchaseRequestDetailDto>();

            purchaseRequestEntity.PurchaseRequestDetails.ForEach(x => purchaseRequestDetails.Add(x.GetDto<PurchaseRequestDetailDto>()));
            purchaseRequestDetails.ForEach(x =>
            {
                if (!string.IsNullOrEmpty(x.Description))
                    x.Description = x.Description.Trim();
            });

            var textInfo = new CultureInfo("en-Us", false).TextInfo;
            foreach (var item in purchaseRequestDetails.Where(item => !string.IsNullOrEmpty(item.UnitOfMeasurement)))
                item.UnitOfMeasurement = textInfo.ToTitleCase(item.UnitOfMeasurement);

            var aggregate = new NewPrAggregate
            {
                PurchaseRequestDto = purchaseRequestEntity.PurchaseRequestDto.GetDto<PurchaseRequestDto>(),
                PurchaseRequestDetails = purchaseRequestDetails,
                PurchaseRequestFiles = purchaseRequestEntity.PurchaseRequestFiles,
                PurchaseRequestNotes = purchaseRequestEntity.PurchaseRequestNotes
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(NewPrAggregate), aggregate }
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}