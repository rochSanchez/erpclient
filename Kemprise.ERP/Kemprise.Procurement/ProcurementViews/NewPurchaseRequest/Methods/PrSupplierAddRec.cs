﻿using ERP.WebApi.DTO.DTOs.Procurement.NewPurchaseRequest;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.NewPurchaseRequest.Methods
{
    public class PrSupplierAddRec : ErpSaveMethodCallBase
    {
        public PrSupplierAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "PrSupplierAddRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override ApiControllers Controller => ApiControllers.NewPurchaseRequest;
        public override string Category => ProcurementTypes.NewPurchaseRequest.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var newPrEntity = entity.As<NewPrEntity>();
            var prDto = newPrEntity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();

            var prSupplierDto = newPrEntity.PrSupplierDto.GetDto<PrSupplierDto>();
            prSupplierDto.PurchaseRequestId = prDto.PurchaseRequestId;

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(PrSupplierDto), prSupplierDto }
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}