﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Procurement.NewPurchaseRequest;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.NewPurchaseRequest.Methods
{
    public class NewPrGetRec : GetMethodCallBase
    {
        public NewPrGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "NewPrGetRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override string Category => ProcurementTypes.NewPurchaseRequest.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.NewPurchaseRequest;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<NewPrAggregate>(MethodName, new object[] { MethodParameter });
            if (aggregate == null) return;

            if (aggregate.PurchaseRequestDto != null)
                entity.SetProperty("PurchaseRequestDto", new DynamicNotifyPropertyChangedProxy(aggregate.PurchaseRequestDto));

            if (aggregate.PurchaseRequestDetails.Any())
            {
                var orderedList = aggregate.PurchaseRequestDetails.OrderBy(x => x.OrderNumber);

                var details = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
                var prDetailsToConvert = new ObservableCollection<PurchaseRequestDetailDto>();

                orderedList.ForEach(x =>
                {
                    details.Add(new DynamicNotifyPropertyChangedProxy(x));

                    if (x.IsPurchaseOrderCreated) return;
                    if (x.PartId != null && x.PartId != 0)
                        prDetailsToConvert.Add(x);
                });

                entity.SetProperty("PurchaseRequestDetails", details);
                entity.SetProperty("PrDetailsToConvert", prDetailsToConvert);
            }

            if (aggregate.PurchaseRequestFiles.Any())
            {
                var files = new ObservableCollection<FileDto>(aggregate.PurchaseRequestFiles);
                entity.SetProperty("PurchaseRequestFiles", files);
            }

            if (aggregate.PurchaseRequestNotes.Any())
            {
                var notes = aggregate.PurchaseRequestNotes.OrderBy(x => x.Item);
                var prNotes = new ObservableCollection<NoteDto>(notes);
                entity.SetProperty("PurchaseRequestNotes", prNotes);
            }

            if (aggregate.PrSuppliers.Any())
            {
                var prSuppliers = new ObservableCollection<PrSupplierAggregate>(aggregate.PrSuppliers);
                foreach (var prSupplier in prSuppliers)
                {
                    foreach (var prQuotation in prSupplier.PrQuotations)
                    {
                        var orderedList = prQuotation.PrQuotationDetails.OrderBy(x => x.OrderNumber);
                        prQuotation.PrQuotationDetails = new ObservableCollection<PrQuotationDetailDto>(orderedList);
                    }
                }

                entity.SetProperty("PrSuppliers", prSuppliers);
            }
        }
    }
}