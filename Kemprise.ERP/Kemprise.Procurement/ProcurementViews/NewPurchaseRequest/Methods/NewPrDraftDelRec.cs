﻿using ERP.WebApi.DTO.DTOs.Procurement.NewPurchaseRequest;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.NewPurchaseRequest.Methods
{
    public class NewPrDraftDelRec : ErpSaveMethodCallBase
    {
        public NewPrDraftDelRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "NewPrDraftDelRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override ApiControllers Controller => ApiControllers.NewPurchaseRequest;
        public override string Category => ProcurementTypes.NewPurchaseRequest.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var purchaseRequestEntity = entity.As<NewPrEntity>();
            var purchaseRequestDetails = new List<PurchaseRequestDetailDto>();

            purchaseRequestEntity.PurchaseRequestDetails.ForEach(x => purchaseRequestDetails.Add(x.GetDto<PurchaseRequestDetailDto>()));

            var aggregate = new NewPrAggregate
            {
                PurchaseRequestDto = purchaseRequestEntity.PurchaseRequestDto.GetDto<PurchaseRequestDto>(),
                PurchaseRequestDetails = purchaseRequestDetails,
                PurchaseRequestFiles = purchaseRequestEntity.PurchaseRequestFiles,
                PurchaseRequestNotes = purchaseRequestEntity.PurchaseRequestNotes
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(NewPrAggregate), aggregate }
            };

            await UpdateRecordAsync(MethodName, dataToSave);
        }
    }
}