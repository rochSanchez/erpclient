﻿using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Procurement.NewPurchaseRequest;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseOrder;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using ERP.WebApi.DTO.Lookups.LookupItems;
using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.Infrastructure.Services.Procurement;
using Kemprise.Infrastructure.Windows.AskForRevision;
using Kemprise.Infrastructure.Windows.CloseEntity;
using Kemprise.Infrastructure.Windows.ReasonForClosing;
using Kemprise.Infrastructure.Windows.ReasonForRevision;
using Kemprise.Procurement.ProcurementViews.NewPurchaseRequest.NewPrViews;
using Kemprise.Procurement.ProcurementViews.PurchaseOrder;
using Kemprise.Procurement.WorkArea;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Procurement.ProcurementViews.NewPurchaseRequest
{
    public sealed class NewPrWorkAreaViewModel : ProcurementViewModelBase
    {
        private NewPrEntity _entity;
        private readonly IFileService _fileService;

        public NewPrWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, ProcurementTypes.NewPurchaseRequest.ToString())
        {
            _fileService = fileService;
            SetView(NewPrViewNames.NewPrMainView);

            OpenCommand = new DelegateCommand(OpenExecute);
            BackCommand = new DelegateCommand(BackExecute);
            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            AddExistingItemCommand = new DelegateCommand(AddExistingItemExecute);
            AddBlankItemCommand = new DelegateCommand(AddBlankItemExecute);
            CancelCommand = new DelegateCommand(CancelExecute);
            ClearCommand = new DelegateCommand(ClearExecute);
            DiscardDraftCommand = new DelegateCommand(DiscardDraftExecute);
            SaveAsDraftCommand = new DelegateCommand(SaveAsDraftExecute);
            PrintDraftCommand = new DelegateCommand(PrintDraftExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSave);
            CloseEntityCommand = new DelegateCommand(CloseEntityExecute);
            ViewClosingDetailsCommand = new DelegateCommand(ViewClosingDetailsExecute);
            AskForRevisionCommand = new DelegateCommand(AskForRevisionExecute);
            ViewReasonForRevisionCommand = new DelegateCommand(ViewReasonForRevisionExecute);
            SaveForLaterCommand = new DelegateCommand(SaveForLaterExecute);
            EditAsCommand = new DelegateCommand(EditAsExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshNewPrExecute);

            UploadFileCommand = new DelegateCommand(UploadFileExecute);
            DownloadFileCommand = new DelegateCommand(DownloadFileExecute);
            AddNoteCommand = new DelegateCommand(AddNoteExecute);
            ApprovePurchaseRequestCommand = new DelegateCommand(ApprovePurchaseRequestExecute);
            AddSupplierCommand = new DelegateCommand(AddSupplierExecute);
            SaveSupplierCommand = new DelegateCommand(SaveSupplierExecute, CanSaveSupplier);
            UploadPrFileCommand = new DelegateCommand(UploadPrFileExecute);
            DownloadPrFileCommand = new DelegateCommand(DownloadPrFileExecute);
            SaveQuotationCommand = new DelegateCommand(SaveQuotationExecute, CanSaveQuotation);
            SortedPrCommand = new DelegateCommand(() => SelectedPrQuotation = null);
            FilteredPrCommand = new DelegateCommand(() => SelectedPrQuotation = null);
            SortedItemCommand = new DelegateCommand(() => SelectedDetail = null);
            FilteredItemCommand = new DelegateCommand(() => SelectedDetail = null);
            ViewQuotesReceivedCommand = new DelegateCommand(ViewQuotesReceivedExecute);
            SortedQuoteCommand = new DelegateCommand(() => SelectedQuotationToAccept = null);
            FilteredQuoteCommand = new DelegateCommand(() => SelectedQuotationToAccept = null);
            EditQuotationCommand = new DelegateCommand(EditQuotationExecute);
            AcceptQuotationCommand = new DelegateCommand(AcceptQuotationExecute);
            UnAcceptQuotationCommand = new DelegateCommand(UnAcceptQuotationExecute, CanUnAcceptQuotation);
            AddPartNumberCommand = new DelegateCommand(AddPartNumberExecute);
            ConvertToPurchaseOrderCommand = new DelegateCommand(ConvertToPurchaseOrderExecute, CanConvertToPurchaseOrder);
            ConvertQuoteToPoCommand = new DelegateCommand(ConvertToPurchaseOrderExecute, CanConvertQuoteToPo);
            DeleteItemCommand = new DelegateCommand<object>(DeleteItemExecute);
            DeleteFileCommand = new DelegateCommand<object>(DeleteFileExecute);
            DeleteNoteCommand = new DelegateCommand<object>(DeleteNoteExecute);
            PrintRfqCommand = new DelegateCommand<object>(PrintRfqExecute, CanPrintRfq);
            AddQuotationCommand = new DelegateCommand<object>(AddQuotationExecute);
            DeletePrFileCommand = new DelegateCommand<object>(DeletePrFileExecute);
            ViewQuotationCommand = new DelegateCommand<object>(ViewQuotationExecute);
            FilteredDetailsCommand = new DelegateCommand<object>(FilteredDetailsExecute);

            SelectedFilter = new KeyValuePair<string, string>("ALL", "ALL");
            Methods["NewPrGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        public DelegateCommand UploadFileCommand { get; set; }
        public DelegateCommand DownloadFileCommand { get; set; }
        public DelegateCommand AddNoteCommand { get; set; }
        public DelegateCommand ApprovePurchaseRequestCommand { get; set; }
        public DelegateCommand AddSupplierCommand { get; set; }
        public DelegateCommand SaveSupplierCommand { get; set; }
        public DelegateCommand UploadPrFileCommand { get; set; }
        public DelegateCommand DownloadPrFileCommand { get; set; }
        public DelegateCommand SaveQuotationCommand { get; set; }
        public DelegateCommand SortedPrCommand { get; set; }
        public DelegateCommand FilteredPrCommand { get; set; }
        public DelegateCommand SortedItemCommand { get; set; }
        public DelegateCommand FilteredItemCommand { get; set; }
        public DelegateCommand ViewQuotesReceivedCommand { get; set; }
        public DelegateCommand SortedQuoteCommand { get; set; }
        public DelegateCommand FilteredQuoteCommand { get; set; }
        public DelegateCommand EditQuotationCommand { get; set; }
        public DelegateCommand AcceptQuotationCommand { get; set; }
        public DelegateCommand UnAcceptQuotationCommand { get; set; }
        public DelegateCommand AddPartNumberCommand { get; set; }
        public DelegateCommand ConvertToPurchaseOrderCommand { get; set; }
        public DelegateCommand ConvertQuoteToPoCommand { get; set; }
        public DelegateCommand<object> DeleteItemCommand { get; set; }
        public DelegateCommand<object> DeleteFileCommand { get; set; }
        public DelegateCommand<object> DeleteNoteCommand { get; set; }
        public DelegateCommand<object> PrintRfqCommand { get; set; }
        public DelegateCommand<object> AddQuotationCommand { get; set; }
        public DelegateCommand<object> DeletePrFileCommand { get; set; }
        public DelegateCommand<object> ViewQuotationCommand { get; set; }
        public DelegateCommand<object> FilteredDetailsCommand { get; set; }

        public Dictionary<string, string> PurchaseRequestFilters { get; set; } = new Dictionary<string, string>
        {
            {"ALL","ALL" },
            {PurchaseRequestStatus.SubmittedForApproval, PurchaseRequestStatus.SubmittedForApproval },
            {PurchaseRequestStatus.RevisionRequested, PurchaseRequestStatus.RevisionRequested },
            {PurchaseRequestStatus.RevisionPending, PurchaseRequestStatus.RevisionPending },
            {PurchaseRequestStatus.Approved, PurchaseRequestStatus.Approved },
            {PurchaseRequestStatus.PurchaseOrderCreated, PurchaseRequestStatus.PurchaseOrderCreated },
            {PurchaseRequestStatus.Superseded, PurchaseRequestStatus.Superseded },
            {PurchaseRequestStatus.Closed, PurchaseRequestStatus.Closed }
        };

        private void WhenGetRecDone()
        {
            if (_entity.IsNew)
            {
                _entity.PurchaseRequestDto.SetProperty("DateCreated", DateTime.Now);
                PurchaseRequestPropertyChange();
            }

            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            IsCloseAndRevisePurchaseRequestVisible = (purchaseRequestDto.Status == PurchaseRequestStatus.SubmittedForApproval ||
                                                      purchaseRequestDto.Status == PurchaseRequestStatus.Approved)
                                                     && ApiSession.HasRoles(ErpRoles.CreatePurchaseOrder);
            IsApproveVisible = purchaseRequestDto.Status == PurchaseRequestStatus.SubmittedForApproval
                               && ApiSession.HasRoles(ErpRoles.CreatePurchaseOrder);
            IsViewDetailsVisible = purchaseRequestDto.Status == PurchaseRequestStatus.Closed ||
                                   purchaseRequestDto.Status == PurchaseRequestStatus.Superseded;
            IsViewReasonVisible = purchaseRequestDto.Status == PurchaseRequestStatus.RevisionRequested ||
                                  purchaseRequestDto.Status == PurchaseRequestStatus.RevisionPending;
            IsAddSupplierVisible = purchaseRequestDto.Status == PurchaseRequestStatus.Approved;
            IsConvertToPurchaseOrderVisible = purchaseRequestDto.Status == PurchaseRequestStatus.Approved
                                              && ApiSession.HasRoles(ErpRoles.CreatePurchaseOrder)
                                              && _entity.PurchaseRequestDetails.Any(x =>
                                                  !x.GetDto<PurchaseRequestDetailDto>().IsPurchaseOrderCreated);
            CheckForConversion();
        }

        private void PurchaseRequestPropertyChange()
        {
            _entity.PurchaseRequestDto.PropertyChanged += (s, e) => SaveCommand.RaiseCanExecuteChanged();
            _entity.PurchaseRequestDetails.CollectionChanged += (s, e) =>
            {
                SaveCommand.RaiseCanExecuteChanged();
                SetNumbering();
            };

            SaveCommand.RaiseCanExecuteChanged();
        }

        private void SetNumbering()
        {
            var count = 1;
            _entity.PurchaseRequestDetails.ForEach(x =>
            {
                x.SetProperty("OrderNumber", count);
                count++;
            });
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new NewPrEntity());
            _entity = Entity.GetDto<NewPrEntity>();

            OperationResponse result;
            var purchaseRequestStatus = SelectedItem.As<PurchaseRequestListItem>().Status;
            switch (purchaseRequestStatus)
            {
                case PurchaseRequestStatus.Draft:
                    _entity.IsNew = true;

                    result = await OpenItemExecute("");
                    if (result.OperationResult != OperationResult.Success) return;

                    Methods["NewPrDraftGetRec"].MethodParameter = SelectedItem.Id;
                    await GetRecordAsync("NewPrDraftGetRec");
                    WhenGetRecDone();

                    IsClearVisible = true;
                    SetView(NewPrViewNames.NewPrEditView);
                    break;

                case PurchaseRequestStatus.RevisionRequested:
                case PurchaseRequestStatus.RevisionPending:
                    _entity.IsNew = true;

                    result = await OpenItemExecute(SelectedItem.Id, true);
                    if (result.OperationResult != OperationResult.Success) return;

                    IsClearVisible = false;
                    SetView(NewPrViewNames.NewPrEditView);
                    break;

                default:
                    result = await OpenItemExecute(SelectedItem.Id);
                    if (result.OperationResult != OperationResult.Success) return;

                    SetView(NewPrViewNames.NewPrDisplayView);
                    break;
            }
        }

        private void BackExecute()
        {
            RefreshExecute();
            SetView(NewPrViewNames.NewPrMainView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<ProcurementWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Purchase Requests"));
        }

        private async void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new NewPrEntity());
            _entity = Entity.GetDto<NewPrEntity>();
            _entity.IsNew = true;

            await OpenItemExecute("");

            IsClearVisible = true;
            SetView(NewPrViewNames.NewPrEditView);
        }

        private void AddExistingItemExecute()
        {
            AddPartCommand = new DelegateCommand(AddPartExecute);

            var partNumberView = new CollectionViewSource { Source = Lookups["Parts"] };
            PartCollectionView = partNumberView;

            PartWindow = new NewPrWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };

            SelectedPart = null;
            SearchForPartNumber = "";
            SearchForDescription = "";
            PartWindow.ShowDialog();
        }

        private void AddBlankItemExecute()
        {
            var newProxy = new DynamicNotifyPropertyChangedProxy(new PurchaseRequestDetailDto { PartNumber = "TBA" });
            _entity.PurchaseRequestDetails.Add(newProxy);
        }

        private void AddPartExecute()
        {
            if (SelectedPart == null) return;

            var partId = Convert.ToInt32(SelectedPart.Id);
            var uom = Lookups["UnitOfMeasurements"].FirstOrDefault(x => x.Id == SelectedPart.UnitOfMeasurementId.ToString());
            var newDetail = new PurchaseRequestDetailDto
            {
                PartId = partId,
                PartNumber = SelectedPart.PartNumber,
                UnitOfMeasurement = uom?.Text ?? "",
                Description = SelectedPart.Text
            };
            var newProxy = new DynamicNotifyPropertyChangedProxy(newDetail);

            _entity.PurchaseRequestDetails.Add(newProxy);
            CancelExecute();
        }

        private void CancelExecute()
        {
            PartWindow?.Close();
            AddSupplierWindow?.Close();
            AddQuotationWindow?.Close();
            ViewQuotationWindow?.Close();
            QuotesReceivedWindow?.Close();
        }

        private void ClearExecute()
        {
            RadWindow.Confirm("This will clear everything. Continue?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.PurchaseRequestDto.ClearValues();
                _entity.PurchaseRequestDetails.Clear();
                _entity.PurchaseRequestFiles.Clear();
                _entity.PurchaseRequestNotes.Clear();

                _entity.PurchaseRequestDto.SetProperty("DateCreated", DateTime.Now);
            });
        }

        private void DiscardDraftExecute()
        {
            RadWindow.Confirm("Discard Purchase Request?", async (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
                if (purchaseRequestDto.PurchaseRequestId != 0)
                    await SaveRecordAsync("NewPrDraftDelRec");

                BackExecute();
            });
        }

        private async void SaveAsDraftExecute()
        {
            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            if (string.IsNullOrEmpty(purchaseRequestDto.Purpose) || !_entity.PurchaseRequestDetails.Any())
            {
                EventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload($"The Following Details are Required {Environment.NewLine} {Environment.NewLine}" +
                                                   $"• Purpose {Environment.NewLine}" +
                                                   "• At least 1 Item to Purchase"));
                return;
            }

            _entity.PurchaseRequestDto.SetProperty("Status", PurchaseRequestStatus.Draft);

            var taskToPerform = purchaseRequestDto.PurchaseRequestId == 0
                ? "NewPrDraftAddRec"
                : "NewPrDraftModiRec";

            await SaveRecordAsync(taskToPerform);
        }

        private void PrintDraftExecute()
        {
            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            var prSupplierDto = new PrSupplierDto
            {
                DateCreated = purchaseRequestDto.DateCreated,
                DateRequired = purchaseRequestDto.DateRequired
            };

            var proxy = new DynamicNotifyPropertyChangedProxy(prSupplierDto);
            Entity.SetProperty("PrSupplierDto", proxy);

            RunReport("RfqDraft");
        }

        private async void SaveExecute()
        {
            if (_entity.PurchaseRequestDetails.Any(x => x.GetDto<PurchaseRequestDetailDto>().Quantity == 0))
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Item/s without Quantity Found"));
                return;
            }

            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            var taskToPerform = purchaseRequestDto.Status == PurchaseRequestStatus.RevisionPending
                ? "NewPrModiRec"
                : "NewPrAddRec";

            _entity.PurchaseRequestDto.SetProperty("Status", PurchaseRequestStatus.SubmittedForApproval);

            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSave()
        {
            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();

            return !string.IsNullOrEmpty(purchaseRequestDto.Purpose)
                   && purchaseRequestDto.DateRequired != null
                   && _entity.PurchaseRequestDetails.Any();
        }

        private void CloseEntityExecute()
        {
            RadWindow.Confirm("Cancel Purchase Request?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var closeWindow = Container.Resolve<CloseEntityWindow>();
                var viewModel = closeWindow.GetViewModel<CloseEntityWindowViewModel>();

                viewModel.Reasons = Lookups["Reasons"];
                viewModel.SetPropertyToDto = () =>
                {
                    _entity.PurchaseRequestDto.SetProperty("ReasonForClosing", viewModel.SelectedReasonText.ToUpper());
                    _entity.PurchaseRequestDto.SetProperty("DetailedReasonForClosing", viewModel.DetailedReasonForClosing);
                };
                viewModel.CompletedAction = ClosePurchaseRequest;

                closeWindow.Owner = Application.Current.MainWindow;
                closeWindow.ShowDialog();
            });
        }

        private async void ClosePurchaseRequest()
        {
            _entity.PurchaseRequestDto.SetProperty("Status", PurchaseRequestStatus.Closed);
            await SaveRecordAsync("NewPrModiRec");
            BackExecute();
        }

        private void ViewClosingDetailsExecute()
        {
            var closedWindow = Container.Resolve<ReasonForClosingWindow>();
            var viewModel = closedWindow.GetViewModel<ReasonForClosingWindowViewModel>();

            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            viewModel.ReasonForClosing = purchaseRequestDto.ReasonForClosing;
            viewModel.DetailedReasonForClosing = purchaseRequestDto.DetailedReasonForClosing;

            closedWindow.Owner = Application.Current.MainWindow;
            closedWindow.ShowDialog();
        }

        private void AskForRevisionExecute()
        {
            RadWindow.Confirm("Ask For Revision?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var revisionWindow = Container.Resolve<AskForRevisionWindow>();
                var viewModel = revisionWindow.GetViewModel<AskForRevisionWindowViewModel>();

                viewModel.SetPropertyToDto = () => _entity.PurchaseRequestDto.SetProperty("RevisionNotes", viewModel.RevisionNotes);
                viewModel.CompletedAction = AskForRevision;

                revisionWindow.Owner = Application.Current.MainWindow;
                revisionWindow.ShowDialog();
            });
        }

        private async void AskForRevision()
        {
            _entity.PurchaseRequestDto.SetProperty("Status", PurchaseRequestStatus.RevisionRequested);
            await SaveRecordAsync("NewPrModiRec");
            BackExecute();
        }

        private void ViewReasonForRevisionExecute()
        {
            var revisionWindow = Container.Resolve<ReasonForRevisionWindow>();
            var viewModel = revisionWindow.GetViewModel<ReasonForRevisionWindowViewModel>();
            viewModel.RevisionNotes = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>().RevisionNotes;

            revisionWindow.Owner = Application.Current.MainWindow;
            revisionWindow.ShowDialog();
        }

        private async void SaveForLaterExecute()
        {
            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            if (purchaseRequestDto.Status == PurchaseRequestStatus.RevisionRequested)
            {
                var saveResult = await SaveRecordAsync("NewPrAddRec");
                if (saveResult.OperationResult != OperationResult.Success) return;

                BackExecute();
            }

            if (purchaseRequestDto.Status != PurchaseRequestStatus.RevisionPending) return;
            await SaveRecordAsync("NewPrModiRec");

            BackExecute();
        }

        private void EditAsExecute()
        {
            _entity.IsNew = true;

            var oldPurchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            var newPurchaseRequestDto = oldPurchaseRequestDto.ToNewPurchaseRequestDto();
            Entity.SetProperty("PurchaseRequestDto", new DynamicNotifyPropertyChangedProxy(newPurchaseRequestDto));
            _entity.PurchaseRequestDetails.ForEach(x =>
            {
                x.ClearPropertyValue("Mismatched");
                x.ClearPropertyValue("MismatchedDescription");
                x.ClearPropertyValue("UnitPrice");
                x.ClearPropertyValue("AcceptedSupplier");
                x.ClearPropertyValue("PrQuotationDetailId");
                x.ClearPropertyValue("IsPurchaseOrderCreated");
                x.ClearPropertyValue("PoCreatedDate");
            });
            PurchaseRequestPropertyChange();

            SetView(NewPrViewNames.NewPrEditView);
        }

        private void RefreshNewPrExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        private void UploadFileExecute()
        {
            _fileService.GetAnyFile(_entity.PurchaseRequestFiles);
        }

        private void DownloadFileExecute()
        {
            if (SelectedFile == null) return;
            _fileService.SaveFile(SelectedFile);
        }

        private void AddNoteExecute()
        {
            _entity.PurchaseRequestNotes.Add(new NoteDto());
        }

        private void ApprovePurchaseRequestExecute()
        {
            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            if (ApiSession.GetId() == purchaseRequestDto.CreatedById)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Unable to Approve your own Purchase Request"));
                return;
            }

            RadWindow.Confirm("Approve Purchase Request?", async (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.PurchaseRequestDto.SetProperty("Status", PurchaseRequestStatus.Approved);
                await SaveRecordAsync("NewPrModiRec");

                BackExecute();
            });
        }

        private void AddSupplierExecute()
        {
            SelectedSupplier = null;

            Entity.SetProperty("PrSupplierDto", new DynamicNotifyPropertyChangedProxy(new PrSupplierDto()));
            _entity.PrSupplierDto.PropertyChanged += (s, e) => { SaveSupplierCommand.RaiseCanExecuteChanged(); };

            AddSupplierWindow = new AddSupplierWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };
            AddSupplierWindow.ShowDialog();
        }

        private async void SaveSupplierExecute()
        {
            CancelExecute();

            var existingSupplier = _entity.PrSuppliers
                .Select(x => x.PrSupplierDto)
                .FirstOrDefault(x => $"{x.SupplierId}" == SelectedSupplier.Id);
            if (existingSupplier != null)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Supplier Already Added"));
                return;
            }

            var addRecResult = await SaveRecordAsync("PrSupplierAddRec");
            if (addRecResult.OperationResult != OperationResult.Success) return;

            await GetRecordAsync("NewPrGetRec");
        }

        private bool CanSaveSupplier()
        {
            var prSupplierDto = _entity.PrSupplierDto.GetDto<PrSupplierDto>();
            return prSupplierDto.SupplierId != 0 && SelectedSupplier != null;
        }

        private void UploadPrFileExecute()
        {
            _fileService.GetAnyFile(_entity.PrQuotationFiles);
        }

        private void DownloadPrFileExecute()
        {
            if (SelectedPrFile == null) return;
            _fileService.SaveFile(SelectedPrFile);
        }

        private async void SaveQuotationExecute()
        {
            CancelExecute();

            var addRecResult = await SaveRecordAsync("PrQuotationAddRec");
            if (addRecResult.OperationResult != OperationResult.Success) return;

            await GetRecordAsync("NewPrGetRec");
        }

        private bool CanSaveQuotation()
        {
            var prQuotationDto = _entity.PrQuotationDto.GetDto<PrQuotationDto>();
            return prQuotationDto.DateReceived != null
                   && prQuotationDto.CurrencyId != null
                   && prQuotationDto.CurrencyId != 0;
        }

        private void ViewQuotesReceivedExecute()
        {
            if (SelectedDetail == null) return;

            _entity.QuotesReceived.Clear();

            var detail = SelectedDetail.GetDto<PurchaseRequestDetailDto>();
            _entity.PrSuppliers.ForEach(prSupplier =>
            {
                prSupplier.PrQuotations.ForEach(prQuotation =>
                {
                    prQuotation.PrQuotationDetails.ForEach(prQuoteDetail =>
                    {
                        if (prQuoteDetail.PurchaseRequestDetailId == detail.PurchaseRequestDetailId &&
                            prQuoteDetail.UnitPrice != 0)
                            _entity.QuotesReceived.Add(prQuoteDetail);
                    });
                });
            });
            CanAcceptPrQuotation = !detail.IsPurchaseOrderCreated;

            QuotesReceivedWindow = new QuotesReceivedWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };
            QuotesReceivedWindow.ShowDialog();
        }

        private void EditQuotationExecute()
        {
            _entity.PrDetailsToConvert.Clear();
            CancelExecute();

            AddQuotationWindow = new AddQuotationWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };
            AddQuotationWindow.ShowDialog();
        }

        private async void AcceptQuotationExecute()
        {
            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            if (purchaseRequestDto.Status != PurchaseRequestStatus.Approved) return;

            if (SelectedQuotationToAccept == null || !CanAcceptPrQuotation) return;

            CancelExecute();
            _entity.QuotesReceived.ForEach(x => x.IsAccepted =
                x.PrQuotationDetailId == SelectedQuotationToAccept.PrQuotationDetailId);

            var saveResult = await SaveRecordAsync("PrQuoteDetailModiRec");
            if (saveResult.OperationResult != OperationResult.Success) return;

            await GetRecordAsync("NewPrGetRec");
        }

        private async void UnAcceptQuotationExecute()
        {
            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            if (purchaseRequestDto.Status != PurchaseRequestStatus.Approved) return;

            if (!CanAcceptPrQuotation) return;
            CancelExecute();

            _entity.QuotesReceived.ForEach(x => x.Unaccept = true);

            var saveResult = await SaveRecordAsync("PrQuoteDetailModiRec");
            if (saveResult.OperationResult != OperationResult.Success) return;

            await GetRecordAsync("NewPrGetRec");
        }

        private bool CanUnAcceptQuotation()
        {
            return _entity.QuotesReceived.Any(x => x.IsAccepted);
        }

        private void AddPartNumberExecute()
        {
            AddPartCommand = new DelegateCommand(AddPartNumber);

            var partNumberView = new CollectionViewSource { Source = Lookups["Parts"] };
            PartCollectionView = partNumberView;

            PartWindow = new NewPrWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };

            SelectedPart = null;
            SearchForPartNumber = "";
            SearchForDescription = "";
            PartWindow.ShowDialog();
        }

        private async void AddPartNumber()
        {
            if (SelectedPart == null) return;
            if (SelectedDetail == null) return;

            var uom = Lookups["UnitOfMeasurements"].FirstOrDefault(x => x.Id == SelectedPart.UnitOfMeasurementId.ToString());
            SelectedDetail.SetProperty("PartId", Convert.ToInt32(SelectedPart.Id));
            SelectedDetail.SetProperty("PartNumber", SelectedPart.PartNumber);
            SelectedDetail.SetProperty("UnitOfMeasurement", uom?.Text ?? "");
            SelectedDetail.SetProperty("Description", SelectedPart.Text);

            CancelExecute();

            var prDetailDto = SelectedDetail.GetDto<PurchaseRequestDetailDto>();
            Entity.SetProperty("PrDetailToSave", prDetailDto);

            var result = await SaveRecordAsync("PrDetailModiRec");
            if (result.OperationResult != OperationResult.Success) return;

            await GetRecordAsync("NewPrGetRec");
        }

        private void ConvertToPurchaseOrderExecute()
        {
            CancelExecute();

            PoWorkAreaView = Container.Resolve<PurchaseOrderWorkAreaView>();
            PoWorkAreaViewModel = PoWorkAreaView.GetViewModel<PurchaseOrderWorkAreaViewModel>();

            PoWorkAreaViewModel.BackCommand = new DelegateCommand(BackPoExecute);
            PoWorkAreaViewModel.SaveForLaterCommand = new DelegateCommand(SaveForLaterPoExecute);
            PoWorkAreaViewModel.SaveCommand = new DelegateCommand(SavePurchaseOrderExecute, CanSavePurchaseOrder);

            PoWorkAreaViewModel.PurchaseRequestAddNewRecord();

            var entity = PoWorkAreaViewModel.Entity;
            var poEntity = entity.GetDto<PurchaseOrderEntity>();

            var prDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            var prSupplierDto = _entity.PrSupplierDto.GetDto<PrSupplierDto>();
            var prQuotationDto = _entity.PrQuotationDto.GetDto<PrQuotationDto>();

            var purchaseOrderDto = prDto.ToPurchaseOrder(prSupplierDto, prQuotationDto);
            var purchaseOrderProxy = new DynamicNotifyPropertyChangedProxy(purchaseOrderDto);

            entity.SetProperty("PurchaseOrderDto", purchaseOrderProxy);
            PoWorkAreaViewModel.SetPoPropertyChange(null, true);

            var poDetails = _entity.PrDetailsToConvert.ToPurchaseOrderDetails();
            poDetails.ForEach(detail =>
            {
                var newDetail = new DynamicNotifyPropertyChangedProxy(detail);
                poEntity.PurchaseOrderDetails.Add(newDetail);
            });
            PoWorkAreaViewModel.SetPoDetailPropertyChange();
            PoWorkAreaViewModel.SetNumbering();

            _entity.PrQuotationFiles.ForEach(poEntity.PurchaseOrderFiles.Add);
            _entity.PurchaseRequestFiles.ForEach(poEntity.PurchaseOrderFiles.Add);
            _entity.PurchaseRequestNotes.ForEach(poEntity.PurchaseOrderNotes.Add);

            PoWorkAreaViewModel.SetSaveCanExecuteChanged();
            PoWorkAreaViewModel.ComputeTotal();
            SetView(NewPrViewNames.NewPrConvertToPoView);
        }

        private void BackPoExecute()
        {
            _entity.PrDetailsToConvert.Clear();
            ConvertToPurchaseOrderCommand.RaiseCanExecuteChanged();
            SetView(NewPrViewNames.NewPrDisplayView);
        }

        private async void SaveForLaterPoExecute()
        {
            var poEntity = PoWorkAreaViewModel.Entity.GetDto<PurchaseOrderEntity>();
            poEntity.PurchaseOrderDto.SetProperty("Status", PurchaseOrderStatus.RevisionPending);

            var saveResult = await PoWorkAreaViewModel.SaveRecordAsync("PurchaseOrderAddRec");
            if (saveResult.OperationResult != OperationResult.Success) return;

            await GetRecordAsync("NewPrGetRec");
            SetView(NewPrViewNames.NewPrDisplayView);
        }

        private async void SavePurchaseOrderExecute()
        {
            var poEntity = PoWorkAreaViewModel.Entity.GetDto<PurchaseOrderEntity>();
            if (poEntity.PurchaseOrderDetails.Any(x => x.GetDto<PurchaseOrderDetailDto>().Quantity == 0))
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Item/s without Quantity Found"));
                return;
            }
            if (poEntity.PurchaseOrderDetails.Any(x => x.GetDto<PurchaseOrderDetailDto>().Price == 0))
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Item/s without Price Found"));
                return;
            }

            poEntity.PurchaseOrderDto.SetProperty("Status", PurchaseOrderStatus.SubmittedForApproval);

            var saveResult = await PoWorkAreaViewModel.SaveRecordAsync("PurchaseOrderAddRec");
            if (saveResult.OperationResult != OperationResult.Success) return;

            await GetRecordAsync("NewPrGetRec");
            SetView(NewPrViewNames.NewPrDisplayView);
        }

        private bool CanSavePurchaseOrder()
        {
            var poEntity = PoWorkAreaViewModel.Entity.GetDto<PurchaseOrderEntity>();
            var poDto = poEntity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();

            return !string.IsNullOrEmpty(poDto.Description)
                   && !string.IsNullOrEmpty(poDto.PaymentTerms)
                   && poEntity.PurchaseOrderDetails.Any()
                   && poDto.DateRequired != null
                   && poDto.CurrencyId != 0
                   && poDto.SupplierId != 0
                   && poDto.Total != 0;
        }

        private bool CanConvertToPurchaseOrder()
        {
            if (!_entity.PrDetailsToConvert.Any())
                return false;

            if (_entity.PrDetailsToConvert.Any(x => string.IsNullOrEmpty(x.AcceptedSupplier)))
                return false;

            var supplierName = _entity.PrDetailsToConvert
                .Select(x => x.AcceptedSupplier)
                .FirstOrDefault();

            return _entity.PrDetailsToConvert.All(x => x.AcceptedSupplier == supplierName);
        }

        private bool CanConvertQuoteToPo()
        {
            return _entity.PrDetailsToConvert.Any();
        }

        private void DeleteItemExecute(object obj)
        {
            if (SelectedDetail == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Remove Item?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.PurchaseRequestDetails.Remove(SelectedDetail);
                    SetNumbering();
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Remove Item?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                    {
                        _entity.PurchaseRequestDetails.Remove(SelectedDetail);
                        SetNumbering();
                    }
                });
            }
        }

        private void DeleteFileExecute(object obj)
        {
            if (SelectedFile == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.PurchaseRequestFiles.Remove(SelectedFile);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.PurchaseRequestFiles.Remove(SelectedFile);
                });
            }
        }

        private void DeleteNoteExecute(object obj)
        {
            if (SelectedNote == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete Note?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.PurchaseRequestNotes.Remove(SelectedNote);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete Note?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.PurchaseRequestNotes.Remove(SelectedNote);
                });
            }
        }

        private void PrintRfqExecute(object obj)
        {
            SelectedPrSupplier = null;
            if (obj == null || (int)obj == 0) return;

            var prSupplierDto = _entity.PrSuppliers
                .Select(x => x.PrSupplierDto)
                .FirstOrDefault(x => x.PrSupplierId == (int)obj);
            Entity.SetProperty("PrSupplierDto", new DynamicNotifyPropertyChangedProxy(prSupplierDto));

            RunReport("NewPrRequestForRfq");
        }

        private bool CanPrintRfq(object obj)
        {
            var purchaseRequest = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            return purchaseRequest.Status != PurchaseRequestStatus.Closed &&
                   purchaseRequest.Status != PurchaseRequestStatus.Superseded;
        }

        private void AddQuotationExecute(object obj)
        {
            SelectedPrSupplier = null;
            if (obj == null || (int)obj == 0) return;

            var prSupplierDto = _entity.PrSuppliers
                .Select(x => x.PrSupplierDto)
                .FirstOrDefault(x => x.PrSupplierId == (int)obj);
            if (prSupplierDto == null) return;

            var prQuotationDto = new PrQuotationDto { PrSupplierId = prSupplierDto.PrSupplierId };
            var newProxy = new DynamicNotifyPropertyChangedProxy(prQuotationDto);
            newProxy.PropertyChanged += (s, e) => SaveQuotationCommand.RaiseCanExecuteChanged();

            Entity.SetProperty("PrSupplierDto", new DynamicNotifyPropertyChangedProxy(prSupplierDto));
            Entity.SetProperty("PrQuotationDto", newProxy);

            _entity.PrQuotationDetails.Clear();
            _entity.PurchaseRequestDetails.ForEach(x =>
            {
                var detail = x.GetDto<PurchaseRequestDetailDto>();
                _entity.PrQuotationDetails.Add(detail.ToPrQuotationDetail());
            });
            _entity.PrQuotationFiles.Clear();

            AddQuotationWindow = new AddQuotationWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };
            AddQuotationWindow.ShowDialog();
        }

        private void DeletePrFileExecute(object obj)
        {
            if (SelectedPrFile == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.PrQuotationFiles.Remove(SelectedPrFile);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.PrQuotationFiles.Remove(SelectedPrFile);
                });
            }
        }

        private void ViewQuotationExecute(object obj)
        {
            if (SelectedPrQuotation == null) return;

            var eventArgs = obj.As<MouseButtonEventArgs>();
            var gridView = eventArgs?.Source.As<RadGridView>();

            var prQuoteAggregate = gridView?.SelectedItem.As<PrQuotationAggregate>();
            if (prQuoteAggregate != null)
                SetPrSupplierAndQuotation(prQuoteAggregate);

            _entity.PrDetailsToConvert.Clear();
            _entity.PrQuotationDetails.ForEach(quoteDetail =>
            {
                if (quoteDetail.UnitPrice == 0) return;

                var proxy = _entity.PurchaseRequestDetails.FirstOrDefault(x =>
                {
                    var detail = x.GetDto<PurchaseRequestDetailDto>();
                    return detail.PurchaseRequestDetailId == quoteDetail.PurchaseRequestDetailId
                           && detail.PartId != null && detail.PartId != 0
                           && !detail.IsPurchaseOrderCreated;
                });

                var prDetail = proxy?.GetDto<PurchaseRequestDetailDto>();
                if (prDetail == null) return;

                _entity.PrDetailsToConvert.Add(quoteDetail.ToPurchaseRequestDetail());
            });
            ConvertQuoteToPoCommand.RaiseCanExecuteChanged();

            ViewQuotationWindow = new ViewQuotationWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };
            ViewQuotationWindow.ShowDialog();
        }

        private void FilteredDetailsExecute(object obj)
        {
            _entity.PrDetailsToConvert.Clear();

            var eventArgs = obj.As<GridViewFilteredEventArgs>();
            var gridView = eventArgs?.Source.As<RadGridView>();

            var items = gridView?.Items.OfType<DynamicNotifyPropertyChangedProxy>();
            items.ForEach(x =>
            {
                var prDetail = x.GetDto<PurchaseRequestDetailDto>();
                if (prDetail.IsPurchaseOrderCreated) return;

                if (prDetail.PartId != null && prDetail.PartId != 0)
                    _entity.PrDetailsToConvert.Add(prDetail);
            });

            CheckForConversion();
        }

        private void CheckForConversion()
        {
            if (!CanConvertToPurchaseOrder())
            {
                ConvertToPurchaseOrderCommand.RaiseCanExecuteChanged();
                return;
            }

            PrQuotationAggregate prQuoteAggregate = null;
            var latestQuoteDetailId = _entity.PrDetailsToConvert.Max(x => x.PrQuotationDetailId);

            _entity.PrSuppliers.ForEach(prSupplier =>
                prSupplier.PrQuotations.ForEach(prQuotation =>
                {
                    prQuotation.PrQuotationDetails.ForEach(prQuoteDetail =>
                    {
                        if (prQuoteDetail.PrQuotationDetailId == latestQuoteDetailId)
                            prQuoteAggregate = prQuotation;
                    });
                }));

            if (prQuoteAggregate != null)
                SetPrSupplierAndQuotation(prQuoteAggregate);

            ConvertToPurchaseOrderCommand.RaiseCanExecuteChanged();
        }

        private void SetPrSupplierAndQuotation(PrQuotationAggregate prQuoteAggregate)
        {
            var prQuotation = prQuoteAggregate?.PrQuotationDto;
            var details = prQuoteAggregate?.PrQuotationDetails;
            var files = prQuoteAggregate?.PrQuotationFiles;

            if (prQuotation == null) return;
            var prSupplier = _entity.PrSuppliers
                .FirstOrDefault(x => x.PrSupplierDto.PrSupplierId == prQuotation.PrSupplierId)
                .Select(x => x.PrSupplierDto);

            Entity.SetProperty("PrQuotationDto", new DynamicNotifyPropertyChangedProxy(prQuotation));
            Entity.SetProperty("PrSupplierDto", new DynamicNotifyPropertyChangedProxy(prSupplier));

            _entity.PrQuotationDetails.Clear();
            var orderedList = details?.OrderBy(x => x.OrderNumber);
            orderedList.ForEach(x => _entity.PrQuotationDetails.Add(x));

            _entity.PrQuotationFiles.Clear();
            files.ForEach(x => _entity.PrQuotationFiles.Add(x));
        }

        public DynamicNotifyPropertyChangedProxy SelectedDetail
        {
            get { return GetValue(() => SelectedDetail); }
            set
            {
                SetValue(() => SelectedDetail, value);
                if (value == null)
                {
                    IsAddPartNumberVisible = false;
                    return;
                }

                var prDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
                if (prDto.Status != PurchaseRequestStatus.Approved)
                {
                    IsAddPartNumberVisible = false;
                    return;
                }

                var prDetail = value.GetDto<PurchaseRequestDetailDto>();
                if (prDetail.IsPurchaseOrderCreated)
                    IsAddPartNumberVisible = false;
                else
                    IsAddPartNumberVisible = prDetail.PartId == null || prDetail.PartId == 0;
            }
        }

        public FileDto SelectedFile
        {
            get { return GetValue(() => SelectedFile); }
            set { SetValue(() => SelectedFile, value); }
        }

        public NoteDto SelectedNote
        {
            get { return GetValue(() => SelectedNote); }
            set { SetValue(() => SelectedNote, value); }
        }

        public bool IsCloseAndRevisePurchaseRequestVisible
        {
            get { return GetValue(() => IsCloseAndRevisePurchaseRequestVisible); }
            set { SetValue(() => IsCloseAndRevisePurchaseRequestVisible, value); }
        }

        public bool IsApproveVisible
        {
            get { return GetValue(() => IsApproveVisible); }
            set { SetValue(() => IsApproveVisible, value); }
        }

        public bool IsViewDetailsVisible
        {
            get { return GetValue(() => IsViewDetailsVisible); }
            set { SetValue(() => IsViewDetailsVisible, value); }
        }

        public bool IsAddSupplierVisible
        {
            get { return GetValue(() => IsAddSupplierVisible); }
            set { SetValue(() => IsAddSupplierVisible, value); }
        }

        public AddSupplierWindow AddSupplierWindow
        {
            get { return GetValue(() => AddSupplierWindow); }
            set { SetValue(() => AddSupplierWindow, value); }
        }

        public SupplierLookup SelectedSupplier
        {
            get { return GetValue(() => SelectedSupplier); }
            set
            {
                SetValue(() => SelectedSupplier, value);
                if (value == null)
                {
                    _entity.PrSupplierDto.SetProperty("SupplierId", 0);
                    return;
                }

                _entity.PrSupplierDto.SetProperty("SupplierId", Convert.ToInt32(value.Id));
            }
        }

        public PrSupplierAggregate SelectedPrSupplier
        {
            get { return GetValue(() => SelectedPrSupplier); }
            set { SetValue(() => SelectedPrSupplier, value); }
        }

        public AddQuotationWindow AddQuotationWindow
        {
            get { return GetValue(() => AddQuotationWindow); }
            set { SetValue(() => AddQuotationWindow, value); }
        }

        public FileDto SelectedPrFile
        {
            get { return GetValue(() => SelectedPrFile); }
            set { SetValue(() => SelectedPrFile, value); }
        }

        public ViewQuotationWindow ViewQuotationWindow
        {
            get { return GetValue(() => ViewQuotationWindow); }
            set { SetValue(() => ViewQuotationWindow, value); }
        }

        public PrQuotationAggregate SelectedPrQuotation
        {
            get { return GetValue(() => SelectedPrQuotation); }
            set { SetValue(() => SelectedPrQuotation, value); }
        }

        public QuotesReceivedWindow QuotesReceivedWindow
        {
            get { return GetValue(() => QuotesReceivedWindow); }
            set { SetValue(() => QuotesReceivedWindow, value); }
        }

        public bool CanAcceptPrQuotation
        {
            get { return GetValue(() => CanAcceptPrQuotation); }
            set { SetValue(() => CanAcceptPrQuotation, value); }
        }

        public PrQuotationDetailDto SelectedQuotationToAccept
        {
            get { return GetValue(() => SelectedQuotationToAccept); }
            set { SetValue(() => SelectedQuotationToAccept, value); }
        }

        public bool IsUnacceptQuotationVisible
        {
            get { return GetValue(() => IsUnacceptQuotationVisible); }
            set { SetValue(() => IsUnacceptQuotationVisible, value); }
        }

        public bool IsAddPartNumberVisible
        {
            get { return GetValue(() => IsAddPartNumberVisible); }
            set { SetValue(() => IsAddPartNumberVisible, value); }
        }

        public bool IsConvertToPurchaseOrderVisible
        {
            get { return GetValue(() => IsConvertToPurchaseOrderVisible); }
            set { SetValue(() => IsConvertToPurchaseOrderVisible, value); }
        }

        public PurchaseOrderWorkAreaView PoWorkAreaView
        {
            get { return GetValue(() => PoWorkAreaView); }
            set { SetValue(() => PoWorkAreaView, value); }
        }

        public PurchaseOrderWorkAreaViewModel PoWorkAreaViewModel
        {
            get { return GetValue(() => PoWorkAreaViewModel); }
            set { SetValue(() => PoWorkAreaViewModel, value); }
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<ProcurementLookupAggregate>(ApiControllers.Lookup, "ProcurementGetLookups");
        }
    }
}