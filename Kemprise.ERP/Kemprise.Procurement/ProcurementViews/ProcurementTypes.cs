﻿namespace Kemprise.Procurement.ProcurementViews
{
    public enum ProcurementTypes
    {
        PurchaseRequest,
        NewPurchaseRequest,
        PurchaseOrder,
    }
}