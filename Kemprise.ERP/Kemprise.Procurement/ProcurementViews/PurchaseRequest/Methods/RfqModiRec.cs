﻿using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO.Constants;

namespace Kemprise.Procurement.ProcurementViews.PurchaseRequest.Methods
{
    public class RfqModiRec : ErpSaveMethodCallBase
    {
        public RfqModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "RfqModiRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override ApiControllers Controller => ApiControllers.PurchaseRequest;
        public override string Category => ProcurementTypes.PurchaseRequest.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var purchaseRequestEntity = entity.As<PurchaseRequestEntity>();

            var rfqDto = purchaseRequestEntity.RfqDto.GetDto<RfqDto>();
            var files = purchaseRequestEntity.QuotationFiles;
            rfqDto.Status = RfqStatus.QuotationReceived;

            var aggregate = new RfqAggregate
            {
                RfqDto = rfqDto,
                QuotationsReceived = files
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(RfqAggregate), aggregate }
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}