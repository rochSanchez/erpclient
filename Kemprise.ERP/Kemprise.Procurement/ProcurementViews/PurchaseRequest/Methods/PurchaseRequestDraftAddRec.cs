﻿using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.PurchaseRequest.Methods
{
    public class PurchaseRequestDraftAddRec : ErpSaveMethodCallBase
    {
        public PurchaseRequestDraftAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "PurchaseRequestDraftAddRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override ApiControllers Controller => ApiControllers.PurchaseRequest;
        public override string Category => ProcurementTypes.PurchaseRequest.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var purchaseRequestEntity = entity.As<PurchaseRequestEntity>();
            purchaseRequestEntity.PurchaseRequestFiles.ForEach(x => x.FileId = 0);
            purchaseRequestEntity.PurchaseRequestNotes.ForEach(x => x.NoteId = 0);

            var purchaseRequestAggregate = new PurchaseRequestAggregate
            {
                PurchaseRequestDto = purchaseRequestEntity.PurchaseRequestDto.GetDto<PurchaseRequestDto>(),
                PurchaseRequestDetails = purchaseRequestEntity.PurchaseRequestDetails,
                PurchaseRequestFiles = purchaseRequestEntity.PurchaseRequestFiles,
                PurchaseRequestNotes = purchaseRequestEntity.PurchaseRequestNotes
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(PurchaseRequestAggregate), purchaseRequestAggregate }
            };

            var result = await SaveNewRecordAsync<PurchaseRequestAggregate>(MethodName, dataToSave);

            var purchaseRequestId = result.PurchaseRequestDto.PurchaseRequestId;
            purchaseRequestEntity.PurchaseRequestDto.SetProperty("PurchaseRequestId", purchaseRequestId);
        }
    }
}