﻿using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.PurchaseRequest.Methods
{
    public class RfqAddRec : ErpSaveMethodCallBase
    {
        public RfqAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "RfqAddRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override ApiControllers Controller => ApiControllers.PurchaseRequest;
        public override string Category => ProcurementTypes.PurchaseRequest.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var purchaseRequestEntity = entity.As<PurchaseRequestEntity>();
            var purchaseRequestDto = purchaseRequestEntity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();

            var rfqDto = purchaseRequestEntity.RfqDto.GetDto<RfqDto>();
            rfqDto.PurchaseRequestId = purchaseRequestDto.PurchaseRequestId;
            rfqDto.DateCreated = DateTime.Now;
            rfqDto.DateRequired = purchaseRequestDto.DateRequired;
            rfqDto.Status = RfqStatus.AwaitingQuotation;

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(RfqDto), rfqDto }
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}