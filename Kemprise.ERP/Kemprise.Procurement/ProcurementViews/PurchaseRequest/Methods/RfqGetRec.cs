﻿using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.PurchaseRequest.Methods
{
    public class RfqGetRec : GetMethodCallBase
    {
        public RfqGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "RfqGetRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override string Category => ProcurementTypes.PurchaseRequest.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => false;
        public override ApiControllers Controller => ApiControllers.PurchaseRequest;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var rfqDto = await GetRecordAsync<RfqDto>(MethodName, new object[] { MethodParameter });
            if (rfqDto == null) return;

            entity.SetProperty("RfqDto", new DynamicNotifyPropertyChangedProxy(rfqDto));
        }
    }
}