﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.PurchaseRequest.Methods
{
    public class PurchaseRequestGetRec : GetMethodCallBase
    {
        public PurchaseRequestGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "PurchaseRequestGetRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override string Category => ProcurementTypes.PurchaseRequest.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.PurchaseRequest;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<PurchaseRequestAggregate>(MethodName, new object[] { MethodParameter });
            if (aggregate == null) return;

            if (aggregate.PurchaseRequestDto != null)
                entity.SetProperty("PurchaseRequestDto", new DynamicNotifyPropertyChangedProxy(aggregate.PurchaseRequestDto));

            if (aggregate.PurchaseRequestDetails.Any())
            {
                var orderedList = aggregate.PurchaseRequestDetails.OrderBy(x => x.OrderNumber);
                var details = new ObservableCollection<PurchaseRequestDetailDto>(orderedList);
                entity.SetProperty("PurchaseRequestDetails", details);
            }

            if (aggregate.PurchaseRequestFiles.Any())
            {
                var files = new ObservableCollection<FileDto>(aggregate.PurchaseRequestFiles);
                entity.SetProperty("PurchaseRequestFiles", files);
            }

            if (aggregate.PurchaseRequestNotes.Any())
            {
                var notes = aggregate.PurchaseRequestNotes.OrderBy(x => x.Item);
                var prNotes = new ObservableCollection<NoteDto>(notes);
                entity.SetProperty("PurchaseRequestNotes", prNotes);
            }

            if (aggregate.RfqAggregates.Any())
            {
                var requestsForQuotation = new ObservableCollection<RfqDto>();
                var quotationsReceived = new ObservableCollection<QuotationsReceivedData>();

                aggregate.RfqAggregates.ForEach(x =>
                {
                    requestsForQuotation.Add(x.RfqDto);

                    x.QuotationsReceived.ForEach(file => quotationsReceived.Add(new QuotationsReceivedData
                    {
                        RfqDto = x.RfqDto,
                        File = file
                    }));
                });

                entity.SetProperty("RequestsForQuotation", requestsForQuotation);
                entity.SetProperty("QuotationsReceived", quotationsReceived);
            }
        }
    }
}