﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.PurchaseRequest.Methods
{
    public class PurchaseRequestDraftGetRec : GetMethodCallBase
    {
        public PurchaseRequestDraftGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "PurchaseRequestDraftGetRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override string Category => ProcurementTypes.PurchaseRequest.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => false;
        public override ApiControllers Controller => ApiControllers.PurchaseRequest;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<PurchaseRequestAggregate>(MethodName, new object[] { MethodParameter });
            if (aggregate == null) return;

            if (aggregate.PurchaseRequestDto != null)
                entity.SetProperty("PurchaseRequestDto", new DynamicNotifyPropertyChangedProxy(aggregate.PurchaseRequestDto));

            if (aggregate.PurchaseRequestDetails.Any())
            {
                var orderedList = aggregate.PurchaseRequestDetails.OrderBy(x => x.OrderNumber);
                var details = new ObservableCollection<PurchaseRequestDetailDto>(orderedList);
                entity.SetProperty("PurchaseRequestDetails", details);
            }

            if (aggregate.PurchaseRequestFiles.Any())
            {
                var files = new ObservableCollection<FileDto>(aggregate.PurchaseRequestFiles);
                entity.SetProperty("PurchaseRequestFiles", files);
            }

            if (aggregate.PurchaseRequestNotes.Any())
            {
                var notes = aggregate.PurchaseRequestNotes.OrderBy(x => x.Item);
                var prNotes = new ObservableCollection<NoteDto>(aggregate.PurchaseRequestNotes);
                entity.SetProperty("PurchaseRequestNotes", prNotes);
            }
        }
    }
}