﻿using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.PurchaseRequest.Methods
{
    public class PurchaseRequestAddRec : ErpSaveMethodCallBase
    {
        public PurchaseRequestAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "PurchaseRequestAddRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override ApiControllers Controller => ApiControllers.PurchaseRequest;
        public override string Category => ProcurementTypes.PurchaseRequest.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var purchaseRequestEntity = entity.As<PurchaseRequestEntity>();
            purchaseRequestEntity.PurchaseRequestDetails.ForEach(x =>
            {
                x.PurchaseRequestDetailId = 0;

                if (!string.IsNullOrEmpty(x.Description))
                    x.Description = x.Description.Trim();
            });

            var textInfo = new CultureInfo("en-Us", false).TextInfo;
            foreach (var item in purchaseRequestEntity.PurchaseRequestDetails.Where(item => !string.IsNullOrEmpty(item.UnitOfMeasurement)))
                item.UnitOfMeasurement = textInfo.ToTitleCase(item.UnitOfMeasurement);

            var purchaseRequestAggregate = new PurchaseRequestAggregate
            {
                PurchaseRequestDto = purchaseRequestEntity.PurchaseRequestDto.GetDto<PurchaseRequestDto>(),
                PurchaseRequestDetails = purchaseRequestEntity.PurchaseRequestDetails,
                PurchaseRequestFiles = purchaseRequestEntity.PurchaseRequestFiles,
                PurchaseRequestNotes = purchaseRequestEntity.PurchaseRequestNotes
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(PurchaseRequestAggregate), purchaseRequestAggregate }
            };

            await SaveNewRecordAsync<PurchaseRequestAggregate>(MethodName, dataToSave);
        }
    }
}