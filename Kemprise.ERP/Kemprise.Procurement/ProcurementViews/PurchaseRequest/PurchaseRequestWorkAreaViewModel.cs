﻿using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseOrder;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using ERP.WebApi.DTO.Lookups.LookupItems;
using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.Infrastructure.Services.Procurement;
using Kemprise.Infrastructure.Windows.AskForRevision;
using Kemprise.Infrastructure.Windows.CloseEntity;
using Kemprise.Infrastructure.Windows.ReasonForClosing;
using Kemprise.Infrastructure.Windows.ReasonForRevision;
using Kemprise.Procurement.ProcurementViews.PurchaseOrder;
using Kemprise.Procurement.ProcurementViews.PurchaseRequest.PurchaseRequestViews;
using Kemprise.Procurement.WorkArea;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Procurement.ProcurementViews.PurchaseRequest
{
    public sealed class PurchaseRequestWorkAreaViewModel : ProcurementViewModelBase
    {
        private readonly IFileService _fileService;
        private PurchaseRequestEntity _entity;

        public PurchaseRequestWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, ProcurementTypes.PurchaseRequest.ToString())
        {
            _fileService = fileService;
            SetView(PurchaseRequestViewNames.PurchaseRequestMainView);

            ClearCommand = new DelegateCommand(ClearExecute);
            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            EditAsCommand = new DelegateCommand(EditAsExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveAsDraftCommand = new DelegateCommand(SaveAsDraftExecute);
            DiscardDraftCommand = new DelegateCommand(DiscardDraftExecute);
            SaveForLaterCommand = new DelegateCommand(SaveForLaterExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSave);
            CancelCommand = new DelegateCommand(CancelExecute);
            AddExistingItemCommand = new DelegateCommand(AddExistingItemExecute);
            AddBlankItemCommand = new DelegateCommand(AddBlankItemExecute);
            AddPartCommand = new DelegateCommand(AddPartExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshPurchaseRequestExecute);

            UploadFileCommand = new DelegateCommand(UploadFileExecute);
            AddNoteCommand = new DelegateCommand(AddNoteExecute);
            DownloadFileCommand = new DelegateCommand(DownloadFileExecute);
            CloseEntityCommand = new DelegateCommand(CloseEntityExecute);
            ViewClosingDetailsCommand = new DelegateCommand(ViewClosingDetailsExecute);
            AskForRevisionCommand = new DelegateCommand(AskForRevisionExecute);
            ViewReasonForRevisionCommand = new DelegateCommand(ViewReasonForRevisionExecute);
            ApprovePurchaseRequestCommand = new DelegateCommand(ApprovePurchaseRequestExecute);
            RequestForQuotationCommand = new DelegateCommand(RequestForQuotationExecute);
            AddRfqCommand = new DelegateCommand(AddRfqExecute, CanAddRfq);
            UploadQuotationFileCommand = new DelegateCommand(UploadQuotationFileExecute);
            SaveQuotationCommand = new DelegateCommand(SaveQuotationExecute, CanSaveQuotation);
            DownloadQuotationCommand = new DelegateCommand(DownloadQuotationExecute);
            ConvertToPurchaseOrderCommand = new DelegateCommand(ConvertToPurchaseOrderExecute);
            DeleteItemCommand = new DelegateCommand<object>(DeleteItemExecute);
            DeleteFileCommand = new DelegateCommand<object>(DeleteFileExecute);
            DeleteNoteCommand = new DelegateCommand<object>(DeleteNoteExecute);
            PrintRfqCommand = new DelegateCommand<object>(PrintRfqExecute, CanPrintRfq);
            QuotationReceivedCommand = new DelegateCommand<object>(QuotationReceivedExecute, CanPrintRfq);
            DummyCommand = new DelegateCommand<object>(o => { }, b => false);
            DeleteQuotationFileCommand = new DelegateCommand<object>(DeleteQuotationFileExecute);

            SelectedFilter = new KeyValuePair<string, string>("ALL", "ALL");
            Methods["PurchaseRequestGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        public DelegateCommand UploadFileCommand { get; set; }
        public DelegateCommand AddNoteCommand { get; set; }
        public DelegateCommand DownloadFileCommand { get; set; }
        public DelegateCommand ApprovePurchaseRequestCommand { get; set; }
        public DelegateCommand RequestForQuotationCommand { get; set; }
        public DelegateCommand AddRfqCommand { get; set; }
        public DelegateCommand UploadQuotationFileCommand { get; set; }
        public DelegateCommand SaveQuotationCommand { get; set; }
        public DelegateCommand DownloadQuotationCommand { get; set; }
        public DelegateCommand ConvertToPurchaseOrderCommand { get; set; }
        public DelegateCommand<object> DeleteItemCommand { get; set; }
        public DelegateCommand<object> DeleteFileCommand { get; set; }
        public DelegateCommand<object> DeleteNoteCommand { get; set; }
        public DelegateCommand<object> PrintRfqCommand { get; set; }
        public DelegateCommand<object> QuotationReceivedCommand { get; set; }
        public DelegateCommand<object> DummyCommand { get; set; }
        public DelegateCommand<object> DeleteQuotationFileCommand { get; set; }

        public Dictionary<string, string> PurchaseRequestFilters { get; set; } = new Dictionary<string, string>
        {
            {"ALL","ALL" },
            {PurchaseRequestStatus.SubmittedForApproval, PurchaseRequestStatus.SubmittedForApproval },
            {PurchaseRequestStatus.RevisionRequested, PurchaseRequestStatus.RevisionRequested },
            {PurchaseRequestStatus.RevisionPending, PurchaseRequestStatus.RevisionPending },
            {PurchaseRequestStatus.Approved, PurchaseRequestStatus.Approved },
            {PurchaseRequestStatus.PurchaseOrderCreated, PurchaseRequestStatus.PurchaseOrderCreated },
            {PurchaseRequestStatus.Superseded, PurchaseRequestStatus.Superseded },
            {PurchaseRequestStatus.Closed, PurchaseRequestStatus.Closed }
        };

        private void WhenGetRecDone()
        {
            if (_entity.IsNew)
            {
                _entity.PurchaseRequestDto.SetProperty("DateCreated", DateTime.Now);
                PurchasedRequestPropertyChange();
            }

            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            IsCloseAndRevisePurchaseRequestVisible = (purchaseRequestDto.Status == PurchaseRequestStatus.SubmittedForApproval ||
                                                      purchaseRequestDto.Status == PurchaseRequestStatus.Approved)
                                                     && ApiSession.HasRoles(ErpRoles.CreatePurchaseOrder);
            IsApproveVisible = purchaseRequestDto.Status == PurchaseRequestStatus.SubmittedForApproval
                               && ApiSession.HasRoles(ErpRoles.CreatePurchaseOrder);
            IsViewDetailsVisible = purchaseRequestDto.Status == PurchaseRequestStatus.Closed ||
                                   purchaseRequestDto.Status == PurchaseRequestStatus.Superseded;
            IsRequestForQuotationVisible = purchaseRequestDto.Status == PurchaseRequestStatus.Approved;
            IsViewReasonVisible = purchaseRequestDto.Status == PurchaseRequestStatus.RevisionRequested ||
                                  purchaseRequestDto.Status == PurchaseRequestStatus.RevisionPending;
            IsConvertToPurchaseOrderVisible = purchaseRequestDto.Status == PurchaseRequestStatus.Approved
                                              && ApiSession.HasRoles(ErpRoles.CreatePurchaseOrder);

            PrintRfqCommand.RaiseCanExecuteChanged();
            QuotationReceivedCommand.RaiseCanExecuteChanged();
        }

        private void PurchasedRequestPropertyChange()
        {
            _entity.PurchaseRequestDto.PropertyChanged += (s, e) => SaveCommand.RaiseCanExecuteChanged();
            _entity.PurchaseRequestDetails.CollectionChanged += (s, e) => { SaveCommand.RaiseCanExecuteChanged(); };

            SaveCommand.RaiseCanExecuteChanged();
        }

        private void ClearExecute()
        {
            RadWindow.Confirm("This will clear everything. Continue?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.PurchaseRequestDto.ClearValues();
                _entity.PurchaseRequestDetails.Clear();
                _entity.PurchaseRequestFiles.Clear();
                _entity.PurchaseRequestNotes.Clear();

                _entity.PurchaseRequestDto.SetProperty("DateCreated", DateTime.Now);
            });
        }

        private async void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new PurchaseRequestEntity());
            _entity = Entity.GetDto<PurchaseRequestEntity>();
            _entity.IsNew = true;

            await OpenItemExecute("");

            IsClearVisible = true;
            SetView(PurchaseRequestViewNames.PurchaseRequestEditView);
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new PurchaseRequestEntity());
            _entity = Entity.GetDto<PurchaseRequestEntity>();

            var purchaseRequestStatus = SelectedItem.As<PurchaseRequestListItem>().Status;
            switch (purchaseRequestStatus)
            {
                case PurchaseRequestStatus.Draft:
                    _entity.IsNew = true;
                    await OpenItemExecute("");

                    Methods["PurchaseRequestDraftGetRec"].MethodParameter = SelectedItem.Id;
                    await GetRecordAsync("PurchaseRequestDraftGetRec");
                    WhenGetRecDone();

                    IsClearVisible = true;
                    SetView(PurchaseRequestViewNames.PurchaseRequestEditView);
                    break;

                case PurchaseRequestStatus.RevisionRequested:
                case PurchaseRequestStatus.RevisionPending:
                    _entity.IsNew = true;
                    await OpenItemExecute(SelectedItem.Id, true);

                    IsClearVisible = false;
                    SetView(PurchaseRequestViewNames.PurchaseRequestEditView);
                    break;

                default:
                    await OpenItemExecute(SelectedItem.Id);
                    SetView(PurchaseRequestViewNames.PurchaseRequestDisplayView);
                    break;
            }
        }

        private void EditAsExecute()
        {
            _entity.IsNew = true;

            var oldPurchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            var newPurchaseRequestDto = oldPurchaseRequestDto.ToNewPurchaseRequestDto();
            Entity.SetProperty("PurchaseRequestDto", new DynamicNotifyPropertyChangedProxy(newPurchaseRequestDto));
            PurchasedRequestPropertyChange();

            SetView(PurchaseRequestViewNames.PurchaseRequestEditView);
        }

        private void BackExecute()
        {
            RefreshExecute();
            SetView(PurchaseRequestViewNames.PurchaseRequestMainView);
        }

        private async void SaveAsDraftExecute()
        {
            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            if (string.IsNullOrEmpty(purchaseRequestDto.Purpose) || !_entity.PurchaseRequestDetails.Any())
            {
                EventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload($"The Following Details are Required {Environment.NewLine} {Environment.NewLine}" +
                                                   $"• Purpose {Environment.NewLine}" +
                                                   "• At least 1 Item to Purchase"));
                return;
            }

            _entity.PurchaseRequestDto.SetProperty("Status", PurchaseRequestStatus.Draft);

            var taskToPerform = purchaseRequestDto.PurchaseRequestId == 0
                ? "PurchaseRequestDraftAddRec"
                : "PurchaseRequestDraftModiRec";

            await SaveRecordAsync(taskToPerform);
        }

        private void DiscardDraftExecute()
        {
            RadWindow.Confirm("Discard Purchase Request?", async (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
                if (purchaseRequestDto.PurchaseRequestId != 0)
                    await SaveRecordAsync("PurchaseRequestDicardDraftModiRec");

                BackExecute();
            });
        }

        private async void SaveForLaterExecute()
        {
            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            if (purchaseRequestDto.Status == PurchaseRequestStatus.RevisionRequested)
            {
                var saveResult = await SaveRecordAsync("PurchaseRequestAddRec");
                if (saveResult.OperationResult != OperationResult.Success) return;

                BackExecute();
            }

            if (purchaseRequestDto.Status != PurchaseRequestStatus.RevisionPending) return;
            await SaveRecordAsync("PurchaseRequestModiRec");
            BackExecute();
        }

        private async void SaveExecute()
        {
            if (_entity.PurchaseRequestDetails.Any(x => x.Quantity == 0))
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Item/s without Quantity Found"));
                return;
            }

            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            var taskToPerform = purchaseRequestDto.Status == PurchaseRequestStatus.RevisionPending
                ? "PurchaseRequestModiRec"
                : "PurchaseRequestAddRec";

            _entity.PurchaseRequestDto.SetProperty("Status", PurchaseRequestStatus.SubmittedForApproval);

            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSave()
        {
            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();

            return !string.IsNullOrEmpty(purchaseRequestDto.Purpose)
                   && purchaseRequestDto.DateRequired != null
                   && _entity.PurchaseRequestDetails.Any();
        }

        private void CancelExecute()
        {
            PartWindow?.Close();
            RequestForQuotationWindow?.Close();
            QuotationReceivedWindow?.Close();
        }

        private void AddExistingItemExecute()
        {
            var partNumberView = new CollectionViewSource { Source = Lookups["Parts"] };
            PartCollectionView = partNumberView;

            PartWindow = new PurchaseRequestWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow,
            };

            SearchForPartNumber = "";
            SearchForDescription = "";
            PartWindow.ShowDialog();
        }

        private void AddBlankItemExecute()
        {
            _entity.PurchaseRequestDetails.Add(new PurchaseRequestDetailDto { PartNumber = "TBA" });
            SetNumbering();
        }

        private void AddPartExecute()
        {
            if (SelectedPart == null) return;

            var partId = Convert.ToInt32(SelectedPart.Id);
            var uom = Lookups["UnitOfMeasurements"].FirstOrDefault(x => x.Id == SelectedPart.UnitOfMeasurementId.ToString());
            var newDetail = new PurchaseRequestDetailDto
            {
                PartId = partId,
                PartNumber = SelectedPart.PartNumber,
                UnitOfMeasurement = uom?.Text ?? "",
                Description = SelectedPart.Text
            };

            _entity.PurchaseRequestDetails.Add(newDetail);
            SetNumbering();
            CancelExecute();
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<ProcurementWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Old Purchase Requests"));
        }

        private void RefreshPurchaseRequestExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        private void UploadFileExecute()
        {
            _fileService.GetAnyFile(_entity.PurchaseRequestFiles);
        }

        private void AddNoteExecute()
        {
            _entity.PurchaseRequestNotes.Add(new NoteDto());
        }

        private void DownloadFileExecute()
        {
            if (SelectedFile == null) return;
            _fileService.SaveFile(SelectedFile);
        }

        private void CloseEntityExecute()
        {
            RadWindow.Confirm("Cancel Purchase Request?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var closeWindow = Container.Resolve<CloseEntityWindow>();
                var viewModel = closeWindow.GetViewModel<CloseEntityWindowViewModel>();

                viewModel.Reasons = Lookups["Reasons"];
                viewModel.SetPropertyToDto = () =>
                {
                    _entity.PurchaseRequestDto.SetProperty("ReasonForClosing", viewModel.SelectedReasonText.ToUpper());
                    _entity.PurchaseRequestDto.SetProperty("DetailedReasonForClosing", viewModel.DetailedReasonForClosing);
                };
                viewModel.CompletedAction = ClosePurchaseRequest;

                closeWindow.Owner = Application.Current.MainWindow;
                closeWindow.ShowDialog();
            });
        }

        private async void ClosePurchaseRequest()
        {
            _entity.PurchaseRequestDto.SetProperty("Status", PurchaseRequestStatus.Closed);
            await SaveRecordAsync("PurchaseRequestModiRec");
            BackExecute();
        }

        private void ViewClosingDetailsExecute()
        {
            var closedWindow = Container.Resolve<ReasonForClosingWindow>();
            var viewModel = closedWindow.GetViewModel<ReasonForClosingWindowViewModel>();

            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            viewModel.ReasonForClosing = purchaseRequestDto.ReasonForClosing;
            viewModel.DetailedReasonForClosing = purchaseRequestDto.DetailedReasonForClosing;

            closedWindow.Owner = Application.Current.MainWindow;
            closedWindow.ShowDialog();
        }

        private void AskForRevisionExecute()
        {
            RadWindow.Confirm("Ask For Revision?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var revisionWindow = Container.Resolve<AskForRevisionWindow>();
                var viewModel = revisionWindow.GetViewModel<AskForRevisionWindowViewModel>();

                viewModel.SetPropertyToDto = () => _entity.PurchaseRequestDto.SetProperty("RevisionNotes", viewModel.RevisionNotes);
                viewModel.CompletedAction = AskForRevision;

                revisionWindow.Owner = Application.Current.MainWindow;
                revisionWindow.ShowDialog();
            });
        }

        private async void AskForRevision()
        {
            _entity.PurchaseRequestDto.SetProperty("Status", PurchaseRequestStatus.RevisionRequested);
            await SaveRecordAsync("PurchaseRequestModiRec");
            BackExecute();
        }

        private void ViewReasonForRevisionExecute()
        {
            var revisionWindow = Container.Resolve<ReasonForRevisionWindow>();
            var viewModel = revisionWindow.GetViewModel<ReasonForRevisionWindowViewModel>();
            viewModel.RevisionNotes = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>().RevisionNotes;

            revisionWindow.Owner = Application.Current.MainWindow;
            revisionWindow.ShowDialog();
        }

        private void ApprovePurchaseRequestExecute()
        {
            var purchaseRequestDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            if (ApiSession.GetId() == purchaseRequestDto.CreatedById)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Unable to Approve your own Purchase Request"));
                return;
            }

            RadWindow.Confirm("Approve Purchase Request?", async (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.PurchaseRequestDto.SetProperty("Status", PurchaseRequestStatus.Approved);
                await SaveRecordAsync("PurchaseRequestModiRec");
                BackExecute();
            });
        }

        private void RequestForQuotationExecute()
        {
            SelectedSupplier = null;

            Entity.SetProperty("RfqDto", new DynamicNotifyPropertyChangedProxy(new RfqDto()));
            _entity.RfqDto.PropertyChanged += (s, e) => { AddRfqCommand.RaiseCanExecuteChanged(); };

            RequestForQuotationWindow = new RequestForQuotationWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };
            RequestForQuotationWindow.ShowDialog();
        }

        private async void AddRfqExecute()
        {
            CancelExecute();
            await SaveRecordAsync("RfqAddRec");
            await GetRecordAsync("PurchaseRequestGetRec");
        }

        private bool CanAddRfq()
        {
            var rfqDto = _entity.RfqDto.GetDto<RfqDto>();
            return rfqDto.SupplierId != 0;
        }

        private void UploadQuotationFileExecute()
        {
            _fileService.GetAnyFile(_entity.QuotationFiles);
        }

        private async void SaveQuotationExecute()
        {
            CancelExecute();
            await SaveRecordAsync("RfqModiRec");
            await GetRecordAsync("PurchaseRequestGetRec");
        }

        private bool CanSaveQuotation()
        {
            var rfqDto = _entity.RfqDto.GetDto<RfqDto>();

            return rfqDto.Total != 0
                   && rfqDto.CurrencyId != null
                   && _entity.QuotationFiles.Any();
        }

        private void DownloadQuotationExecute()
        {
            if (SelectedQuotation == null) return;
            _fileService.SaveFile(SelectedQuotation.File);
        }

        private void ConvertToPurchaseOrderExecute()
        {
            if (SelectedQuotation == null)
            {
                EventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload("Please Select Quotation"));
                return;
            }

            PurchaseOrderWorkAreaView = Container.Resolve<PurchaseOrderWorkAreaView>();
            PurchaseOrderWorkAreaViewModel = PurchaseOrderWorkAreaView.GetViewModel<PurchaseOrderWorkAreaViewModel>();

            PurchaseOrderWorkAreaViewModel.BackCommand = new DelegateCommand(BackExecute);
            PurchaseOrderWorkAreaViewModel.SaveCommand = new DelegateCommand(SavePurchaseOrderExecute, CanSavePurchaseOrder);

            PurchaseOrderWorkAreaViewModel.PurchaseRequestAddNewRecord();

            var poEntity = PurchaseOrderWorkAreaViewModel.Entity;
            var purchaseOrderEntity = poEntity.GetDto<PurchaseOrderEntity>();

            var prDto = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            var purchaseOrderDto = SelectedQuotation.RfqDto.ToPurchaseOrder(prDto.Purpose);
            var purchaseOrderProxy = new DynamicNotifyPropertyChangedProxy(purchaseOrderDto);

            poEntity.SetProperty("PurchaseOrderDto", purchaseOrderProxy);
            PurchaseOrderWorkAreaViewModel.SetPoPropertyChange(null, true);

            var purchaseOrderDetails = _entity.PurchaseRequestDetails.ToPurchaseOrderDetails();
            purchaseOrderDetails.ForEach(detail =>
            {
                var newDetail = new DynamicNotifyPropertyChangedProxy(detail);
                purchaseOrderEntity.PurchaseOrderDetails.Add(newDetail);
            });
            PurchaseOrderWorkAreaViewModel.SetPoDetailPropertyChange();

            purchaseOrderEntity.PurchaseOrderFiles.Add(SelectedQuotation.File);
            _entity.PurchaseRequestFiles.ForEach(purchaseOrderEntity.PurchaseOrderFiles.Add);
            _entity.PurchaseRequestNotes.ForEach(purchaseOrderEntity.PurchaseOrderNotes.Add);

            PurchaseOrderWorkAreaViewModel.SetSaveCanExecuteChanged();
            PurchaseOrderWorkAreaViewModel.ComputeTotal();
            SetView(PurchaseRequestViewNames.ConvertToPurchaseOrderView);
        }

        private async void SavePurchaseOrderExecute()
        {
            var purchaseOrderEntity = PurchaseOrderWorkAreaViewModel.Entity.GetDto<PurchaseOrderEntity>();
            if (purchaseOrderEntity.PurchaseOrderDetails.Any(x => x.GetDto<PurchaseOrderDetailDto>().Quantity == 0))
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Item/s without Quantity Found"));
                return;
            }
            if (purchaseOrderEntity.PurchaseOrderDetails.Any(x => x.GetDto<PurchaseOrderDetailDto>().Price == 0))
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Item/s without Price Found"));
                return;
            }

            purchaseOrderEntity.PurchaseOrderDto.SetProperty("Status", PurchaseOrderStatus.SubmittedForApproval);

            var saveResult = await PurchaseOrderWorkAreaViewModel.SaveRecordAsync("PurchaseOrderAddRec");
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSavePurchaseOrder()
        {
            var purchaseOrderEntity = PurchaseOrderWorkAreaViewModel.Entity.GetDto<PurchaseOrderEntity>();
            var purchaseOrderDto = purchaseOrderEntity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();

            return !string.IsNullOrEmpty(purchaseOrderDto.Description)
                   && !string.IsNullOrEmpty(purchaseOrderDto.PaymentTerms)
                   && purchaseOrderEntity.PurchaseOrderDetails.Any()
                   && purchaseOrderDto.DateRequired != null
                   && purchaseOrderDto.CurrencyId != 0
                   && purchaseOrderDto.SupplierId != 0
                   && purchaseOrderDto.Total != 0;
        }

        private void DeleteItemExecute(object obj)
        {
            if (SelectedDetail == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Remove Item?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.PurchaseRequestDetails.Remove(SelectedDetail);
                    SetNumbering();
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Remove Item?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                    {
                        _entity.PurchaseRequestDetails.Remove(SelectedDetail);
                        SetNumbering();
                    }
                });
            }
        }

        private void DeleteFileExecute(object obj)
        {
            if (SelectedFile == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.PurchaseRequestFiles.Remove(SelectedFile);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.PurchaseRequestFiles.Remove(SelectedFile);
                });
            }
        }

        private void DeleteNoteExecute(object obj)
        {
            if (SelectedNote == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete Note?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.PurchaseRequestNotes.Remove(SelectedNote);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete Note?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.PurchaseRequestNotes.Remove(SelectedNote);
                });
            }
        }

        private void PrintRfqExecute(object obj)
        {
            if (obj == null || (int)obj == 0) return;

            var rfqDto = _entity.RequestsForQuotation.FirstOrDefault(x => x.RfqId == (int)obj);
            _entity.SetProperty("RfqDto", new DynamicNotifyPropertyChangedProxy(rfqDto));

            RunReport("RequestForQuotation");
        }

        private bool CanPrintRfq(object obj)
        {
            var purchaseRequest = _entity.PurchaseRequestDto.GetDto<PurchaseRequestDto>();
            return purchaseRequest.Status != PurchaseRequestStatus.Closed &&
                   purchaseRequest.Status != PurchaseRequestStatus.Superseded;
        }

        private void QuotationReceivedExecute(object obj)
        {
            if (obj == null || (int)obj == 0) return;

            var rfqDto = _entity.RequestsForQuotation.FirstOrDefault(x => x.RfqId == (int)obj);
            _entity.SetProperty("RfqDto", new DynamicNotifyPropertyChangedProxy(rfqDto));
            _entity.QuotationFiles.Clear();

            _entity.RfqDto.PropertyChanged += (s, e) => SaveQuotationCommand.RaiseCanExecuteChanged();
            _entity.QuotationFiles.CollectionChanged += (s, e) => SaveQuotationCommand.RaiseCanExecuteChanged();

            QuotationReceivedWindow = new QuotationReceivedWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };
            QuotationReceivedWindow.ShowDialog();
        }

        private void DeleteQuotationFileExecute(object obj)
        {
            if (SelectedQuotationFile == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.QuotationFiles.Remove(SelectedQuotationFile);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.QuotationFiles.Remove(SelectedQuotationFile);
                });
            }
        }

        private void SetNumbering()
        {
            var backUpList = new ObservableCollection<PurchaseRequestDetailDto>(_entity.PurchaseRequestDetails);
            _entity.PurchaseRequestDetails.Clear();

            var count = 1;
            backUpList.ForEach(x =>
            {
                x.OrderNumber = count;
                count++;

                _entity.PurchaseRequestDetails.Add(x);
            });
        }

        public PurchaseRequestDetailDto SelectedDetail
        {
            get { return GetValue(() => SelectedDetail); }
            set { SetValue(() => SelectedDetail, value); }
        }

        public FileDto SelectedFile
        {
            get { return GetValue(() => SelectedFile); }
            set { SetValue(() => SelectedFile, value); }
        }

        public NoteDto SelectedNote
        {
            get { return GetValue(() => SelectedNote); }
            set { SetValue(() => SelectedNote, value); }
        }

        public bool IsCloseAndRevisePurchaseRequestVisible
        {
            get { return GetValue(() => IsCloseAndRevisePurchaseRequestVisible); }
            set { SetValue(() => IsCloseAndRevisePurchaseRequestVisible, value); }
        }

        public bool IsApproveVisible
        {
            get { return GetValue(() => IsApproveVisible); }
            set { SetValue(() => IsApproveVisible, value); }
        }

        public bool IsViewDetailsVisible
        {
            get { return GetValue(() => IsViewDetailsVisible); }
            set { SetValue(() => IsViewDetailsVisible, value); }
        }

        public bool IsRequestForQuotationVisible
        {
            get { return GetValue(() => IsRequestForQuotationVisible); }
            set { SetValue(() => IsRequestForQuotationVisible, value); }
        }

        public RequestForQuotationWindow RequestForQuotationWindow
        {
            get { return GetValue(() => RequestForQuotationWindow); }
            set { SetValue(() => RequestForQuotationWindow, value); }
        }

        public SupplierLookup SelectedSupplier
        {
            get { return GetValue(() => SelectedSupplier); }
            set
            {
                SetValue(() => SelectedSupplier, value);
                if (value == null)
                {
                    _entity.RfqDto.SetProperty("SupplierId", 0);
                    return;
                }

                _entity.RfqDto.SetProperty("SupplierId", Convert.ToInt32(value.Id));
            }
        }

        public QuotationReceivedWindow QuotationReceivedWindow
        {
            get { return GetValue(() => QuotationReceivedWindow); }
            set { SetValue(() => QuotationReceivedWindow, value); }
        }

        public FileDto SelectedQuotationFile
        {
            get { return GetValue(() => SelectedQuotationFile); }
            set { SetValue(() => SelectedQuotationFile, value); }
        }

        public QuotationsReceivedData SelectedQuotation
        {
            get { return GetValue(() => SelectedQuotation); }
            set { SetValue(() => SelectedQuotation, value); }
        }

        public bool IsConvertToPurchaseOrderVisible
        {
            get { return GetValue(() => IsConvertToPurchaseOrderVisible); }
            set { SetValue(() => IsConvertToPurchaseOrderVisible, value); }
        }

        public PurchaseOrderWorkAreaView PurchaseOrderWorkAreaView
        {
            get { return GetValue(() => PurchaseOrderWorkAreaView); }
            set { SetValue(() => PurchaseOrderWorkAreaView, value); }
        }

        public PurchaseOrderWorkAreaViewModel PurchaseOrderWorkAreaViewModel
        {
            get { return GetValue(() => PurchaseOrderWorkAreaViewModel); }
            set { SetValue(() => PurchaseOrderWorkAreaViewModel, value); }
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<ProcurementLookupAggregate>(ApiControllers.Lookup, "ProcurementGetLookups");
        }
    }
}