﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseRequest;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using System.Collections.ObjectModel;

namespace Kemprise.Procurement.ProcurementViews.PurchaseRequest
{
    public class PurchaseRequestEntity : EntityBase
    {
        public PurchaseRequestEntity()
        {
            PurchaseRequestDto = new DynamicNotifyPropertyChangedProxy(new PurchaseRequestDto());
            PurchaseRequestDetails = new ObservableCollection<PurchaseRequestDetailDto>();
            PurchaseRequestFiles = new ObservableCollection<FileDto>();
            PurchaseRequestNotes = new ObservableCollection<NoteDto>();
            RequestsForQuotation = new ObservableCollection<RfqDto>();
            QuotationsReceived = new ObservableCollection<QuotationsReceivedData>();

            RfqDto = new DynamicNotifyPropertyChangedProxy(new RfqDto());
            QuotationFiles = new ObservableCollection<FileDto>();
        }

        public DynamicNotifyPropertyChangedProxy PurchaseRequestDto { get; set; }
        public ObservableCollection<PurchaseRequestDetailDto> PurchaseRequestDetails { get; set; }
        public ObservableCollection<FileDto> PurchaseRequestFiles { get; set; }
        public ObservableCollection<NoteDto> PurchaseRequestNotes { get; set; }
        public ObservableCollection<RfqDto> RequestsForQuotation { get; set; }
        public ObservableCollection<QuotationsReceivedData> QuotationsReceived { get; set; }

        public DynamicNotifyPropertyChangedProxy RfqDto { get; set; }
        public ObservableCollection<FileDto> QuotationFiles { get; set; }
    }

    public class QuotationsReceivedData
    {
        public RfqDto RfqDto { get; set; }
        public FileDto File { get; set; }
    }
}