﻿namespace Kemprise.Procurement.ProcurementViews.PurchaseRequest.PurchaseRequestViews
{
    public class PurchaseRequestViewNames
    {
        public const string PurchaseRequestMainView = "PurchaseRequest Main View";
        public const string PurchaseRequestEditView = "PurchaseRequest Edit View";
        public const string PurchaseRequestDisplayView = "PurchaseRequest Display View";
        public const string ConvertToPurchaseOrderView = "Convert To Purchase Order View";
    }
}