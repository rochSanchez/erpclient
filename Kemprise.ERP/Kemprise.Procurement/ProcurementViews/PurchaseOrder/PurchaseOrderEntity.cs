﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseOrder;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using System.Collections.ObjectModel;

namespace Kemprise.Procurement.ProcurementViews.PurchaseOrder
{
    public class PurchaseOrderEntity : EntityBase
    {
        public PurchaseOrderEntity()
        {
            PurchaseOrderDto = new DynamicNotifyPropertyChangedProxy(new PurchaseOrderDto());
            PurchaseOrderDetails = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            PurchaseOrderNotes = new ObservableCollection<NoteDto>();
            PurchaseOrderFiles = new ObservableCollection<FileDto>();
        }

        public DynamicNotifyPropertyChangedProxy PurchaseOrderDto { get; set; }
        public ObservableCollection<DynamicNotifyPropertyChangedProxy> PurchaseOrderDetails { get; set; }
        public ObservableCollection<NoteDto> PurchaseOrderNotes { get; set; }
        public ObservableCollection<FileDto> PurchaseOrderFiles { get; set; }
    }
}