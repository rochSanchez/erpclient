﻿namespace Kemprise.Procurement.ProcurementViews.PurchaseOrder.PurchaseOrderViews
{
    public class PurchaseOrderViewNames
    {
        public const string PurchaseOrderMainView = "PurchaseOrder Main View";
        public const string PurchaseOrderEditView = "PurchaseOrder Edit View";
        public const string PurchaseOrderDisplayView = "PurchaseOrder Display View";
        public const string ConvertToPurchaseOrderView = "Convert To Purchase Order View";
    }
}