﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseOrder;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.PurchaseOrder.Methods
{
    public class PurchaseOrderGetRec : GetMethodCallBase
    {
        public PurchaseOrderGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "PurchaseOrderGetRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override string Category => ProcurementTypes.PurchaseOrder.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.PurchaseOrder;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<PurchaseOrderAggregate>(MethodName, new object[] { MethodParameter });
            if (aggregate == null) return;

            if (aggregate.PurchaseOrderDto != null)
                entity.SetProperty("PurchaseOrderDto", new DynamicNotifyPropertyChangedProxy(aggregate.PurchaseOrderDto));

            if (aggregate.PurchaseOrderDetails.Any())
            {
                var orderedList = aggregate.PurchaseOrderDetails.OrderBy(x => x.OrderNumber);
                var details = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
                orderedList.ForEach(x => details.Add(new DynamicNotifyPropertyChangedProxy(x)));
                entity.SetProperty("PurchaseOrderDetails", details);
            }

            if (aggregate.PurchaseOrderFiles.Any())
            {
                var files = new ObservableCollection<FileDto>(aggregate.PurchaseOrderFiles);
                entity.SetProperty("PurchaseOrderFiles", files);
            }

            if (aggregate.PurchaseOrderNotes.Any())
            {
                var notes = aggregate.PurchaseOrderNotes.OrderBy(x => x.Item);
                var poNotes = new ObservableCollection<NoteDto>(notes);
                entity.SetProperty("PurchaseOrderNotes", poNotes);
            }
        }
    }
}