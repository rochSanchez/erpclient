﻿using ERP.WebApi.DTO.DTOs.Procurement.PurchaseOrder;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Procurement.ProcurementViews.PurchaseOrder.Methods
{
    public class PurchaseOrderAddRec : ErpSaveMethodCallBase
    {
        public PurchaseOrderAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "PurchaseOrderAddRec";
        public override ErpModules ModuleName => ErpModules.Procurement;
        public override ApiControllers Controller => ApiControllers.PurchaseOrder;
        public override string Category => ProcurementTypes.PurchaseOrder.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var purchaseOrderEntity = entity.As<PurchaseOrderEntity>();
            var purchaseOrderDetails = new List<PurchaseOrderDetailDto>();

            purchaseOrderEntity.PurchaseOrderDetails.ForEach(x => purchaseOrderDetails.Add(x.GetDto<PurchaseOrderDetailDto>()));
            purchaseOrderDetails.ForEach(x =>
            {
                x.PurchaseOrderDetailId = 0;

                if (!string.IsNullOrEmpty(x.Description))
                    x.Description = x.Description.Trim();
            });

            var textInfo = new CultureInfo("en-Us", false).TextInfo;
            foreach (var item in purchaseOrderDetails.Where(item => !string.IsNullOrEmpty(item.UnitOfMeasurement)))
                item.UnitOfMeasurement = textInfo.ToTitleCase(item.UnitOfMeasurement);

            var aggregate = new PurchaseOrderAggregate
            {
                PurchaseOrderDto = purchaseOrderEntity.PurchaseOrderDto.GetDto<PurchaseOrderDto>(),
                PurchaseOrderDetails = purchaseOrderDetails,
                PurchaseOrderFiles = purchaseOrderEntity.PurchaseOrderFiles,
                PurchaseOrderNotes = purchaseOrderEntity.PurchaseOrderNotes
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(PurchaseOrderAggregate), aggregate }
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}