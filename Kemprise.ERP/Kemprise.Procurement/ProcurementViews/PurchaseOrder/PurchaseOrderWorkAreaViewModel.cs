﻿using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Procurement.PurchaseOrder;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using ERP.WebApi.DTO.Lookups.LookupItems;
using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Currency;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.Infrastructure.Services.Procurement;
using Kemprise.Infrastructure.Windows.AskForRevision;
using Kemprise.Infrastructure.Windows.CloseEntity;
using Kemprise.Infrastructure.Windows.ReasonForClosing;
using Kemprise.Infrastructure.Windows.ReasonForRevision;
using Kemprise.Procurement.ProcurementViews.PurchaseOrder.PurchaseOrderViews;
using Kemprise.Procurement.WorkArea;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Procurement.ProcurementViews.PurchaseOrder
{
    public sealed class PurchaseOrderWorkAreaViewModel : ProcurementViewModelBase
    {
        private readonly IFileService _fileService;
        private PurchaseOrderEntity _entity;

        public PurchaseOrderWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, ProcurementTypes.PurchaseOrder.ToString())
        {
            _fileService = fileService;
            SetView(PurchaseOrderViewNames.PurchaseOrderMainView);

            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            EditAsCommand = new DelegateCommand(EditAsExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveForLaterCommand = new DelegateCommand(SaveForLaterExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSave);
            CancelCommand = new DelegateCommand(CancelExecute);
            AddExistingItemCommand = new DelegateCommand(AddExistingItemExecute);
            AddBlankItemCommand = new DelegateCommand(AddBlankItemExecute);
            AddPartCommand = new DelegateCommand(AddPartExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshPurchaseOrderExecute);

            UploadFileCommand = new DelegateCommand(UploadFileExecute);
            AddNoteCommand = new DelegateCommand(AddNoteExecute);
            DownloadFileCommand = new DelegateCommand(DownloadFileExecute);
            CloseEntityCommand = new DelegateCommand(CloseEntityExecute);
            ViewClosingDetailsCommand = new DelegateCommand(ViewClosingDetailsExecute);
            AskForRevisionCommand = new DelegateCommand(AskForRevisionExecute);
            ViewReasonForRevisionCommand = new DelegateCommand(ViewReasonForRevisionExecute);
            ApprovePurchaseOrderCommand = new DelegateCommand(ApprovePurchaseOrderExecute);
            PrintPurchaseOrderDraftCommand = new DelegateCommand(PrintPurchaseOrderDraftExecute);
            PrintPurchaseOrderCommand = new DelegateCommand(PrintPurchaseOrderExecute);
            PurchaseOrderSentCommand = new DelegateCommand(PurchaseOrderSentExecute);
            DeliveryReceivedCommand = new DelegateCommand(DeliveryReceivedExecute);
            DeleteItemCommand = new DelegateCommand<object>(DeleteItemExecute);
            DeleteFileCommand = new DelegateCommand<object>(DeleteFileExecute);
            DeleteNoteCommand = new DelegateCommand<object>(DeleteNoteExecute);

            SelectedFilter = new KeyValuePair<string, string>("ALL", "ALL");
            Methods["PurchaseOrderGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        public DelegateCommand UploadFileCommand { get; set; }
        public DelegateCommand AddNoteCommand { get; set; }
        public DelegateCommand DownloadFileCommand { get; set; }
        public DelegateCommand ApprovePurchaseOrderCommand { get; set; }
        public DelegateCommand PrintPurchaseOrderDraftCommand { get; set; }
        public DelegateCommand PrintPurchaseOrderCommand { get; set; }
        public DelegateCommand PurchaseOrderSentCommand { get; set; }
        public DelegateCommand DeliveryReceivedCommand { get; set; }
        public DelegateCommand<object> DeleteItemCommand { get; set; }
        public DelegateCommand<object> DeleteFileCommand { get; set; }
        public DelegateCommand<object> DeleteNoteCommand { get; set; }

        public Dictionary<string, string> PurchaseOrderFilters { get; set; } = new Dictionary<string, string>
        {
            {"ALL","ALL" },
            {PurchaseOrderStatus.SubmittedForApproval, PurchaseOrderStatus.SubmittedForApproval },
            {PurchaseOrderStatus.Approved, PurchaseOrderStatus.Approved },
            {PurchaseOrderStatus.RevisionRequested, PurchaseOrderStatus.RevisionRequested },
            {PurchaseOrderStatus.RevisionPending, PurchaseOrderStatus.RevisionPending },
            {PurchaseOrderStatus.AwaitingDelivery, PurchaseOrderStatus.AwaitingDelivery },
            {PurchaseOrderStatus.Completed, PurchaseOrderStatus.Completed },
            {PurchaseOrderStatus.Superseded, PurchaseOrderStatus.Superseded },
            {PurchaseOrderStatus.Closed, PurchaseOrderStatus.Closed },
        };

        public Dictionary<string, string> PaymentTerms { get; set; } = new Dictionary<string, string>
        {
            {"7 Days","7 Days" },
            {"10 Days","10 Days" },
            {"15 Days","15 Days" },
            {"20 Days","20 Days" },
            {"25 Days","25 Days" },
            {"30 Days","30 Days" },
            {"60 Days","60 Days" },
            {"90 Days","90 Days" },
            {"Other","Other" }
        };

        private void WhenGetRecDone()
        {
            if (_entity.IsNew)
                SetSaveCanExecuteChanged();

            var purchaseOrderDto = _entity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();
            IsCloseAndRevisePurchaseOrderVisible = (purchaseOrderDto.Status == PurchaseOrderStatus.SubmittedForApproval ||
                                                    purchaseOrderDto.Status == PurchaseOrderStatus.Approved ||
                                                    purchaseOrderDto.Status == PurchaseOrderStatus.AwaitingDelivery)
                                                   && ApiSession.HasRoles(ErpRoles.CreatePurchaseOrder);
            IsViewReasonVisible = purchaseOrderDto.Status == PurchaseOrderStatus.RevisionRequested ||
                                  purchaseOrderDto.Status == PurchaseOrderStatus.RevisionPending;
            IsViewDetailsVisible = purchaseOrderDto.Status == PurchaseOrderStatus.Closed ||
                                   purchaseOrderDto.Status == PurchaseOrderStatus.Superseded;
            IsApproveVisible = purchaseOrderDto.Status == PurchaseRequestStatus.SubmittedForApproval
                               && ApiSession.HasRoles(ErpRoles.ApprovePurchaseOrder);
            IsPrintPurchaseOrderDraftVisible = string.IsNullOrEmpty(purchaseOrderDto.Status) ||
                                               purchaseOrderDto.Status == PurchaseOrderStatus.SubmittedForApproval ||
                                               purchaseOrderDto.Status == PurchaseOrderStatus.RevisionRequested ||
                                               purchaseOrderDto.Status == PurchaseOrderStatus.RevisionPending;
            IsPrintPurchaseOrderVisible = purchaseOrderDto.Status == PurchaseOrderStatus.Approved ||
                                          purchaseOrderDto.Status == PurchaseOrderStatus.AwaitingDelivery;
            IsPurchaseOrderSentVisible = purchaseOrderDto.Status == PurchaseOrderStatus.Approved;
            IsDeliveryReceivedVisible = purchaseOrderDto.Status == PurchaseOrderStatus.AwaitingDelivery;
        }

        private async void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new PurchaseOrderEntity());
            _entity = Entity.GetDto<PurchaseOrderEntity>();
            _entity.IsNew = true;

            await OpenItemExecute("");

            SetCurrency(true);
            SelectedSupplier = null;
            _entity.PurchaseOrderDto.SetProperty("DateCreated", DateTime.Now);
            _entity.PurchaseOrderDto.SetProperty("ShipTo", "Kemprise Development and Trading FZE");
            _entity.PurchaseOrderDto.SetProperty("ShippingAddress", "S18-4, RAKEZ Technology Park Al Hamra Free Zone RAK, United Arab Emirates");

            SetPoPropertyChange();

            IsPaymentTermsReadOnly = true;
            SetView(PurchaseOrderViewNames.PurchaseOrderEditView);
        }

        public async void PurchaseRequestAddNewRecord()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new PurchaseOrderEntity());
            _entity = Entity.GetDto<PurchaseOrderEntity>();
            _entity.IsNew = true;

            IsPaymentTermsReadOnly = true;
            SetView(PurchaseOrderViewNames.ConvertToPurchaseOrderView);

            await OpenItemExecute("");
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new PurchaseOrderEntity());
            _entity = Entity.GetDto<PurchaseOrderEntity>();

            var result = await OpenItemExecute(SelectedItem.Id);
            if (result.OperationResult != OperationResult.Success) return;

            string viewToShow;
            var poDto = _entity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();
            if ((poDto.Status == PurchaseOrderStatus.RevisionRequested ||
                 poDto.Status == PurchaseOrderStatus.RevisionPending) &&
                ApiSession.HasRoles(ErpRoles.CreatePurchaseOrder))
            {
                _entity.IsNew = true;

                SetPoPropertyChange(poDto);
                SetPoDetailPropertyChange();
                SetSaveCanExecuteChanged();

                var fromPr = _entity.PurchaseOrderDetails.Any(x =>
                    x.GetDto<PurchaseOrderDetailDto>().PurchaseRequestDetailId != 0);
                viewToShow = fromPr
                    ? PurchaseOrderViewNames.ConvertToPurchaseOrderView
                    : PurchaseOrderViewNames.PurchaseOrderEditView;
            }
            else
                viewToShow = PurchaseOrderViewNames.PurchaseOrderDisplayView;

            ComputeTotal();
            SetView(viewToShow);
        }

        private void EditAsExecute()
        {
            _entity.IsNew = true;

            var oldPurchaseOrderDto = _entity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();
            var newPurchaseOrderDto = oldPurchaseOrderDto.ToNewPurchaseOrderDto();
            Entity.SetProperty("PurchaseOrderDto", new DynamicNotifyPropertyChangedProxy(newPurchaseOrderDto));
            _entity.PurchaseOrderDetails.ForEach(x =>
            {
                var poDetailDto = x.GetDto<PurchaseOrderDetailDto>();
                x.SetProperty("Price", poDetailDto.GetLatestPrice(Lookups["Parts"]));
                x.SetProperty("Total", 0M);
            });

            SetPoPropertyChange(newPurchaseOrderDto);
            SetPoDetailPropertyChange();
            SetSaveCanExecuteChanged();
            ComputeTotal();

            SetView(PurchaseOrderViewNames.PurchaseOrderEditView);
        }

        private void BackExecute()
        {
            RefreshExecute();
            SetView(PurchaseOrderViewNames.PurchaseOrderMainView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<ProcurementWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Purchase Orders"));
        }

        private async void SaveForLaterExecute()
        {
            var purchaseOrderDto = _entity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();
            if (purchaseOrderDto.Status == PurchaseOrderStatus.RevisionRequested)
            {
                var saveResult = await SaveRecordAsync("PurchaseOrderAddRec");
                if (saveResult.OperationResult != OperationResult.Success) return;

                BackExecute();
            }

            if (purchaseOrderDto.Status != PurchaseOrderStatus.RevisionPending) return;
            await SaveRecordAsync("PurchaseOrderModiRec");
            BackExecute();
        }

        private async void SaveExecute()
        {
            if (_entity.PurchaseOrderDetails.Any(x => x.GetDto<PurchaseOrderDetailDto>().Quantity == 0))
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Item/s without Quantity Found"));
                return;
            }

            if (_entity.PurchaseOrderDetails.Any(x => x.GetDto<PurchaseOrderDetailDto>().Price == 0))
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Item/s without Price Found"));
                return;
            }

            var purchaseOrderDto = _entity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();
            var taskToPerform = purchaseOrderDto.Status == PurchaseOrderStatus.RevisionPending
                ? "PurchaseOrderModiRec"
                : _entity.IsNew
                    ? "PurchaseOrderAddRec"
                    : "PurchaseOrderModiRec";

            _entity.PurchaseOrderDto.SetProperty("Status", PurchaseOrderStatus.SubmittedForApproval);

            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSave()
        {
            var purchaseOrderDto = _entity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();

            return !string.IsNullOrEmpty(purchaseOrderDto.Description)
                   && !string.IsNullOrEmpty(purchaseOrderDto.PaymentTerms)
                   && _entity.PurchaseOrderDetails.Any()
                   && purchaseOrderDto.DateRequired != null
                   && purchaseOrderDto.CurrencyId != 0
                   && purchaseOrderDto.SupplierId != 0
                   && purchaseOrderDto.Total != 0;
        }

        private void CancelExecute()
        {
            PartWindow?.Close();
        }

        private void AddExistingItemExecute()
        {
            var partNumberView = new CollectionViewSource { Source = Lookups["Parts"] };
            PartCollectionView = partNumberView;

            PartWindow = new PurchaseOrderWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow,
            };

            SearchForPartNumber = "";
            SearchForDescription = "";
            PartWindow.ShowDialog();
        }

        private void AddBlankItemExecute()
        {
            var newDetail = new DynamicNotifyPropertyChangedProxy(new PurchaseOrderDetailDto { PartNumber = "TBA" });
            _entity.PurchaseOrderDetails.Add(newDetail);
            SetPoDetailPropertyChange();
        }

        private void AddPartExecute()
        {
            if (SelectedPart == null) return;

            var partId = Convert.ToInt32(SelectedPart.Id);
            var uom = Lookups["UnitOfMeasurements"].FirstOrDefault(x => x.Id == SelectedPart.UnitOfMeasurementId.ToString());
            var detail = new PurchaseOrderDetailDto
            {
                PartId = partId,
                PartNumber = SelectedPart.PartNumber,
                UnitOfMeasurement = uom?.Text ?? "",
                Description = SelectedPart.Text
            };

            var newDetail = new DynamicNotifyPropertyChangedProxy(detail);
            _entity.PurchaseOrderDetails.Add(newDetail);
            SetPoDetailPropertyChange();

            CancelExecute();
        }

        private void RefreshPurchaseOrderExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        public void SetPoPropertyChange(PurchaseOrderDto poDto = null, bool fromPurchaseRequest = false)
        {
            _entity.PurchaseOrderDto.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName != "TaxPercentage" &&
                    e.PropertyName != "DiscountAmount" &&
                    e.PropertyName != "CurrencyId") return;
                SetCurrency();
                ComputeTotal();
            };

            if (fromPurchaseRequest) return;
            if (poDto == null)
            {
                IsPaymentTermsReadOnly = true;
                SelectedSupplier = null;
            }
            else
            {
                var paymentTerms = PaymentTerms.FirstOrDefault(x => x.Value == poDto.PaymentTerms).Value;
                IsPaymentTermsReadOnly = !string.IsNullOrEmpty(paymentTerms);

                var lookUpEntity = Lookups["Suppliers"].FirstOrDefault(x => x.Id == poDto.SupplierId.ToString());
                SelectedSupplier = lookUpEntity?.As<SupplierLookup>();
            }
        }

        private void SetCurrency(bool initial = false)
        {
            if (initial)
            {
                _entity.PurchaseOrderDto.SetProperty("CurrencyId", 2);
                _entity.PurchaseOrderDto.SetProperty("CurrencyName", "AED");
                ComputeTotal();
            }
            else
            {
                var poDto = _entity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();
                var currency = Lookups["Currencies"].FirstOrDefault(x => x.Id == poDto.CurrencyId.ToString());
                _entity.PurchaseOrderDto.SetProperty("CurrencyName", currency?.Text);
            }
        }

        public void SetPoDetailPropertyChange()
        {
            _entity.PurchaseOrderDetails.ForEach(detail =>
            {
                detail.PropertyChanged += (s, e) =>
                {
                    var detailProxy = s.As<DynamicNotifyPropertyChangedProxy>();
                    var poDetail = detailProxy.GetDto<PurchaseOrderDetailDto>();
                    detailProxy.SetProperty("Total", poDetail.Price * poDetail.Quantity);

                    ComputeTotal();
                    SaveCommand.RaiseCanExecuteChanged();
                };
            });
        }

        public void SetSaveCanExecuteChanged()
        {
            _entity.PurchaseOrderDto.PropertyChanged += (s, e) => { SaveCommand.RaiseCanExecuteChanged(); };
            _entity.PurchaseOrderDetails.CollectionChanged += (s, e) =>
            {
                SaveCommand.RaiseCanExecuteChanged();
                SetNumbering();
                ComputeTotal();
            };
        }

        private void UploadFileExecute()
        {
            _fileService.GetAnyFile(_entity.PurchaseOrderFiles);
        }

        private void AddNoteExecute()
        {
            _entity.PurchaseOrderNotes.Add(new NoteDto());
        }

        private void DownloadFileExecute()
        {
            if (SelectedFile == null) return;
            _fileService.SaveFile(SelectedFile);
        }

        private void CloseEntityExecute()
        {
            RadWindow.Confirm("Cancel Purchase Order?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var closeWindow = Container.Resolve<CloseEntityWindow>();
                var viewModel = closeWindow.GetViewModel<CloseEntityWindowViewModel>();

                viewModel.Reasons = Lookups["Reasons"];
                viewModel.SetPropertyToDto = () =>
                {
                    _entity.PurchaseOrderDto.SetProperty("ReasonForClosing", viewModel.SelectedReasonText.ToUpper());
                    _entity.PurchaseOrderDto.SetProperty("DetailedReasonForClosing", viewModel.DetailedReasonForClosing);
                };
                viewModel.CompletedAction = ClosePurchaseOrder;

                closeWindow.Owner = Application.Current.MainWindow;
                closeWindow.ShowDialog();
            });
        }

        private async void ClosePurchaseOrder()
        {
            _entity.PurchaseOrderDto.SetProperty("Status", PurchaseOrderStatus.Closed);
            await SaveRecordAsync("PurchaseOrderModiRec");
            BackExecute();
        }

        private void ViewClosingDetailsExecute()
        {
            var closedWindow = Container.Resolve<ReasonForClosingWindow>();
            var viewModel = closedWindow.GetViewModel<ReasonForClosingWindowViewModel>();

            var purchaseOrderDto = _entity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();
            viewModel.ReasonForClosing = purchaseOrderDto.ReasonForClosing;
            viewModel.DetailedReasonForClosing = purchaseOrderDto.DetailedReasonForClosing;

            closedWindow.Owner = Application.Current.MainWindow;
            closedWindow.ShowDialog();
        }

        private void AskForRevisionExecute()
        {
            RadWindow.Confirm("Ask For Revision?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var revisionWindow = Container.Resolve<AskForRevisionWindow>();
                var viewModel = revisionWindow.GetViewModel<AskForRevisionWindowViewModel>();

                viewModel.SetPropertyToDto = () => _entity.PurchaseOrderDto.SetProperty("RevisionNotes", viewModel.RevisionNotes);
                viewModel.CompletedAction = AskForRevision;

                revisionWindow.Owner = Application.Current.MainWindow;
                revisionWindow.ShowDialog();
            });
        }

        private async void AskForRevision()
        {
            _entity.PurchaseOrderDto.SetProperty("Status", PurchaseOrderStatus.RevisionRequested);
            await SaveRecordAsync("PurchaseOrderModiRec");
            BackExecute();
        }

        private void ViewReasonForRevisionExecute()
        {
            var revisionWindow = Container.Resolve<ReasonForRevisionWindow>();
            var viewModel = revisionWindow.GetViewModel<ReasonForRevisionWindowViewModel>();
            viewModel.RevisionNotes = _entity.PurchaseOrderDto.GetDto<PurchaseOrderDto>().RevisionNotes;

            revisionWindow.Owner = Application.Current.MainWindow;
            revisionWindow.ShowDialog();
        }

        private void ApprovePurchaseOrderExecute()
        {
            var purchaseOrderDto = _entity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();
            if (ApiSession.GetId() == purchaseOrderDto.CreatedById)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Unable to Approve your own Purchase Order"));
                return;
            }

            RadWindow.Confirm("Approve Purchase Order?", async (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.PurchaseOrderDto.SetProperty("Status", PurchaseOrderStatus.Approved);
                await SaveRecordAsync("PurchaseOrderModiRec");
                BackExecute();
            });
        }

        private void PrintPurchaseOrderDraftExecute()
        {
            RunReport("PurchaseOrderDraft");
        }

        private void PrintPurchaseOrderExecute()
        {
            RunReport("PurchaseOrder");
        }

        private void PurchaseOrderSentExecute()
        {
            RadWindow.Confirm("Purchase Order Sent?", async (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.PurchaseOrderDto.SetProperty("Status", PurchaseOrderStatus.AwaitingDelivery);
                await SaveRecordAsync("PurchaseOrderModiRec");
                BackExecute();
            });
        }

        private void DeliveryReceivedExecute()
        {
            RadWindow.Confirm("Delivery Received?", async (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.PurchaseOrderDto.SetProperty("Status", PurchaseOrderStatus.Completed);
                await SaveRecordAsync("PurchaseOrderModiRec");
                BackExecute();
            });
        }

        private void DeleteItemExecute(object obj)
        {
            if (SelectedDetail == null) return;

            var poDetail = SelectedDetail.GetDto<PurchaseOrderDetailDto>();
            if (poDetail.PurchaseRequestDetailId != 0)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Unable to Remove Items from Purchase Request"));

                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;
                eventArgs.Cancel = true;

                return;
            }

            if (obj == null)
            {
                RadWindow.Confirm("Remove Item?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.PurchaseOrderDetails.Remove(SelectedDetail);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Remove Item?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.PurchaseOrderDetails.Remove(SelectedDetail);
                });
            }
        }

        private void DeleteFileExecute(object obj)
        {
            if (SelectedFile == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.PurchaseOrderFiles.Remove(SelectedFile);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.PurchaseOrderFiles.Remove(SelectedFile);
                });
            }
        }

        private void DeleteNoteExecute(object obj)
        {
            if (SelectedNote == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete Note?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.PurchaseOrderNotes.Remove(SelectedNote);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete Note?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.PurchaseOrderNotes.Remove(SelectedNote);
                });
            }
        }

        public void SetNumbering()
        {
            var count = 1;
            _entity.PurchaseOrderDetails.ForEach(x =>
            {
                x.SetProperty("OrderNumber", count);
                count++;
            });
        }

        public void ComputeTotal()
        {
            var purchaseOrderDto = _entity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();

            var totalDetails = _entity.PurchaseOrderDetails.Sum(x => x.GetDto<PurchaseOrderDetailDto>().Total);

            var tax = purchaseOrderDto.TaxPercentage;
            var amountToTax = totalDetails * (tax / 100);

            var discountAmount = purchaseOrderDto.DiscountAmount;
            var discountedTotal = totalDetails - discountAmount;

            var total = discountedTotal + amountToTax;

            _entity.PurchaseOrderDto.SetProperty("TaxAmount", amountToTax);
            _entity.PurchaseOrderDto.SetProperty("SubTotal", totalDetails);
            _entity.PurchaseOrderDto.SetProperty("Total", total);

            SetTotalBasedOnCurrency();
        }

        private void SetTotalBasedOnCurrency()
        {
            var purchaseOrderDto = _entity.PurchaseOrderDto.GetDto<PurchaseOrderDto>();
            Total = purchaseOrderDto.Total.ToCurrency(purchaseOrderDto.CurrencyName, purchaseOrderDto.Prefix);
        }

        public SupplierLookup SelectedSupplier
        {
            get { return GetValue(() => SelectedSupplier); }
            set
            {
                SetValue(() => SelectedSupplier, value);
                if (value == null)
                {
                    _entity.PurchaseOrderDto.SetProperty("SupplierId", 0);
                    return;
                }

                _entity.PurchaseOrderDto.SetProperty("SupplierId", Convert.ToInt32(value.Id));
            }
        }

        public KeyValuePair<string, string> SelectedPaymentTerms
        {
            get { return GetValue(() => SelectedPaymentTerms); }
            set
            {
                SetValue(() => SelectedPaymentTerms, value);
                switch (value.Value)
                {
                    case null:
                        return;

                    case "Other":
                        _entity.PurchaseOrderDto.SetProperty("PaymentTerms", "");
                        SelectedPaymentTerms = default;
                        IsPaymentTermsReadOnly = false;
                        break;

                    default:
                        _entity.PurchaseOrderDto.SetProperty("PaymentTerms", value.Value);
                        SelectedPaymentTerms = default;
                        IsPaymentTermsReadOnly = true;
                        break;
                }
            }
        }

        public bool IsPaymentTermsReadOnly
        {
            get { return GetValue(() => IsPaymentTermsReadOnly); }
            set { SetValue(() => IsPaymentTermsReadOnly, value); }
        }

        public DynamicNotifyPropertyChangedProxy SelectedDetail
        {
            get { return GetValue(() => SelectedDetail); }
            set { SetValue(() => SelectedDetail, value); }
        }

        public FileDto SelectedFile
        {
            get { return GetValue(() => SelectedFile); }
            set { SetValue(() => SelectedFile, value); }
        }

        public NoteDto SelectedNote
        {
            get { return GetValue(() => SelectedNote); }
            set { SetValue(() => SelectedNote, value); }
        }

        public bool IsCloseAndRevisePurchaseOrderVisible
        {
            get { return GetValue(() => IsCloseAndRevisePurchaseOrderVisible); }
            set { SetValue(() => IsCloseAndRevisePurchaseOrderVisible, value); }
        }

        public bool IsViewDetailsVisible
        {
            get { return GetValue(() => IsViewDetailsVisible); }
            set { SetValue(() => IsViewDetailsVisible, value); }
        }

        public bool IsApproveVisible
        {
            get { return GetValue(() => IsApproveVisible); }
            set { SetValue(() => IsApproveVisible, value); }
        }

        public bool IsPrintPurchaseOrderDraftVisible
        {
            get { return GetValue(() => IsPrintPurchaseOrderDraftVisible); }
            set { SetValue(() => IsPrintPurchaseOrderDraftVisible, value); }
        }

        public bool IsPrintPurchaseOrderVisible
        {
            get { return GetValue(() => IsPrintPurchaseOrderVisible); }
            set { SetValue(() => IsPrintPurchaseOrderVisible, value); }
        }

        public bool IsPurchaseOrderSentVisible
        {
            get { return GetValue(() => IsPurchaseOrderSentVisible); }
            set { SetValue(() => IsPurchaseOrderSentVisible, value); }
        }

        public bool IsDeliveryReceivedVisible
        {
            get { return GetValue(() => IsDeliveryReceivedVisible); }
            set { SetValue(() => IsDeliveryReceivedVisible, value); }
        }

        public string Total
        {
            get { return GetValue(() => Total); }
            set { SetValue(() => Total, value); }
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<ProcurementLookupAggregate>(ApiControllers.Lookup, "ProcurementGetLookups");
        }
    }
}