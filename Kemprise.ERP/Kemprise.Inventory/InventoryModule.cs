﻿using System.Linq;
using System.Reflection;
using ERP.WebApi.DTO;
using Kemprise.Infrastructure.Controllers;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Inventory.WorkArea;
using Prism.Ioc;
using Prism.Unity;
using Unity;
using Unity.Lifetime;
using Unity.RegistrationByConvention;

namespace Kemprise.Inventory
{
    public class InventoryModule : ErpModule
    {
        public InventoryModule(IUnityContainer container)
        {
            container.RegisterType(typeof(IControllerBase), typeof(InventoryController),
                nameof(InventoryController), new ContainerControlledLifetimeManager());
        }

        protected override Assembly GetBusinessAssembly()
        {
            return Assembly.GetAssembly(typeof(EntityListItem));
        }

        protected override Assembly GetModuleAssembly()
        {
            return Assembly.GetAssembly(typeof(InventoryModule));
        }

        public override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.GetContainer().RegisterType<object, InventoryWorkAreaView>(typeof(InventoryWorkAreaView).FullName,
                new ContainerControlledLifetimeManager());
            containerRegistry.GetContainer()
                .RegisterTypes(AllClasses.FromLoadedAssemblies().Where(x => typeof(IChildNavigationItem).IsAssignableFrom(x)),
                    WithMappings.FromAllInterfaces, WithName.TypeName, WithLifetime.ContainerControlled);
        }
    }

    public enum InventoryTypes
    {
        Warehouse, Receive, Deliver, Internal,
        Inventory
    }
}