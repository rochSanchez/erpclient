﻿using ERP.WebApi.DTO.Constants;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Prism.Commands;
using Prism.Events;
using Unity;

namespace Kemprise.Inventory.QuickAccess
{
    public class InventoryQuickAccessViewModel : NotifyPropertyChangedBase, IQuickAccess
    {
        private readonly IEventAggregator _eventAggregator;

        public InventoryQuickAccessViewModel(IEventAggregator eventAggregator, IUnityContainer container)
        {
            _eventAggregator = eventAggregator;
            eventAggregator.GetEvent<DeSelectModuleEvent<InventoryController>>().Subscribe(OnDeSelectModule);

            OpenModuleCommand = new DelegateCommand(OpenModuleExecute);

            IsVisible = ApiSession.HasRoles(ErpRoles.InventoryRoles);
        }

        public DelegateCommand OpenModuleCommand { get; set; }

        private void OpenModuleExecute()
        {
            _eventAggregator.GetEvent<OpenModuleEvent>().Publish(new OpenModuleEventPayload(ErpModules.Inventory, this));
        }

        private void OnDeSelectModule()
        {
            IsSelected = false;
        }

        public bool IsSelected
        {
            get { return GetValue(() => IsSelected); }
            set { SetValue(() => IsSelected, value); }
        }

        public bool IsVisible
        {
            get { return GetValue(() => IsVisible); }
            set { SetValue(() => IsVisible, value); }
        }
    }
}