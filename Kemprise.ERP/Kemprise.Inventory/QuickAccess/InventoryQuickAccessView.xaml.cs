﻿using Kemprise.Infrastructure.Modules;

namespace Kemprise.Inventory.QuickAccess
{
    /// <summary>
    /// Interaction logic for ManagementQuickAccessView.xaml
    /// </summary>
    public partial class InventoryQuickAccessView : IQuickAccessView
    {
        public InventoryQuickAccessView()
        {
            InitializeComponent();
        }

        public int Order => 5;
    }
}