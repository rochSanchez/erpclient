﻿using Kemprise.Infrastructure.Controllers;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Events.Security;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Regions;
using Kemprise.Inventory.WorkArea;
using Prism.Events;
using Prism.Regions;
using Unity;
using Unity.Lifetime;

namespace Kemprise.Inventory
{
    public class InventoryController : ControllerBase
    {
        public InventoryController(IEventAggregator eventAggregator, IRegionManager regionManager,
            IUnityContainer container) :
            base(eventAggregator, regionManager, container)
        {
            eventAggregator.GetEvent<OpenModuleEvent>().Subscribe(OnOpenModule);
            eventAggregator.GetEvent<LogoutUserEvent>().Subscribe(OnLogoutUserEvent);
        }

        private void OnLogoutUserEvent()
        {
            Container.RegisterType<object, InventoryWorkAreaView>(typeof(InventoryWorkAreaView).FullName,
                new ContainerControlledLifetimeManager());
        }

        private void OnOpenModule(OpenModuleEventPayload obj)
        {
            if (obj.ErpModule == ErpModules.Inventory)
            {
                obj.QuickAccess.IsSelected = true;
            }
            else
            {
                EventAggregator.GetEvent<DeSelectModuleEvent<InventoryController>>().Publish();
                return;
            }

            RegionManager.RequestNavigate(RegionNames.MainWorkAreaRegion, typeof(InventoryWorkAreaView).FullName);
        }
    }
}

