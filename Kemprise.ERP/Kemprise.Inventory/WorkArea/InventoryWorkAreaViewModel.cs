﻿using System.Collections.Generic;
using System.Linq;
using ERP.WebApi.DTO;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.WorkArea;
using Prism.Events;
using Unity;

namespace Kemprise.Inventory.WorkArea
{
    public class InventoryWorkAreaViewModel : ErpWorkAreaViewModelBase
    {
        public InventoryWorkAreaViewModel(IUnityContainer container, IEventAggregator eventAggregator) :
            base(container, eventAggregator)
        {
            ItemsListDictionary = new Dictionary<string, IEnumerable<EntityListItem>>();

            NavigationItems = container.ResolveAll<IParentNavigationItem>()
                .Where(item => item.Module == ErpModules.Inventory)
                .OrderBy(item => item.Order);

            eventAggregator.GetEvent<RefreshItemListEvent<InventoryWorkAreaViewModel>>().Subscribe(OnRefreshItemList);
        }
    }
}