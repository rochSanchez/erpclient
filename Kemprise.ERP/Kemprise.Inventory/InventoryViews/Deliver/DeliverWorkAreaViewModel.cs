﻿using ERP.WebApi.DTO.DTOs.Inventory;
using ERP.WebApi.DTO.Lookups;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.Inventory.InventoryViews.Common;
using Kemprise.Inventory.InventoryViews.Deliver.DeliveryViews;
using Kemprise.Inventory.InventoryViews.Deliver.Windows;
using Kemprise.Inventory.InventoryViews.Internal;
using Kemprise.Inventory.InventoryViews.Receive;
using Kemprise.Inventory.WorkArea;
using Prism.Commands;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Inventory.InventoryViews.Deliver
{
    public sealed class DeliverWorkAreaViewModel : InventoryViewModelBase
    {
        private SelectOrderWindow _selectOrderWindow;
        private readonly IGenericFileService<OperationItemFileDto> _fileService;
        private SelectLotTrackingItemWindow _selectLotTrackingItemWindow;
        private SelectNonTrackingItemWindow _selectNonTrackingItemWindow;
        private SelectSerialTrackingItemWindow _selectSerialTrackingItemWindow;
        private DeliveryLotStockWindow _deliveryLotStockWindow;
        private DeliveryNonTrackingStockWindow _deliveryNonTrackingStockWindow;
        private readonly DeliverEntity _entity;

        public DelegateCommand SelectDeliveryOrderCommand { get; set; }
        public DelegateCommand CancelSelectOrderCommand { get; set; }
        public DelegateCommand RefreshOrdersCommand { get; set; }
        public DelegateCommand ProcessOrderCommand { get; set; }
        public DelegateCommand CancelProcessCommand { get; set; }
        public DelegateCommand DeliverCommand { get; set; }
        public DelegateCommand UploadFileCommand { get; set; }
        public DelegateCommand<object> DeleteFileCommand { get; set; }
        public DelegateCommand DownloadFileCommand { get; set; }
        public DelegateCommand DeleteItemToReceiveCommand { get; set; }
        public DelegateCommand AddItemToReceiveCommand { get; set; }
        public DelegateCommand DeleteItemCommand { get; set; }
        public DelegateCommand SaveQcCommand { get; set; }
        public DelegateCommand QualityCheckCommand { get; set; }
        public DelegateCommand OkCommand { get; set; }
        public DelegateCommand NewDeliveryOkCommand { get; set; }
        public Dictionary<string, string> Companies { get; set; }
        public DelegateCommand CancelSelectItemCommand { get; set; }
        public DelegateCommand ConfirmCommand { get; set; }
        public DelegateCommand NewDeliverCommand { get; set; }
        public DelegateCommand DeleteItemToMoveCommand { get; set; }
        public DelegateCommand AddItemToMoveCommand { get; set; }
        public DelegateCommand CancelSelectStockCommand { get; set; }
        public DelegateCommand<GridViewCellEditEndedEventArgs> CellEditEndedCommand { get; set; }
        public DelegateCommand DeleteNewItemToDeliverCommand { get; set; }
        public DelegateCommand ConfirmNewDeliveryCommand { get; set; }

        public DeliverWorkAreaViewModel(IUnityContainer container) : base(container, InventoryTypes.Deliver.ToString())
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new DeliverEntity());
            _entity = Entity.GetDto<DeliverEntity>();
            SetView(DeliverViewNames.DeliverMainView);
            SelectDeliveryOrderCommand = new DelegateCommand(SelectDeliveryOrderExecute);
            CancelSelectOrderCommand = new DelegateCommand(CancelSelectDeliveryOrderExecute);
            RefreshOrdersCommand = new DelegateCommand(RefreshOrdersExecute);
            ProcessOrderCommand = new DelegateCommand(ProcessOrderExecute, ProcessOrderCanExecute);
            CancelProcessCommand = new DelegateCommand(CancelProcessExecute);
            DeliverCommand = new DelegateCommand(DeliverExecute, DeliverCanExecute);
            CancelSelectItemCommand = new DelegateCommand(CancelSelectItemExecute);
            UploadFileCommand = new DelegateCommand(UploadFileExecute);
            DownloadFileCommand = new DelegateCommand(DownloadFileExecute);
            DeleteFileCommand = new DelegateCommand<object>(DeleteFileExecute);
            OkCommand = new DelegateCommand(OkExecute);
            NewDeliveryOkCommand = new DelegateCommand(NewDeliveryOkExecute);
            SaveCommand = new DelegateCommand(SaveExecute, SaveCanExecute);
            DeleteItemCommand = new DelegateCommand(DeleteItemExecute, () => SelectedDeliveredItem != null);
            AddItemToReceiveCommand = new DelegateCommand(AddItemExecute);
            DeleteItemToReceiveCommand = new DelegateCommand(DeleteItemToDeliverExecute, DeleteItemToDeliverCanExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            QualityCheckCommand = new DelegateCommand(QualityCheckExecute);
            SaveQcCommand = new DelegateCommand(SaveQcExecute);
            ConfirmCommand = new DelegateCommand(ConfirmExecute, ConfirmCanExecute);
            AddItemToMoveCommand = new DelegateCommand(AddItemToMoveExecute, AddItemToMoveCanExecute);
            DeleteItemToMoveCommand = new DelegateCommand(DeleteItemToMoveExecute, DeleteItemToReceiveCanExecute);
            CancelSelectStockCommand = new DelegateCommand(CancelSelectStockExecute);
            CellEditEndedCommand = new DelegateCommand<GridViewCellEditEndedEventArgs>(CellEditEndedExecute);
            DeleteNewItemToDeliverCommand =
                new DelegateCommand(DeleteNewItemsToDeliverExecute, DeleteNewItemsToDeliverCanExecute);
            ConfirmNewDeliveryCommand = new DelegateCommand(ConfirmNewDeliveryExecute, ConfirmNewDeliveryCanExecute);
            Companies = new Dictionary<string, string> { { "Company 1", "Company 1" }, { "Company 2", "Company 2" } };
            NewDeliverCommand = new DelegateCommand(NewDeliverExecute, NewDeliverCanExecute);
            Methods["DeliverOrderGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
            Methods["DeliverOperationGetRec"].As<IErpGetMethodCall>().WhenDone += WhenOperationGetRecDone;
        }

        private bool ConfirmNewDeliveryCanExecute()
        {
            if (string.IsNullOrEmpty(_entity.InventoryOperationDto.GetDto<InventoryOperationDto>().Company))
                return false;
            if (_entity.InventoryOperationDto.GetDto<InventoryOperationDto>().WarehouseId == 0) return false;
            if (!_entity.NewDeliverOperationItems.Any()) return false;
            return true;
        }

        private async void ConfirmNewDeliveryExecute()
        {
            var saveResult = await SaveRecordAsync("NewDeliverOperationAddRec");
            if (saveResult.OperationResult != OperationResult.Success) return;
            CancelProcessExecute();
        }

        private void ClearDeliveryAllItems()
        {
            foreach (var item in _entity.NewDeliverOperationItems)
            {
                var itemToRemove = item;
                _entity.NewDeliverOperationItems.Remove(itemToRemove);
                var part = _entity.OrderItems.FirstOrDefault(x => x.GetDto<OrderItemDto>().PartId == itemToRemove.PartId);
                var itemRemovedQuantity = itemToRemove.Quantity;
                var currentStockValue = part.GetDto<OrderItemDto>().Quantity;
                part.SetProperty("Quantity", currentStockValue + itemRemovedQuantity);
            }
        }

        private void DeleteNewItemsToDeliverExecute()
        {
            var itemToRemove = SelectedNewItemToDeliver;
            _entity.NewDeliverOperationItems.Remove(itemToRemove);
            var part = _entity.OrderItems.FirstOrDefault(x => x.GetDto<OrderItemDto>().PartId == itemToRemove.PartId);
            var itemRemovedQuantity = itemToRemove.Quantity;
            var currentStockValue = part.GetDto<OrderItemDto>().Quantity;
            part.SetProperty("Quantity", currentStockValue + itemRemovedQuantity);
        }

        private bool DeleteNewItemsToDeliverCanExecute()
        {
            return SelectedNewItemToDeliver != null;
        }

        private void NewDeliveryOkExecute()
        {
            if (_entity.ItemsToDeliver.Any(x => x.GetDto<InternalOpItem>().Quantity == 0))
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Item with 0 quantity found.",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
            else
            {
                _entity.ItemsToDeliver.ForEach(item =>
                {
                    var selectedItem = NewDeliverSelectedOrderItem.GetDto<OrderItemDto>();
                    var totalQuantity = item.GetDto<InternalOpItem>().Quantity;
                    if (selectedItem.Quantity == totalQuantity)
                        NewDeliverSelectedOrderItem.SetProperty("Quantity", 0m);
                    else
                        NewDeliverSelectedOrderItem.SetProperty("Quantity", selectedItem.Quantity - totalQuantity);

                    var newItem = item.GetDto<InternalOpItem>();
                    _entity.NewDeliverOperationItems.Add(new OperationItemDto()
                    {
                        LotNumber = newItem.LotNumber,
                        PartId = newItem.PartId,
                        Quantity = newItem.Quantity,
                        SerialNumber = newItem.SerialNumber,
                        TrackingNumber = newItem.PartDto.Tracking == Tracking.Lot
                            ? newItem.LotNumber
                            : newItem.SerialNumber,
                        PartDto = newItem.PartDto,
                        StockId = newItem.StockId
                    });
                });

                CancelSelectStockExecute();
            }
        }

        private void CellEditEndedExecute(GridViewCellEditEndedEventArgs obj)
        {
            var internalOpItems =
                obj.Cell.DataContext.As<DynamicNotifyPropertyChangedProxy>().GetDto<InternalOpItem>();
            var originalStockQuantity = _entity.PartStocksBackup
                .FirstOrDefault(x => x.StockId == internalOpItems.StockId)?.Quantity;
            var stock = _entity.PartStocks.FirstOrDefault(x => x.GetDto<StockDto>().StockId == internalOpItems.StockId);

            var remainingQuantity = SelectedStock.GetDto<StockDto>().Quantity;
            if (obj.NewData.ToString() != obj.OldData.ToString())
            {
                var value = (decimal)obj.NewData;
                if (value > remainingQuantity)
                {
                    RadWindow.Alert(new DialogParameters
                    {
                        Content = "Exceeded max quantity.",
                        Owner = Application.Current.MainWindow,
                        DialogStartupLocation = WindowStartupLocation.CenterScreen
                    });

                    //BUG
                    SelectedOperationItem.SetProperty("Quantity", 0m);

                    var stockOpItemsCount = _entity.ItemsToDeliver
                        .Where(x => x.GetDto<InternalOpItem>().StockId == internalOpItems.StockId)
                        .Sum(x => x.GetDto<InternalOpItem>().Quantity);

                    if (originalStockQuantity == stockOpItemsCount)
                        stock.SetProperty("Quantity", 0m);
                    else
                    {
                        var difference = originalStockQuantity - stockOpItemsCount;
                        stock.SetProperty("Quantity", difference);
                    }
                }
                else
                {
                    if (decimal.Parse(obj.NewData.ToString()) == 0)
                    {
                        var newVal = SelectedStock.GetDto<StockDto>().Quantity + decimal.Parse(obj.OldData.ToString());
                        stock.SetProperty("Quantity", newVal);
                    }
                    else
                    {
                        var newVal = SelectedStock.GetDto<StockDto>().Quantity - value;
                        stock.SetProperty("Quantity", newVal);
                    }
                }
            }
        }

        private void CancelSelectStockExecute()
        {
            _deliveryLotStockWindow?.Close();
            _deliveryNonTrackingStockWindow?.Close();
        }

        private bool DeleteItemToReceiveCanExecute()
        {
            return SelectedOperationItem != null;
        }

        private void DeleteItemToMoveExecute()
        {
            var itemToRemove = SelectedOperationItem;
            _entity.ItemsToDeliver.Remove(itemToRemove);

            var stock = _entity.PartStocks.FirstOrDefault(x => x.GetDto<StockDto>().StockId == itemToRemove.GetDto<InternalOpItem>().StockId);

            var itemRemovedQuantity = itemToRemove.GetDto<InternalOpItem>().Quantity;
            var currentStockValue = stock.GetDto<StockDto>().Quantity;
            stock.SetProperty("Quantity", currentStockValue + itemRemovedQuantity);
        }

        private bool AddItemToMoveCanExecute()
        {
            return SelectedStock != null;
        }

        private void AddItemToMoveExecute()
        {
            if (SelectedStock == null) return;
            var selectedStock = SelectedStock.GetDto<StockDto>();
            if (selectedStock.Quantity == 0)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Remaining quantity is 0.",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
            else
            {
                var item = new InternalOpItem
                {
                    LotNumber = selectedStock.LotNumber,
                    Description = selectedStock.Description,
                    PartId = selectedStock.PartId,
                    SerialNumber = selectedStock.SerialNumber,
                    StockId = selectedStock.StockId,
                    TrackingNumber = selectedStock.PartDto.Tracking == Tracking.Lot
                        ? selectedStock.LotNumber
                        : selectedStock.SerialNumber,
                    PartDto = selectedStock.PartDto
                };

                _entity.ItemsToDeliver.Add(new DynamicNotifyPropertyChangedProxy(item));
            }
        }

        private bool NewDeliverCanExecute()
        {
            if (NewDeliverSelectedOrderItem == null) return false;
            if (NewDeliverSelectedOrderItem.GetDto<OrderItemDto>().Quantity == 0) return false;
            if (SelectedWarehouse == null) return false;
            return true;
        }

        private async void NewDeliverExecute()
        {
            var selectedItem = NewDeliverSelectedOrderItem.GetDto<OrderItemDto>();
            if (selectedItem.PartDto.Tracking == 0)
            {
                RadWindow.Alert("Tracking configuration required!");
            }
            else
            {
                _entity.SelectedWarehouse = int.Parse(SelectedWarehouse.Id);
                _entity.SelectedStockPartId = NewDeliverSelectedOrderItem.GetDto<OrderItemDto>().PartId;
                _entity.PartStocks.Clear();
                _entity.ItemsToDeliver.Clear();
                var result = await GetRecordAsync("GetWarehousePartStocks");
                if (result.OperationResult == OperationResult.Success)
                {
                    if (selectedItem.PartDto.Tracking == Tracking.None)
                    {
                        _deliveryNonTrackingStockWindow = new DeliveryNonTrackingStockWindow { DataContext = this, Owner = Application.Current.MainWindow };
                        _deliveryNonTrackingStockWindow.ShowDialog();
                    }
                    else if (selectedItem.PartDto.Tracking == Tracking.Lot)
                    {
                        _deliveryLotStockWindow = new DeliveryLotStockWindow { DataContext = this, Owner = Application.Current.MainWindow };
                        _deliveryLotStockWindow.ShowDialog();
                    }
                    else
                    {
                        // _selectSerialTrackingItemWindow = new SelectSerialTrackingItemWindow { DataContext = this };
                        // _selectSerialTrackingItemWindow.ShowDialog();
                    }
                }
            }
        }

        private bool ConfirmCanExecute()
        {
            if (string.IsNullOrEmpty(_entity.InventoryOperationDto.GetDto<InventoryOperationDto>().Company))
                return false;
            if (_entity.InventoryOperationDto.GetDto<InventoryOperationDto>().WarehouseId == 0) return false;
            if (!_entity.DeliveredItems.Any()) return false;
            return true;
        }

        private void ConfirmExecute()
        {
            RadWindow.Confirm("Confirm Delivery?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;
                ConfirmDelivery();
            });
        }

        private async void ConfirmDelivery()
        {
            var saveResult = await SaveRecordAsync("ConfirmDelivery");
            if (saveResult.OperationResult != OperationResult.Success) return;
            RefreshOrdersExecute();
            CancelProcessExecute();
        }

        private void WhenOperationGetRecDone()
        {
            QcVisibility =
                _entity.InventoryOperationDto.GetDto<InventoryOperationDto>().OperationStatus ==
                OperationStatus.QualityControl
                    ? Visibility.Visible
                    : Visibility.Collapsed;
        }

        private void WhenGetRecDone()
        {
            _entity.InventoryOperationDto.PropertyChanged += (s, e) =>
            {
                SaveCommand.RaiseCanExecuteChanged();
                ConfirmCommand.RaiseCanExecuteChanged();
                ConfirmNewDeliveryCommand.RaiseCanExecuteChanged();
            };
            _entity.DeliveredItems.CollectionChanged += (s, a) =>
            {
                SaveCommand.RaiseCanExecuteChanged();
                ConfirmCommand.RaiseCanExecuteChanged();
            };

            _entity.NewDeliverOperationItems.CollectionChanged += (s, a) =>
            {
                ConfirmNewDeliveryCommand.RaiseCanExecuteChanged();
            };
        }

        private void SaveQcExecute()
        {
            RadWindow.Confirm("Submit quality check result?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;
                SubmitQcResult();
            });
        }

        private async void SubmitQcResult()
        {
            var saveResult = await SaveRecordAsync("SaveDeliveryQualityCheck");
            if (saveResult.OperationResult != OperationResult.Success) return;
            RefreshOrdersExecute();
            CancelProcessExecute();
        }

        private async void QualityCheckExecute()
        {
            if (_entity.InventoryOperationDto == null)
            {
                _entity.DeliveredItems.Clear();
                RegisterLookups();
                await GetLookupsAsync();
                Methods["OperationGetRec"].MethodParameter = SelectedItem.Id;
                await GetRecordAsync("OperationGetRec");
                SetView(DeliverViewNames.DeliverDisplayView);
            }

            SetView(DeliverViewNames.QualityCheckView);
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select  an Operation Item",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            _entity.DeliveredItems.Clear();
            RegisterLookups();
            await GetLookupsAsync();
            Methods["DeliverOperationGetRec"].MethodParameter = SelectedItem.Id;
            await GetRecordAsync("DeliverOperationGetRec");
            SetView(DeliverViewNames.DeliverDisplayView);
        }

        private bool DeleteItemToDeliverCanExecute()
        {
            return SelectedItemToDeliver != null;
        }

        private void DeleteItemToDeliverExecute()
        {
            _entity.OperationItemDetails.Remove(SelectedItemToDeliver);
        }

        private void AddItemExecute()
        {
            var selectedItem = SelectedOrderItem.GetDto<OrderItemDto>();
            if (selectedItem.PartDto.Tracking != Tracking.Serial)
            {
                _entity.OperationItemDetails.Add(new OperationItemDetail
                {
                    OperationItemDto = new OperationItemDto
                    {
                        PartId = selectedItem.PartId.Value,
                        PartNumber = selectedItem.PartDto.PartNumber,
                        Name = selectedItem.PartDto.Name,
                        UnitOfMeasurementId = selectedItem.PartDto.UnitOfMeasurementId.Value
                    }
                });
            }
            else
            {
                _entity.OperationItemDetails.Add(new OperationItemDetail
                {
                    OperationItemDto = new OperationItemDto
                    {
                        PartId = selectedItem.PartId.Value,
                        PartNumber = selectedItem.PartDto.PartNumber,
                        Name = selectedItem.PartDto.Name,
                        UnitOfMeasurementId = selectedItem.PartDto.UnitOfMeasurementId.Value,
                        Quantity = 1
                    }
                });
            }
        }

        private void DeleteItemExecute()
        {
            var order = _entity.OrderItems.FirstOrDefault(x =>
                x.GetDto<OrderItemDto>().PartId == SelectedDeliveredItem.PartId);
            if (order != null)
            {
                if (order.GetDto<OrderItemDto>().PartDto.Tracking == Tracking.None)
                {
                    if (order.GetDto<OrderItemDto>().PartDto.UnitOfMeasurementId == 32)
                        order.SetProperty("Quantity", order.GetDto<OrderItemDto>().Quantity + 1);
                    else
                        order.SetProperty("Quantity",
                            order.GetDto<OrderItemDto>().Quantity + SelectedDeliveredItem.Quantity);
                }
                else
                {
                    order.SetProperty("Quantity",
                        order.GetDto<OrderItemDto>().Quantity + SelectedDeliveredItem.Quantity);
                }
            }
            else
            {
                var orderItem = _entity.BackingOrderItems.First(x => x.PartId == SelectedDeliveredItem.PartId);
                orderItem.Quantity = SelectedDeliveredItem.Quantity;
                _entity.OrderItems.Add(new DynamicNotifyPropertyChangedProxy(orderItem));
            }

            _entity.DeliveredItems.Remove(SelectedDeliveredItem);
        }

        private bool SaveCanExecute()
        {
            if (string.IsNullOrEmpty(_entity.InventoryOperationDto.GetDto<InventoryOperationDto>().Company))
                return false;
            if (_entity.InventoryOperationDto.GetDto<InventoryOperationDto>().WarehouseId == 0) return false;
            if (!_entity.DeliveredItems.Any()) return false;
            return true;
        }

        private async void SaveExecute()
        {
            var saveResult = await SaveRecordAsync("DeliverOperationAddRec");
            if (saveResult.OperationResult != OperationResult.Success) return;
            CancelProcessExecute();
        }

        private bool ValidateItems()
        {
            if (SelectedOrderItem.GetDto<OrderItemDto>().PartDto.Tracking == Tracking.Lot)
            {
                if (_entity.OperationItemDetails.Any(x => string.IsNullOrEmpty(x.OperationItemDto.LotNumber)))
                {
                    RadWindow.Alert("Lot number is required for each item!");
                    return false;
                }
            }
            
            if (SelectedOrderItem.GetDto<OrderItemDto>().PartDto.Tracking == Tracking.Serial)
            {
                if (_entity.OperationItemDetails.Any(x => string.IsNullOrEmpty(x.OperationItemDto.SerialNumber)))
                {
                    RadWindow.Alert("Serial number is required for each item!");
                    return false;
                }

                if (_entity.OperationItemDetails.Any(x => x.OperationItemDto.Quantity > 1))
                {
                    RadWindow.Alert("Quantity value must not be greater than one!");
                    return false;
                }
            }

            if (_entity.OperationItemDetails.Any(x => x.OperationItemDto.Quantity == 0))
            {
                RadWindow.Alert("Quantity value is required for each item!");
                return false;
            }

            var totalQuantity = _entity.OperationItemDetails.Select(x => x.OperationItemDto).Sum(x => x.Quantity);
            if (totalQuantity > SelectedOrderItem.GetDto<OrderItemDto>().Quantity)
            {
                RadWindow.Alert($"Quantity entered exceed.");
                return false;
            }

            if (SelectedOrderItem.GetDto<OrderItemDto>().PartDto.Tracking != Tracking.None)
            {
                if (SelectedOrderItem.GetDto<OrderItemDto>().PartDto.Tracking == Tracking.Lot)
                {
                    var hasDuplicate = _entity.OperationItemDetails.GroupBy(x => x.OperationItemDto.LotNumber)
                        .Any(g => g.Count() > 1);
                    if (hasDuplicate)
                    {
                        RadWindow.Alert("Duplicate tracking number detected!");
                        return false;
                    }
                }
                else
                {
                    var hasDuplicate = _entity.OperationItemDetails.GroupBy(x => x.OperationItemDto.SerialNumber)
                        .Any(g => g.Count() > 1);
                    if (hasDuplicate)
                    {
                        RadWindow.Alert("Duplicate tracking number detected!");
                        return false;
                    }
                }
            }

            return true;
        }

        private void OkExecute()
        {
            if (ValidateItems())
            {
                var selectedItem = SelectedOrderItem.GetDto<OrderItemDto>();
                var totalQuantity = _entity.OperationItemDetails.Select(x => x.OperationItemDto).Sum(x => x.Quantity);
                if (selectedItem.Quantity == totalQuantity)
                    SelectedOrderItem.SetProperty("Quantity", 0m);
                else
                    SelectedOrderItem.SetProperty("Quantity", selectedItem.Quantity - totalQuantity);

                foreach (var itemDetail in _entity.OperationItemDetails)
                {
                    var addedItem = itemDetail.OperationItemDto;
                    addedItem.Files = new List<OperationItemFileDto>(itemDetail.OperationItemFiles);
                    _entity.DeliveredItems.Add(addedItem);
                }

                SelectedOrderItem = null;
                CancelSelectItemExecute();
                RefreshOrdersExecute();
            }
        }

        private void DeleteFileExecute(object obj)
        {
            if (SelectedFile == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    SelectedOperationItemDetail.OperationItemFiles.Remove(SelectedFile);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        SelectedOperationItemDetail.OperationItemFiles.Remove(SelectedFile);
                });
            }
        }

        private void DownloadFileExecute()
        {
            if (SelectedFile == null) return;
            _fileService.SaveFile(SelectedFile);
        }

        private void UploadFileExecute()
        {
            _fileService.GetAnyFile(SelectedOperationItemDetail.OperationItemFiles);
        }

        private void CancelSelectItemExecute()
        {
            _selectNonTrackingItemWindow?.Close();
            _selectSerialTrackingItemWindow?.Close();
            _selectLotTrackingItemWindow?.Close();
        }

        private bool DeliverCanExecute()
        {
            if (SelectedOrderItem == null) return false;
            if (SelectedOrderItem.GetDto<OrderItemDto>().Quantity == 0) return false;
            return true;
        }

        private void DeliverExecute()
        {
            var selectedItem = SelectedOrderItem.GetDto<OrderItemDto>();
            if (selectedItem.PartDto.Tracking == 0)
            {
                RadWindow.Alert("Tracking configuration required!");
            }
            else
            {
                _entity.OperationItemDetails.Clear();
                if (selectedItem.PartDto.Tracking == Tracking.None)
                {
                    _selectNonTrackingItemWindow = new SelectNonTrackingItemWindow { DataContext = this };
                    _selectNonTrackingItemWindow.ShowDialog();
                }
                else if (selectedItem.PartDto.Tracking == Tracking.Lot)
                {
                    _selectLotTrackingItemWindow = new SelectLotTrackingItemWindow { DataContext = this };
                    _selectLotTrackingItemWindow.ShowDialog();
                }
                else
                {
                    _selectSerialTrackingItemWindow = new SelectSerialTrackingItemWindow { DataContext = this };
                    _selectSerialTrackingItemWindow.ShowDialog();
                }
            }
        }

        private bool ProcessOrderCanExecute()
        {
            return SelectedOrder != null;
        }

        private void CancelProcessExecute()
        {
            SetView(DeliverViewNames.DeliverMainView);
        }

        private async void ProcessOrderExecute()
        {
            _entity.DeliveredItems.Clear();
            RegisterLookups();
            await GetLookupsAsync();
            _entity.NewDeliverOperationItems.Clear();
            Methods["DeliverOrderGetRec"].MethodParameter = SelectedOrder.OrderId.ToString();
            await GetRecordAsync("DeliverOrderGetRec");
            SetView(_entity.OrderDto.IsInternal
                ? DeliverViewNames.DeliverInternalEditView
                : DeliverViewNames.NewDeliveryView);
            _selectOrderWindow.Close();
        }

        private void RefreshOrdersExecute()
        {
            EventAggregator.GetEvent<RefreshItemListEvent<InventoryWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Deliver"));
        }

        public OperationItemDetail SelectedOperationItemDetail
        {
            get { return GetValue(() => SelectedOperationItemDetail); }
            set { SetValue(() => SelectedOperationItemDetail, value); }
        }

        private void CancelSelectDeliveryOrderExecute()
        {
            _selectOrderWindow.Close();
        }

        private void SelectDeliveryOrderExecute()
        {
            _selectOrderWindow = new SelectOrderWindow { DataContext = this };
            _selectOrderWindow.ShowDialog();
        }

        public string SelectOrderHeaderText => "Select Delivery";

        public int OrderCount
        {
            get { return GetValue(() => OrderCount); }
            set { SetValue(() => OrderCount, value); }
        }

        public OrderDto SelectedOrder
        {
            get { return GetValue(() => SelectedOrder); }
            set { SetValue(() => SelectedOrder, value, ProcessOrderCommand.RaiseCanExecuteChanged); }
        }

        public DynamicNotifyPropertyChangedProxy SelectedOrderItem
        {
            get { return GetValue(() => SelectedOrderItem); }
            set { SetValue(() => SelectedOrderItem, value, DeliverCommand.RaiseCanExecuteChanged); }
        }

        public LookupEntity SelectedWarehouse
        {
            get { return GetValue(() => SelectedWarehouse); }
            set
            {
                SetValue(() => SelectedWarehouse, value, () =>
            {
                NewDeliverCommand.RaiseCanExecuteChanged();
                ClearDeliveryAllItems();
                _entity.ItemsToDeliver.Clear();
            });
            }
        }

        public DynamicNotifyPropertyChangedProxy NewDeliverSelectedOrderItem
        {
            get { return GetValue(() => NewDeliverSelectedOrderItem); }
            set { SetValue(() => NewDeliverSelectedOrderItem, value, NewDeliverCommand.RaiseCanExecuteChanged); }
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<InventoryLookupAggregate>(ApiControllers.Lookup, "InventoryGetLookups");
        }

        public OperationItemFileDto SelectedFile
        {
            get { return GetValue(() => SelectedFile); }
            set { SetValue(() => SelectedFile, value); }
        }

        public OperationItemDto SelectedNewDeliveredItem
        {
            get { return GetValue(() => SelectedNewDeliveredItem); }
            set { SetValue(() => SelectedNewDeliveredItem, value, DeleteNewItemToDeliverCommand.RaiseCanExecuteChanged); }
        }

        public OperationItemDto SelectedDeliveredItem
        {
            get { return GetValue(() => SelectedDeliveredItem); }
            set { SetValue(() => SelectedDeliveredItem, value, DeleteItemCommand.RaiseCanExecuteChanged); }
        }

        public OperationItemDetail SelectedItemToDeliver
        {
            get { return GetValue(() => SelectedItemToDeliver); }
            set { SetValue(() => SelectedItemToDeliver, value, DeleteItemToReceiveCommand.RaiseCanExecuteChanged); }
        }

        public Visibility QcVisibility
        {
            get { return GetValue(() => QcVisibility); }
            set { SetValue(() => QcVisibility, value); }
        }

        public DynamicNotifyPropertyChangedProxy SelectedStock
        {
            get { return GetValue(() => SelectedStock); }
            set { SetValue(() => SelectedStock, value, AddItemToMoveCommand.RaiseCanExecuteChanged); }
        }

        public DynamicNotifyPropertyChangedProxy SelectedOperationItem
        {
            get { return GetValue(() => SelectedOperationItem); }
            set { SetValue(() => SelectedOperationItem, value, DeleteItemToMoveCommand.RaiseCanExecuteChanged); }
        }

        public OperationItemDto SelectedNewItemToDeliver
        {
            get { return GetValue(() => SelectedNewItemToDeliver); }
            set { SetValue(() => SelectedNewItemToDeliver, value, DeleteNewItemToDeliverCommand.RaiseCanExecuteChanged); }
        }
    }
}