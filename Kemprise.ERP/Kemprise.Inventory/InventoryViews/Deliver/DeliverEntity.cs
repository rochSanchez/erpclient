﻿using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Inventory.InventoryViews.Receive;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Kemprise.Inventory.InventoryViews.Deliver
{
    public class DeliverEntity : EntityBase
    {
        public DeliverEntity()
        {
            Orders = new ObservableCollection<OrderDto>();
            OrderItems = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            OrderDto = new OrderDto();
            DeliveredItems = new ObservableCollection<OperationItemDto>();
            OperationItemDetails = new ObservableCollection<OperationItemDetail>();
            BackingOrderItems = new List<OrderItemDto>();
            PartStocksBackup = new List<StockDto>();
            PartStocks = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            ItemsToDeliver = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            NewDeliverOperationItems = new ObservableCollection<OperationItemDto>();
        }

        public OrderDto OrderDto { get; set; }
        public ObservableCollection<DynamicNotifyPropertyChangedProxy> OrderItems { get; set; }
        public List<OrderItemDto> BackingOrderItems { get; set; }
        public ObservableCollection<OrderDto> Orders { get; set; }
        public DynamicNotifyPropertyChangedProxy InventoryOperationDto { get; set; }
        public ObservableCollection<OperationItemDto> DeliveredItems { get; set; }
        public ObservableCollection<OperationItemDetail> OperationItemDetails { get; set; }
        public ObservableCollection<DynamicNotifyPropertyChangedProxy> PartStocks { get; set; }
        public List<StockDto> PartStocksBackup { get; set; }
        public int SelectedWarehouse { get; set; }
        public int? SelectedStockPartId { get; set; }
        public ObservableCollection<DynamicNotifyPropertyChangedProxy> ItemsToDeliver { get; set; }
        public ObservableCollection<OperationItemDto> NewDeliverOperationItems { get; set; }
    }
}