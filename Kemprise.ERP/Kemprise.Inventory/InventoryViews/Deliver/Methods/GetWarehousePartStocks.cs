﻿using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Kemprise.Inventory.InventoryViews.Deliver.Methods
{
    public class GetWarehousePartStocks : GetMethodCallBase
    {
        public GetWarehousePartStocks(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "GetWarehousePartStocks";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Deliver.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var deliveryEntity = entity.GetDto<DeliverEntity>();
            var aggregate = await GetRecordAsync<InventoryAggregate>("GetWarehousePartStocks", new object[] { deliveryEntity.SelectedWarehouse, deliveryEntity.SelectedStockPartId });
            if (aggregate == null) return;

            var partStocks = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            aggregate.PartStocks.ForEach(item =>
            {
                item.Stocks.ForEach(stock => partStocks.Add(new DynamicNotifyPropertyChangedProxy(stock)));
            });
            entity.SetProperty("PartStocks", partStocks);

            var copy = new List<StockDto>();
            aggregate.PartStocks.ForEach(item =>
            {
                item.Stocks.ForEach(stock => copy.Add(new StockDto { StockId = stock.StockId, PartId = stock.PartId, Quantity = stock.Quantity }));
            });

            entity.SetProperty("PartStocksBackup", copy);
        }
    }
}