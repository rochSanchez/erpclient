﻿using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Kemprise.Inventory.InventoryViews.Deliver.Methods
{
    public class OrderGetRecList : GetMethodCallBase
    {
        public OrderGetRecList(IApiRequestManager requestManager) : base(requestManager)
        {
            MethodParameter = "1";
        }

        public override string MethodName => "OrderGetRecList";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Deliver.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var orderDtos = await GetRecordsAsync<OrderDto>(MethodName, new object[] { MethodParameter });
            if (orderDtos == null) return;

            entity.SetProperty("Orders", new ObservableCollection<OrderDto>(orderDtos));
        }
    }
}