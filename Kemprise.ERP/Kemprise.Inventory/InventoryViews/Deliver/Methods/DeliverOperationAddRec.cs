﻿using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Inventory.InventoryViews.Deliver.Methods
{
    public class DeliverOperationAddRec : ErpSaveMethodCallBase
    {
        public DeliverOperationAddRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "DeliverOperationAddRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override ApiControllers Controller => ApiControllers.Inventory;
        public override string Category => InventoryTypes.Deliver.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var receiveEntity = entity.As<DeliverEntity>();

            var order = receiveEntity.OrderDto;
            var operation = receiveEntity.InventoryOperationDto.GetDto<InventoryOperationDto>();
            var status = receiveEntity.OrderItems.Any(x => x.GetDto<OrderItemDto>().Quantity != 0)
                ? OrderStatus.Partial
                : OrderStatus.Completed;
            order.OrderStatus = status;
            var items = receiveEntity.DeliveredItems.ToList();
            var orderItems = receiveEntity.OrderItems.ToList();
            operation.OperationType = OperationType.Deliver;
            var inventoryAggregate = new InventoryAggregate
            {
                OrderDto = receiveEntity.OrderDto,
                InventoryOperationDto = operation,
                OperationItems = items,
                OrderItems = new List<OrderItemDto>()
            };

            orderItems.ForEach(item => inventoryAggregate.OrderItems.Add(item.GetDto<OrderItemDto>()));

            var dataToSave = new Dictionary<string, object>
            {
                {nameof(InventoryAggregate), inventoryAggregate}
            };

            await SaveNewRecordAsync("OperationAddRec", dataToSave);
        }
    }
}