﻿using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Kemprise.Inventory.InventoryViews.Deliver.Methods
{
    public class DeliverOrderGetRec : GetMethodCallBase
    {
        public DeliverOrderGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "DeliverOrderGetRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Deliver.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<InventoryAggregate>("OrderGetRec", new object[] { MethodParameter });
            if (aggregate == null) return;
            var order = aggregate.OrderDto;
            entity.SetProperty("OrderDto", order);
            var orderItems = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            aggregate.OrderItems.ForEach(item =>
            {
                orderItems.Add(new DynamicNotifyPropertyChangedProxy(item));
            });
            entity.SetProperty("OrderItems", orderItems);
            entity.SetProperty("BackingOrderItems", aggregate.OrderItems);
            entity.SetProperty("InventoryOperationDto",
                new DynamicNotifyPropertyChangedProxy(new InventoryOperationDto
                {
                    SourceDocument = order.ReferenceNumber,
                    OperationType = OperationType.Deliver,
                    CustomerId = order.CustomerId,
                    To = order.CustomerName,
                    ScheduledDate = order.ExpirationDate,
                    IsInternal = order.IsInternal
                }));
        }
    }
}