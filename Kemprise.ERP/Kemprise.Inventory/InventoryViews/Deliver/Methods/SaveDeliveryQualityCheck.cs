﻿using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Inventory.InventoryViews.Deliver.Methods
{
    public class SaveDeliveryQualityCheck : ErpSaveMethodCallBase
    {
        public SaveDeliveryQualityCheck(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "SaveDeliveryQualityCheck";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override ApiControllers Controller => ApiControllers.Inventory;
        public override string Category => InventoryTypes.Deliver.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var receiveEntity = entity.As<DeliverEntity>();

            var operation = receiveEntity.InventoryOperationDto.GetDto<InventoryOperationDto>();
            var items = receiveEntity.DeliveredItems.ToList();

            var inventoryAggregate = new InventoryAggregate
            {
                InventoryOperationDto = operation,
                OperationItems = items
            };

            var dataToSave = new Dictionary<string, object>
            {
                {nameof(InventoryAggregate), inventoryAggregate}
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}