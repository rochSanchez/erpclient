﻿using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Kemprise.Inventory.InventoryViews.Deliver.Methods
{
    public class DeliverOperationGetRec : GetMethodCallBase
    {
        public DeliverOperationGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "DeliverOperationGetRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Deliver.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => false;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<InventoryAggregate>("OperationGetRec", new object[] { MethodParameter });
            entity.SetProperty("InventoryOperationDto",
                new DynamicNotifyPropertyChangedProxy(aggregate.InventoryOperationDto));
            entity.SetProperty("DeliveredItems", new ObservableCollection<OperationItemDto>(aggregate.OperationItems));
        }
    }
}