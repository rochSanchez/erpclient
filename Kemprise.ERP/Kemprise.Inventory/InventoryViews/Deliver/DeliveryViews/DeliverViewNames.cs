﻿namespace Kemprise.Inventory.InventoryViews.Deliver.DeliveryViews
{
    public class DeliverViewNames
    {
        public const string DeliverMainView = "Deliver Main View";
        public const string DeliverInternalEditView = "Deliver Edit View";
        public const string DeliverDisplayView = "Deliver Display View";
        public const string QualityCheckView = "Deliver Quality Check View";
        public static string NewDeliveryView = "New Delivery View";
    }
}