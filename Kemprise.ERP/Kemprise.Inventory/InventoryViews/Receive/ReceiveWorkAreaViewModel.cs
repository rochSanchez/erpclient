﻿using ERP.WebApi.DTO.DTOs.Inventory;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.Inventory.InventoryViews.Common;
using Kemprise.Inventory.InventoryViews.Receive.ReceiveViews;
using Kemprise.Inventory.WorkArea;
using Prism.Commands;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Kemprise.Infrastructure.Extensions;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Inventory.InventoryViews.Receive.Methods;

namespace Kemprise.Inventory.InventoryViews.Receive
{
    public sealed class ReceiveWorkAreaViewModel : InventoryViewModelBase
    {
        private readonly ReceiveEntity _entity;
        private readonly IGenericFileService<OperationItemFileDto> _fileService;
        private SelectLotTrackingItemWindow _selectLotTrackingItemWindow;
        private SelectNonTrackingItemWindow _selectNonTrackingItemWindow;
        private SelectOrderWindow _selectOrderWindow;
        private SelectSerialTrackingItemWindow _selectSerialTrackingItemWindow;

        public ReceiveWorkAreaViewModel(IUnityContainer container,
            IGenericFileService<OperationItemFileDto> fileService) : base(container, InventoryTypes.Receive.ToString())
        {
            _fileService = fileService;
            Entity = new DynamicNotifyPropertyChangedProxy(new ReceiveEntity());
            _entity = Entity.GetDto<ReceiveEntity>();
            SetView(ReceiveViewNames.ReceiveMainView);
            SelectReceiveOrderCommand = new DelegateCommand(SelectDeliveryOrderExecute);
            CancelSelectOrderCommand = new DelegateCommand(CancelSelectDeliveryOrderExecute);
            RefreshOrdersCommand = new DelegateCommand(RefreshOrdersExecute);
            ProcessOrderCommand = new DelegateCommand(ProcessOrderExecute, ProcessOrderCanExecute);
            CancelProcessCommand = new DelegateCommand(CancelProcessExecute);
            ReceiveCommand = new DelegateCommand(ReceiveExecute, ReceiveCanExecute);
            CancelSelectItemCommand = new DelegateCommand(CancelSelectItemExecute);
            UploadFileCommand = new DelegateCommand(UploadFileExecute);
            DownloadFileCommand = new DelegateCommand(DownloadFileExecute);
            DeleteFileCommand = new DelegateCommand<object>(DeleteFileExecute);
            OkCommand = new DelegateCommand(OkExecute);
            SaveCommand = new DelegateCommand(SaveExecute, SaveCanExecute);
            DeleteItemCommand = new DelegateCommand(DeleteItemExecute, () => SelectedReceivedItem != null);
            AddItemToReceiveCommand = new DelegateCommand(AddItemExecute);
            DeleteItemToReceiveCommand = new DelegateCommand(DeleteItemToReceiveExecute, DeleteItemToReceiveCanExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            QualityCheckCommand = new DelegateCommand(QualityCheckExecute);
            SaveQcCommand = new DelegateCommand(SaveQcExecute);
            Companies = new Dictionary<string, string> { { "Company 1", "Company 1" }, { "Company 2", "Company 2" } };
            Methods["ReceiveOrderGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
            Methods["OperationGetRec"].As<IErpGetMethodCall>().WhenDone += WhenOperationGetRecDone;
        }

        public DelegateCommand QualityCheckCommand { get; set; }
        public DelegateCommand AddItemToReceiveCommand { get; set; }
        public DelegateCommand CancelProcessCommand { get; set; }
        public DelegateCommand CancelSelectItemCommand { get; set; }
        public DelegateCommand CancelSelectOrderCommand { get; set; }
        public Dictionary<string, string> Companies { get; set; }
        public DelegateCommand<object> DeleteFileCommand { get; set; }
        public DelegateCommand DeleteItemCommand { get; set; }
        public DelegateCommand DeleteItemToReceiveCommand { get; set; }
        public DelegateCommand DownloadFileCommand { get; set; }
        public DelegateCommand OkCommand { get; set; }
        public DelegateCommand ProcessOrderCommand { get; set; }
        public DelegateCommand ReceiveCommand { get; set; }
        public DelegateCommand RefreshOrdersCommand { get; set; }
        public DelegateCommand SelectReceiveOrderCommand { get; set; }
        public DelegateCommand UploadFileCommand { get; set; }
        public DelegateCommand SaveQcCommand { get; set; }

        public Visibility QcVisibility
        {
            get { return GetValue(() => QcVisibility); }
            set { SetValue(() => QcVisibility, value); }
        }

        public Visibility QcMenuVisibility
        {
            get { return GetValue(() => QcMenuVisibility); }
            set { SetValue(() => QcMenuVisibility, value); }
        }

        public int OrderCount
        {
            get { return GetValue(() => OrderCount); }
            set { SetValue(() => OrderCount, value); }
        }

        public OperationItemFileDto SelectedFile
        {
            get { return GetValue(() => SelectedFile); }
            set { SetValue(() => SelectedFile, value); }
        }

        public new InventoryOperationListItem SelectedItem
        {
            get { return GetValue(() => SelectedItem); }
            set
            {
                SetValue(() => SelectedItem, value, ConfigureQcButtonVisibility);
            }
        }

        private void ConfigureQcButtonVisibility()
        {
            if(SelectedItem == null) return;
            QcMenuVisibility = SelectedItem.OperationStatus == OperationStatus.QualityControl
                ? Visibility.Visible
                : Visibility.Collapsed;
        }

        public OperationItemDetail SelectedItemToReceive
        {
            get { return GetValue(() => SelectedItemToReceive); }
            set { SetValue(() => SelectedItemToReceive, value, DeleteItemToReceiveCommand.RaiseCanExecuteChanged); }
        }

        public OperationItemDetail SelectedOperationItemDetail
        {
            get { return GetValue(() => SelectedOperationItemDetail); }
            set { SetValue(() => SelectedOperationItemDetail, value); }
        }

        public OrderDto SelectedOrder
        {
            get { return GetValue(() => SelectedOrder); }
            set { SetValue(() => SelectedOrder, value, ProcessOrderCommand.RaiseCanExecuteChanged); }
        }

        public DynamicNotifyPropertyChangedProxy SelectedOrderItem
        {
            get { return GetValue(() => SelectedOrderItem); }
            set { SetValue(() => SelectedOrderItem, value, ReceiveCommand.RaiseCanExecuteChanged); }
        }

        public OperationItemDto SelectedReceivedItem
        {
            get { return GetValue(() => SelectedReceivedItem); }
            set { SetValue(() => SelectedReceivedItem, value, DeleteItemCommand.RaiseCanExecuteChanged); }
        }

        public string SelectOrderHeaderText => "Select Receipt";

        private void WhenOperationGetRecDone()
        {
            QcVisibility =
                _entity.InventoryOperationDto.GetDto<InventoryOperationDto>().OperationStatus ==
                OperationStatus.QualityControl
                    ? Visibility.Visible
                    : Visibility.Collapsed;
        }

        private async void SaveQcExecute()
        {
            RadWindow.Confirm("Submit quality check result?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;
                SubmitQcResult();
            });
        }

        private async void SubmitQcResult()
        {
            var saveResult = await SaveRecordAsync("SaveQualityCheckResult");
            if (saveResult.OperationResult != OperationResult.Success) return;
            RefreshOrdersExecute();
            CancelProcessExecute();
        }

        private async void QualityCheckExecute()
        {
            if (_entity.InventoryOperationDto == null)
            {
                _entity.ReceivedItems.Clear();
                RegisterLookups();
                await GetLookupsAsync();
                Methods["OperationGetRec"].MethodParameter = SelectedItem.Id;
                await GetRecordAsync("OperationGetRec");
                SetView(ReceiveViewNames.ReceiveDisplayView);
            }
            SetView(ReceiveViewNames.QualityCheckView);
        }

        private void WhenGetRecDone()
        {
            _entity.InventoryOperationDto.PropertyChanged += (s, e) => SaveCommand.RaiseCanExecuteChanged();
            _entity.ReceivedItems.CollectionChanged += (s, a) => SaveCommand.RaiseCanExecuteChanged();
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<InventoryLookupAggregate>(ApiControllers.Lookup, "InventoryGetLookups");
        }

        private void AddItemExecute()
        {
            var selectedItem = SelectedOrderItem.GetDto<OrderItemDto>();
            if (selectedItem.PartDto.Tracking != Tracking.Serial)
            {
                _entity.OperationItemDetails.Add(new OperationItemDetail
                {
                    OperationItemDto = new OperationItemDto
                    {
                        PartId = selectedItem.PartId.Value,
                        PartNumber = selectedItem.PartDto.PartNumber,
                        Name = selectedItem.PartDto.Name,
                        UnitOfMeasurementId = selectedItem.PartDto.UnitOfMeasurementId.Value
                    }
                });
            }
            else
            {
                _entity.OperationItemDetails.Add(new OperationItemDetail
                {
                    OperationItemDto = new OperationItemDto
                    {
                        PartId = selectedItem.PartId.Value,
                        PartNumber = selectedItem.PartDto.PartNumber,
                        Name = selectedItem.PartDto.Name,
                        UnitOfMeasurementId = selectedItem.PartDto.UnitOfMeasurementId.Value, 
                        Quantity = 1
                    }
                });
            }
        }

        private void CancelProcessExecute()
        {
            SetView(ReceiveViewNames.ReceiveMainView);
        }

        private void CancelSelectDeliveryOrderExecute()
        {
            _selectOrderWindow.Close();
        }

        private void CancelSelectItemExecute()
        {
            _selectNonTrackingItemWindow?.Close();
            _selectSerialTrackingItemWindow?.Close();
            _selectLotTrackingItemWindow?.Close();
        }

        private void DeleteFileExecute(object obj)
        {
            if (SelectedFile == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    SelectedOperationItemDetail.OperationItemFiles.Remove(SelectedFile);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        SelectedOperationItemDetail.OperationItemFiles.Remove(SelectedFile);
                });
            }
        }

        private void DeleteItemExecute()
        {
            var order = _entity.OrderItems.FirstOrDefault(x =>
                x.GetDto<OrderItemDto>().PartId == SelectedReceivedItem.PartId);
            if (order != null)
            {
                if (order.GetDto<OrderItemDto>().PartDto.Tracking == Tracking.None)
                {
                    if (order.GetDto<OrderItemDto>().PartDto.UnitOfMeasurementId == 32)
                        order.SetProperty("Quantity", order.GetDto<OrderItemDto>().Quantity + 1);
                    else
                        order.SetProperty("Quantity",
                            order.GetDto<OrderItemDto>().Quantity + SelectedReceivedItem.Quantity);
                }
                else
                {
                    order.SetProperty("Quantity",
                        order.GetDto<OrderItemDto>().Quantity + SelectedReceivedItem.Quantity);
                }
            }
            else
            {
                var orderItem = _entity.BackingOrderItems.First(x => x.PartId == SelectedReceivedItem.PartId);
                orderItem.Quantity = SelectedReceivedItem.Quantity;
                _entity.OrderItems.Add(new DynamicNotifyPropertyChangedProxy(orderItem));
            }

            _entity.ReceivedItems.Remove(SelectedReceivedItem);
        }

        private bool DeleteItemToReceiveCanExecute()
        {
            return SelectedItemToReceive != null;
        }

        private void DeleteItemToReceiveExecute()
        {
            _entity.OperationItemDetails.Remove(SelectedItemToReceive);
        }

        private void DownloadFileExecute()
        {
            if (SelectedFile == null) return;
            _fileService.SaveFile(SelectedFile);
        }

        private void OkExecute()
        {
            if (ValidateReceivedItems())
            {
                var selectedItem = SelectedOrderItem.GetDto<OrderItemDto>();
                var totalQuantity = _entity.OperationItemDetails.Select(x => x.OperationItemDto).Sum(x => x.Quantity);
                if (selectedItem.Quantity == totalQuantity)
                    SelectedOrderItem.SetProperty("Quantity", 0m);
                else
                    SelectedOrderItem.SetProperty("Quantity", selectedItem.Quantity - totalQuantity);

                foreach (var itemDetail in _entity.OperationItemDetails)
                {
                    var addedItem = itemDetail.OperationItemDto;
                    addedItem.Files = new List<OperationItemFileDto>(itemDetail.OperationItemFiles);
                    _entity.ReceivedItems.Add(addedItem);
                }

                SelectedOrderItem = null;
                CancelSelectItemExecute();
                RefreshOrdersExecute();
            }
        }

        private bool ProcessOrderCanExecute()
        {
            return SelectedOrder != null;
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select  an Operation Item",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            _entity.ReceivedItems.Clear();
            RegisterLookups();
            await GetLookupsAsync();
            Methods["OperationGetRec"].MethodParameter = SelectedItem.Id;
            await GetRecordAsync("OperationGetRec");
            SetView(ReceiveViewNames.ReceiveDisplayView);
        }

        private async void ProcessOrderExecute()
        {
            _entity.ReceivedItems.Clear();
            RegisterLookups();
            await GetLookupsAsync();
            Methods["ReceiveOrderGetRec"].MethodParameter = SelectedOrder.OrderId.ToString();
            await GetRecordAsync("ReceiveOrderGetRec");
            SetView(ReceiveViewNames.ReceiveEditView);
            _selectOrderWindow.Close();
        }

        private bool ReceiveCanExecute()
        {
            if (SelectedOrderItem == null) return false;
            if (SelectedOrderItem.GetDto<OrderItemDto>().Quantity == 0) return false;
            return true;
        }

        private void ReceiveExecute()
        {
            var selectedItem = SelectedOrderItem.GetDto<OrderItemDto>();
            if (selectedItem.PartDto.Tracking == 0)
            {
                RadWindow.Alert("Tracking configuration required!");
            }
            else
            {
                _entity.OperationItemDetails.Clear();
                if (selectedItem.PartDto.Tracking == Tracking.None)
                {
                    // CreateNonTrackingItemSelection();
                    _selectNonTrackingItemWindow = new SelectNonTrackingItemWindow { DataContext = this };
                    _selectNonTrackingItemWindow.ShowDialog();
                }
                else if (selectedItem.PartDto.Tracking == Tracking.Lot)
                {
                    // CreateTrackingItemSelection();
                    _selectLotTrackingItemWindow = new SelectLotTrackingItemWindow { DataContext = this };
                    _selectLotTrackingItemWindow.ShowDialog();
                }
                else
                {
                    // CreateTrackingItemSelection();
                    _selectSerialTrackingItemWindow = new SelectSerialTrackingItemWindow { DataContext = this };
                    _selectSerialTrackingItemWindow.ShowDialog();
                }
            }
        }

        private void RefreshOrdersExecute()
        {
            EventAggregator.GetEvent<RefreshItemListEvent<InventoryWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Receive"));
        }

        private bool SaveCanExecute()
        {
            if (string.IsNullOrEmpty(_entity.InventoryOperationDto.GetDto<InventoryOperationDto>().Company))
                return false;
            if (_entity.InventoryOperationDto.GetDto<InventoryOperationDto>().WarehouseId == 0) return false;
            if (!_entity.ReceivedItems.Any()) return false;
            return true;
        }

        private async void SaveExecute()
        {
            var saveResult = await SaveRecordAsync("OperationAddRec");
            if (saveResult.OperationResult != OperationResult.Success) return;
            CancelProcessExecute();
        }

        private void SelectDeliveryOrderExecute()
        {
            _selectOrderWindow = new SelectOrderWindow { DataContext = this };
            _selectOrderWindow.ShowDialog();
        }

        private void UploadFileExecute()
        {
            _fileService.GetAnyFile(SelectedOperationItemDetail.OperationItemFiles);
        }

        private bool ValidateReceivedItems()
        {
            if (SelectedOrderItem.GetDto<OrderItemDto>().PartDto.Tracking == Tracking.Lot)
            {
                if (_entity.OperationItemDetails.Any(x => string.IsNullOrEmpty(x.OperationItemDto.LotNumber)))
                {
                    RadWindow.Alert("Lot number is required for each item!");
                    return false;
                }
            }

            if (SelectedOrderItem.GetDto<OrderItemDto>().PartDto.Tracking == Tracking.Serial)
            {
                if (_entity.OperationItemDetails.Any(x => string.IsNullOrEmpty(x.OperationItemDto.SerialNumber)))
                {
                    RadWindow.Alert("Serial number is required for each item!");
                    return false;
                }

                if (_entity.OperationItemDetails.Any(x => x.OperationItemDto.Quantity > 1))
                {
                    RadWindow.Alert("Quantity value must not be greater than one!");
                    return false;
                }
            }

            if (_entity.OperationItemDetails.Any(x => x.OperationItemDto.Quantity == 0))
            {
                RadWindow.Alert("Quantity value is required for each item!");
                return false;
            }

            var totalQuantity = _entity.OperationItemDetails.Select(x => x.OperationItemDto).Sum(x => x.Quantity);
            if (totalQuantity > SelectedOrderItem.GetDto<OrderItemDto>().Quantity)
            {
                RadWindow.Alert($"Quantity entered exceed.");
                return false;
            }

            if (SelectedOrderItem.GetDto<OrderItemDto>().PartDto.Tracking != Tracking.None)
            {
                if (SelectedOrderItem.GetDto<OrderItemDto>().PartDto.Tracking == Tracking.Lot)
                {
                    var hasDuplicate = _entity.OperationItemDetails.GroupBy(x => x.OperationItemDto.LotNumber)
                        .Any(g => g.Count() > 1);
                    if (hasDuplicate)
                    {
                        RadWindow.Alert("Duplicate tracking number detected!");
                        return false;
                    }
                }
                else
                {
                    var hasDuplicate = _entity.OperationItemDetails.GroupBy(x => x.OperationItemDto.SerialNumber)
                        .Any(g => g.Count() > 1);
                    if (hasDuplicate)
                    {
                        RadWindow.Alert("Duplicate tracking number detected!");
                        return false;
                    }
                }
            }

            return true;
        }
    }
}