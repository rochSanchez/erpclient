﻿using System.Windows.Controls;

namespace Kemprise.Inventory.InventoryViews.Receive
{
    /// <summary>
    /// Interaction logic for ReceiveWorkAreaView.xaml
    /// </summary>
    public partial class ReceiveWorkAreaView : UserControl
    {
        public ReceiveWorkAreaView()
        {
            InitializeComponent();
        }
    }
}
