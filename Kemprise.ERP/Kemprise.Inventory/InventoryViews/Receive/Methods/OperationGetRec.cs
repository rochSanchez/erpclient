﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Receive.Methods
{
    public class OperationGetRec : GetMethodCallBase
    {
        public OperationGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "OperationGetRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Receive.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => false;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<InventoryAggregate>(MethodName, new object[] { MethodParameter });
            entity.SetProperty("InventoryOperationDto",
                new DynamicNotifyPropertyChangedProxy(aggregate.InventoryOperationDto));
            entity.SetProperty("ReceivedItems", new ObservableCollection<OperationItemDto>(aggregate.OperationItems));
        }
    }
}