﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Receive.Methods
{
    public class ReceiveOrderGetRec : GetMethodCallBase
    {
        public ReceiveOrderGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "ReceiveOrderGetRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Receive.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => false;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<InventoryAggregate>("OrderGetRec", new object[] { MethodParameter });
            if (aggregate == null) return;
            var order = aggregate.OrderDto;
            var items = aggregate.OrderItems;
            entity.SetProperty("OrderDto", order);
            var orderItems = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            aggregate.OrderItems.ForEach(item =>
            {
               orderItems.Add(new DynamicNotifyPropertyChangedProxy(item));
            });
            entity.SetProperty("OrderItems", orderItems);
            entity.SetProperty("BackingOrderItems", aggregate.OrderItems);
            entity.SetProperty("InventoryOperationDto",
                new DynamicNotifyPropertyChangedProxy(new InventoryOperationDto
                {
                    SourceDocument = order.ReferenceNumber, OperationType = OperationType.Deliver,
                    CustomerId = order.CustomerId, From = order.CustomerName, ScheduledDate = order.ExpirationDate
                }));
        }
    }
}