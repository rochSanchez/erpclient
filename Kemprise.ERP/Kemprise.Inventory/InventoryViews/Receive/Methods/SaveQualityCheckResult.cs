﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Receive.Methods
{
    public class SaveQualityCheckResult : ErpSaveMethodCallBase
    {
        public SaveQualityCheckResult(IApiRequestManager requestManager) : base(requestManager)
        {
        }


        public override string MethodName => "SaveQualityCheckResult";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override ApiControllers Controller => ApiControllers.Inventory;
        public override string Category => InventoryTypes.Receive.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var receiveEntity = entity.As<ReceiveEntity>();

            var operation = receiveEntity.InventoryOperationDto.GetDto<InventoryOperationDto>();
            var items = receiveEntity.ReceivedItems.ToList();

            var inventoryAggregate = new InventoryAggregate
            {
                InventoryOperationDto = operation,
                OperationItems = items
            };
            
            var dataToSave = new Dictionary<string, object>
            {
                {nameof(InventoryAggregate), inventoryAggregate}
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}