﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Receive.Methods
{
    public class OperationAddRec : ErpSaveMethodCallBase
    {
        public OperationAddRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }


        public override string MethodName => "OperationAddRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override ApiControllers Controller => ApiControllers.Inventory;
        public override string Category => InventoryTypes.Receive.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var receiveEntity = entity.As<ReceiveEntity>();

            var order = receiveEntity.OrderDto;
            var operation = receiveEntity.InventoryOperationDto.GetDto<InventoryOperationDto>();
            var status = receiveEntity.OrderItems.Any(x => x.GetDto<OrderItemDto>().Quantity != 0)
                ? OrderStatus.Partial
                : OrderStatus.Completed;
            order.OrderStatus = status;
            operation.OperationType = OperationType.Receive;
            var items = receiveEntity.ReceivedItems.ToList();
            var orderItems = receiveEntity.OrderItems.ToList(); 

            var inventoryAggregate = new InventoryAggregate
            {
                OrderDto = receiveEntity.OrderDto,
                InventoryOperationDto = operation,
                OperationItems = items
            };

            inventoryAggregate.OrderItems = new List<OrderItemDto>();
            orderItems.ForEach(item =>inventoryAggregate.OrderItems.Add(item.GetDto<OrderItemDto>()));

            var dataToSave = new Dictionary<string, object>
            {
                {nameof(InventoryAggregate), inventoryAggregate}
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}