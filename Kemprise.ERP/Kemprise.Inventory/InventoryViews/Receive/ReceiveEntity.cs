﻿using System.Collections.Generic;
using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using System.Collections.ObjectModel;

namespace Kemprise.Inventory.InventoryViews.Receive
{
    public class ReceiveEntity : EntityBase
    {
        public ReceiveEntity()
        {
            Orders = new ObservableCollection<OrderDto>();
            OrderItems = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            OrderDto = new OrderDto();
            ReceivedItems = new ObservableCollection<OperationItemDto>();
            OperationItemDetails = new ObservableCollection<OperationItemDetail>();
            BackingOrderItems = new List<OrderItemDto>();
        }

        public OrderDto OrderDto { get; set; }
        public ObservableCollection<DynamicNotifyPropertyChangedProxy> OrderItems { get; set; }
        public List<OrderItemDto> BackingOrderItems { get; set; }
        public ObservableCollection<OrderDto> Orders { get; set; }
        public DynamicNotifyPropertyChangedProxy InventoryOperationDto { get; set; }
        public ObservableCollection<OperationItemDto> ReceivedItems { get; set; }
        public ObservableCollection<OperationItemDetail> OperationItemDetails{ get; set; }
    }

    public class OperationItemDetail
    {
        public OperationItemDetail()
        {
            OperationItemFiles = new ObservableCollection<OperationItemFileDto>();
        }
        public OperationItemDto OperationItemDto { get; set; }
        public ObservableCollection<OperationItemFileDto> OperationItemFiles { get; set; }
    }
}