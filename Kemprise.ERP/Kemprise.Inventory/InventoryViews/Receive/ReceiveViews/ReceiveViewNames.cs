﻿namespace Kemprise.Inventory.InventoryViews.Receive.ReceiveViews
{
    public class ReceiveViewNames
    {
        public const string ReceiveMainView = "Receive Main View";
        public const string ReceiveEditView = "Receive Edit View";
        public const string ReceiveDisplayView = "Receive Display View";
        public const string QualityCheckView = "Quality Check View";
    }
}