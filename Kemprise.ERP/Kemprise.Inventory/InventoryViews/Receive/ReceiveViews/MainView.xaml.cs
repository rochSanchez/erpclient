﻿using System.Windows.Controls;

namespace Kemprise.Inventory.InventoryViews.Receive.ReceiveViews
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        public MainView()
        {
            InitializeComponent();
        }
    }
}
