﻿using System.Windows.Controls;

namespace Kemprise.Inventory.InventoryViews.Receive.ReceiveViews
{
    /// <summary>
    /// Interaction logic for DisplayView.xaml
    /// </summary>
    public partial class DisplayView : UserControl
    {
        public DisplayView()
        {
            InitializeComponent();
        }
    }
}
