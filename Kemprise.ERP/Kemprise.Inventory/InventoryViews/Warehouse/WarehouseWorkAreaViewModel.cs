﻿using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.Inventory.InventoryViews.Warehouse.WarehouseViews;
using Kemprise.Inventory.WorkArea;
using Prism.Commands;
using System.Collections.Generic;
using System.Windows;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Inventory.InventoryViews.Warehouse
{
    public sealed class WarehouseWorkAreaViewModel : InventoryViewModelBase
    {
        private WarehouseEntity _entity;
        private AddLocationWindow _addLocationWindow;
        private AddSubLocationWindow _addSubLocationWindow;

        public WarehouseWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, InventoryTypes.Warehouse.ToString())
        {
            SetView(WarehouseViewNames.WarehouseMainView);

            ClearCommand = new DelegateCommand(ClearExecute);
            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            EditCommand = new DelegateCommand(EditExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSaveExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshWarehousesExecute);
            RefreshLocationsCommand = new DelegateCommand(RefreshLocationsExecute);
            RefreshSubLocationsCommand =
                new DelegateCommand(RefreshSubLocationsExecute, () => SelectedLocationDto != null);
            GetSubLocationsCommand = new DelegateCommand(GetSubLocationsExecute, GetSubLocationsCanExecute);
            AddSubLocationCommand = new DelegateCommand(AddSubLocationExecute, () => SelectedLocationDto != null);
            AddLocationCommand = new DelegateCommand(AddLocationExecute);
            CancelCommand = new DelegateCommand(CancelExecute);
            Companies = new Dictionary<string, string>() { { "Company 1", "Company 1" }, { "Company 2", "Company 2" } };
            SaveNewLocationCommand = new DelegateCommand(SaveNewLocationExecute);
            SaveNewSubLocationCommand = new DelegateCommand(SaveSubLocationExecute);
            EditLocationCommand = new DelegateCommand(EditLocationExecute, () => SelectedLocationDto != null);
            EditSubLocationCommand = new DelegateCommand(EditSubLocationExecute, () => SelectedSubLocationDto != null);
            Methods["WarehouseGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        private async void EditSubLocationExecute()
        {
            Methods["SubLocationGetRec"].MethodParameter = SelectedSubLocationDto.SubLocationId.ToString();
            var result = await GetRecordAsync("SubLocationGetRec");
            if (result.OperationResult != OperationResult.Success) return;
            _addSubLocationWindow = new AddSubLocationWindow
            {
                DataContext = this
            };
            _addSubLocationWindow.Show();
        }

        private async void EditLocationExecute()
        {
            Methods["LocationGetRec"].MethodParameter = SelectedLocationDto.LocationId.ToString();
            var result = await GetRecordAsync("LocationGetRec");
            if (result.OperationResult != OperationResult.Success) return;
            _addLocationWindow = new AddLocationWindow
            {
                DataContext = this
            };
            _addLocationWindow.Show();
        }

        private async void SaveSubLocationExecute()
        {
            var taskToPerform = _entity.SubLocation.GetDto<SubLocationDto>().SubLocationId == 0 ? "SubLocationAddRec" : "SubLocationModiRec";
            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;
            RefreshSubLocationsExecute();
            CancelExecute();
        }

        private async void SaveNewLocationExecute()
        {
            var taskToPerform = _entity.Location.GetDto<LocationDto>().LocationId == 0 ? "LocationAddRec" : "LocationModiRec";
            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;
            RefreshLocationsExecute();
            CancelExecute();
        }

        private void CancelExecute()
        {
            _addSubLocationWindow?.Close();
            _addLocationWindow?.Close();
        }

        private void AddLocationExecute()
        {
            _entity.Location = new DynamicNotifyPropertyChangedProxy(new LocationDto
            { WarehouseId = _entity.WarehouseDto.GetDto<WarehouseDto>().WarehouseId });

            _addLocationWindow = new AddLocationWindow
            {
                DataContext = this
            };
            _addLocationWindow.Show();
        }

        private void AddSubLocationExecute()
        {
            _entity.SubLocation = new DynamicNotifyPropertyChangedProxy(new SubLocationDto()
            { LocationId = SelectedLocationDto.LocationId });

            _addSubLocationWindow = new AddSubLocationWindow
            {
                DataContext = this
            };
            _addSubLocationWindow.Show();
        }

        public Dictionary<string, string> Companies { get; set; }

        public DelegateCommand RefreshLocationsCommand { get; set; }
        public DelegateCommand RefreshSubLocationsCommand { get; set; }
        public DelegateCommand GetSubLocationsCommand { get; set; }
        public DelegateCommand AddSubLocationCommand { get; set; }
        public DelegateCommand AddLocationCommand { get; set; }
        public DelegateCommand SaveNewLocationCommand { get; set; }
        public DelegateCommand SaveNewSubLocationCommand { get; set; }
        public DelegateCommand EditLocationCommand { get; set; }
        public DelegateCommand EditSubLocationCommand { get; set; }

        private async void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new WarehouseEntity());

            _entity = Entity.GetDto<WarehouseEntity>();
            _entity.IsNew = true;

            await OpenItemExecute("");

            IsClearVisible = true;
            SetView(WarehouseViewNames.WarehouseEditView);
        }

        private void BackExecute()
        {
            SetView(WarehouseViewNames.WarehouseMainView);
            RefreshExecute();
        }

        private bool CanSaveExecute()
        {
            var warehouseDto = _entity.WarehouseDto.GetDto<WarehouseDto>();
            if (warehouseDto == null) return false;
            return !string.IsNullOrEmpty(warehouseDto.Name) && !string.IsNullOrEmpty(warehouseDto.Prefix) &&
                   !string.IsNullOrEmpty(warehouseDto.Company) && !string.IsNullOrEmpty(warehouseDto.Address);
        }

        private void ClearExecute()
        {
            RadWindow.Confirm("This will clear everything. Continue?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.WarehouseDto.ClearValues();
            });
        }

        private async void EditExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select a Warehouse",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            Entity = new DynamicNotifyPropertyChangedProxy(new WarehouseEntity());
            _entity = Entity.GetDto<WarehouseEntity>();

            await OpenItemExecute(SelectedItem.Id);

            IsClearVisible = false;
            SetView(WarehouseViewNames.WarehouseEditView);
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new WarehouseEntity());
            _entity = Entity.GetDto<WarehouseEntity>();
            await OpenItemExecute(SelectedItem.Id);
            SetView(WarehouseViewNames.WarehouseDisplayView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<InventoryWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Warehouses"));
        }

        private async void RefreshLocationsExecute()
        {
            _entity.Locations.Clear();
            _entity.SubLocations.Clear();
            await GetRecordAsync("LocationGetRecList");
        }

        private async void RefreshSubLocationsExecute()
        {
            Methods["SubLocationGetRecList"].MethodParameter = SelectedLocationDto.LocationId.ToString();
            _entity.SubLocations.Clear();
            await GetRecordAsync("SubLocationGetRecList");
        }

        private bool GetSubLocationsCanExecute()
        {
            return SelectedLocationDto != null;
        }

        private async void GetSubLocationsExecute()
        {
            Methods["SubLocationGetRecList"].MethodParameter = SelectedLocationDto.LocationId.ToString();
            _entity.SubLocations.Clear();
            await GetRecordAsync("SubLocationGetRecList");
            SelectedSubLocationDto = null;
        }

        private void RefreshWarehousesExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        private async void SaveExecute()
        {
            var taskToPerform = _entity.IsNew ? "WarehouseAddRec" : "WarehouseModirec";
            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private void WhenGetRecDone()
        {
            _entity.WarehouseDto.PropertyChanged += (s, e) => { SaveCommand.RaiseCanExecuteChanged(); };
            SaveCommand.RaiseCanExecuteChanged();
        }

        public LocationDto SelectedLocationDto
        {
            get { return GetValue(() => SelectedLocationDto); }
            set
            {
                SetValue(() => SelectedLocationDto, value, () =>
                {
                    RefreshSubLocationsCommand.RaiseCanExecuteChanged();
                    EditLocationCommand.RaiseCanExecuteChanged();
                    AddSubLocationCommand.RaiseCanExecuteChanged();
                });
            }
        }

        public SubLocationDto SelectedSubLocationDto
        {
            get { return GetValue(() => SelectedSubLocationDto); }
            set
            {
                SetValue(() => SelectedSubLocationDto, value, () =>
                {
                    EditSubLocationCommand.RaiseCanExecuteChanged();
                });
            }
        }
    }
}