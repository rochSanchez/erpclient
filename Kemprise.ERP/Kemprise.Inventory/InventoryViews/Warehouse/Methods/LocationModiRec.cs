﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Warehouse.Methods
{
    public class LocationModiRec : ErpSaveMethodCallBase
    {
        public LocationModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "LocationModiRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override ApiControllers Controller => ApiControllers.Inventory;
        public override string Category => InventoryTypes.Warehouse.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var warehouseEntity = entity.As<WarehouseEntity>();

            var warehouseAggregate = new WarehouseAggregate
            {
                LocationDto = warehouseEntity.Location.GetDto<LocationDto>()
            };

            var dataToSave = new Dictionary<string, object>
            {
                {nameof(WarehouseAggregate), warehouseAggregate}
            };

            await UpdateRecordAsync(MethodName, dataToSave);
        }
    }
}