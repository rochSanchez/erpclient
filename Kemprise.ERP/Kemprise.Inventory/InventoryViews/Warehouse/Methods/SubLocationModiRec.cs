﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Warehouse.Methods
{
    public class SubLocationModiRec : ErpSaveMethodCallBase
    {
        public SubLocationModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "SubLocationModiRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override ApiControllers Controller => ApiControllers.Inventory;
        public override string Category => InventoryTypes.Warehouse.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var warehouseEntity = entity.As<WarehouseEntity>();

            var warehouseAggregate = new WarehouseAggregate
            {
                SubLocationDto = warehouseEntity.SubLocation.GetDto<SubLocationDto>()
            };

            var dataToSave = new Dictionary<string, object>
            {
                {nameof(WarehouseAggregate), warehouseAggregate}
            };

            await UpdateRecordAsync(MethodName, dataToSave);
        }
    }
}