﻿using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Warehouse.Methods
{
    public class LocationGetRec : GetMethodCallBase
    {
        public LocationGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "LocationGetRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Warehouse.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var location = await GetRecordAsync<LocationDto>(MethodName, new object[] { MethodParameter });
            if (location == null) return;
            entity.SetProperty("Location", new DynamicNotifyPropertyChangedProxy(location));
        }
    }
}