﻿using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Warehouse.Methods
{
    public class SubLocationGetRec : GetMethodCallBase
    {
        public SubLocationGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "SubLocationGetRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Warehouse.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var subLocationDto = await GetRecordAsync<SubLocationDto>(MethodName, new object[] { MethodParameter });
            if (subLocationDto == null) return;
            entity.SetProperty("SubLocation", new DynamicNotifyPropertyChangedProxy(subLocationDto));
        }
    }
}