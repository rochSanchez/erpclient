﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Warehouse.Methods
{
    public class WarehouseGetRec : GetMethodCallBase
    {
        public WarehouseGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "WarehouseGetRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Warehouse.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var warehouseAggregate =
                await GetRecordAsync<WarehouseAggregate>(MethodName, new object[] {MethodParameter});
            if (warehouseAggregate == null) return;
            
            if (warehouseAggregate.WarehouseDto != null)
                entity.SetProperty("WarehouseDto",
                    new DynamicNotifyPropertyChangedProxy(warehouseAggregate.WarehouseDto));

            entity.SetProperty("Locations",
                new ObservableCollection<LocationDto>(warehouseAggregate.Locations));
        }
    }
}