﻿namespace Kemprise.Inventory.InventoryViews.Warehouse.WarehouseViews
{
    public class WarehouseViewNames
    {
        public const string WarehouseMainView = "Warehouse Main View";
        public const string WarehouseEditView = "Warehouse Edit View";
        public const string WarehouseDisplayView = "Warehouse Display View";
    }
}