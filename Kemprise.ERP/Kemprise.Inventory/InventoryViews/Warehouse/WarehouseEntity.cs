﻿using System.Collections.ObjectModel;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.Inventory.InventoryViews.Warehouse
{
    public class WarehouseEntity : EntityBase
    {
        public WarehouseEntity()
        {
            WarehouseDto = new DynamicNotifyPropertyChangedProxy(new WarehouseDto());
            Locations = new ObservableCollection<LocationDto>();
            SubLocations = new ObservableCollection<SubLocationDto>();
            Location = new DynamicNotifyPropertyChangedProxy(new LocationDto());
            SubLocation = new DynamicNotifyPropertyChangedProxy(new SubLocationDto());
        }
        public DynamicNotifyPropertyChangedProxy WarehouseDto { get; set; }
        public ObservableCollection<LocationDto> Locations { get; set; }
        public ObservableCollection<SubLocationDto> SubLocations { get; set; }

        public DynamicNotifyPropertyChangedProxy Location { get; set; }
        public DynamicNotifyPropertyChangedProxy SubLocation { get; set; }
    }
}