﻿using System.Windows.Controls;

namespace Kemprise.Inventory.InventoryViews.Internal.InternalViews
{
    /// <summary>
    /// Interaction logic for EditView.xaml
    /// </summary>
    public partial class EditView : UserControl
    {
        public EditView()
        {
            InitializeComponent();
        }
    }
}
