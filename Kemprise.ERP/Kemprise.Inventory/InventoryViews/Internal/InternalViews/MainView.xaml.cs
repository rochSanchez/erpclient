﻿using System.Windows.Controls;

namespace Kemprise.Inventory.InventoryViews.Internal.InternalViews
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        public MainView()
        {
            InitializeComponent();
        }
    }
}
