﻿namespace Kemprise.Inventory.InventoryViews.Internal.InternalViews
{
    public class InternalViewNames
    {
        public const string InternalMainView = "Internal Main View";
        public const string InternalEditView = "Internal Edit View";
        public const string InternalDisplayView = "Internal Display View";
        public const string InternalNewView = "Internal New View";
    }
}