﻿using System.Windows.Controls;

namespace Kemprise.Inventory.InventoryViews.Internal.InternalViews
{
    /// <summary>
    /// Interaction logic for DisplayView.xaml
    /// </summary>
    public partial class DisplayView : UserControl
    {
        public DisplayView()
        {
            InitializeComponent();
        }
    }
}
