﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using ERP.WebApi.DTO.DTOs.Inventory;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.Inventory.InventoryViews.Internal
{
    public class InternalEntity : EntityBase
    {
        public InternalEntity()
        {
            InternalOperationDto = new InternalOperationDto();
            InternalOperationItems = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            NewInternalOperation = new DynamicNotifyPropertyChangedProxy(InternalOperationDto);
            NewInternalOperationItems = new ObservableCollection<InternalOperationItemDto>();
            PartStocks = new ObservableCollection<DynamicPartStock>();
            PartStocksBackup = new List<StockDto>();
        }

        public DynamicNotifyPropertyChangedProxy NewInternalOperation { get; set; }
        public InternalOperationDto InternalOperationDto { get; set; }
        public ObservableCollection<DynamicNotifyPropertyChangedProxy> InternalOperationItems { get; set; }
        public ObservableCollection<InternalOperationItemDto> NewInternalOperationItems { get; set; }
        public StockDto SelectedStock { get; set; }
        public ObservableCollection<DynamicPartStock> PartStocks { get; set; }
        public List<StockDto> PartStocksBackup { get; set; }
        public List<StockDto> AffectedStocks { get; set; }
    }

    public class DynamicPartStock
    {
        public int PartId { get; set; }
        public string Name { get; set; }
        public ObservableCollection<DynamicNotifyPropertyChangedProxy> Stocks { get; set; }
    }

    public class InternalOpItem
    {
        public int PartId { get; set; }
        public int StockId { get; set; }
        public string Description { get; set; }
        public string LotNumber { get; set; }
        public string SerialNumber { get; set; }
        public decimal Quantity { get; set; }
        public string TrackingNumber { get; set; }
        public PartDto PartDto { get; set; }
    }
}