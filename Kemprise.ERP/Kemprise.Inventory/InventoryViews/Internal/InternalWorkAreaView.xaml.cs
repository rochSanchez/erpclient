﻿using System.Windows.Controls;

namespace Kemprise.Inventory.InventoryViews.Internal
{
    /// <summary>
    /// Interaction logic for ReceiveWorkAreaView.xaml
    /// </summary>
    public partial class InternalWorkAreaView : UserControl
    {
        public InternalWorkAreaView()
        {
            InitializeComponent();
        }
    }
}
