﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Internal.Methods
{
    public class ConfirmInternalOperation : ErpSaveMethodCallBase
    {
        public ConfirmInternalOperation(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "ConfirmInternalOperation";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override ApiControllers Controller => ApiControllers.Inventory;
        public override string Category => InventoryTypes.Internal.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var internalEntity = entity.As<InternalEntity>();


            var inventoryAggregate = new InventoryAggregate
            {
                InternalOperationDto = internalEntity.InternalOperationDto
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(InventoryAggregate), inventoryAggregate }
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}