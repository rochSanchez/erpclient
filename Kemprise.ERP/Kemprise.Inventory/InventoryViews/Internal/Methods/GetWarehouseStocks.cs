﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Internal.Methods
{
    public class GetWarehouseStocks : GetMethodCallBase
    {
        public GetWarehouseStocks(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "GetWarehouseStocks";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Internal.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => false;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<InventoryAggregate>("GetWarehouseStocks", new object[] { MethodParameter });
            if (aggregate == null) return;

            var partStocks = new ObservableCollection<DynamicPartStock>();
            aggregate.PartStocks.ForEach(item =>
            {
                var dynamicPartStock = new DynamicPartStock
                {
                    PartId = item.PartId, Name = item.PartName,
                    Stocks = new ObservableCollection<DynamicNotifyPropertyChangedProxy>()
                };

                var proxies = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
                item.Stocks.ForEach(stock =>proxies.Add(new DynamicNotifyPropertyChangedProxy(stock)));
                dynamicPartStock.Stocks = proxies;
                partStocks.Add(dynamicPartStock);
            });
            entity.SetProperty("PartStocks", partStocks);
            
            var copy = new List<StockDto>();
            aggregate.PartStocks.ForEach(item =>
            {
                item.Stocks.ForEach(stock => copy.Add(new StockDto {StockId = stock.StockId, PartId = stock.PartId, Quantity = stock.Quantity}));
            });

            entity.SetProperty("PartStocksBackup", copy);
        }
    }
}