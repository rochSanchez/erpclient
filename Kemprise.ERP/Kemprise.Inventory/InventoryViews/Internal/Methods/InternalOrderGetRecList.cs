﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Internal.Methods
{
    public class InternalOrderGetRecList : GetMethodCallBase
    {
        public InternalOrderGetRecList(IApiRequestManager requestManager) : base(requestManager)
        {
            MethodParameter = "3";
        }

        public override string MethodName => "InternalOrderGetRecList";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Internal.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => false;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var orderDtos = await GetRecordsAsync<OrderDto>("OrderGetRecList", new object[] { MethodParameter });
            if (orderDtos == null) return;

            entity.SetProperty("Orders", new ObservableCollection<OrderDto>(orderDtos));
        }
    }
}