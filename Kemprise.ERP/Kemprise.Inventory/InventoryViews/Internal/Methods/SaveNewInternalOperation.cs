﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Internal.Methods
{
    public class SaveNewInternalOperation : ErpSaveMethodCallBase
    {
        public SaveNewInternalOperation(IApiRequestManager requestManager) : base(requestManager)
        {
        }


        public override string MethodName => "SaveNewInternalOperation";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override ApiControllers Controller => ApiControllers.Inventory;
        public override string Category => InventoryTypes.Internal.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var internalEntity = entity.As<InternalEntity>();


            var inventoryAggregate = new InventoryAggregate
            {
                InternalOperationDto = internalEntity.NewInternalOperation.GetDto<InternalOperationDto>(),
                InternalOperationItems = internalEntity.NewInternalOperationItems.ToList(),
                Stocks = internalEntity.AffectedStocks
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(InventoryAggregate), inventoryAggregate }
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}