﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Inventory.InventoryViews.Receive;

namespace Kemprise.Inventory.InventoryViews.Internal.Methods
{
    public class InternalOperationGetRec : GetMethodCallBase
    {
        public InternalOperationGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "InternalOperationGetRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Internal.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => false;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<InventoryAggregate>(MethodName, new object[] { MethodParameter });
            if (aggregate == null) return;
            var dto = aggregate.InternalOperationDto;
            entity.SetProperty("InternalOperationDto", dto);
            var internalOperationItems = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            aggregate.InternalOperationItems.ForEach(item =>
            {
               internalOperationItems.Add(new DynamicNotifyPropertyChangedProxy(item));
            });
            entity.SetProperty("InternalOperationItems", internalOperationItems);
        }
    }
}