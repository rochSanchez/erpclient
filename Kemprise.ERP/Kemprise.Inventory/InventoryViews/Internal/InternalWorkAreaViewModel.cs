﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using ERP.WebApi.DTO.DTOs.Inventory;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Inventory.InventoryViews.Common;
using Kemprise.Inventory.InventoryViews.Internal.InternalViews;
using Kemprise.Inventory.InventoryViews.Internal.Methods;
using Prism.Commands;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Inventory.InventoryViews.Internal
{
    public sealed class InternalWorkAreaViewModel : InventoryViewModelBase
    {
        private SelectOrderWindow _selectOrderWindow;
        private SelectLotStockWindow _selectLotStockWindow;
        private InternalEntity _entity;

        public DelegateCommand RefreshOrdersCommand { get; set; }
        public DelegateCommand CancelProcessCommand { get; set; }
        public DelegateCommand ConfirmCommand { get; set; }
        public DelegateCommand DeliverySlipCommand { get; set; }
        public DelegateCommand PickTicketCommand { get; set; }
        public DelegateCommand NewInternalCommand { get; set; }
        public DelegateCommand SelectWarehouseStocksCommand { get; set; }
        public DelegateCommand CancelSelectStock { get; set; }
        public DelegateCommand OkCommand { get; set; }
        public DelegateCommand DeleteItemToMoveCommand { get; set; }
        public DelegateCommand DeleteItemCommand { get; set; }
        public DelegateCommand AddItemToMoveCommand { get; set; }
        public DelegateCommand NewOperationConfirmCommand { get; set; }
        public DelegateCommand<GridViewCellEditEndedEventArgs> CellEditEndedCommand { get; set; }
        public Dictionary<string, string> Companies { get; set; }


        public InternalWorkAreaViewModel(IUnityContainer container) : base(container,
            InventoryTypes.Internal.ToString())
        {
            SetView(InternalViewNames.InternalMainView);
            RefreshOrdersCommand = new DelegateCommand(RefreshOrdersExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            CancelProcessCommand = new DelegateCommand(CancelProcessExecute);
            ConfirmCommand = new DelegateCommand(ConfirmExecute);
            PickTicketCommand = new DelegateCommand(PickTicketExecute);
            DeliverySlipCommand = new DelegateCommand(DeliverySlipExecute);
            NewInternalCommand = new DelegateCommand(NewInternalExecute);
            SelectWarehouseStocksCommand =
                new DelegateCommand(SelectWarehouseStocksExecute, SelectWarehouseStocksCanExecute);
            CancelSelectStock = new DelegateCommand(CancelSelectStockExecute);
            AddItemToMoveCommand = new DelegateCommand(AddItemToMoveExecute, AddItemToMoveCanExecute);
            DeleteItemToMoveCommand = new DelegateCommand(DeleteItemExecute, DeleteItemToReceiveCanExecute);
            CellEditEndedCommand = new DelegateCommand<GridViewCellEditEndedEventArgs>(CellEditEndedExecute);
            DeleteItemCommand = new DelegateCommand(DeleteInternalItemExecute, DeleteInternalItemCanExecute);
            NewOperationConfirmCommand = new DelegateCommand(NewOperationConfirmExecute, NewOperationConfirmCanExecute);
            OkCommand = new DelegateCommand(OkExecute);
            Companies = new Dictionary<string, string>() { { "Company 1", "Company 1" }, { "Company 2", "Company 2" } };
            Methods["InternalOperationGetRec"].As<IErpGetMethodCall>().WhenDone += WhenDone;
        }
        
        private bool NewOperationConfirmCanExecute()
        {
            if (_entity.NewInternalOperation.GetDto<InternalOperationDto>().FromWarehouseId == 0) return false;
            if (_entity.NewInternalOperation.GetDto<InternalOperationDto>().ToWarehouseId == 0) return false;
            if (_entity.NewInternalOperation.GetDto<InternalOperationDto>().ScheduledDate == null) return false;
            if (!_entity.NewInternalOperationItems.Any()) return false;
            return true;
        }

        private async void SaveNewInternalOperation()
        {
            _entity.AffectedStocks = new List<StockDto>();
            _entity.NewInternalOperationItems.ForEach(item =>
            {
                var stock = _entity.PartStocks.First(x => x.PartId == item.PartId).Stocks
                    .FirstOrDefault(x => x.GetDto<StockDto>().StockId == item.StockId);
                if (_entity.AffectedStocks.Any(x => x.StockId == item.StockId)) return;
                _entity.AffectedStocks.Add(stock.GetDto<StockDto>());
            });

            var saveResult = await SaveRecordAsync("SaveNewInternalOperation");
            if (saveResult.OperationResult != OperationResult.Success) return;
            RefreshOrdersExecute();
            CancelProcessExecute();
        }

        private void NewOperationConfirmExecute()
        {
            RadWindow.Confirm("Submit new internal operation?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;
                SaveNewInternalOperation();
            });
        }

        private bool DeleteInternalItemCanExecute()
        {
            return SelectedInternalItem != null;
        }

        private void DeleteInternalItemExecute()
        {
            var itemToRemove = SelectedInternalItem;
            _entity.NewInternalOperationItems.Remove(itemToRemove);


            var stock = _entity.PartStocks.First(x => x.PartId == itemToRemove.PartId).Stocks
                .FirstOrDefault(x => x.GetDto<StockDto>().StockId == itemToRemove.StockId);

            var itemRemovedQuantity = itemToRemove.Quantity;
            var currentStockValue = stock.GetDto<StockDto>().Quantity;
            stock.SetProperty("Quantity", currentStockValue + itemRemovedQuantity);
        }

        private void OkExecute()
        {
            if (_entity.InternalOperationItems.Any(x => x.GetDto<InternalOpItem>().Quantity == 0))
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Item with 0 quantity found.",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
            else
            {
                _entity.InternalOperationItems.ForEach(item =>
                {
                    var newItem = item.GetDto<InternalOpItem>();
                    _entity.NewInternalOperationItems.Add(new InternalOperationItemDto
                    {
                        LotNumber = newItem.LotNumber, PartId = newItem.PartId, Quantity = newItem.Quantity,
                        SerialNumber = newItem.SerialNumber, Description = newItem.Description,
                        TrackingNumber = newItem.PartDto.Tracking == Tracking.Lot
                            ? newItem.LotNumber
                            : newItem.SerialNumber,
                        PartDto = newItem.PartDto,StockId = newItem.StockId
                    });
                });

                _selectLotStockWindow.Close();
            }
        }

        private void CellEditEndedExecute(GridViewCellEditEndedEventArgs obj)
        {
            var internalOpItems =
                obj.Cell.DataContext.As<DynamicNotifyPropertyChangedProxy>().GetDto<InternalOpItem>();
            var originalStockQuantity = _entity.PartStocksBackup
                .FirstOrDefault(x => x.StockId == internalOpItems.StockId)?.Quantity;
            var stock = _entity.PartStocks.First(x => x.PartId == internalOpItems.PartId).Stocks
                .FirstOrDefault(x => x.GetDto<StockDto>().StockId == internalOpItems.StockId);

            var remainingQuantity = SelectedStock.GetDto<StockDto>().Quantity;
            if (obj.NewData.ToString() != obj.OldData.ToString())
            {
                var value = (decimal)obj.NewData;
                if (value > remainingQuantity)
                {
                    RadWindow.Alert(new DialogParameters
                    {
                        Content = "Exceeded max quantity.",
                        Owner = Application.Current.MainWindow,
                        DialogStartupLocation = WindowStartupLocation.CenterScreen
                    });

                    //BUG
                    SelectedOperationItem.SetProperty("Quantity", 0m);

                    var stockOpItemsCount = _entity.InternalOperationItems
                        .Where(x => x.GetDto<InternalOpItem>().StockId == internalOpItems.StockId)
                        .Sum(x => x.GetDto<InternalOpItem>().Quantity);

                    if (originalStockQuantity == stockOpItemsCount)
                        stock.SetProperty("Quantity", 0m);
                    else
                    {
                        var difference = originalStockQuantity - stockOpItemsCount;
                        stock.SetProperty("Quantity", difference);
                    }
                }
                else
                {
                    if (decimal.Parse(obj.NewData.ToString()) == 0)
                    {
                        var newVal = SelectedStock.GetDto<StockDto>().Quantity + decimal.Parse(obj.OldData.ToString());
                        stock.SetProperty("Quantity", newVal);
                    }
                    else
                    {
                        var newVal = SelectedStock.GetDto<StockDto>().Quantity - value;
                        stock.SetProperty("Quantity", newVal);
                    }
                }
            }
        }

        private bool AddItemToMoveCanExecute()
        {
            return SelectedStock != null;
        }

        private bool DeleteItemToReceiveCanExecute()
        {
            return SelectedOperationItem != null;
        }

        private void DeleteItemExecute()
        {
            var itemToRemove = SelectedOperationItem;
            _entity.InternalOperationItems.Remove(itemToRemove);

            var stock = _entity.PartStocks.First(x => x.PartId == itemToRemove.GetDto<InternalOpItem>().PartId).Stocks
                .FirstOrDefault(x => x.GetDto<StockDto>().StockId == itemToRemove.GetDto<InternalOpItem>().StockId);

            var itemRemovedQuantity = itemToRemove.GetDto<InternalOpItem>().Quantity;
            var currentStockValue = stock.GetDto<StockDto>().Quantity;
            stock.SetProperty("Quantity", currentStockValue + itemRemovedQuantity);
        }

        private void AddItemToMoveExecute()
        {
            if (SelectedStock == null) return;
            var selectedStock = SelectedStock.GetDto<StockDto>();
            if (selectedStock.Quantity == 0)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Remaining quantity is 0.",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
            else
            {
                var item = new InternalOpItem
                {
                    LotNumber = selectedStock.LotNumber,
                    Description = selectedStock.Description,
                    PartId = selectedStock.PartId,
                    SerialNumber = selectedStock.SerialNumber,
                    StockId = selectedStock.StockId,
                    TrackingNumber = selectedStock.PartDto.Tracking == Tracking.Lot
                        ? selectedStock.LotNumber
                        : selectedStock.SerialNumber,
                    PartDto = selectedStock.PartDto
                };

                _entity.InternalOperationItems.Add(new DynamicNotifyPropertyChangedProxy(item));
            }
        }
        
        private void CancelSelectStockExecute()
        {
            _selectLotStockWindow?.Close();
        }

        public InternalOperationItemDto SelectedInternalItem
        {
            get { return GetValue(() => SelectedInternalItem); }
            set { SetValue(() => SelectedInternalItem, value, ()=>DeleteItemCommand.RaiseCanExecuteChanged()); }
        }

        public DynamicNotifyPropertyChangedProxy SelectedOperationItem
        {
            get { return GetValue(() => SelectedOperationItem); }
            set { SetValue(() => SelectedOperationItem, value, DeleteItemToMoveCommand.RaiseCanExecuteChanged); }
        }

        public DynamicPartStock SelectedPartStock
        {
            get { return GetValue(() => SelectedPartStock); }
            set { SetValue(() => SelectedPartStock, value); }
        }

        public DynamicNotifyPropertyChangedProxy SelectedStock
        {
            get { return GetValue(() => SelectedStock); }
            set { SetValue(() => SelectedStock, value, AddItemToMoveCommand.RaiseCanExecuteChanged); }
        }

        private bool SelectWarehouseStocksCanExecute()
        {
            if (_entity.NewInternalOperation.GetDto<InternalOperationDto>().FromWarehouseId == 0) return false;
            return true;
        }

        private async void SelectWarehouseStocksExecute()
        {
            if (_entity.PartStocks.Any())
            {
                _entity.InternalOperationItems = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
                _selectLotStockWindow = new SelectLotStockWindow
                    { DataContext = this, Owner = Application.Current.MainWindow };
                _selectLotStockWindow.ShowDialog();
            }
            else
            {
                Methods["GetWarehouseStocks"].MethodParameter = _entity.NewInternalOperation.GetDto<InternalOperationDto>()
                    .FromWarehouseId.ToString();
                var result = await GetRecordAsync("GetWarehouseStocks");
                if (result.OperationResult == OperationResult.Failed) return;
                _entity.InternalOperationItems = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
                _selectLotStockWindow = new SelectLotStockWindow
                    { DataContext = this, Owner = Application.Current.MainWindow };
                _selectLotStockWindow.ShowDialog();
            }
        }

        private async void NewInternalExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new InternalEntity {IsNew = true});
            _entity = Entity.GetDto<InternalEntity>();
            RegisterLookups();
            await GetLookupsAsync();
            _entity.PartStocks = new ObservableCollection<DynamicPartStock>();
            _entity.NewInternalOperation = new DynamicNotifyPropertyChangedProxy(new InternalOperationDto
                { InternalSource = InternalSource.Internal });
            _entity.InternalOperationItems = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            _entity.NewInternalOperationItems.Clear();
            _entity.NewInternalOperation.PropertyChanged += NewInternalOperationOnPropertyChanged;
            _entity.NewInternalOperationItems.CollectionChanged += (s,e)=>NewOperationConfirmCommand.RaiseCanExecuteChanged();
            SetView(InternalViewNames.InternalNewView);
        }

        private void NewInternalOperationOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "FromWarehouseId") SelectWarehouseStocksCommand.RaiseCanExecuteChanged();
            NewOperationConfirmCommand.RaiseCanExecuteChanged();
        }

        private void DeliverySlipExecute()
        {
            RunReport("DeliverySlip");
        }

        private void PickTicketExecute()
        {
            RunReport("PickTicket");
        }

        public Visibility ConfirmVisibility
        {
            get { return GetValue(() => ConfirmVisibility); }
            set { SetValue(() => ConfirmVisibility, value); }
        }

        private void WhenDone()
        {
            if (_entity.InternalOperationDto.OperationStatus == OperationStatus.Done)
                ConfirmVisibility = Visibility.Collapsed;
        }

        private void ConfirmExecute()
        {
            RadWindow.Confirm("Confirm internal operation?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;
                Confirm();
            });
        }

        private async void Confirm()
        {
            var methodName = _entity.InternalOperationDto.InternalSource == InternalSource.Deliver
                ? "ConfirmDeliveryInternalOperation"
                : "ConfirmInternalOperation";
            var saveResult = await SaveRecordAsync(methodName);
            if (saveResult.OperationResult != OperationResult.Success) return;
            RefreshOrdersExecute();
            CancelProcessExecute();
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;
            Entity = new DynamicNotifyPropertyChangedProxy(new InternalEntity());
            _entity = Entity.GetDto<InternalEntity>();
            ConfirmVisibility = Visibility.Visible;
            RegisterLookups();
            await GetLookupsAsync();
            Methods["InternalOperationGetRec"].MethodParameter = SelectedItem.Id;
            await GetRecordAsync("InternalOperationGetRec");
            SetView(InternalViewNames.InternalEditView);
        }

        private void CancelProcessExecute()
        {
            SetView(InternalViewNames.InternalMainView);
        }

        private void RefreshOrdersExecute()
        {
            EventAggregator.GetEvent<RefreshItemListEvent<WorkArea.InventoryWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Internal"));
        }

        public string SelectOrderHeaderText => "Select Internal";

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<InventoryLookupAggregate>(ApiControllers.Lookup, "InventoryGetLookups");
        }
    }
}