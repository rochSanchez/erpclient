﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Inventory.Methods
{
    public class InventoryPartModiRec : ErpSaveMethodCallBase
    {
        public InventoryPartModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "InventoryPartModiRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override ApiControllers Controller => ApiControllers.Inventory;
        public override string Category => InventoryTypes.Inventory.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var partDto = entity.As<InventoryEntity>().PartDto.GetDto<PartDto>();

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(PartDto), partDto }
            };

            await SaveNewRecordAsync<PartDto>("PartModiRec", dataToSave);
        }
    }
}