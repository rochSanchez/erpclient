﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Inventory.Methods
{
    public class ItemsLocationGetRecList : GetMethodCallBase
    {
        public ItemsLocationGetRecList(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "ItemsLocationGetRecList";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Inventory.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var locationDtos = await GetRecordsAsync<LocationDto>("LocationGetRecList", new object[] { MethodParameter });

            if (locationDtos == null) return;
            entity.SetProperty("Locations",
                new ObservableCollection<LocationDto>(locationDtos));
        }
    }
}