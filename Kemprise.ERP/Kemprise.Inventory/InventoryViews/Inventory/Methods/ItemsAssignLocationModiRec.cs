﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Inventory.Methods
{
    public class ItemsAssignLocationModiRec : ErpSaveMethodCallBase
    {
        public ItemsAssignLocationModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "ItemsAssignLocationModiRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override ApiControllers Controller => ApiControllers.Inventory;
        public override string Category => InventoryTypes.Inventory.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var stock = entity.As<InventoryEntity>().SelectedStock;

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(StockDto), stock }
            };

            await SaveNewRecordAsync<StockDto>("AssignLocationModiRec", dataToSave);
        }
    }
}