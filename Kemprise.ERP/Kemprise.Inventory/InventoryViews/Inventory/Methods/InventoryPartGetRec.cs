﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using ERP.WebApi.DTO.DTOs.Management.Currency;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Inventory.Methods
{
    public class InventoryPartGetRec : GetMethodCallBase
    {
        public InventoryPartGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override ApiControllers Controller => ApiControllers.Inventory;
        public override string MethodName => "InventoryPartGetRec";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Inventory.ToString();
        public override bool RunAtStartup => true;
        public override bool UseDefaultParameter => true;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var part = await GetRecordAsync<InventoryAggregate>("PartGetRec", new object[] { MethodParameter });
            if (part == null) return;
            entity.SetProperty("PartDto", new DynamicNotifyPropertyChangedProxy(part.PartDto));
            entity.SetProperty("Stocks", new ObservableCollection<StockDto>(part.Stocks));
        }
    }
}