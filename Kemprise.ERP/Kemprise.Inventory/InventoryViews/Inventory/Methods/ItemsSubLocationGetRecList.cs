﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;

namespace Kemprise.Inventory.InventoryViews.Inventory.Methods
{
    public class ItemsSubLocationGetRecList : GetMethodCallBase
    {
        public ItemsSubLocationGetRecList(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "ItemsSubLocationGetRecList";
        public override ErpModules ModuleName => ErpModules.Inventory;
        public override string Category => InventoryTypes.Inventory.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => false;
        public override ApiControllers Controller => ApiControllers.Inventory;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var subLocationDtos = await GetRecordsAsync<SubLocationDto>("SubLocationGetRecList", new object[] { MethodParameter });
            if (subLocationDtos == null) return;
            entity.SetProperty("SubLocations",
                new ObservableCollection<SubLocationDto>(subLocationDtos));
        }
    }
}