﻿using System.Windows.Controls;

namespace Kemprise.Inventory.InventoryViews.Inventory
{
    /// <summary>
    /// Interaction logic for ReceiveWorkAreaView.xaml
    /// </summary>
    public partial class InventoryWorkAreaView : UserControl
    {
        public InventoryWorkAreaView()
        {
            InitializeComponent();
        }
    }
}
