﻿using System.Collections.ObjectModel;
using ERP.WebApi.DTO.DTOs.Inventory;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.Inventory.InventoryViews.Inventory
{
    public class InventoryEntity : EntityBase
    {
        public InventoryEntity()
        {
            PartDto = new DynamicNotifyPropertyChangedProxy(new PartDto());
            Locations = new ObservableCollection<LocationDto>();
            SubLocations = new ObservableCollection<SubLocationDto>();
        }

        public ObservableCollection<SubLocationDto> SubLocations { get; set; }
        public ObservableCollection<LocationDto> Locations { get; set; }
        public DynamicNotifyPropertyChangedProxy PartDto { get; set; }
        public ObservableCollection<StockDto> Stocks { get; set; }
        public StockDto Stock { get; set; }
        public StockDto SelectedStock { get; set; }
    }
}