﻿using System.Collections.Generic;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Inventory.InventoryViews.Inventory.InventoryViews;
using System.Windows;
using ERP.WebApi.DTO.DTOs.Inventory;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Inventory.InventoryViews.Common;
using Prism.Commands;

namespace Kemprise.Inventory.InventoryViews.Inventory
{
    public sealed class InventoryWorkAreaViewModel : InventoryViewModelBase
    {
        private InventoryEntity _entity;
        private AssignLocationWindow _assignLocationWindow;
        public InventoryWorkAreaViewModel(IUnityContainer container) : base(container,
            InventoryTypes.Inventory.ToString())
        {
            SetView(InventoryViewNames.InventoryMainView);
            OpenCommand = new DelegateCommand(OpenExecute);
            CancelCommand  = new DelegateCommand(CancelExecute);
            ConfigCommand = new DelegateCommand(ConfigExecute);
            SaveConfigCommand = new DelegateCommand(SaveConfigExecute, SaveConfigCanExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshExecute);
            ManageLocationCommand = new DelegateCommand(ManageLocationExecute, ManageLocationCanExecute);
            CancelLocationAssignmentCommand = new DelegateCommand(CancelLocationAssignmentExecute);
            RefreshLocationsCommand = new DelegateCommand(RefreshLocationsExecute, ManageLocationCanExecute);
            AssignLocationCommand = new DelegateCommand(AssignLocationExecute, AssignLocationCanExecute);
            GetSubLocationsCommand = new DelegateCommand(GetSubLocationsExecute);
            ProductTypes = new Dictionary<ProductType, string> { { ProductType.Stock, "Stock" }, { ProductType.NonStock, "Non-Stock" } };
            Trackings = new Dictionary<Tracking, string> { { Tracking.Lot, "Lot Number" }, { Tracking.Serial, "Serial Number" },
                { Tracking.None, "No Tracking" } };

  
            Methods["InventoryPartGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        private async void GetSubLocationsExecute()
        {
            if(SelectedLocationDto == null) return;
            Methods["ItemsSubLocationGetRecList"].As<IErpGetMethodCall>().MethodParameter = SelectedLocationDto.LocationId.ToString();
            await GetRecordAsync("ItemsSubLocationGetRecList");
        }

        private bool AssignLocationCanExecute()
        {
            return SelectedLocationDto != null;
        }

        private async void AssignLocationExecute()
        {
            SelectedStock.LocationId = SelectedLocationDto?.LocationId;
            SelectedStock.SubLocationId = SelectedSubLocationDto?.SubLocationId;
            _entity.SelectedStock = SelectedStock;
            var saveResult = await SaveRecordAsync("ItemsAssignLocationModiRec");
            if (saveResult.OperationResult != OperationResult.Success) return;
            SelectedStock = null;
            SelectedLocationDto = null;
            SelectedSubLocationDto = null;
            await GetRecordAsync("InventoryPartGetRec");
            CancelLocationAssignmentExecute();
        }

        private async void RefreshLocationsExecute()
        {
            SelectedSubLocationDto = null;
            Methods["ItemsLocationGetRecList"].As<IErpGetMethodCall>().MethodParameter = SelectedStock.WarehouseId.ToString();
            await GetRecordAsync("ItemsLocationGetRecList");
        }

        private void CancelLocationAssignmentExecute()
        {
            _assignLocationWindow?.Close();
        }

        private bool ManageLocationCanExecute()
        {
            return SelectedStock != null;
        }

        private async void ManageLocationExecute()
        {
            Methods["ItemsLocationGetRecList"].As<IErpGetMethodCall>().MethodParameter = SelectedStock.WarehouseId.ToString();
            await GetRecordAsync("ItemsLocationGetRecList");
            _assignLocationWindow = new AssignLocationWindow { DataContext = this };
            _assignLocationWindow.ShowDialog();
        }

        public DelegateCommand RefreshLocationsCommand { get; set; }
        public DelegateCommand CancelLocationAssignmentCommand { get; set; }
        public DelegateCommand AssignLocationCommand { get; set; }
        public DelegateCommand ManageLocationCommand { get; set; }
        public DelegateCommand GetSubLocationsCommand { get; set; }
        public StockDto SelectedStock
        {
            get { return GetValue(() => SelectedStock); }
            set { SetValue(() => SelectedStock, value, () =>
            {
                ManageLocationCommand.RaiseCanExecuteChanged();
                RefreshLocationsCommand.RaiseCanExecuteChanged();
            }); }
        }

        public LocationDto SelectedLocationDto
        {
            get { return GetValue(() => SelectedLocationDto); }
            set
            {
                SetValue(() => SelectedLocationDto, value, () =>
                {
                    AssignLocationCommand.RaiseCanExecuteChanged();
                });
            }
        }

        public SubLocationDto SelectedSubLocationDto
        {
            get { return GetValue(() => SelectedSubLocationDto); }
            set
            {
                SetValue(() => SelectedSubLocationDto, value, () =>
                {
                    AssignLocationCommand.RaiseCanExecuteChanged();
                });
            }
        }

        private void WhenGetRecDone()
        {
            _entity.PartDto.PropertyChanged += (s, e) => { SaveConfigCommand.RaiseCanExecuteChanged(); };
            SaveConfigCommand.RaiseCanExecuteChanged();
        }

        private bool SaveConfigCanExecute()
        {
            var part = _entity.PartDto.GetDto<PartDto>();
            if (part.CanBeSold) if (part.SalesPrice == 0) return false;
            return _entity.PartDto.GetDto<PartDto>().ProductType != 0 && _entity.PartDto.GetDto<PartDto>().Tracking != 0;
        }

        public DelegateCommand SaveConfigCommand { get; set; }

        private async void SaveConfigExecute()
        {
            var saveResult = await SaveRecordAsync("InventoryPartModiRec");
            if (saveResult.OperationResult != OperationResult.Success) return;

            CancelExecute();
        }

        public Dictionary<Tracking, string> Trackings { get; set; }
        public Dictionary<ProductType, string> ProductTypes { get; set; }
        
        public Visibility NoTrackingStockCountVisibility
        {
            get { return GetValue(() => NoTrackingStockCountVisibility); }
            set { SetValue(() => NoTrackingStockCountVisibility, value); }
        }

        private async void ConfigExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select a Product",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
            else
            {
                Entity = new DynamicNotifyPropertyChangedProxy(new InventoryEntity());
                _entity = Entity.GetDto<InventoryEntity>();

                await OpenItemExecute(SelectedItem.Id);

                SetView(InventoryViewNames.InventoryEditView);
            }
        }

        public DelegateCommand ConfigCommand { get; set; }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new InventoryEntity());
            _entity = Entity.GetDto<InventoryEntity>();

            await OpenItemExecute(SelectedItem.Id);

            if (_entity.PartDto.GetDto<PartDto>().Tracking == 0 || _entity.PartDto.GetDto<PartDto>().ProductType == 0)
                SetView(InventoryViewNames.InventoryEditView);
            else
            {
                if (_entity.PartDto.GetDto<PartDto>().ProductType == ProductType.NonStock)
                    SetView(InventoryViewNames.InventoryNonStockView);
                else
                    ConfigureView();
            }
        }

        private void ConfigureView()
        {
            var tracking = _entity.PartDto.GetDto<PartDto>().Tracking;
            if(tracking == Tracking.None) SetView(InventoryViewNames.InventoryNoneView);
            if (tracking == Tracking.Lot) SetView(InventoryViewNames.InventoryLotView);
            if (tracking == Tracking.Serial) SetView(InventoryViewNames.InventorySerialView);
        }

        private void CancelExecute()        
        {
            SetView(InventoryViewNames.InventoryMainView);
        }

        private void RefreshExecute(object o)
        {
            EventAggregator.GetEvent<RefreshItemListEvent<WorkArea.InventoryWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Items"));
        }
        
        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<InventoryLookupAggregate>(ApiControllers.Lookup, "InventoryGetLookups");
        }
    }
}