﻿using System.Windows.Controls;

namespace Kemprise.Inventory.InventoryViews.Inventory.InventoryViews
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        public MainView()
        {
            InitializeComponent();
        }
    }
}
