﻿using System.Windows.Controls;

namespace Kemprise.Inventory.InventoryViews.Inventory.InventoryViews
{
    /// <summary>
    /// Interaction logic for EditView.xaml
    /// </summary>
    public partial class EditView : UserControl
    {
        public EditView()
        {
            InitializeComponent();
        }
    }
}
