﻿using System.Windows.Controls;

namespace Kemprise.Inventory.InventoryViews.Inventory.InventoryViews
{
    /// <summary>
    /// Interaction logic for SerialDisplayView.xaml
    /// </summary>
    public partial class SerialDisplayView : UserControl
    {
        public SerialDisplayView()
        {
            InitializeComponent();
        }
    }
}
