﻿namespace Kemprise.Inventory.InventoryViews.Inventory.InventoryViews
{
    public class InventoryViewNames
    {
        public const string InventoryMainView = "Inventory Main View";
        public const string InventoryEditView = "Inventory Edit View";
        public const string InventorySerialView = "Inventory Serial View";
        public const string InventoryLotView = "Inventory Lot View";
        public const string InventoryNoneView = "Inventory None View";
        public const string InventoryNonStockView = "Inventory Nonstock";
    }
}