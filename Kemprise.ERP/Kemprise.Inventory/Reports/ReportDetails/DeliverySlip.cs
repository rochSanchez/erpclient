﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using ERP.WebApi.DTO.Lookups;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Reporting;
using Kemprise.Inventory.InventoryViews.Internal;

namespace Kemprise.Inventory.Reports.ReportDetails
{
    public class DeliverySlip : IModuleReportDetail
    {
        public string ModuleName => ErpModules.Inventory.ToString();
        public string ReportTitle => "Delivery Slip";
        public string ReportFileName => "deliveryslip";

        public DataSet ConfigureReportData(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            var internalEntity = entity.As<InternalEntity>();
            var dataSet = new DataSet();
            dataSet.Tables.Add(new ReportData().ToDataTable());
            return dataSet;
        }

        public ReportParameters GetReportParameters(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            return new ReportParameters();
        }

        public class ReportData
        {

        }
    }
}