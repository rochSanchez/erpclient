﻿using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.ViewModel;
using Unity;

namespace Kemprise.Inventory
{
    public class InventoryViewModelBase : ErpViewModelBase
    {
        protected InventoryViewModelBase(IUnityContainer container, string category) :
            base(container, ErpModules.Inventory, category)

        {
        }

        protected override string ModuleReportAssembly => "Kemprise.Inventory";
        protected override string ModuleReportFilePath => @"Reports\ReportFiles";
    }
}
