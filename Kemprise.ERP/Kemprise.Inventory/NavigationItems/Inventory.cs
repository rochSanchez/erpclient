﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.Inventory;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Inventory.InventoryViews.Inventory;
using Kemprise.Inventory.InventoryViews.Warehouse;
using Unity;

namespace Kemprise.Inventory.NavigationItems
{
    public class Inventory : ParentNavigationItemBase
    {
        private readonly InventoryWorkAreaView _workAreaView;

        public Inventory(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<InventoryWorkAreaView>();
            Order = 3;
        }

        public override string Name => "Items";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Stocks.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Inventory;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<NewPartListItem>(ApiControllers.PLM, "ReleasedPartsGetRecList");
        }
    }
}