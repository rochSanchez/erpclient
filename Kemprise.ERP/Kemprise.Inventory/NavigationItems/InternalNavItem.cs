﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Inventory.InventoryViews.Internal;
using Unity;

namespace Kemprise.Inventory.NavigationItems
{
    public class InternalNavItem : ChildNavigationItemBase, IChildNavigationItem
    {
        private readonly InternalWorkAreaView _workAreaView;

        public InternalNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<InternalWorkAreaView>();
        }

        public override string Name => "Internal";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Internal.png", UriKind.Relative);

        public override Type Parent => typeof(OperationNavItem);

        public override ErpModules Module => ErpModules.Inventory;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            // var vm = _workAreaView.GetViewModel().As<InternalWorkAreaViewModel>();
            // await vm.GetRecordAsync("InternalOrderGetRecList");
            // vm.OrderCount = vm.Entity.GetDto<InternalEntity>().Orders.Count;
            return await RequestManager.GetRecordsAsync<InternalOperationListItem>(ApiControllers.Inventory, "InternalOperationGetRecList");
        }
    }
}