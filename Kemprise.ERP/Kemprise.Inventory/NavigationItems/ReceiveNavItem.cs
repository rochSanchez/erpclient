﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Inventory.InventoryViews.Deliver;
using Kemprise.Inventory.InventoryViews.Receive;
using Unity;

namespace Kemprise.Inventory.NavigationItems
{
    public class ReceiveNavItem : ChildNavigationItemBase, IChildNavigationItem
    {
        private readonly ReceiveWorkAreaView _workAreaView;

        public ReceiveNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<ReceiveWorkAreaView>();
        }

        public override string Name => "Receive";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Receive.png", UriKind.Relative);

        public override Type Parent => typeof(OperationNavItem);

        public override ErpModules Module => ErpModules.Inventory;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            var vm = _workAreaView.GetViewModel().As<ReceiveWorkAreaViewModel>();
            await vm.GetRecordAsync("ReceiveOrderGetRecList");
            vm.OrderCount = vm.Entity.GetDto<ReceiveEntity>().Orders.Count;
            return await RequestManager.GetRecordsAsync<InventoryOperationListItem>(ApiControllers.Inventory, "InventoryOperationGetRecList", new object[] { 2 });
        }
    }
}