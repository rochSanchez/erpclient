﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.Inventory;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Inventory.InventoryViews.Deliver;
using Unity;

namespace Kemprise.Inventory.NavigationItems
{
    public class DeliverNavItem : ChildNavigationItemBase, IChildNavigationItem
    {
        private readonly DeliverWorkAreaView _workAreaView;

        public DeliverNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<DeliverWorkAreaView>();
        }

        public override string Name => "Deliver";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Deliver.png", UriKind.Relative);

        public override Type Parent => typeof(OperationNavItem);

        public override ErpModules Module => ErpModules.Inventory;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            var vm = _workAreaView.GetViewModel().As<DeliverWorkAreaViewModel>();
            await vm.GetRecordAsync("OrderGetRecList");
            vm.OrderCount = vm.Entity.GetDto<DeliverEntity>().Orders.Count;
            
            return await RequestManager.GetRecordsAsync<InventoryOperationListItem>(ApiControllers.Inventory, "InventoryOperationGetRecList", new object[] { 1 });
        }
    }
}