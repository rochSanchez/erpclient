﻿using System;
using System.Linq;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Unity;

namespace Kemprise.Inventory.NavigationItems
{
    public class OperationNavItem : ParentNavigationItemBase
    {
        public OperationNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            Children = container.ResolveAll<IChildNavigationItem>()
                .Where(item => item.Parent == typeof(OperationNavItem));
            Order = 1;
        }

        public override string Name => "Operations";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Inventory.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Inventory;
    }
}