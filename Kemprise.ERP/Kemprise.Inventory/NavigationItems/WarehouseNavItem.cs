﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.Inventory;
using ERP.WebApi.DTO.DTOs.Management.Currency;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Inventory.InventoryViews.Warehouse;
using Unity;

namespace Kemprise.Inventory.NavigationItems
{
    public class WarehouseNavItem : ParentNavigationItemBase
    {
        private readonly WarehouseWorkAreaView _workAreaView;

        public WarehouseNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<WarehouseWorkAreaView>();
            Order = 2;
        }

        public override string Name => "Warehouse";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Warehouse.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Inventory;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<WarehouseListItem>(ApiControllers.Inventory, "WarehouseGetRecList");
        }
    }
}