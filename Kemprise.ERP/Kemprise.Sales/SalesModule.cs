﻿using ERP.WebApi.DTO;
using Kemprise.Infrastructure.Controllers;
using Kemprise.Infrastructure.Modules;
using Kemprise.Sales.Controller;
using Kemprise.Sales.WorkArea;
using Prism.Ioc;
using Prism.Unity;
using System.Reflection;
using Unity;
using Unity.Lifetime;

namespace Kemprise.Sales
{
    public class SalesModule : ErpModule
    {
        public SalesModule(IUnityContainer container)
        {
            container.RegisterType(typeof(IControllerBase), typeof(SalesController),
                nameof(SalesController), new ContainerControlledLifetimeManager());
        }

        protected override Assembly GetBusinessAssembly()
        {
            return Assembly.GetAssembly(typeof(EntityListItem));
        }

        protected override Assembly GetModuleAssembly()
        {
            return Assembly.GetAssembly(typeof(SalesModule));
        }

        public override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.GetContainer().RegisterType<object, SalesWorkAreaView>(typeof(SalesWorkAreaView).FullName, new ContainerControlledLifetimeManager());
        }
    }
}