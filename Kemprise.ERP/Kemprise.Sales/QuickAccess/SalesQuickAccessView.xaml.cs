﻿using Kemprise.Infrastructure.Modules;

namespace Kemprise.Sales.QuickAccess
{
    /// <summary>
    /// Interaction logic for ManagementQuickAccessView.xaml
    /// </summary>
    public partial class SalesQuickAccessView : IQuickAccessView
    {
        public SalesQuickAccessView()
        {
            InitializeComponent();
        }

        public int Order => 2;
    }
}