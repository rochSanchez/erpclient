﻿using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.ViewModel;
using Unity;

namespace Kemprise.Sales
{
    public class SalesViewModelBase : ErpViewModelBase
    {
        protected SalesViewModelBase(IUnityContainer container, string category) :
            base(container, ErpModules.Sales, category)
        {
        }

        protected override string ModuleReportAssembly => "Kemprise.Sales";
        protected override string ModuleReportFilePath => @"Reports\ReportFiles";
    }
}