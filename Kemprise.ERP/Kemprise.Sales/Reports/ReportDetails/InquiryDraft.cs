﻿using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using ERP.WebApi.DTO.Lookups;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Reporting;
using Kemprise.Sales.SalesViews.Inquiry;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace Kemprise.Sales.Reports.ReportDetails
{
    public class InquiryDraft : IModuleReportDetail
    {
        public string ModuleName => ErpModules.Sales.ToString();
        public string ReportTitle => "Inquiry";
        public string ReportFileName => "inquirydraft";

        public DataSet ConfigureReportData(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            var inquiryEntity = entity.As<InquiryEntity>();
            var inquiryDto = inquiryEntity.InquiryDto.GetDto<InquiryDto>();

            var inquiryReportData = new InquiryReportData
            {
                InquiryNumber = inquiryDto.InquiryNumber,
                DateCreated = inquiryDto.DateCreated == null
                    ? ""
                    : inquiryDto.DateCreated.Value.ToString("dd/MMM/yyyy"),
                ExpirationDate = inquiryDto.ExpirationDate == null
                    ? ""
                    : inquiryDto.ExpirationDate.Value.ToString("dd/MMM/yyyy"),
                PaymentTerms = inquiryDto.PaymentTerms,
                ShipTo = inquiryDto.CustomerName.ToUpper(),
                BillingAddress = $"{inquiryDto.BillingAddress}{Environment.NewLine}{inquiryDto.BillingCountry}",
                ShippingAddress = $"{inquiryDto.ShippingAddress}{Environment.NewLine}{inquiryDto.ShippingCountry}"
            };
            var inquiryTable = inquiryReportData.ToDataTable();

            var inquiryDetailsData = new List<InquiryDetailsData>();
            var inquiryDetails = inquiryEntity.InquiryDetails;
            inquiryDetails.ForEach(x =>
            {
                var detail = x.GetDto<InquiryDetailDto>();

                inquiryDetailsData.Add(new InquiryDetailsData
                {
                    Item = detail.OrderNumber.ToString(),
                    PartNumber = detail.PartNumber ?? "",
                    Description = string.IsNullOrEmpty(detail.Description)
                        ? ""
                        : detail.Description.Trim(),
                    UnitOfMeasurement = detail.UnitOfMeasurement ?? "",
                    Quantity = detail.Quantity.ToString("G29")
                });
            });
            var inquiryDetailsTable = inquiryDetailsData.ToDataTable();

            var dataSet = new DataSet();
            dataSet.Tables.Add(inquiryTable);
            dataSet.Tables.Add(inquiryDetailsTable);

            return dataSet;
        }

        public ReportParameters GetReportParameters(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            return new ReportParameters();
        }
    }

    public class InquiryReportData
    {
        public string InquiryNumber { get; set; }
        public string Description { get; set; }
        public string DateCreated { get; set; }
        public string ExpirationDate { get; set; }
        public string PaymentTerms { get; set; }
        public string ShipTo { get; set; }
        public string BillingAddress { get; set; }
        public string ShippingAddress { get; set; }
    }

    public class InquiryDetailsData
    {
        public string Item { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string UnitOfMeasurement { get; set; }
        public string Quantity { get; set; }
    }
}