﻿using ERP.WebApi.DTO.DTOs.Sales.Invoicing;
using ERP.WebApi.DTO.Lookups;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Reporting;
using Kemprise.Infrastructure.Services.Currency;
using Kemprise.Sales.SalesViews.Invoicing;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace Kemprise.Sales.Reports.ReportDetails
{
    public class Invoice : IModuleReportDetail
    {
        public string ModuleName => ErpModules.Sales.ToString();
        public string ReportTitle => "Invoice";
        public string ReportFileName => "invoice";

        public DataSet ConfigureReportData(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            var invoiceEntity = entity.As<InvoiceEntity>();
            var invoiceDto = invoiceEntity.InvoiceDto.GetDto<InvoiceDto>();
            var total = invoiceDto.Total.ToCurrency(invoiceDto.CurrencyName, invoiceDto.Prefix);

            var invoiceReportData = new InvoiceReportData
            {
                InvoiceNumber = invoiceDto.InvoiceNumber,
                DateCreated = invoiceDto.DateCreated == null
                    ? ""
                    : $"{invoiceDto.DateCreated.Value:dd/MMM/yyyy}",
                PurchaseOrderNumber = invoiceDto.PurchaseOrderNumber,
                CustomerName = invoiceDto.CustomerName,
                BillingAddress = invoiceDto.BillingAddress,
                ShippingAddress = invoiceDto.ShippingAddress,
                SubTotal = $"{invoiceDto.SubTotal:N2}",
                TaxAmount = $"{invoiceDto.TaxAmount:N2}",
                TotalDiscount = $"{invoiceDto.TotalDiscount:N2}",
                Total = total
            };
            var invoiceTable = invoiceReportData.ToDataTable();

            var invoiceDetailsData = new List<InvoiceDetailsData>();
            var invoiceDetails = invoiceEntity.InvoiceDetails;
            invoiceDetails.ForEach(x =>
            {
                var detail = x.GetDto<InvoiceDetailDto>();

                invoiceDetailsData.Add(new InvoiceDetailsData
                {
                    Item = detail.OrderNumber.ToString(),
                    PartNumber = detail.PartNumber,
                    Description = string.IsNullOrEmpty(detail.Description)
                        ? ""
                        : detail.Description.Trim(),
                    UnitOfMeasurement = detail.UnitOfMeasurement,
                    Quantity = detail.Quantity.ToString("G29"),
                    Price = detail.Price.ToString("N2"),
                    Total = detail.Total.ToString("N2")
                });
            });
            var invoiceDetailsTable = invoiceDetailsData.ToDataTable();

            var dataSet = new DataSet();
            dataSet.Tables.Add(invoiceTable);
            dataSet.Tables.Add(invoiceDetailsTable);

            return dataSet;
        }

        public ReportParameters GetReportParameters(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            return new ReportParameters();
        }
    }

    public class InvoiceReportData
    {
        public string InvoiceNumber { get; set; }
        public string DateCreated { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public string CustomerName { get; set; }
        public string BillingAddress { get; set; }
        public string ShippingAddress { get; set; }
        public string SubTotal { get; set; }
        public string TotalDiscount { get; set; }
        public string TaxAmount { get; set; }
        public string Total { get; set; }
    }

    public class InvoiceDetailsData
    {
        public string Item { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string UnitOfMeasurement { get; set; }
        public string Quantity { get; set; }
        public string Price { get; set; }
        public string Total { get; set; }
    }
}