﻿using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using ERP.WebApi.DTO.Lookups;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Reporting;
using Kemprise.Infrastructure.Services.Currency;
using Kemprise.Sales.SalesViews.Quotation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace Kemprise.Sales.Reports.ReportDetails
{
    public class Quotation : IModuleReportDetail
    {
        public string ModuleName => ErpModules.Sales.ToString();
        public string ReportTitle => "Quotation";
        public string ReportFileName => "quotation";

        public DataSet ConfigureReportData(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            var quotationEntity = entity.As<QuotationEntity>();
            var quotationDto = quotationEntity.QuotationDto.GetDto<QuotationDto>();
            var total = quotationDto.Total.ToCurrency(quotationDto.CurrencyName, quotationDto.Prefix);

            var shippingTerms = string.Empty;
            if (!string.IsNullOrEmpty(quotationDto.ShippingTerms))
            {
                var shippingTermsArray = quotationDto.ShippingTerms.Split(',');
                var length = shippingTermsArray.Length;

                var counter = 0;
                do
                {
                    shippingTerms += $"{shippingTermsArray[counter].Trim()}{Environment.NewLine}";
                    counter++;
                } while (counter < length);
            }

            var quotationReportData = new QuotationReportData
            {
                QuotationNumber = quotationDto.ClientVersion > 0
                    ? $"{quotationDto.QuotationNumber}-{quotationDto.ClientVersion:N0}"
                    : $"{quotationDto.QuotationNumber}",
                DateCreated = quotationDto.DateCreated == null
                    ? ""
                    : $"{quotationDto.DateCreated.Value:dd/MMM/yyyy}",
                ExpirationDate = quotationDto.ExpirationDate == null
                    ? ""
                    : $"{quotationDto.ExpirationDate.Value:dd/MMM/yyyy}",
                PaymentTerms = quotationDto.PaymentTerms,
                SubTotal = $"{quotationDto.SubTotal:N2}",
                TaxAmount = $"{quotationDto.TaxAmount:N2}",
                TotalDiscount = $"{quotationDto.TotalDiscount:N2}",
                Total = total,
                ShipTo = quotationDto.CustomerName.ToUpper(),
                BillingAddress = $"{quotationDto.BillingAddress}{Environment.NewLine}{quotationDto.BillingCountry}",
                ShippingAddress = $"{quotationDto.ShippingAddress}{Environment.NewLine}{quotationDto.ShippingCountry}",
                Delivery = $"{quotationDto.Delivery}",
                ShippingTerms = shippingTerms
            };
            var quotationTable = quotationReportData.ToDataTable();

            var quotationDetailsData = new List<QuotationDetailsData>();
            var quotationDetails = quotationEntity.QuotationDetails;
            quotationDetails.ForEach(x =>
            {
                var detail = x.GetDto<QuotationDetailDto>();

                quotationDetailsData.Add(new QuotationDetailsData
                {
                    Item = detail.OrderNumber.ToString(),
                    PartNumber = detail.PartNumber,
                    Description = string.IsNullOrEmpty(detail.Description)
                        ? ""
                        : detail.Description.Trim(),
                    UnitOfMeasurement = detail.UnitOfMeasurement,
                    Quantity = detail.Quantity.ToString("G29"),
                    Price = detail.Price.ToString("N2"),
                    Total = detail.Total.ToString("N2")
                });
            });
            var quotationDetailsTable = quotationDetailsData.ToDataTable();

            var quotationNotesData = new List<QuotationNotesData>();
            quotationEntity.QuotationNotes.ForEach(note =>
            {
                quotationNotesData.Add(new QuotationNotesData
                {
                    Item = note.Item.ToString("N0"),
                    Name = note.Name
                });
            });
            var quotationNotesTable = quotationNotesData.ToDataTable();

            var dataSet = new DataSet();
            dataSet.Tables.Add(quotationTable);
            dataSet.Tables.Add(quotationDetailsTable);
            dataSet.Tables.Add(quotationNotesTable);

            return dataSet;
        }

        public ReportParameters GetReportParameters(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            return new ReportParameters();
        }
    }

    public class QuotationReportData
    {
        public string QuotationNumber { get; set; }
        public string DateCreated { get; set; }
        public string ExpirationDate { get; set; }
        public string PaymentTerms { get; set; }
        public string SubTotal { get; set; }
        public string TaxAmount { get; set; }
        public string TotalDiscount { get; set; }
        public string Total { get; set; }
        public string ShipTo { get; set; }
        public string BillingAddress { get; set; }
        public string ShippingAddress { get; set; }
        public string Delivery { get; set; }
        public string ShippingTerms { get; set; }
    }

    public class QuotationDetailsData
    {
        public string Item { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string UnitOfMeasurement { get; set; }
        public string Quantity { get; set; }
        public string Price { get; set; }
        public string Total { get; set; }
    }

    public class QuotationNotesData
    {
        public string Item { get; set; }
        public string Name { get; set; }
    }
}