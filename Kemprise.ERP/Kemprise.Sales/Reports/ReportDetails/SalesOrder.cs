﻿using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using ERP.WebApi.DTO.Lookups;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Reporting;
using Kemprise.Infrastructure.Services.Currency;
using Kemprise.Sales.SalesViews.SalesOrder;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace Kemprise.Sales.Reports.ReportDetails
{
    public class SalesOrder : IModuleReportDetail
    {
        public string ModuleName => ErpModules.Sales.ToString();
        public string ReportTitle => "Sales Order";
        public string ReportFileName => "salesorder";

        public DataSet ConfigureReportData(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            var quotationEntity = entity.As<SalesOrderEntity>();
            var quotationDto = quotationEntity.QuotationDto.GetDto<QuotationDto>();
            var total = quotationDto.Total.ToCurrency(quotationDto.CurrencyName, quotationDto.Prefix);

            var shippingTerms = string.Empty;
            if (!string.IsNullOrEmpty(quotationDto.ShippingTerms))
            {
                var shippingTermsArray = quotationDto.ShippingTerms.Split(',');
                var length = shippingTermsArray.Length;

                var counter = 0;
                do
                {
                    shippingTerms += $"{shippingTermsArray[counter].Trim()}{Environment.NewLine}";
                    counter++;
                } while (counter < length);
            }

            var salesOrderReportData = new SalesOrderReportData
            {
                PurchaseOrderNumber = quotationDto.PurchaseOrderNumber,
                QuotationNumber = quotationDto.QuotationNumber,
                PaymentTerms = quotationDto.PaymentTerms,
                SubTotal = quotationDto.SubTotal.ToString("N2"),
                TaxAmount = quotationDto.TaxAmount.ToString("N2"),
                TotalDiscount = quotationDto.TotalDiscount.ToString("N2"),
                Total = total,
                ShipTo = quotationDto.CustomerName.ToUpper(),
                BillingAddress = $"{quotationDto.BillingAddress}{Environment.NewLine}{quotationDto.BillingCountry}",
                ShippingAddress = $"{quotationDto.ShippingAddress}{Environment.NewLine}{quotationDto.ShippingCountry}",
                SalesOrderDate = quotationDto.SalesOrderDate == null
                    ? ""
                    : quotationDto.SalesOrderDate.Value.ToString("dd/MMM/yyyy"),
                Delivery = quotationDto.Delivery,
                ShippingTerms = shippingTerms
            };
            var salesOrderTable = salesOrderReportData.ToDataTable();

            var quotationDetailsData = new List<QuotationDetailsData>();
            var quotationDetails = quotationEntity.QuotationDetails;
            quotationDetails.ForEach(x =>
            {
                var detail = x.GetDto<QuotationDetailDto>();

                quotationDetailsData.Add(new QuotationDetailsData
                {
                    Item = detail.OrderNumber.ToString(),
                    PartNumber = detail.PartNumber,
                    Description = detail.Description.Trim(),
                    UnitOfMeasurement = detail.UnitOfMeasurement,
                    Quantity = detail.Quantity.ToString("G29"),
                    Price = detail.Price.ToString("N2"),
                    Total = detail.Total.ToString("N2")
                });
            });
            var quotationDetailsTable = quotationDetailsData.ToDataTable();

            var quotationNotesData = new List<QuotationNotesData>();
            quotationEntity.QuotationNotes.ForEach(note =>
            {
                quotationNotesData.Add(new QuotationNotesData
                {
                    Item = note.Item.ToString("N0"),
                    Name = note.Name
                });
            });
            var quotationNotesTable = quotationNotesData.ToDataTable();

            var dataSet = new DataSet();
            dataSet.Tables.Add(salesOrderTable);
            dataSet.Tables.Add(quotationDetailsTable);
            dataSet.Tables.Add(quotationNotesTable);

            return dataSet;
        }

        public ReportParameters GetReportParameters(EntityBase entity, Dictionary<string, ObservableCollection<LookupEntity>> lookups)
        {
            return new ReportParameters();
        }

        public class SalesOrderReportData
        {
            public string PurchaseOrderNumber { get; set; }
            public string QuotationNumber { get; set; }
            public string PaymentTerms { get; set; }
            public string SubTotal { get; set; }
            public string TaxAmount { get; set; }
            public string TotalDiscount { get; set; }
            public string Total { get; set; }
            public string ShipTo { get; set; }
            public string BillingAddress { get; set; }
            public string ShippingAddress { get; set; }
            public string SalesOrderDate { get; set; }
            public string Delivery { get; set; }
            public string ShippingTerms { get; set; }
        }
    }
}