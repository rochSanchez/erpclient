﻿using Kemprise.Infrastructure.Controllers;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Events.Security;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Regions;
using Kemprise.Sales.WorkArea;
using Prism.Events;
using Prism.Regions;
using Unity;
using Unity.Lifetime;

namespace Kemprise.Sales.Controller
{
    public class SalesController : ControllerBase
    {
        public SalesController(IEventAggregator eventAggregator, IRegionManager regionManager, IUnityContainer container) :
            base(eventAggregator, regionManager, container)
        {
            eventAggregator.GetEvent<OpenModuleEvent>().Subscribe(OnOpenModule);
            eventAggregator.GetEvent<LogoutUserEvent>().Subscribe(OnLogoutUserEvent);
        }

        private void OnLogoutUserEvent()
        {
            Container.RegisterType<object, SalesWorkAreaView>(typeof(SalesWorkAreaView).FullName, new ContainerControlledLifetimeManager());
        }

        private void OnOpenModule(OpenModuleEventPayload obj)
        {
            if (obj.ErpModule == ErpModules.Sales)
                obj.QuickAccess.IsSelected = true;
            else
            {
                EventAggregator.GetEvent<DeSelectModuleEvent<SalesController>>().Publish();
                return;
            }

            RegionManager.RequestNavigate(RegionNames.MainWorkAreaRegion, typeof(SalesWorkAreaView).FullName);
        }
    }
}