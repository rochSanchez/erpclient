﻿using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Sales.SalesViews.Inquiry;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.Sales.NavigationItems
{
    public class InquiryNavItem : ParentNavigationItemBase
    {
        private readonly InquiryWorkAreaView _workAreaView;

        public InquiryNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<InquiryWorkAreaView>();
        }

        public override string Name => "Inquiry";
        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Inquiry.png", UriKind.Relative);
        public override ErpModules Module => ErpModules.Sales;
        public override object View => _workAreaView;
        public override int Order => 1;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<InquiryListItem>(ApiControllers.Inquiry, "InquiryGetRecList");
        }
    }
}