﻿using ERP.WebApi.DTO;
using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Currency;
using Kemprise.Sales.SalesViews.Invoicing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.Sales.NavigationItems
{
    public class InvoiceNavItem : ParentNavigationItemBase
    {
        private readonly InvoiceWorkAreaView _workAreaView;

        public InvoiceNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<InvoiceWorkAreaView>();

            IsVisible = ApiSession.HasRoles(ErpRoles.CreateQuotation);
        }

        public override string Name => "Invoicing";
        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Invoice.png", UriKind.Relative);
        public override ErpModules Module => ErpModules.Sales;
        public override object View => _workAreaView;
        public override int Order => 4;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            var itemList = await RequestManager.GetRecordsAsync<QuotationListItem>(ApiControllers.Invoice, "InvoiceGetRecList");
            itemList = itemList.ToList();

            itemList.ForEach(x => x.StringTotal = x.Total.ToCurrency(x.CurrencyCode, x.Prefix));
            return itemList;
        }
    }
}