﻿using ERP.WebApi.DTO;
using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Currency;
using Kemprise.Sales.SalesViews.SalesOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.Sales.NavigationItems
{
    public class SalesOrderNavItem : ParentNavigationItemBase
    {
        private readonly SalesOrderWorkAreaView _workAreaView;

        public SalesOrderNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<SalesOrderWorkAreaView>();

            IsVisible = ApiSession.HasRoles(ErpRoles.CreateQuotation);
        }

        public override string Name => "Sales Order";
        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/SalesOrder.png", UriKind.Relative);
        public override ErpModules Module => ErpModules.Sales;
        public override object View => _workAreaView;
        public override int Order => 3;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            var itemList = await RequestManager.GetRecordsAsync<QuotationListItem>(ApiControllers.SalesOrder, "SalesOrderGetRecList");
            itemList = itemList.ToList();

            itemList.ForEach(x => x.StringTotal = x.Total.ToCurrency(x.CurrencyCode, x.Prefix));
            return itemList;
        }
    }
}