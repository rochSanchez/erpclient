﻿using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using ERP.WebApi.DTO.Lookups.LookupItems;
using Kemprise.Infrastructure.Converters;
using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Currency;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.Infrastructure.Services.Sales;
using Kemprise.Infrastructure.Windows.AskForRevision;
using Kemprise.Infrastructure.Windows.CloseEntity;
using Kemprise.Infrastructure.Windows.ReasonForClosing;
using Kemprise.Infrastructure.Windows.ReasonForRevision;
using Kemprise.Sales.SalesViews.Quotation.QuotationViews;
using Kemprise.Sales.WorkArea;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Sales.SalesViews.Quotation
{
    public sealed class QuotationWorkAreaViewModel : SalesViewModelBase
    {
        private readonly IFileService _fileService;
        private QuotationEntity _entity;

        public QuotationWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, SalesTypes.Quotation.ToString())
        {
            _fileService = fileService;
            SetView(QuotationViewNames.QuotationMainView);

            SortedInquiryCommand = new DelegateCommand(() => SelectedInquiry = null);
            GroupedInquiryCommand = new DelegateCommand(() => SelectedInquiry = null);
            FilteredInquiryCommand = new DelegateCommand(() => SelectedInquiry = null);

            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            BackCommand = new DelegateCommand(BackExecute);
            EditAsCommand = new DelegateCommand(EditAsExecute);
            PrintDraftCommand = new DelegateCommand(PrintDraftExecute);
            SaveForLaterCommand = new DelegateCommand(SaveForLaterExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSave);
            CancelCommand = new DelegateCommand(CancelExecute);
            AddExistingItemCommand = new DelegateCommand(AddExistingItemExecute);
            AddBlankItemCommand = new DelegateCommand(AddBlankItemExecute);
            AddPartCommand = new DelegateCommand(AddPartExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshQuotationExecute);

            CreateQuotationCommand = new DelegateCommand(CreateQuotationExecute);
            UploadFileCommand = new DelegateCommand(UploadFileExecute);
            DownloadFileCommand = new DelegateCommand(DownloadFileExecute);
            AddNoteCommand = new DelegateCommand(AddNoteExecute);
            AskForRevisionCommand = new DelegateCommand(AskForRevisionExecute);
            ViewReasonForRevisionCommand = new DelegateCommand(ViewReasonForRevisionExecute);
            SubmitForApprovalCommand = new DelegateCommand(SubmitForApprovalExecute);
            ApproveQuotationCommand = new DelegateCommand(ApproveQuotationExecute);
            CloseEntityCommand = new DelegateCommand(CloseEntityExecute);
            ViewClosingDetailsCommand = new DelegateCommand(ViewClosingDetailsExecute);
            PrintQuotationCommand = new DelegateCommand(PrintQuotationExecute);
            AddPartNumberCommand = new DelegateCommand(AddPartNumberExecute);
            OrderReceivedCommand = new DelegateCommand(OrderReceivedExecute);
            DeleteItemCommand = new DelegateCommand<object>(DeleteItemExecute);
            DeleteFileCommand = new DelegateCommand<object>(DeleteFileExecute);
            DeleteNoteCommand = new DelegateCommand<object>(DeleteNoteExecute);
            RadioButtonIsCheckedCommand = new DelegateCommand<object>(RadioButtonIsCheckedExecute);

            SelectedFilter = new KeyValuePair<string, string>("ALL", "ALL");
            Methods["QuotationGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        public DelegateCommand SortedInquiryCommand { get; set; }
        public DelegateCommand GroupedInquiryCommand { get; set; }
        public DelegateCommand FilteredInquiryCommand { get; set; }
        public DelegateCommand CreateQuotationCommand { get; set; }
        public DelegateCommand UploadFileCommand { get; set; }
        public DelegateCommand DownloadFileCommand { get; set; }
        public DelegateCommand AddNoteCommand { get; set; }
        public DelegateCommand SubmitForApprovalCommand { get; set; }
        public DelegateCommand ApproveQuotationCommand { get; set; }
        public DelegateCommand PrintQuotationCommand { get; set; }
        public DelegateCommand AddPartNumberCommand { get; set; }
        public DelegateCommand OrderReceivedCommand { get; set; }
        public DelegateCommand<object> DeleteItemCommand { get; set; }
        public DelegateCommand<object> DeleteFileCommand { get; set; }
        public DelegateCommand<object> DeleteNoteCommand { get; set; }

        public Dictionary<string, string> QuotationFilters { get; set; } = new Dictionary<string, string>
        {
            {"ALL","ALL" },
            {QuotationStatus.SubmittedForApproval, QuotationStatus.SubmittedForApproval },
            {QuotationStatus.RevisionRequested, QuotationStatus.RevisionRequested },
            {QuotationStatus.RevisionPending, QuotationStatus.RevisionPending },
            {QuotationStatus.Approved, QuotationStatus.Approved },
            {QuotationStatus.SalesOrder, QuotationStatus.SalesOrder },
            {QuotationStatus.Superseded, QuotationStatus.Superseded },
            {QuotationStatus.Closed, QuotationStatus.Closed },
        };

        public Dictionary<string, string> PaymentTerms { get; set; } = new Dictionary<string, string>
        {
             {"7 Days","7 Days" },
             {"10 Days","10 Days" },
             {"15 Days","15 Days" },
             {"20 Days","20 Days" },
             {"25 Days","25 Days" },
             {"30 Days","30 Days" },
             {"60 Days","60 Days" },
             {"90 Days","90 Days" },
             {"Other","Other" }
        };

        private void WhenGetRecDone()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();
            IsApproveVisible = quotationDto.Status == QuotationStatus.SubmittedForApproval &&
                               ApiSession.HasRoles(ErpRoles.ApproveQuotation);
            IsViewReasonVisible = quotationDto.Status == QuotationStatus.RevisionRequested ||
                                  quotationDto.Status == QuotationStatus.RevisionPending;
            IsCloseAndReviseQuotationVisible = quotationDto.Status == QuotationStatus.SubmittedForApproval ||
                                               quotationDto.Status == QuotationStatus.Approved;
            IsReasonForClosingVisible = quotationDto.Status == QuotationStatus.Closed ||
                                        quotationDto.Status == QuotationStatus.Superseded;
            IsPrintDraftVisible = string.IsNullOrEmpty(quotationDto.Status) ||
                                  quotationDto.Status == QuotationStatus.SubmittedForApproval ||
                                  quotationDto.Status == QuotationStatus.RevisionRequested ||
                                  quotationDto.Status == QuotationStatus.RevisionPending;
            IsSaveForLaterVisible = string.IsNullOrEmpty(quotationDto.Status) ||
                                    quotationDto.Status == QuotationStatus.RevisionRequested ||
                                    quotationDto.Status == QuotationStatus.RevisionPending;
            IsOrderReceivedVisible = quotationDto.Status == QuotationStatus.Approved;
            IsPrintQuotationVisible = quotationDto.Status == QuotationStatus.Approved ||
                                      quotationDto.Status == QuotationStatus.SalesOrder;
            IsPurchaseOrderNumberVisible = quotationDto.Status == QuotationStatus.SalesOrder;

            SetSalesPerson();
            SetTotalBasedOnCurrency();
        }

        private void QuotationDtoPropertyChanged()
        {
            _entity.QuotationDto.PropertyChanged += (s, e) =>
            {
                var quotation = _entity.QuotationDto.GetDto<QuotationDto>();

                switch (e.PropertyName)
                {
                    case "IsSameAsBillingAddress" when quotation.IsSameAsBillingAddress:
                        {
                            if (string.IsNullOrEmpty(quotation.CustomerType)) break;

                            _entity.QuotationDto.SetProperty("ShipTo", quotation.CustomerName);
                            _entity.QuotationDto.SetProperty("ShippingAddress", quotation.BillingAddress);
                            _entity.QuotationDto.SetProperty("ShippingCountry", quotation.BillingCountry);
                            break;
                        }

                    case "IsSameAsBillingAddress" when !quotation.IsSameAsBillingAddress:
                        {
                            _entity.QuotationDto.SetProperty("ShipTo", "");
                            _entity.QuotationDto.SetProperty("ShippingAddress", "");
                            _entity.QuotationDto.SetProperty("ShippingCountry", "");
                            break;
                        }
                }

                ComputeTotal();
                SaveCommand.RaiseCanExecuteChanged();
            };

            _entity.QuotationDetails.CollectionChanged += (s, e) =>
            {
                SaveCommand.RaiseCanExecuteChanged();
                SetNumbering();
            };

            _entity.QuotationDetails.ForEach(x => x.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName != "Quantity" && e.PropertyName != "Price") return;
                var qDetail = x.GetDto<QuotationDetailDto>();
                x.SetProperty("Total", qDetail.Quantity * qDetail.Price);
                ComputeTotal();
            });
        }

        private async void AddNewRecordExecute()
        {
            CreateNewQuotationEntity();

            await GetRecordAsync("QuotationInquiryGetRecList");
            var inquiryView = new CollectionViewSource { Source = _entity.Inquiries };
            InquiryCollectionView = inquiryView;

            SetView(QuotationViewNames.QuotationSelectInquiryView);
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new QuotationEntity());
            _entity = Entity.GetDto<QuotationEntity>();

            var quotationStatus = SelectedItem.As<QuotationListItem>().Status;
            if ((quotationStatus == QuotationStatus.RevisionRequested ||
                 quotationStatus == QuotationStatus.RevisionPending)
                && ApiSession.HasRoles(ErpRoles.CreateQuotation))
            {
                _entity.IsNew = true;

                await OpenItemExecute(SelectedItem.Id, true);
                QuotationDtoPropertyChanged();

                SetView(QuotationViewNames.QuotationManagersEditView);
            }
            else
            {
                await OpenItemExecute(SelectedItem.Id);
                SetView(QuotationViewNames.QuotationDisplayView);
            }
        }

        private void BackExecute()
        {
            RefreshExecute();
            SetView(QuotationViewNames.QuotationMainView);
        }

        private void EditAsExecute()
        {
            RadWindow.Confirm("This will create a New Draft in the Inquiry Screen.", async (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var quotationDetails = new ObservableCollection<QuotationDetailDto>();
                _entity.QuotationDetails.ForEach(x => quotationDetails.Add(x.GetDto<QuotationDetailDto>()));

                _entity.InquiryDto = _entity.QuotationDto.GetDto<QuotationDto>().ToInquiry();
                _entity.InquiryDetails = quotationDetails.ToInquiryDetails();
                _entity.InquiryFiles = _entity.QuotationFiles;

                await SaveRecordAsync("InquiryDraftAddRec");
                BackExecute();
            });
        }

        private async void SaveForLaterExecute()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();

            switch (quotationDto.Status)
            {
                case QuotationStatus.RevisionRequested:
                    {
                        var saveResult = await SaveRecordAsync("QuotationAddRec");
                        if (saveResult.OperationResult != OperationResult.Success) return;

                        BackExecute();
                        break;
                    }

                case QuotationStatus.RevisionPending:
                    {
                        await SaveRecordAsync("QuotationModiRec");
                        BackExecute();
                        break;
                    }

                case null:
                case "":
                    {
                        _entity.QuotationDto.SetProperty("Status", QuotationStatus.RevisionPending);

                        var saveResult = await SaveRecordAsync("QuotationAddRec");
                        if (saveResult.OperationResult != OperationResult.Success) return;

                        BackExecute();
                        break;
                    }
            }
        }

        private async void SaveExecute()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();
            var taskToPerform = quotationDto.Status == QuotationStatus.RevisionPending
                ? "QuotationModiRec"
                : "QuotationAddRec";

            _entity.QuotationDto.SetProperty("Status", QuotationStatus.SubmittedForApproval);

            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSave()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();

            return quotationDto.CustomerId != 0
                   && quotationDto.CurrencyId != 0
                   && quotationDto.ExpirationDate != null
                   && !string.IsNullOrEmpty(quotationDto.ProjectName)
                   && !string.IsNullOrEmpty(quotationDto.Requester)
                   && !string.IsNullOrEmpty(quotationDto.Delivery)
                   && !string.IsNullOrEmpty(quotationDto.ShippingTerms)
                   && !string.IsNullOrEmpty(quotationDto.PaymentTerms)
                   && !string.IsNullOrEmpty(quotationDto.ShipTo)
                   && !string.IsNullOrEmpty(quotationDto.ShippingAddress)
                   && _entity.QuotationDetails.Any();
        }

        private void CancelExecute()
        {
            PartWindow?.Close();
        }

        private void AddExistingItemExecute()
        {
            AddPartCommand = new DelegateCommand(AddPartExecute);

            var partNumberView = new CollectionViewSource { Source = Lookups["Parts"] };
            PartCollectionView = partNumberView;

            PartWindow = new QuotationWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };

            SearchForPartNumber = "";
            SearchForDescription = "";
            PartWindow.ShowDialog();
        }

        private void AddBlankItemExecute()
        {
            var newDetail = new QuotationDetailDto { PartNumber = "TBA" };
            var newProxy = new DynamicNotifyPropertyChangedProxy(newDetail);
            newProxy.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName != "Quantity" && e.PropertyName != "Price") return;
                var qDetail = newProxy.GetDto<QuotationDetailDto>();
                newProxy.SetProperty("Total", qDetail.Quantity * qDetail.Price);
                ComputeTotal();
            };
            _entity.QuotationDetails.Add(newProxy);
        }

        private void AddPartExecute()
        {
            if (SelectedPart == null) return;

            var partId = Convert.ToInt32(SelectedPart.Id);
            var uom = Lookups["UnitOfMeasurements"].FirstOrDefault(x => x.Id == SelectedPart.UnitOfMeasurementId.ToString());
            var newDetail = new QuotationDetailDto
            {
                PartId = partId,
                PartNumber = SelectedPart.PartNumber,
                Description = SelectedPart.Text,
                UnitOfMeasurement = uom?.Text ?? "",
                Price = SelectedPart.Price
            };
            var newProxy = new DynamicNotifyPropertyChangedProxy(newDetail);
            newProxy.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName != "Quantity" && e.PropertyName != "Price") return;
                var qDetail = newProxy.GetDto<QuotationDetailDto>();
                newProxy.SetProperty("Total", qDetail.Quantity * qDetail.Price);
                ComputeTotal();
            };

            _entity.QuotationDetails.Add(newProxy);

            CancelExecute();
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            SearchForQuoteNo = "";
            EventAggregator.GetEvent<RefreshItemListEvent<SalesWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Quotation"));
        }

        private void RefreshQuotationExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        public async void CreateQuotationExecute()
        {
            if (SelectedInquiry == null) return;

            await OpenItemExecute("");

            Methods["InquiryGetRec"].MethodParameter = SelectedInquiry.Id;
            await GetRecordAsync("InquiryGetRec");

            var quotationDto = _entity.InquiryDto.ToQuotation();
            Entity.SetProperty("QuotationDto", new DynamicNotifyPropertyChangedProxy(quotationDto));

            _entity.InquiryDetails.ToQuotationDetails().ForEach(detail =>
            {
                var newDetailProxy = new DynamicNotifyPropertyChangedProxy(detail);
                _entity.QuotationDetails.Add(newDetailProxy);
            });
            _entity.InquiryFiles.ForEach(_entity.QuotationFiles.Add);

            _entity.QuotationDto.SetProperty("DateCreated", DateTime.Now);
            SaveCommand.RaiseCanExecuteChanged();

            SetSalesPerson();
            QuotationDtoPropertyChanged();
            ComputeTotal();

            IsPaymentTermsReadOnly = PaymentTerms.ContainsKey(quotationDto.PaymentTerms);
            SetView(QuotationViewNames.QuotationManagersEditView);
        }

        public void CreateNewQuotationEntity()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new QuotationEntity());
            _entity = Entity.GetDto<QuotationEntity>();
            _entity.IsNew = true;
        }

        private void UploadFileExecute()
        {
            _fileService.GetAnyFile(_entity.QuotationFiles);
        }

        private void DownloadFileExecute()
        {
            if (SelectedFile == null) return;
            _fileService.SaveFile(SelectedFile);
        }

        private void AddNoteExecute()
        {
            _entity.QuotationNotes.Add(new NoteDto());
        }

        private void AskForRevisionExecute()
        {
            RadWindow.Confirm("Ask For Revision?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var revisionWindow = Container.Resolve<AskForRevisionWindow>();
                var viewModel = revisionWindow.GetViewModel<AskForRevisionWindowViewModel>();

                viewModel.SetPropertyToDto = () => _entity.QuotationDto.SetProperty("RevisionNotes", viewModel.RevisionNotes);
                viewModel.CompletedAction = AskForRevision;

                revisionWindow.Owner = Application.Current.MainWindow;
                revisionWindow.ShowDialog();
            });
        }

        private async void AskForRevision()
        {
            _entity.QuotationDto.SetProperty("Status", QuotationStatus.RevisionRequested);

            await SaveRecordAsync("QuotationModiRec");
            BackExecute();
        }

        private void ViewReasonForRevisionExecute()
        {
            var revisionWindow = Container.Resolve<ReasonForRevisionWindow>();
            var viewModel = revisionWindow.GetViewModel<ReasonForRevisionWindowViewModel>();
            viewModel.RevisionNotes = _entity.QuotationDto.GetDto<QuotationDto>().RevisionNotes;

            revisionWindow.Owner = Application.Current.MainWindow;
            revisionWindow.ShowDialog();
        }

        private void SubmitForApprovalExecute()
        {
            RadWindow.Confirm("Submit For Approval?", async (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.QuotationDto.SetProperty("Status", QuotationStatus.SubmittedForApproval);
                await SaveRecordAsync("QuotationModiRec");
                BackExecute();
            });
        }

        private void ApproveQuotationExecute()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();
            if (ApiSession.GetId() == quotationDto.CreatedById)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Unable to Approve your own Quotation"));
                return;
            }

            RadWindow.Confirm("Approve Quotation?", async (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.QuotationDto.SetProperty("Status", QuotationStatus.Approved);
                await SaveRecordAsync("QuotationModiRec");
                BackExecute();
            });
        }

        private void CloseEntityExecute()
        {
            RadWindow.Confirm("Cancel Quotation?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var closeWindow = Container.Resolve<CloseEntityWindow>();
                var viewModel = closeWindow.GetViewModel<CloseEntityWindowViewModel>();

                viewModel.Reasons = Lookups["Reasons"];
                viewModel.SetPropertyToDto = () =>
                {
                    _entity.QuotationDto.SetProperty("ReasonForClosing", viewModel.SelectedReasonText.ToUpper());
                    _entity.QuotationDto.SetProperty("DetailedReasonForClosing", viewModel.DetailedReasonForClosing);
                };
                viewModel.CompletedAction = CloseQuotation;

                closeWindow.Owner = Application.Current.MainWindow;
                closeWindow.ShowDialog();
            });
        }

        private async void CloseQuotation()
        {
            _entity.QuotationDto.SetProperty("Status", QuotationStatus.Closed);
            await SaveRecordAsync("QuotationModiRec");
            BackExecute();
        }

        private void ViewClosingDetailsExecute()
        {
            var closedWindow = Container.Resolve<ReasonForClosingWindow>();
            var viewModel = closedWindow.GetViewModel<ReasonForClosingWindowViewModel>();

            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();
            viewModel.ReasonForClosing = quotationDto.ReasonForClosing;
            viewModel.DetailedReasonForClosing = quotationDto.DetailedReasonForClosing;

            closedWindow.Owner = Application.Current.MainWindow;
            closedWindow.ShowDialog();
        }

        private void PrintDraftExecute()
        {
            RunReport("QuotationDraft");
        }

        private void PrintQuotationExecute()
        {
            RunReport("Quotation");
        }

        private void AddPartNumberExecute()
        {
            AddPartCommand = new DelegateCommand(AddPartNumber);

            var partNumberView = new CollectionViewSource { Source = Lookups["Parts"] };
            PartCollectionView = partNumberView;

            PartWindow = new QuotationWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };

            SelectedPart = null;
            SearchForPartNumber = "";
            SearchForDescription = "";
            PartWindow.ShowDialog();
        }

        private async void AddPartNumber()
        {
            if (SelectedPart == null) return;
            if (SelectedDetail == null) return;

            var uom = Lookups["UnitOfMeasurements"].FirstOrDefault(x => x.Id == SelectedPart.UnitOfMeasurementId.ToString());
            SelectedDetail.SetProperty("PartId", Convert.ToInt32(SelectedPart.Id));
            SelectedDetail.SetProperty("PartNumber", SelectedPart.PartNumber);
            SelectedDetail.SetProperty("UnitOfMeasurement", uom?.Text ?? "");
            SelectedDetail.SetProperty("Description", SelectedPart.Text);

            CancelExecute();

            var quotationDetailDto = SelectedDetail.GetDto<QuotationDetailDto>();
            Entity.SetProperty("QuotationDetailToSave", quotationDetailDto);

            var result = await SaveRecordAsync("QuotationDetailModiRec");
            if (result.OperationResult != OperationResult.Success) return;

            await GetRecordAsync("QuotationGetRec");
        }

        private void OrderReceivedExecute()
        {
            var hasItemWithOutPartNumber = _entity.QuotationDetails.Any(x =>
                x.GetDto<QuotationDetailDto>().PartId == null ||
                x.GetDto<QuotationDetailDto>().PartId == 0);
            if (hasItemWithOutPartNumber)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Item/s without Part Number/s Found"));
                return;
            }

            RadWindow.Confirm("Order Received?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var quotationPoWindow = Container.Resolve<QuotationPoWindow>();
                var viewModel = quotationPoWindow.GetViewModel<QuotationPoWindowViewModel>();

                viewModel.SetPropertyToDto = () => _entity.QuotationDto.SetProperty("PurchaseOrderNumber", viewModel.PoNumber);
                viewModel.CompletedAction = OrderReceived;

                quotationPoWindow.Owner = Application.Current.MainWindow;
                quotationPoWindow.ShowDialog();
            });
        }

        private async void OrderReceived()
        {
            _entity.QuotationDto.SetProperty("SalesOrderDate", DateTime.Now);
            _entity.QuotationDto.SetProperty("Status", QuotationStatus.SalesOrder);

            _entity.QuotationDetails.ForEach(x =>
            {
                var detail = x.GetDto<QuotationDetailDto>();
                detail.Pending = detail.Quantity;
            });

            await SaveRecordAsync("QuotationModiRec");
            await SaveRecordAsync("CreateSalesOrderAddRec");
            BackExecute();
        }

        private void DeleteItemExecute(object obj)
        {
            if (SelectedDetail == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Remove Item?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.QuotationDetails.Remove(SelectedDetail);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Remove Item?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.QuotationDetails.Remove(SelectedDetail);
                });
            }
        }

        private void DeleteFileExecute(object obj)
        {
            if (SelectedFile == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.QuotationFiles.Remove(SelectedFile);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.QuotationFiles.Remove(SelectedFile);
                });
            }
        }

        private void DeleteNoteExecute(object obj)
        {
            if (SelectedNote == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete Note?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.QuotationNotes.Remove(SelectedNote);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete Note?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.QuotationNotes.Remove(SelectedNote);
                });
            }
        }

        private void SetNumbering()
        {
            var count = 1;
            _entity.QuotationDetails.ForEach(x =>
            {
                x.SetProperty("OrderNumber", count);
                count++;
            });
        }

        private void RadioButtonIsCheckedExecute(object obj)
        {
            if (!(obj is ObjectParameter param)) return;
            _entity.QuotationDto.SetProperty(param.PropertyName, param.NewValue);
        }

        private void SetSalesPerson()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();
            var salesPerson = Lookups["SalesPersons"].FirstOrDefault(x => x.Id == quotationDto.SalesPersonId);
            _entity.QuotationDto.SetProperty("SalesPersonName", salesPerson?.Text ?? "");
        }

        private void ComputeTotal()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();

            var totalDetails = _entity.QuotationDetails.Sum(x => x.GetDto<QuotationDetailDto>().Total);
            var discountPercentage = quotationDto.DiscountPercentage;
            var discountAmount = quotationDto.DiscountAmount;
            var tax = quotationDto.TaxPercentage;

            var amountToDiscount = totalDetails * (discountPercentage / 100);
            var totalDiscount = amountToDiscount + discountAmount;
            var discountedTotal = totalDetails - totalDiscount;

            var amountToTax = totalDetails * (tax / 100);
            var total = discountedTotal + amountToTax;

            _entity.QuotationDto.SetProperty("SubTotal", totalDetails);
            _entity.QuotationDto.SetProperty("TotalDiscount", totalDiscount);
            _entity.QuotationDto.SetProperty("TaxAmount", amountToTax);
            _entity.QuotationDto.SetProperty("Total", total);

            SetTotalBasedOnCurrency();
        }

        private void SetTotalBasedOnCurrency()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();
            Total = quotationDto.Total.ToCurrency(quotationDto.CurrencyName, quotationDto.Prefix);
        }

        public void SetQuotationView(string view)
        {
            SetView(view);
        }

        public string SearchForQuoteNo
        {
            get { return GetValue(() => SearchForQuoteNo); }
            set
            {
                SetValue(() => SearchForQuoteNo, value, () =>
                {
                    if (value == null) return;
                    var view = CollectionView == null
                        ? CollectionViewSource.GetDefaultView(EntityListItems)
                        : CollectionView.View;
                    if (view == null) return;

                    view.Filter = obj =>
                    {
                        if (obj == null) return false;
                        return obj.As<QuotationListItem>().QuotationNumber.IndexOf(value, StringComparison.OrdinalIgnoreCase) > -1;
                    };
                });
            }
        }

        public CustomerLookup SelectedCustomer
        {
            get { return GetValue(() => SelectedCustomer); }
            set
            {
                SetValue(() => SelectedCustomer, value);
                if (value == null) return;
                _entity.QuotationDto.SetProperty("CustomerId", Convert.ToInt32(value.Id));
                _entity.QuotationDto.SetProperty("CustomerName", value.Text);
                _entity.QuotationDto.SetProperty("BillingAddress", value.CustomerAddress);
                SelectedCustomer = null;
            }
        }

        public KeyValuePair<string, string> SelectedPaymentTerms
        {
            get { return GetValue(() => SelectedPaymentTerms); }
            set
            {
                SetValue(() => SelectedPaymentTerms, value);
                switch (value.Value)
                {
                    case null:
                        return;

                    case "Other":
                        _entity.QuotationDto.SetProperty("PaymentTerms", "");
                        SelectedPaymentTerms = default;
                        IsPaymentTermsReadOnly = false;
                        break;

                    default:
                        _entity.QuotationDto.SetProperty("PaymentTerms", value.Value);
                        SelectedPaymentTerms = default;
                        IsPaymentTermsReadOnly = true;
                        break;
                }
            }
        }

        public bool IsPaymentTermsReadOnly
        {
            get { return GetValue(() => IsPaymentTermsReadOnly); }
            set { SetValue(() => IsPaymentTermsReadOnly, value); }
        }

        public CollectionViewSource InquiryCollectionView
        {
            get { return GetValue(() => InquiryCollectionView); }
            set { SetValue(() => InquiryCollectionView, value); }
        }

        public string SearchForInquiry
        {
            get { return GetValue(() => SearchForInquiry); }
            set
            {
                SetValue(() => SearchForInquiry, value, () =>
                {
                    if (value == null) return;
                    var view = InquiryCollectionView == null
                        ? CollectionViewSource.GetDefaultView(_entity.Inquiries)
                        : InquiryCollectionView.View;
                    if (view == null) return;

                    view.Filter = obj =>
                    {
                        if (obj == null) return false;
                        return obj.As<InquiryListItem>().ProjectName.IndexOf(value, StringComparison.OrdinalIgnoreCase) > -1;
                    };
                });
            }
        }

        public InquiryListItem SelectedInquiry
        {
            get { return GetValue(() => SelectedInquiry); }
            set { SetValue(() => SelectedInquiry, value); }
        }

        public string Total
        {
            get { return GetValue(() => Total); }
            set { SetValue(() => Total, value); }
        }

        public DynamicNotifyPropertyChangedProxy SelectedDetail
        {
            get { return GetValue(() => SelectedDetail); }
            set
            {
                SetValue(() => SelectedDetail, value);
                if (value == null)
                {
                    IsAddPartNumberVisible = false;
                    return;
                }

                var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();
                if (quotationDto.Status != QuotationStatus.Approved)
                {
                    IsAddPartNumberVisible = false;
                    return;
                }

                var quotationDetailDto = value.GetDto<QuotationDetailDto>();
                IsAddPartNumberVisible = quotationDetailDto.PartId == null ||
                                         quotationDetailDto.PartId == 0;
            }
        }

        public FileDto SelectedFile
        {
            get { return GetValue(() => SelectedFile); }
            set { SetValue(() => SelectedFile, value); }
        }

        public NoteDto SelectedNote
        {
            get { return GetValue(() => SelectedNote); }
            set { SetValue(() => SelectedNote, value); }
        }

        public bool IsApproveVisible
        {
            get { return GetValue(() => IsApproveVisible); }
            set { SetValue(() => IsApproveVisible, value); }
        }

        public bool IsCloseAndReviseQuotationVisible
        {
            get { return GetValue(() => IsCloseAndReviseQuotationVisible); }
            set { SetValue(() => IsCloseAndReviseQuotationVisible, value); }
        }

        public bool IsAddPartNumberVisible
        {
            get { return GetValue(() => IsAddPartNumberVisible); }
            set { SetValue(() => IsAddPartNumberVisible, value); }
        }

        public bool IsPurchaseOrderNumberVisible
        {
            get { return GetValue(() => IsPurchaseOrderNumberVisible); }
            set { SetValue(() => IsPurchaseOrderNumberVisible, value); }
        }

        public bool IsReasonForClosingVisible
        {
            get { return GetValue(() => IsReasonForClosingVisible); }
            set { SetValue(() => IsReasonForClosingVisible, value); }
        }

        public bool IsOrderReceivedVisible
        {
            get { return GetValue(() => IsOrderReceivedVisible); }
            set { SetValue(() => IsOrderReceivedVisible, value); }
        }

        public bool IsPrintQuotationVisible
        {
            get { return GetValue(() => IsPrintQuotationVisible); }
            set { SetValue(() => IsPrintQuotationVisible, value); }
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<SalesLookupAggregate>(ApiControllers.Lookup, "SalesGetLookups");
        }
    }
}