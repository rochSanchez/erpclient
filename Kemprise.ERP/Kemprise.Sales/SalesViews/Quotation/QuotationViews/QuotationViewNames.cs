﻿namespace Kemprise.Sales.SalesViews.Quotation.QuotationViews
{
    public class QuotationViewNames
    {
        public const string QuotationMainView = "Quotation Main View";
        public const string QuotationManagersEditView = "Quotation Mangers Edit View";
        public const string QuotationDisplayView = "Quotation Display View";
        public const string QuotationSelectInquiryView = "Quotation Select Inquiry View";
    }
}