﻿using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Prism.Commands;
using System;
using Telerik.Windows.Controls;

namespace Kemprise.Sales.SalesViews.Quotation.QuotationViews
{
    public class QuotationPoWindowViewModel : NotifyPropertyChangedBase
    {
        public QuotationPoWindowViewModel()
        {
            CloseCommand = new DelegateCommand<object>(CloseExecute);
            OkCommand = new DelegateCommand<object>(OkExecute, CanOk);
        }

        public DelegateCommand<object> CloseCommand { get; set; }
        public DelegateCommand<object> OkCommand { get; set; }

        public Action SetPropertyToDto { get; set; }
        public Action CompletedAction { get; set; }

        private void CloseExecute(object obj)
        {
            var alertWindow = obj.As<RadWindow>();
            alertWindow?.Close();
        }

        private void OkExecute(object obj)
        {
            var alertWindow = obj.As<RadWindow>();
            alertWindow?.Close();
            CompletedAction();
        }

        private bool CanOk(object arg)
        {
            return !string.IsNullOrEmpty(PoNumber);
        }

        public string PoNumber
        {
            get { return GetValue(() => PoNumber); }
            set
            {
                SetValue(() => PoNumber, value);
                OkCommand.RaiseCanExecuteChanged();
                SetPropertyToDto();
            }
        }
    }
}