﻿using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Sales.SalesViews.Quotation.Methods
{
    public class QuotationDetailModiRec : ErpSaveMethodCallBase
    {
        public QuotationDetailModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "QuotationDetailModiRec";
        public override ErpModules ModuleName => ErpModules.Sales;
        public override ApiControllers Controller => ApiControllers.Quotation;
        public override string Category => SalesTypes.Quotation.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var quotationEntity = entity.As<QuotationEntity>();

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(QuotationDetailDto), quotationEntity.QuotationDetailToSave }
            };

            await UpdateRecordAsync(MethodName, dataToSave);
        }
    }
}