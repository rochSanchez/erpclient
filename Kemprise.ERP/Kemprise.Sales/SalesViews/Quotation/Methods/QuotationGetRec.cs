﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Sales.SalesViews.Quotation.Methods
{
    public class QuotationGetRec : GetMethodCallBase
    {
        public QuotationGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "QuotationGetRec";
        public override ErpModules ModuleName => ErpModules.Sales;
        public override string Category => SalesTypes.Quotation.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Quotation;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<QuotationAggregate>(MethodName, new object[] { MethodParameter });
            if (aggregate == null) return;

            if (aggregate.QuotationDto != null)
                entity.SetProperty("QuotationDto", new DynamicNotifyPropertyChangedProxy(aggregate.QuotationDto));

            if (aggregate.QuotationDetails.Any())
            {
                var orderedList = aggregate.QuotationDetails.OrderBy(x => x.OrderNumber);
                var details = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
                orderedList.ForEach(x => details.Add(new DynamicNotifyPropertyChangedProxy(x)));

                entity.SetProperty("QuotationDetails", details);
            }

            if (aggregate.QuotationNotes.Any())
            {
                var notes = aggregate.QuotationNotes.OrderBy(x => x.Item).ToList();
                var quotationNotes = new ObservableCollection<NoteDto>(notes);
                entity.SetProperty("QuotationNotes", quotationNotes);
            }

            if (aggregate.QuotationFiles.Any())
            {
                var files = new ObservableCollection<FileDto>(aggregate.QuotationFiles);
                entity.SetProperty("QuotationFiles", files);
            }
        }
    }
}