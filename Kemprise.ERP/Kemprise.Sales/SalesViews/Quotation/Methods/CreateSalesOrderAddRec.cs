﻿using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Sales.SalesViews.Quotation.Methods
{
    public class CreateSalesOrderAddRec : ErpSaveMethodCallBase
    {
        public CreateSalesOrderAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "CreateSalesOrderAddRec";
        public override ErpModules ModuleName => ErpModules.Sales;
        public override ApiControllers Controller => ApiControllers.SalesOrder;
        public override string Category => SalesTypes.Quotation.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var quotationEntity = entity.As<QuotationEntity>();
            var quotationDetails = new List<QuotationDetailDto>();

            quotationEntity.QuotationDetails.ForEach(x => quotationDetails.Add(x.GetDto<QuotationDetailDto>()));
            quotationDetails.ForEach(x =>
            {
                if (!string.IsNullOrEmpty(x.Description))
                    x.Description = x.Description.Trim();
            });

            var quotationAggregate = new QuotationAggregate
            {
                QuotationDto = quotationEntity.QuotationDto.GetDto<QuotationDto>(),
                QuotationDetails = quotationDetails,
                QuotationNotes = quotationEntity.QuotationNotes,
                QuotationFiles = quotationEntity.QuotationFiles
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(QuotationAggregate), quotationAggregate }
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}