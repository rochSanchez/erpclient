﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Sales.SalesViews.Quotation.Methods
{
    public class QuotationInquiryGetRec : GetMethodCallBase
    {
        public QuotationInquiryGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "InquiryGetRec";
        public override ErpModules ModuleName => ErpModules.Sales;
        public override string Category => SalesTypes.Quotation.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => false;
        public override ApiControllers Controller => ApiControllers.Inquiry;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<InquiryAggregate>(MethodName, new object[] { MethodParameter });

            if (aggregate.InquiryDto != null)
                entity.SetProperty("InquiryDto", aggregate.InquiryDto);

            if (aggregate.InquiryDetails.Any())
            {
                var orderedList = aggregate.InquiryDetails.OrderBy(x => x.OrderNumber);
                var details = new ObservableCollection<InquiryDetailDto>(orderedList);

                entity.SetProperty("InquiryDetails", details);
            }

            if (aggregate.InquiryFiles.Any())
            {
                var files = new ObservableCollection<FileDto>(aggregate.InquiryFiles);
                entity.SetProperty("InquiryFiles", files);
            }
        }
    }
}