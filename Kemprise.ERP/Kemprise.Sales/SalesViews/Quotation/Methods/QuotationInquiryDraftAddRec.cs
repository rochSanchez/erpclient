﻿using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Sales.SalesViews.Quotation.Methods
{
    public class QuotationInquiryDraftAddRec : ErpSaveMethodCallBase
    {
        public QuotationInquiryDraftAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "InquiryDraftAddRec";
        public override ErpModules ModuleName => ErpModules.Sales;
        public override ApiControllers Controller => ApiControllers.Inquiry;
        public override string Category => SalesTypes.Quotation.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var quotationEntity = entity.As<QuotationEntity>();

            quotationEntity.InquiryDetails.ForEach(x =>
            {
                if (!string.IsNullOrEmpty(x.Description))
                    x.Description = x.Description.Trim();
            });

            var textInfo = new CultureInfo("en-Us", false).TextInfo;
            foreach (var item in quotationEntity.InquiryDetails.Where(item => !string.IsNullOrEmpty(item.UnitOfMeasurement)))
                item.UnitOfMeasurement = textInfo.ToTitleCase(item.UnitOfMeasurement);

            var inquiryAggregate = new InquiryAggregate
            {
                InquiryDto = quotationEntity.InquiryDto,
                InquiryDetails = quotationEntity.InquiryDetails,
                InquiryFiles = quotationEntity.InquiryFiles
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(InquiryAggregate), inquiryAggregate }
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}