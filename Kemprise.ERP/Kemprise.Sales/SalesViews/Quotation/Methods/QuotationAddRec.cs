﻿using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Sales.SalesViews.Quotation.Methods
{
    public class QuotationAddRec : ErpSaveMethodCallBase
    {
        public QuotationAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "QuotationAddRec";
        public override ErpModules ModuleName => ErpModules.Sales;
        public override ApiControllers Controller => ApiControllers.Quotation;
        public override string Category => SalesTypes.Quotation.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var quotationEntity = entity.As<QuotationEntity>();
            var quotationDetails = new List<QuotationDetailDto>();

            quotationEntity.QuotationDetails.ForEach(x => quotationDetails.Add(x.GetDto<QuotationDetailDto>()));
            quotationDetails.ForEach(x =>
            {
                x.QuotationDetailId = 0;

                if (!string.IsNullOrEmpty(x.Description))
                    x.Description = x.Description.Trim();
            });

            var textInfo = new CultureInfo("en-Us", false).TextInfo;
            foreach (var item in quotationDetails.Where(item => !string.IsNullOrEmpty(item.UnitOfMeasurement)))
                item.UnitOfMeasurement = textInfo.ToTitleCase(item.UnitOfMeasurement);

            var quotationAggregate = new QuotationAggregate
            {
                QuotationDto = quotationEntity.QuotationDto.GetDto<QuotationDto>(),
                QuotationDetails = quotationDetails,
                QuotationNotes = quotationEntity.QuotationNotes,
                QuotationFiles = quotationEntity.QuotationFiles
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(QuotationAggregate), quotationAggregate }
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}