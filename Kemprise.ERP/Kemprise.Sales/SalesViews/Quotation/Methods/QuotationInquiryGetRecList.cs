﻿using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Sales.SalesViews.Quotation.Methods
{
    public class QuotationInquiryGetRecList : GetMethodCallBase
    {
        public QuotationInquiryGetRecList(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "QuotationInquiryGetRecList";
        public override ErpModules ModuleName => ErpModules.Sales;
        public override string Category => SalesTypes.Quotation.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => false;
        public override ApiControllers Controller => ApiControllers.Quotation;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var listItems = await GetRecordsAsync<InquiryListItem>(MethodName);
            var itemsList = listItems.ToList();

            entity.SetProperty("Inquiries", itemsList);
        }
    }
}