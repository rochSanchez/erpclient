﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Kemprise.Sales.SalesViews.Quotation
{
    public class QuotationEntity : EntityBase
    {
        public QuotationEntity()
        {
            QuotationDto = new DynamicNotifyPropertyChangedProxy(new QuotationDto());
            QuotationDetails = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            QuotationNotes = new ObservableCollection<NoteDto>();
            QuotationFiles = new ObservableCollection<FileDto>();

            Inquiries = new List<InquiryListItem>();
            InquiryDto = new InquiryDto();
            InquiryDetails = new ObservableCollection<InquiryDetailDto>();
            InquiryFiles = new ObservableCollection<FileDto>();
        }

        public DynamicNotifyPropertyChangedProxy QuotationDto { get; set; }
        public ObservableCollection<DynamicNotifyPropertyChangedProxy> QuotationDetails { get; set; }
        public ObservableCollection<NoteDto> QuotationNotes { get; set; }
        public ObservableCollection<FileDto> QuotationFiles { get; set; }

        public List<InquiryListItem> Inquiries { get; set; }
        public InquiryDto InquiryDto { get; set; }
        public ICollection<InquiryDetailDto> InquiryDetails { get; set; }
        public ObservableCollection<FileDto> InquiryFiles { get; set; }

        public QuotationDetailDto QuotationDetailToSave { get; set; }
    }
}