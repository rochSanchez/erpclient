﻿using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Currency;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.Infrastructure.Windows.AskForRevision;
using Kemprise.Infrastructure.Windows.CloseEntity;
using Kemprise.Infrastructure.Windows.ReasonForClosing;
using Kemprise.Infrastructure.Windows.ReasonForRevision;
using Kemprise.Sales.SalesViews.SalesOrder.SalesOrderViews;
using Kemprise.Sales.WorkArea;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Sales.SalesViews.SalesOrder
{
    public sealed class SalesOrderWorkAreaViewModel : SalesViewModelBase
    {
        private readonly IFileService _fileService;
        private SalesOrderEntity _entity;

        public SalesOrderWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, SalesTypes.SalesOrder.ToString())
        {
            _fileService = fileService;
            SetView(SalesOrderViewNames.SalesOrderMainView);

            OpenCommand = new DelegateCommand(OpenExecute);
            BackCommand = new DelegateCommand(BackExecute);
            CloseEntityCommand = new DelegateCommand(CloseEntityExecute);
            ViewClosingDetailsCommand = new DelegateCommand(ViewClosingDetailsExecute);
            AskForRevisionCommand = new DelegateCommand(AskForRevisionExecute);
            ViewReasonForRevisionCommand = new DelegateCommand(ViewReasonForRevisionExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSave);
            RefreshCommand = new DelegateCommand<object>(RefreshSalesOrderExecute);

            PrintSalesOrderCommand = new DelegateCommand(PrintSalesOrderExecute);
            DownloadFileCommand = new DelegateCommand(DownloadFileExecute);
            AddNoteCommand = new DelegateCommand(AddNoteExecute);
            ApproveSalesOrderCommand = new DelegateCommand(ApproveSalesOrderExecute);
            DeleteNoteCommand = new DelegateCommand<object>(DeleteNoteExecute);

            Methods["SalesOrderGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        public DelegateCommand PrintSalesOrderCommand { get; set; }
        public DelegateCommand DownloadFileCommand { get; set; }
        public DelegateCommand AddNoteCommand { get; set; }
        public DelegateCommand ApproveSalesOrderCommand { get; set; }
        public DelegateCommand<object> DeleteNoteCommand { get; set; }

        public Dictionary<string, string> PaymentTerms { get; set; } = new Dictionary<string, string>
        {
            {"7 Days","7 Days" },
            {"10 Days","10 Days" },
            {"15 Days","15 Days" },
            {"20 Days","20 Days" },
            {"25 Days","25 Days" },
            {"30 Days","30 Days" },
            {"60 Days","60 Days" },
            {"90 Days","90 Days" },
            {"Other","Other" }
        };

        private void WhenGetRecDone()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();
            IsCloseAndReviseSalesOrderVisible = quotationDto.InvoiceStatus != InvoiceStatus.PartiallyInvoiced &&
                                                quotationDto.InvoiceStatus != InvoiceStatus.Completed &&
                                                (quotationDto.Status == QuotationStatus.SubmittedForApproval ||
                                                quotationDto.Status == QuotationStatus.SalesOrder);
            IsPrintSalesOrderVisible = quotationDto.Status == QuotationStatus.SalesOrder;
            IsApproveVisible = quotationDto.Status == QuotationStatus.SubmittedForApproval;
            IsReasonForClosingVisible = quotationDto.Status == QuotationStatus.Closed ||
                                        quotationDto.Status == QuotationStatus.Superseded;

            SetSalesPerson();
            SetTotalBasedOnCurrency();
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new SalesOrderEntity());
            _entity = Entity.GetDto<SalesOrderEntity>();

            var quotationStatus = SelectedItem.As<QuotationListItem>().Status;
            if (quotationStatus == QuotationStatus.ReviseSalesOrder)
            {
                _entity.IsNew = true;

                await OpenItemExecute(SelectedItem.Id, true);
                QuotationDtoPropertyChanged();

                var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();
                IsPaymentTermsReadOnly = PaymentTerms.ContainsKey(quotationDto.PaymentTerms);

                SetView(SalesOrderViewNames.SalesOrderEditView);
            }
            else
            {
                await OpenItemExecute(SelectedItem.Id);
                SetView(SalesOrderViewNames.SalesOrderDisplayView);
            }
        }

        private void BackExecute()
        {
            RefreshExecute();
            SetView(SalesOrderViewNames.SalesOrderMainView);
        }

        private void CloseEntityExecute()
        {
            RadWindow.Confirm("Cancel Sales Order?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var closeWindow = Container.Resolve<CloseEntityWindow>();
                var viewModel = closeWindow.GetViewModel<CloseEntityWindowViewModel>();

                viewModel.Reasons = Lookups["Reasons"];
                viewModel.SetPropertyToDto = () =>
                {
                    _entity.QuotationDto.SetProperty("ReasonForClosing", viewModel.SelectedReasonText.ToUpper());
                    _entity.QuotationDto.SetProperty("DetailedReasonForClosing", viewModel.DetailedReasonForClosing);
                };
                viewModel.CompletedAction = CloseSalesOrder;

                closeWindow.Owner = Application.Current.MainWindow;
                closeWindow.ShowDialog();
            });
        }

        private async void CloseSalesOrder()
        {
            _entity.QuotationDto.SetProperty("Status", QuotationStatus.Closed);
            await SaveRecordAsync("SalesOrderModiRec");
            BackExecute();
        }

        private void ViewClosingDetailsExecute()
        {
            var closedWindow = Container.Resolve<ReasonForClosingWindow>();
            var viewModel = closedWindow.GetViewModel<ReasonForClosingWindowViewModel>();

            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();
            viewModel.ReasonForClosing = quotationDto.ReasonForClosing;
            viewModel.DetailedReasonForClosing = quotationDto.DetailedReasonForClosing;

            closedWindow.Owner = Application.Current.MainWindow;
            closedWindow.ShowDialog();
        }

        private void AskForRevisionExecute()
        {
            RadWindow.Confirm("Ask For Revision?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var revisionWindow = Container.Resolve<AskForRevisionWindow>();
                var viewModel = revisionWindow.GetViewModel<AskForRevisionWindowViewModel>();

                viewModel.SetPropertyToDto = () => _entity.QuotationDto.SetProperty("RevisionNotes", viewModel.RevisionNotes);
                viewModel.CompletedAction = AskForRevision;

                revisionWindow.Owner = Application.Current.MainWindow;
                revisionWindow.ShowDialog();
            });
        }

        private async void AskForRevision()
        {
            _entity.QuotationDto.SetProperty("Status", QuotationStatus.ReviseSalesOrder);

            var saveResult = await SaveRecordAsync("SalesOrderModiRec");
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private void ViewReasonForRevisionExecute()
        {
            var revisionWindow = Container.Resolve<ReasonForRevisionWindow>();
            var viewModel = revisionWindow.GetViewModel<ReasonForRevisionWindowViewModel>();
            viewModel.RevisionNotes = _entity.QuotationDto.GetDto<QuotationDto>().RevisionNotes;

            revisionWindow.Owner = Application.Current.MainWindow;
            revisionWindow.ShowDialog();
        }

        private async void SaveExecute()
        {
            _entity.QuotationDto.SetProperty("Status", QuotationStatus.SubmittedForApproval);

            var saveResult = await SaveRecordAsync("SalesOrderAddRec");
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSave()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();

            return quotationDto.CustomerId != 0
                   && quotationDto.CurrencyId != 0
                   && quotationDto.ExpirationDate != null
                   && !string.IsNullOrEmpty(quotationDto.ProjectName)
                   && !string.IsNullOrEmpty(quotationDto.Requester)
                   && !string.IsNullOrEmpty(quotationDto.Delivery)
                   && !string.IsNullOrEmpty(quotationDto.ShippingTerms)
                   && !string.IsNullOrEmpty(quotationDto.PaymentTerms)
                   && !string.IsNullOrEmpty(quotationDto.ShipTo)
                   && !string.IsNullOrEmpty(quotationDto.ShippingAddress)
                   && _entity.QuotationDetails.Any();
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            SearchForSalesNo = "";
            EventAggregator.GetEvent<RefreshItemListEvent<SalesWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Sales Order"));
        }

        private void RefreshSalesOrderExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        private void PrintSalesOrderExecute()
        {
            RunReport("SalesOrder");
        }

        private void DownloadFileExecute()
        {
            if (SelectedFile == null) return;
            _fileService.SaveFile(SelectedFile);
        }

        private void AddNoteExecute()
        {
            _entity.QuotationNotes.Add(new NoteDto());
        }

        private void ApproveSalesOrderExecute()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();
            if (ApiSession.GetId() == quotationDto.CreatedById)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Unable to Approve your own Sales Order"));
                return;
            }

            RadWindow.Confirm("Approve Sales Order?", async (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.QuotationDto.SetProperty("Status", QuotationStatus.SalesOrder);
                await SaveRecordAsync("SalesOrderModiRec");
                BackExecute();
            });
        }

        private void DeleteNoteExecute(object obj)
        {
            if (SelectedNote == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete Note?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.QuotationNotes.Remove(SelectedNote);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete Note?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.QuotationNotes.Remove(SelectedNote);
                });
            }
        }

        private void QuotationDtoPropertyChanged()
        {
            _entity.QuotationDto.PropertyChanged += (s, e) =>
            {
                var quotation = _entity.QuotationDto.GetDto<QuotationDto>();

                switch (e.PropertyName)
                {
                    case "IsSameAsBillingAddress" when quotation.IsSameAsBillingAddress:
                        {
                            if (string.IsNullOrEmpty(quotation.CustomerType)) break;

                            _entity.QuotationDto.SetProperty("ShipTo", quotation.CustomerName);
                            _entity.QuotationDto.SetProperty("ShippingAddress", quotation.BillingAddress);
                            _entity.QuotationDto.SetProperty("ShippingCountry", quotation.BillingCountry);
                            break;
                        }

                    case "IsSameAsBillingAddress" when !quotation.IsSameAsBillingAddress:
                        {
                            _entity.QuotationDto.SetProperty("ShipTo", "");
                            _entity.QuotationDto.SetProperty("ShippingAddress", "");
                            _entity.QuotationDto.SetProperty("ShippingCountry", "");
                            break;
                        }
                }

                ComputeTotal();
                SaveCommand.RaiseCanExecuteChanged();
            };

            _entity.QuotationDetails.CollectionChanged += (s, e) =>
            {
                SaveCommand.RaiseCanExecuteChanged();
                SetNumbering();
            };

            _entity.QuotationDetails.ForEach(x => x.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName != "Quantity" && e.PropertyName != "Price") return;
                var qDetail = x.GetDto<QuotationDetailDto>();
                x.SetProperty("Total", qDetail.Quantity * qDetail.Price);
                ComputeTotal();
            });
        }

        private void SetNumbering()
        {
            var count = 1;
            _entity.QuotationDetails.ForEach(x =>
            {
                x.SetProperty("OrderNumber", count);
                count++;
            });
        }

        private void SetSalesPerson()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();
            var salesPerson = Lookups["SalesPersons"].FirstOrDefault(x => x.Id == quotationDto.SalesPersonId);
            _entity.QuotationDto.SetProperty("SalesPersonName", salesPerson?.Text ?? "");
        }

        private void ComputeTotal()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();

            var totalDetails = _entity.QuotationDetails.Sum(x => x.GetDto<QuotationDetailDto>().Total);
            var discountPercentage = quotationDto.DiscountPercentage;
            var discountAmount = quotationDto.DiscountAmount;
            var tax = quotationDto.TaxPercentage;

            var amountToDiscount = totalDetails * (discountPercentage / 100);
            var totalDiscount = amountToDiscount + discountAmount;
            var discountedTotal = totalDetails - totalDiscount;

            var amountToTax = totalDetails * (tax / 100);
            var total = discountedTotal + amountToTax;

            _entity.QuotationDto.SetProperty("SubTotal", totalDetails);
            _entity.QuotationDto.SetProperty("TotalDiscount", totalDiscount);
            _entity.QuotationDto.SetProperty("TaxAmount", amountToTax);
            _entity.QuotationDto.SetProperty("Total", total);

            SetTotalBasedOnCurrency();
        }

        private void SetTotalBasedOnCurrency()
        {
            var quotationDto = _entity.QuotationDto.GetDto<QuotationDto>();
            Total = quotationDto.Total.ToCurrency(quotationDto.CurrencyName, quotationDto.Prefix);
        }

        public string SearchForSalesNo
        {
            get { return GetValue(() => SearchForSalesNo); }
            set
            {
                SetValue(() => SearchForSalesNo, value, () =>
                {
                    if (value == null) return;
                    var view = CollectionView == null
                        ? CollectionViewSource.GetDefaultView(EntityListItems)
                        : CollectionView.View;
                    if (view == null) return;

                    view.Filter = obj =>
                    {
                        if (obj == null) return false;
                        return obj.As<QuotationListItem>().QuotationNumber.IndexOf(value, StringComparison.OrdinalIgnoreCase) > -1;
                    };
                });
            }
        }

        public KeyValuePair<string, string> SelectedPaymentTerms
        {
            get { return GetValue(() => SelectedPaymentTerms); }
            set
            {
                SetValue(() => SelectedPaymentTerms, value);
                switch (value.Value)
                {
                    case null:
                        return;

                    case "Other":
                        _entity.QuotationDto.SetProperty("PaymentTerms", "");
                        SelectedPaymentTerms = default;
                        IsPaymentTermsReadOnly = false;
                        break;

                    default:
                        _entity.QuotationDto.SetProperty("PaymentTerms", value.Value);
                        SelectedPaymentTerms = default;
                        IsPaymentTermsReadOnly = true;
                        break;
                }
            }
        }

        public bool IsPaymentTermsReadOnly
        {
            get { return GetValue(() => IsPaymentTermsReadOnly); }
            set { SetValue(() => IsPaymentTermsReadOnly, value); }
        }

        public string Total
        {
            get { return GetValue(() => Total); }
            set { SetValue(() => Total, value); }
        }

        public FileDto SelectedFile
        {
            get { return GetValue(() => SelectedFile); }
            set { SetValue(() => SelectedFile, value); }
        }

        public NoteDto SelectedNote
        {
            get { return GetValue(() => SelectedNote); }
            set { SetValue(() => SelectedNote, value); }
        }

        public bool IsCloseAndReviseSalesOrderVisible
        {
            get { return GetValue(() => IsCloseAndReviseSalesOrderVisible); }
            set { SetValue(() => IsCloseAndReviseSalesOrderVisible, value); }
        }

        public bool IsPrintSalesOrderVisible
        {
            get { return GetValue(() => IsPrintSalesOrderVisible); }
            set { SetValue(() => IsPrintSalesOrderVisible, value); }
        }

        public bool IsApproveVisible
        {
            get { return GetValue(() => IsApproveVisible); }
            set { SetValue(() => IsApproveVisible, value); }
        }

        public bool IsReasonForClosingVisible
        {
            get { return GetValue(() => IsReasonForClosingVisible); }
            set { SetValue(() => IsReasonForClosingVisible, value); }
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<SalesLookupAggregate>(ApiControllers.Lookup, "SalesGetLookups");
        }
    }
}