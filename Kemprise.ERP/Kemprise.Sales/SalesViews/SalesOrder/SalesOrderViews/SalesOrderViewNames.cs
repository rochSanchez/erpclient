﻿namespace Kemprise.Sales.SalesViews.SalesOrder.SalesOrderViews
{
    public class SalesOrderViewNames
    {
        public const string SalesOrderMainView = "Sales Order Main View";
        public const string SalesOrderEditView = "Sales Order Edit View";
        public const string SalesOrderDisplayView = "Sales Order Display View";
    }
}