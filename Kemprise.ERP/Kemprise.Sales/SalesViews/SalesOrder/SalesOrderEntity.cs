﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using System.Collections.ObjectModel;

namespace Kemprise.Sales.SalesViews.SalesOrder
{
    public class SalesOrderEntity : EntityBase
    {
        public SalesOrderEntity()
        {
            QuotationDto = new DynamicNotifyPropertyChangedProxy(new QuotationDto());
            QuotationDetails = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            QuotationNotes = new ObservableCollection<NoteDto>();
            QuotationFiles = new ObservableCollection<FileDto>();
        }

        public DynamicNotifyPropertyChangedProxy QuotationDto { get; set; }
        public ObservableCollection<DynamicNotifyPropertyChangedProxy> QuotationDetails { get; set; }
        public ObservableCollection<NoteDto> QuotationNotes { get; set; }
        public ObservableCollection<FileDto> QuotationFiles { get; set; }
    }
}