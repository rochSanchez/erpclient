﻿namespace Kemprise.Sales.SalesViews
{
    public enum SalesTypes
    {
        Inquiry,
        Quotation,
        SalesOrder,
        Invoicing
    }
}