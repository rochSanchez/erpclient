﻿namespace Kemprise.Sales.SalesViews.Invoicing.InvoicingViews
{
    public class InvoiceViewNames
    {
        public const string InvoiceMainView = "Invoice Main View";
        public const string InvoiceDisplayView = "Invoice Display View";
    }
}