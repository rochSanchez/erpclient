﻿using ERP.WebApi.DTO.DTOs.Sales.Invoicing;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Currency;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Sales.SalesViews.Invoicing.Methods
{
    public class InvoiceGetRec : GetMethodCallBase
    {
        public InvoiceGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "InvoiceGetRec";
        public override ErpModules ModuleName => ErpModules.Sales;
        public override string Category => SalesTypes.Invoicing.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Invoice;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<QuotationAggregate>(MethodName, new object[] { MethodParameter });
            if (aggregate == null) return;

            if (aggregate.QuotationDto != null)
                entity.SetProperty("QuotationDto", aggregate.QuotationDto);

            if (aggregate.QuotationDetails.Any())
            {
                var orderedList = aggregate.QuotationDetails.OrderBy(x => x.OrderNumber);
                var details = new ObservableCollection<QuotationDetailDto>(orderedList);

                entity.SetProperty("QuotationDetails", details);
            }

            if (aggregate.Invoices.Any())
            {
                var orderedList = aggregate.Invoices
                    .OrderByDescending(x => x.InvoiceDto.Sequence)
                    .ToList();
                orderedList.ForEach(x => x.InvoiceDto.StringTotal =
                    x.InvoiceDto.Total.ToCurrency(x.InvoiceDto.CurrencyName, x.InvoiceDto.Prefix));

                var invoices = new ObservableCollection<InvoiceAggregate>(orderedList);
                entity.SetProperty("Invoices", invoices);
            }
        }
    }
}