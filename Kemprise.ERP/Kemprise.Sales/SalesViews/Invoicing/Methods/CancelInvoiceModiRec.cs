﻿using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Sales.Invoicing;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Sales.SalesViews.Invoicing.Methods
{
    public class CancelInvoiceModiRec : ErpSaveMethodCallBase
    {
        public CancelInvoiceModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "CancelInvoiceModiRec";
        public override ErpModules ModuleName => ErpModules.Sales;
        public override ApiControllers Controller => ApiControllers.Invoice;
        public override string Category => SalesTypes.Invoicing.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var invoiceEntity = entity.As<InvoiceEntity>();
            var invoiceDetails = new List<InvoiceDetailDto>();

            invoiceEntity.InvoiceDetails.ForEach(x =>
                invoiceDetails.Add(x.GetDto<InvoiceDetailDto>()));

            var aggregate = new InvoiceAggregate
            {
                InvoiceDto = invoiceEntity.InvoiceDto.GetDto<InvoiceDto>(),
                InvoiceDetails = invoiceDetails
            };

            aggregate.InvoiceDto.Status = InvoiceStatus.Closed;

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(InvoiceAggregate), aggregate }
            };

            await UpdateRecordAsync(MethodName, dataToSave);
        }
    }
}