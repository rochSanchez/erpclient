﻿using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Sales.Invoicing;
using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.Currency;
using Kemprise.Infrastructure.Services.Sales;
using Kemprise.Sales.SalesViews.Invoicing.InvoicingViews;
using Kemprise.Sales.WorkArea;
using Prism.Commands;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Sales.SalesViews.Invoicing
{
    public sealed class InvoiceWorkAreaViewModel : SalesViewModelBase
    {
        private InvoiceEntity _entity;

        public InvoiceWorkAreaViewModel(IUnityContainer container) :
            base(container, SalesTypes.Invoicing.ToString())
        {
            SetView(InvoiceViewNames.InvoiceMainView);

            OpenCommand = new DelegateCommand(OpenExecute);
            BackCommand = new DelegateCommand(BackExecute);
            CancelCommand = new DelegateCommand(CancelExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshInvoicesExecute);

            AddInvoiceCommand = new DelegateCommand(AddInvoiceExecute);
            SaveInvoiceCommand = new DelegateCommand(SaveInvoiceExecute, CanSaveInvoice);
            SubmitInvoiceDateCommand = new DelegateCommand(SubmitInvoiceDateExecute);
            PrintInvoiceCommand = new DelegateCommand<object>(PrintInvoiceExecute);
            UpdateInvoiceDateCommand = new DelegateCommand<object>(UpdateInvoiceDateExecute);
            CancelInvoiceCommand = new DelegateCommand<object>(CancelInvoiceExecute);

            SelectedFilter = new KeyValuePair<string, string>("ALL", "ALL");
            Methods["InvoiceGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        public DelegateCommand AddInvoiceCommand { get; set; }
        public DelegateCommand SaveInvoiceCommand { get; set; }
        public DelegateCommand SubmitInvoiceDateCommand { get; set; }
        public DelegateCommand<object> PrintInvoiceCommand { get; set; }
        public DelegateCommand<object> UpdateInvoiceDateCommand { get; set; }
        public DelegateCommand<object> CancelInvoiceCommand { get; set; }

        public Dictionary<string, string> InvoiceFilters { get; set; } = new Dictionary<string, string>
        {
            {"ALL","ALL" },
            {InvoiceStatus.NotYetInvoiced, InvoiceStatus.NotYetInvoiced },
            {InvoiceStatus.PartiallyInvoiced, InvoiceStatus.PartiallyInvoiced },
            {InvoiceStatus.Completed, InvoiceStatus.Completed }
        };

        private void WhenGetRecDone()
        {
            var allInvoiced = _entity.QuotationDetails.All(x => x.Pending == 0);
            IsAddInvoiceVisible = !allInvoiced;
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new InvoiceEntity());
            _entity = Entity.GetDto<InvoiceEntity>();

            await OpenItemExecute(SelectedItem.Id, true);

            if (_entity.QuotationDto.Status != QuotationStatus.SalesOrder)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Sales Order Not Found. Please Refresh List"));
                return;
            }

            SetView(InvoiceViewNames.InvoiceDisplayView);
        }

        private void BackExecute()
        {
            RefreshExecute();
            SetView(InvoiceViewNames.InvoiceMainView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<SalesWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Invoicing"));
        }

        private void CancelExecute()
        {
            AddInvoiceWindow?.Close();
            UpdateInvoiceDateWindow?.Close();
        }

        private void RefreshInvoicesExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        private void AddInvoiceExecute()
        {
            _entity.InvoiceDetails.Clear();

            var invoiceDto = _entity.QuotationDto.ToInvoiceDto();
            Entity.SetProperty("InvoiceDto", new DynamicNotifyPropertyChangedProxy(invoiceDto));

            var detailsWithNoInvoice = _entity.QuotationDetails
                .Where(x => x.Pending > 0)
                .OrderBy(x => x.OrderNumber)
                .ToList();
            detailsWithNoInvoice.ToInvoiceDetails().ForEach(detail =>
            {
                var proxy = new DynamicNotifyPropertyChangedProxy(detail);
                _entity.InvoiceDetails.Add(proxy);
            });

            SetNumbering();
            InvoicePropertyChanged();
            ComputeTotal();

            AddInvoiceWindow = new AddInvoiceWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };
            AddInvoiceWindow.ShowDialog();
        }

        private void SetNumbering()
        {
            var count = 1;
            _entity.InvoiceDetails.ForEach(x =>
            {
                x.SetProperty("OrderNumber", count);
                count++;
            });
        }

        private void InvoicePropertyChanged()
        {
            _entity.InvoiceDto.PropertyChanged += (s, e) =>
            {
                ComputeTotal();
                SaveInvoiceCommand.RaiseCanExecuteChanged();
            };
            _entity.InvoiceDetails.CollectionChanged += (s, e) => SetNumbering();
            _entity.InvoiceDetails.ForEach(x => x.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName != "Quantity") return;

                var detail = x.GetDto<InvoiceDetailDto>();
                x.SetProperty("Total", detail.Quantity * detail.Price);

                ComputeTotal();
                SaveInvoiceCommand.RaiseCanExecuteChanged();
            });
        }

        private void ComputeTotal()
        {
            var invoiceDto = _entity.InvoiceDto.GetDto<InvoiceDto>();

            var totalDetails = _entity.InvoiceDetails.Sum(x => x.GetDto<InvoiceDetailDto>().Total);
            var discountPercentage = invoiceDto.DiscountPercentage;
            var discountAmount = invoiceDto.DiscountAmount;
            var tax = invoiceDto.TaxPercentage;

            var amountToDiscount = totalDetails * (discountPercentage / 100);
            var totalDiscount = amountToDiscount + discountAmount;
            var discountedTotal = totalDetails - totalDiscount;

            var amountToTax = totalDetails * (tax / 100);
            var total = discountedTotal + amountToTax;

            _entity.InvoiceDto.SetProperty("SubTotal", totalDetails);
            _entity.InvoiceDto.SetProperty("TotalDiscount", totalDiscount);
            _entity.InvoiceDto.SetProperty("TaxAmount", amountToTax);
            _entity.InvoiceDto.SetProperty("Total", total);

            SetTotalBasedOnCurrency();
        }

        private void SetTotalBasedOnCurrency()
        {
            var invoiceDto = _entity.InvoiceDto.GetDto<InvoiceDto>();
            Total = invoiceDto.Total.ToCurrency(invoiceDto.CurrencyName, invoiceDto.Prefix);
        }

        private async void SaveInvoiceExecute()
        {
            CancelExecute();

            var notInvoicedDetails = _entity.InvoiceDetails
                .Where(x => x.GetDto<InvoiceDetailDto>().Quantity <= 0)
                .ToList();
            if (notInvoicedDetails.Any())
            {
                notInvoicedDetails.ForEach(x => _entity.InvoiceDetails.Remove(x));
                SetNumbering();
            }

            var result = await SaveRecordAsync("InvoiceAddRec");
            if (result.OperationResult != OperationResult.Success) return;

            await GetRecordAsync("InvoiceGetRec");
        }

        private bool CanSaveInvoice()
        {
            var invalidQty = _entity.InvoiceDetails.Any(x =>
            {
                var detail = x.GetDto<InvoiceDetailDto>();
                return detail.Quantity > detail.Pending;
            });

            return !invalidQty;
        }

        private async void SubmitInvoiceDateExecute()
        {
            CancelExecute();

            var result = await SaveRecordAsync("InvoiceDateModiRec");
            if (result.OperationResult != OperationResult.Success) return;

            await GetRecordAsync("InvoiceGetRec");
        }

        private void PrintInvoiceExecute(object obj)
        {
            _entity.InvoiceDetails.Clear();

            var invoiceAggregate = _entity.Invoices.FirstOrDefault(x => x.InvoiceDto.InvoiceId == (int)obj);
            if (invoiceAggregate == null) return;

            Entity.SetProperty("InvoiceDto", new DynamicNotifyPropertyChangedProxy(invoiceAggregate.InvoiceDto));
            invoiceAggregate.InvoiceDetails.ForEach(x =>
            {
                var proxy = new DynamicNotifyPropertyChangedProxy(x);
                _entity.InvoiceDetails.Add(proxy);
            });

            RunReport("Invoice");
        }

        private void UpdateInvoiceDateExecute(object obj)
        {
            _entity.InvoiceDetails.Clear();

            var invoiceAggregate = _entity.Invoices.FirstOrDefault(x => x.InvoiceDto.InvoiceId == (int)obj);
            if (invoiceAggregate == null) return;

            Entity.SetProperty("InvoiceDto", new DynamicNotifyPropertyChangedProxy(invoiceAggregate.InvoiceDto));
            invoiceAggregate.InvoiceDetails.ForEach(x =>
            {
                var proxy = new DynamicNotifyPropertyChangedProxy(x);
                _entity.InvoiceDetails.Add(proxy);
            });

            UpdateInvoiceDateWindow = new UpdateInvoiceDateWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };
            UpdateInvoiceDateWindow.ShowDialog();
        }

        private void CancelInvoiceExecute(object obj)
        {
            var invoiceAggregate = _entity.Invoices.FirstOrDefault(x => x.InvoiceDto.InvoiceId == (int)obj);
            if (invoiceAggregate == null) return;

            if (invoiceAggregate.InvoiceDto.Status == InvoiceStatus.Closed)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Invoice is already Cancelled"));
                return;
            }

            RadWindow.Confirm("Cancel Invoice?", async (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.InvoiceDetails.Clear();

                Entity.SetProperty("InvoiceDto", new DynamicNotifyPropertyChangedProxy(invoiceAggregate.InvoiceDto));
                invoiceAggregate.InvoiceDetails.ForEach(x =>
                {
                    var proxy = new DynamicNotifyPropertyChangedProxy(x);
                    _entity.InvoiceDetails.Add(proxy);
                });

                var result = await SaveRecordAsync("CancelInvoiceModiRec");
                if (result.OperationResult != OperationResult.Success) return;

                await GetRecordAsync("InvoiceGetRec");
            });
        }

        public bool IsAddInvoiceVisible
        {
            get { return GetValue(() => IsAddInvoiceVisible); }
            set { SetValue(() => IsAddInvoiceVisible, value); }
        }

        public AddInvoiceWindow AddInvoiceWindow
        {
            get { return GetValue(() => AddInvoiceWindow); }
            set { SetValue(() => AddInvoiceWindow, value); }
        }

        public UpdateInvoiceDateWindow UpdateInvoiceDateWindow
        {
            get { return GetValue(() => UpdateInvoiceDateWindow); }
            set { SetValue(() => UpdateInvoiceDateWindow, value); }
        }

        public string Total
        {
            get { return GetValue(() => Total); }
            set { SetValue(() => Total, value); }
        }
    }
}