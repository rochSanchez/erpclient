﻿using ERP.WebApi.DTO.DTOs.Sales.Invoicing;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using System.Collections.ObjectModel;

namespace Kemprise.Sales.SalesViews.Invoicing
{
    public class InvoiceEntity : EntityBase
    {
        public InvoiceEntity()
        {
            QuotationDto = new QuotationDto();
            QuotationDetails = new ObservableCollection<QuotationDetailDto>();
            Invoices = new ObservableCollection<InvoiceAggregate>();

            InvoiceDto = new DynamicNotifyPropertyChangedProxy(new InvoiceDto());
            InvoiceDetails = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
        }

        public QuotationDto QuotationDto { get; set; }
        public ObservableCollection<QuotationDetailDto> QuotationDetails { get; set; }
        public ObservableCollection<InvoiceAggregate> Invoices { get; set; }

        public DynamicNotifyPropertyChangedProxy InvoiceDto { get; set; }
        public ObservableCollection<DynamicNotifyPropertyChangedProxy> InvoiceDetails { get; set; }
    }
}