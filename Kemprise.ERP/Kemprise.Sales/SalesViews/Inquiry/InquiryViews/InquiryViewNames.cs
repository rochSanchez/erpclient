﻿namespace Kemprise.Sales.SalesViews.Inquiry.InquiryViews
{
    public class InquiryViewNames
    {
        public const string InquiryMainView = "Inquiry Main View"; 
        public const string InquiryEditView = "Inquiry Edit View"; 
        public const string InquiryDisplayView = "Inquiry Display View";
        public const string InquiryConvertToQuotationView = "Inquiry Convert To Quotation View";
    }
}