﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using System.Collections.ObjectModel;

namespace Kemprise.Sales.SalesViews.Inquiry
{
    public class InquiryEntity : EntityBase
    {
        public InquiryEntity()
        {
            InquiryDto = new DynamicNotifyPropertyChangedProxy(new InquiryDto());
            InquiryDetails = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
            InquiryFiles = new ObservableCollection<FileDto>();
        }

        public DynamicNotifyPropertyChangedProxy InquiryDto { get; set; }
        public ObservableCollection<DynamicNotifyPropertyChangedProxy> InquiryDetails { get; set; }
        public ObservableCollection<FileDto> InquiryFiles { get; set; }
    }
}