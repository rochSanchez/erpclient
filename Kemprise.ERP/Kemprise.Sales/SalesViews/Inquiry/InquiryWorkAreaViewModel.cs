﻿using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using ERP.WebApi.DTO.Lookups.LookupItems;
using Kemprise.Infrastructure.Converters;
using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.Infrastructure.Services.Sales;
using Kemprise.Infrastructure.Windows.CloseEntity;
using Kemprise.Infrastructure.Windows.ReasonForClosing;
using Kemprise.Sales.SalesViews.Inquiry.InquiryViews;
using Kemprise.Sales.SalesViews.Quotation;
using Kemprise.Sales.SalesViews.Quotation.QuotationViews;
using Kemprise.Sales.WorkArea;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Sales.SalesViews.Inquiry
{
    public sealed class InquiryWorkAreaViewModel : SalesViewModelBase
    {
        private readonly IFileService _fileService;
        private InquiryEntity _entity;

        public InquiryWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, SalesTypes.Inquiry.ToString())
        {
            _fileService = fileService;
            SetView(InquiryViewNames.InquiryMainView);

            ClearCommand = new DelegateCommand(ClearExecute);
            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            EditAsCommand = new DelegateCommand(EditAsExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveAsDraftCommand = new DelegateCommand(SaveAsDraftExecute);
            PrintDraftCommand = new DelegateCommand(PrintDraftExecute);
            DiscardDraftCommand = new DelegateCommand(DiscardDraftExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSave);
            CancelCommand = new DelegateCommand(CancelExecute);
            AddExistingItemCommand = new DelegateCommand(AddExistingItemExecute);
            AddBlankItemCommand = new DelegateCommand(AddBlankItemExecute);
            AddPartCommand = new DelegateCommand(AddPartExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshInquiriesExecute);

            UploadFileCommand = new DelegateCommand(UploadFileExecute);
            DownloadFileCommand = new DelegateCommand(DownloadFileExecute);
            ConvertToQuotationCommand = new DelegateCommand(ConvertToQuotationExecute);
            CloseEntityCommand = new DelegateCommand(CloseEntityExecute);
            ViewClosingDetailsCommand = new DelegateCommand(ViewClosingDetailsExecute);
            DeleteItemCommand = new DelegateCommand<object>(DeleteItemExecute);
            DeleteFileCommand = new DelegateCommand<object>(DeleteFileExecute);
            RadioButtonIsCheckedCommand = new DelegateCommand<object>(RadioButtonIsCheckedExecute);

            SelectedFilter = new KeyValuePair<string, string>("ALL", "ALL");
            Methods["InquiryGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        public DelegateCommand UploadFileCommand { get; set; }
        public DelegateCommand DownloadFileCommand { get; set; }
        public DelegateCommand ConvertToQuotationCommand { get; set; }
        public DelegateCommand<object> DeleteItemCommand { get; set; }
        public DelegateCommand<object> DeleteFileCommand { get; set; }

        public Dictionary<string, string> PaymentTerms { get; set; } = new Dictionary<string, string>
        {
             {"7 Days","7 Days" },
             {"10 Days","10 Days" },
             {"15 Days","15 Days" },
             {"20 Days","20 Days" },
             {"25 Days","25 Days" },
             {"30 Days","30 Days" },
             {"60 Days","60 Days" },
             {"90 Days","90 Days" },
             {"Other","Other" }
        };

        public Dictionary<string, string> InquiryFilters { get; set; } = new Dictionary<string, string>
        {
            {"ALL","ALL" },
            {InquiryStatus.PendingReview, InquiryStatus.PendingReview },
            {InquiryStatus.QuotationCreated, InquiryStatus.QuotationCreated },
            {InquiryStatus.Closed, InquiryStatus.Closed }
        };

        private void WhenGetRecDone()
        {
            if (_entity.IsNew)
            {
                _entity.InquiryDto.SetProperty("DateCreated", DateTime.Now);
                InquiryDtoPropertyChange();
            }

            var inquiryDto = _entity.InquiryDto.GetDto<InquiryDto>();
            IsCloseInquiryVisible = inquiryDto.Status == InquiryStatus.PendingReview &&
                                    ApiSession.HasRoles(ErpRoles.CreateQuotation);
            IsReasonForClosingVisible = inquiryDto.Status == InquiryStatus.Closed;
            IsPrintDraftVisible = string.IsNullOrEmpty(inquiryDto.Status) ||
                                  inquiryDto.Status == InquiryStatus.Draft ||
                                  inquiryDto.Status == InquiryStatus.PendingReview;

            var salesPerson = Lookups["SalesPersons"].FirstOrDefault(x => x.Id == inquiryDto.SalesPersonId);
            _entity.InquiryDto.SetProperty("SalesPersonName", salesPerson?.Text ?? "");
        }

        private void InquiryDtoPropertyChange()
        {
            _entity.InquiryDto.PropertyChanged += (s, e) =>
            {
                var inquiry = _entity.InquiryDto.GetDto<InquiryDto>();

                switch (e.PropertyName)
                {
                    case "IsSameAsBillingAddress" when inquiry.IsSameAsBillingAddress:
                        {
                            _entity.InquiryDto.SetProperty("ShipTo", inquiry.CustomerName);
                            _entity.InquiryDto.SetProperty("ShippingAddress", inquiry.BillingAddress);
                            _entity.InquiryDto.SetProperty("ShippingCountry", inquiry.BillingCountry);
                            break;
                        }

                    case "IsSameAsBillingAddress" when !inquiry.IsSameAsBillingAddress:
                        {
                            _entity.InquiryDto.SetProperty("ShipTo", "");
                            _entity.InquiryDto.SetProperty("ShippingAddress", "");
                            _entity.InquiryDto.SetProperty("ShippingCountry", "");
                            break;
                        }
                }

                SaveCommand.RaiseCanExecuteChanged();
            };

            _entity.InquiryDetails.CollectionChanged += (s, e) =>
            {
                SaveCommand.RaiseCanExecuteChanged();
                SetNumbering();
            };

            SaveCommand.RaiseCanExecuteChanged();
        }

        private void ClearExecute()
        {
            RadWindow.Confirm("This will clear everything. Continue?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.InquiryDto.ClearValues();
                _entity.InquiryDetails.Clear();
                _entity.InquiryFiles.Clear();

                _entity.InquiryDto.SetProperty("DateCreated", DateTime.Now);
            });
        }

        private async void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new InquiryEntity());
            _entity = Entity.GetDto<InquiryEntity>();
            _entity.IsNew = true;

            await OpenItemExecute("");

            IsClearVisible = true;
            IsPaymentTermsReadOnly = true;
            SelectedCustomer = null;
            SetView(InquiryViewNames.InquiryEditView);
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new InquiryEntity());
            _entity = Entity.GetDto<InquiryEntity>();

            var inquiryStatus = SelectedItem.As<InquiryListItem>().Status;
            if (inquiryStatus == InquiryStatus.Draft)
            {
                _entity.IsNew = true;
                await OpenItemExecute("");

                Methods["InquiryDraftGetRec"].MethodParameter = SelectedItem.Id;
                await GetRecordAsync("InquiryDraftGetRec");
                WhenGetRecDone();

                var inquiryDto = _entity.InquiryDto.GetDto<InquiryDto>();

                IsClearVisible = true;
                IsPaymentTermsReadOnly = string.IsNullOrEmpty(inquiryDto.PaymentTerms)
                                         || PaymentTerms.ContainsKey(inquiryDto.PaymentTerms);
                SetView(InquiryViewNames.InquiryEditView);
            }
            else
            {
                await OpenItemExecute(SelectedItem.Id);
                SetView(InquiryViewNames.InquiryDisplayView);
            }
        }

        private void EditAsExecute()
        {
            _entity.IsNew = true;

            var oldInquiryDto = _entity.InquiryDto.GetDto<InquiryDto>();
            var newInquiryDto = oldInquiryDto.ToNewInquiry();
            Entity.SetProperty("InquiryDto", new DynamicNotifyPropertyChangedProxy(newInquiryDto));
            InquiryDtoPropertyChange();

            SetView(InquiryViewNames.InquiryEditView);
        }

        private void BackExecute()
        {
            RefreshExecute();
            SetView(InquiryViewNames.InquiryMainView);
        }

        private async void SaveAsDraftExecute()
        {
            var inquiryDto = _entity.InquiryDto.GetDto<InquiryDto>();
            if (string.IsNullOrEmpty(inquiryDto.ProjectName) || string.IsNullOrEmpty(inquiryDto.SalesPersonId) ||
                inquiryDto.CurrencyId == 0 || inquiryDto.CustomerId == 0)
            {
                EventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload($"The Following Details are Required {Environment.NewLine} {Environment.NewLine}" +
                                                   $"• Description {Environment.NewLine}" +
                                                   $"• Customer {Environment.NewLine}" +
                                                   $"• Sales Person {Environment.NewLine}" +
                                                   "• Currency"));
                return;
            }

            _entity.InquiryDto.SetProperty("Status", InquiryStatus.Draft);

            var taskToPerform = inquiryDto.InquiryId == 0
                ? "InquiryDraftAddRec"
                : "InquiryDraftModiRec";

            await SaveRecordAsync(taskToPerform);
        }

        private void PrintDraftExecute()
        {
            RunReport("InquiryDraft");
        }

        private void DiscardDraftExecute()
        {
            RadWindow.Confirm("Discard Inquiry?", OnDiscardDraft);
        }

        private async void SaveExecute()
        {
            _entity.InquiryDto.SetProperty("Status", InquiryStatus.PendingReview);

            if (_entity.InquiryDetails.Any(x => string.IsNullOrEmpty(x.GetDto<InquiryDetailDto>().UnitOfMeasurement)))
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload("Item/s without UOM found."));
                return;
            }

            var saveResult = await SaveRecordAsync("InquiryAddRec");
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSave()
        {
            var inquiryDto = _entity.InquiryDto.GetDto<InquiryDto>();

            return inquiryDto.CurrencyId != 0
                   && inquiryDto.ExpirationDate != null
                   && !string.IsNullOrEmpty(inquiryDto.ProjectName)
                   && !string.IsNullOrEmpty(inquiryDto.Requester)
                   && !string.IsNullOrEmpty(inquiryDto.CustomerName)
                   && !string.IsNullOrEmpty(inquiryDto.CustomerType)
                   && !string.IsNullOrEmpty(inquiryDto.PaymentTerms)
                   && !string.IsNullOrEmpty(inquiryDto.SalesPersonId)
                   && !string.IsNullOrEmpty(inquiryDto.BillingAddress)
                   && !string.IsNullOrEmpty(inquiryDto.ShippingAddress)
                   && _entity.InquiryDetails.Any();
        }

        private void CancelExecute()
        {
            PartWindow?.Close();
        }

        private void AddExistingItemExecute()
        {
            var partNumberView = new CollectionViewSource { Source = Lookups["Parts"] };
            PartCollectionView = partNumberView;

            PartWindow = new InquiryWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };

            SearchForPartNumber = "";
            SearchForDescription = "";
            PartWindow.ShowDialog();
        }

        private void AddBlankItemExecute()
        {
            var newProxy = new DynamicNotifyPropertyChangedProxy(new InquiryDetailDto { PartNumber = "TBA" });
            _entity.InquiryDetails.Add(newProxy);
        }

        private void AddPartExecute()
        {
            if (SelectedPart == null) return;

            var partId = Convert.ToInt32(SelectedPart.Id);
            var uom = Lookups["UnitOfMeasurements"].FirstOrDefault(x => x.Id == SelectedPart.UnitOfMeasurementId.ToString());
            var newDetail = new InquiryDetailDto
            {
                PartId = partId,
                PartNumber = SelectedPart.PartNumber,
                Description = SelectedPart.Text,
                UnitOfMeasurement = uom?.Text ?? "",
                Price = SelectedPart.Price
            };
            var newProxy = new DynamicNotifyPropertyChangedProxy(newDetail);

            _entity.InquiryDetails.Add(newProxy);
            CancelExecute();
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<SalesWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Inquiry"));
        }

        private void RefreshInquiriesExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        private void UploadFileExecute()
        {
            _fileService.GetAnyFile(_entity.InquiryFiles);
        }

        private void DownloadFileExecute()
        {
            if (SelectedFile == null) return;
            _fileService.SaveFile(SelectedFile);
        }

        private void ConvertToQuotationExecute()
        {
            QuotationView = Container.Resolve<QuotationWorkAreaView>();
            QuotationViewModel = QuotationView.GetViewModel<QuotationWorkAreaViewModel>();

            var inquiryDto = _entity.InquiryDto.GetDto<InquiryDto>();
            QuotationViewModel.SelectedInquiry = new InquiryListItem { Id = inquiryDto.InquiryId.ToString() };

            QuotationViewModel.BackCommand = new DelegateCommand(BackToInquiryExecute);
            QuotationViewModel.SaveForLaterCommand = new DelegateCommand(SaveForLaterExecute);
            QuotationViewModel.SaveCommand = new DelegateCommand(SaveQuotationExecute, CanSaveQuotation);

            QuotationViewModel.CreateNewQuotationEntity();
            QuotationViewModel.CreateQuotationExecute();

            QuotationViewModel.SetQuotationView(QuotationViewNames.QuotationManagersEditView);
            SetView(InquiryViewNames.InquiryConvertToQuotationView);
        }

        private void BackToInquiryExecute()
        {
            SetView(InquiryViewNames.InquiryDisplayView);
        }

        private async void SaveForLaterExecute()
        {
            var quotationEntity = QuotationViewModel.Entity.GetDto<QuotationEntity>();
            quotationEntity.QuotationDto.SetProperty("Status", QuotationStatus.RevisionPending);

            var saveResult = await QuotationViewModel.SaveRecordAsync("QuotationAddRec");
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private async void SaveQuotationExecute()
        {
            var quotationEntity = QuotationViewModel.Entity.GetDto<QuotationEntity>();
            quotationEntity.QuotationDto.SetProperty("Status", QuotationStatus.SubmittedForApproval);

            var saveResult = await QuotationViewModel.SaveRecordAsync("QuotationAddRec");
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSaveQuotation()
        {
            var quotationEntity = QuotationViewModel.Entity.GetDto<QuotationEntity>();
            var quotationDto = quotationEntity.QuotationDto.GetDto<QuotationDto>();

            return quotationDto.CustomerId != 0
                   && quotationDto.CurrencyId != 0
                   && quotationDto.ExpirationDate != null
                   && !string.IsNullOrEmpty(quotationDto.ProjectName)
                   && !string.IsNullOrEmpty(quotationDto.Requester)
                   && !string.IsNullOrEmpty(quotationDto.Delivery)
                   && !string.IsNullOrEmpty(quotationDto.ShippingTerms)
                   && !string.IsNullOrEmpty(quotationDto.PaymentTerms)
                   && !string.IsNullOrEmpty(quotationDto.ShipTo)
                   && !string.IsNullOrEmpty(quotationDto.ShippingAddress)
                   && quotationEntity.QuotationDetails.Any();
        }

        private async void OnDiscardDraft(object sender, WindowClosedEventArgs e)
        {
            if (e.DialogResult == false || e.DialogResult == null) return;

            var inquiryDto = _entity.InquiryDto.GetDto<InquiryDto>();
            if (inquiryDto.InquiryId != 0)
                await SaveRecordAsync("InquiryDicardDraftModiRec");

            BackExecute();
        }

        private void CloseEntityExecute()
        {
            RadWindow.Confirm("Cancel Inquiry?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                var closeWindow = Container.Resolve<CloseEntityWindow>();
                var viewModel = closeWindow.GetViewModel<CloseEntityWindowViewModel>();

                viewModel.Reasons = Lookups["Reasons"];
                viewModel.SetPropertyToDto = () =>
                {
                    _entity.InquiryDto.SetProperty("ReasonForClosing", viewModel.SelectedReasonText.ToUpper());
                    _entity.InquiryDto.SetProperty("DetailedReasonForClosing", viewModel.DetailedReasonForClosing);
                };
                viewModel.CompletedAction = CloseInquiry;

                closeWindow.Owner = Application.Current.MainWindow;
                closeWindow.ShowDialog();
            });
        }

        private async void CloseInquiry()
        {
            _entity.InquiryDto.SetProperty("Status", InquiryStatus.Closed);
            await SaveRecordAsync("InquiryModiRec");
            BackExecute();
        }

        private void ViewClosingDetailsExecute()
        {
            var closedWindow = Container.Resolve<ReasonForClosingWindow>();
            var viewModel = closedWindow.GetViewModel<ReasonForClosingWindowViewModel>();

            var inquiryDto = _entity.InquiryDto.GetDto<InquiryDto>();
            viewModel.ReasonForClosing = inquiryDto.ReasonForClosing;
            viewModel.DetailedReasonForClosing = inquiryDto.DetailedReasonForClosing;

            closedWindow.Owner = Application.Current.MainWindow;
            closedWindow.ShowDialog();
        }

        private void DeleteItemExecute(object obj)
        {
            if (SelectedDetail == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Remove Item?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.InquiryDetails.Remove(SelectedDetail);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Remove Item?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                    {
                        _entity.InquiryDetails.Remove(SelectedDetail);
                    }
                });
            }
        }

        private void DeleteFileExecute(object obj)
        {
            if (SelectedFile == null) return;

            if (obj == null)
            {
                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null) return;
                    _entity.InquiryFiles.Remove(SelectedFile);
                });
            }
            else
            {
                if (!(obj is GridViewDeletingEventArgs eventArgs)) return;

                RadWindow.Confirm("Delete File?", (s, e) =>
                {
                    if (e.DialogResult == false || e.DialogResult == null)
                        eventArgs.Cancel = true;
                    else
                        _entity.InquiryFiles.Remove(SelectedFile);
                });
            }
        }

        private void SetNumbering()
        {
            var count = 1;
            _entity.InquiryDetails.ForEach(x =>
            {
                x.SetProperty("OrderNumber", count);
                count++;
            });
        }

        private void RadioButtonIsCheckedExecute(object obj)
        {
            if (!(obj is ObjectParameter param)) return;
            _entity.InquiryDto.SetProperty(param.PropertyName, param.NewValue);
        }

        public CustomerLookup SelectedCustomer
        {
            get { return GetValue(() => SelectedCustomer); }
            set
            {
                SetValue(() => SelectedCustomer, value);
                if (value == null)
                {
                    _entity.InquiryDto.SetProperty("CustomerId", 0);
                    _entity.InquiryDto.SetProperty("CustomerName", "");
                    _entity.InquiryDto.SetProperty("CustomerType", "");
                    _entity.InquiryDto.SetProperty("BillingAddress", "");
                    _entity.InquiryDto.SetProperty("BillingCountry", "");
                    return;
                }

                _entity.InquiryDto.SetProperty("CustomerId", Convert.ToInt32(value.Id));
                _entity.InquiryDto.SetProperty("CustomerName", value.Text);
                _entity.InquiryDto.SetProperty("CustomerType", value.CustomerType);
                _entity.InquiryDto.SetProperty("BillingAddress", value.CustomerAddress);
                _entity.InquiryDto.SetProperty("BillingCountry", value.CustomerCountry);
            }
        }

        public KeyValuePair<string, string> SelectedPaymentTerms
        {
            get { return GetValue(() => SelectedPaymentTerms); }
            set
            {
                SetValue(() => SelectedPaymentTerms, value);
                switch (value.Value)
                {
                    case null:
                        return;

                    case "Other":
                        _entity.InquiryDto.SetProperty("PaymentTerms", "");
                        SelectedPaymentTerms = default;
                        IsPaymentTermsReadOnly = false;
                        break;

                    default:
                        _entity.InquiryDto.SetProperty("PaymentTerms", value.Value);
                        SelectedPaymentTerms = default;
                        IsPaymentTermsReadOnly = true;
                        break;
                }
            }
        }

        public bool IsPaymentTermsReadOnly
        {
            get { return GetValue(() => IsPaymentTermsReadOnly); }
            set { SetValue(() => IsPaymentTermsReadOnly, value); }
        }

        public DynamicNotifyPropertyChangedProxy SelectedDetail
        {
            get { return GetValue(() => SelectedDetail); }
            set { SetValue(() => SelectedDetail, value); }
        }

        public FileDto SelectedFile
        {
            get { return GetValue(() => SelectedFile); }
            set { SetValue(() => SelectedFile, value); }
        }

        public bool IsCloseInquiryVisible
        {
            get { return GetValue(() => IsCloseInquiryVisible); }
            set { SetValue(() => IsCloseInquiryVisible, value); }
        }

        public bool IsReasonForClosingVisible
        {
            get { return GetValue(() => IsReasonForClosingVisible); }
            set { SetValue(() => IsReasonForClosingVisible, value); }
        }

        public QuotationWorkAreaView QuotationView
        {
            get { return GetValue(() => QuotationView); }
            set { SetValue(() => QuotationView, value); }
        }

        public QuotationWorkAreaViewModel QuotationViewModel
        {
            get { return GetValue(() => QuotationViewModel); }
            set { SetValue(() => QuotationViewModel, value); }
        }

        protected override void RegisterLookups()
        {
            LookupService.RegisterLookup<SalesLookupAggregate>(ApiControllers.Lookup, "SalesGetLookups");
        }
    }
}