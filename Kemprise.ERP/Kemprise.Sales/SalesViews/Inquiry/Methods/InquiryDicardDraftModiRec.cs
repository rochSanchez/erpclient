﻿using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Sales.SalesViews.Inquiry.Methods
{
    public class InquiryDicardDraftModiRec : ErpSaveMethodCallBase
    {
        public InquiryDicardDraftModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "InquiryDicardDraftModiRec";
        public override ErpModules ModuleName => ErpModules.Sales;
        public override ApiControllers Controller => ApiControllers.Inquiry;
        public override string Category => SalesTypes.Inquiry.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var inquiryEntity = entity.As<InquiryEntity>();
            var inquiryDetails = new List<InquiryDetailDto>();

            inquiryEntity.InquiryDetails.ForEach(x => inquiryDetails.Add(x.GetDto<InquiryDetailDto>()));

            var inquiryAggregate = new InquiryAggregate
            {
                InquiryDto = inquiryEntity.InquiryDto.GetDto<InquiryDto>(),
                InquiryDetails = inquiryDetails,
                InquiryFiles = inquiryEntity.InquiryFiles
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(InquiryAggregate), inquiryAggregate }
            };

            await UpdateRecordAsync(MethodName, dataToSave);
        }
    }
}