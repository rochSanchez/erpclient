﻿using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Sales.SalesViews.Inquiry.Methods
{
    public class InquiryDraftAddRec : ErpSaveMethodCallBase
    {
        public InquiryDraftAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "InquiryDraftAddRec";
        public override ErpModules ModuleName => ErpModules.Sales;
        public override ApiControllers Controller => ApiControllers.Inquiry;
        public override string Category => SalesTypes.Inquiry.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var inquiryEntity = entity.As<InquiryEntity>();
            var inquiryDetails = new List<InquiryDetailDto>();

            inquiryEntity.InquiryDetails.ForEach(x => inquiryDetails.Add(x.GetDto<InquiryDetailDto>()));
            inquiryDetails.ForEach(x =>
            {
                if (!string.IsNullOrEmpty(x.Description))
                    x.Description = x.Description.Trim();
            });

            var textInfo = new CultureInfo("en-Us", false).TextInfo;
            foreach (var item in inquiryDetails.Where(item => !string.IsNullOrEmpty(item.UnitOfMeasurement)))
                item.UnitOfMeasurement = textInfo.ToTitleCase(item.UnitOfMeasurement);

            var inquiryAggregate = new InquiryAggregate
            {
                InquiryDto = inquiryEntity.InquiryDto.GetDto<InquiryDto>(),
                InquiryDetails = inquiryDetails,
                InquiryFiles = inquiryEntity.InquiryFiles
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(InquiryAggregate), inquiryAggregate }
            };

            var result = await SaveNewRecordAsync<InquiryAggregate>(MethodName, dataToSave);

            var inquiryId = result.InquiryDto.InquiryId;
            inquiryEntity.InquiryDto.SetProperty("InquiryId", inquiryId);
        }
    }
}