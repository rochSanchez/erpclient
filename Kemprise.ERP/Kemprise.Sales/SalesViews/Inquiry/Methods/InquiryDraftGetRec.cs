﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Sales.SalesViews.Inquiry.Methods
{
    public class InquiryDraftGetRec : GetMethodCallBase
    {
        public InquiryDraftGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "InquiryDraftGetRec";
        public override ErpModules ModuleName => ErpModules.Sales;
        public override string Category => SalesTypes.Inquiry.ToString();
        public override bool RunAtStartup => false;

        public override bool UseDefaultParameter => false;
        public override ApiControllers Controller => ApiControllers.Inquiry;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var aggregate = await GetRecordAsync<InquiryAggregate>(MethodName, new object[] { MethodParameter });
            if (aggregate == null) return;

            if (aggregate.InquiryDto != null)
                entity.SetProperty("InquiryDto", new DynamicNotifyPropertyChangedProxy(aggregate.InquiryDto));

            if (aggregate.InquiryDetails.Any())
            {
                var orderedList = aggregate.InquiryDetails.OrderBy(x => x.OrderNumber);
                var details = new ObservableCollection<DynamicNotifyPropertyChangedProxy>();
                orderedList.ForEach(x => details.Add(new DynamicNotifyPropertyChangedProxy(x)));

                entity.SetProperty("InquiryDetails", details);
            }

            if (aggregate.InquiryFiles.Any())
            {
                var files = new ObservableCollection<FileDto>(aggregate.InquiryFiles);
                entity.SetProperty("InquiryFiles", files);
            }
        }
    }
}