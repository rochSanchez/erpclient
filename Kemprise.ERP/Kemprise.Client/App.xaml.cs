﻿using Flurl.Http;
using Kemprise.Client.Shell;
using Kemprise.Infrastructure.Constants;
using Kemprise.Infrastructure.Controllers;
using Kemprise.Infrastructure.Events.Security;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Regions;
using Kemprise.Infrastructure.Reporting;
using Kemprise.Infrastructure.Reporting.Service;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.Infrastructure.Services.Lookup;
using Kemprise.Infrastructure.Services.Notifications;
using Kemprise.Infrastructure.Tools.ExcelReader;
using Kemprise.Inventory;
using Kemprise.Management;
using Kemprise.PLM;
using Kemprise.Procurement;
using Kemprise.Sales;
using Kemprise.Security;
using Kemprise.Security.Controller;
using Prism.Events;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Unity;
using System;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using Telerik.Windows.Controls;
using Unity;
using Unity.RegistrationByConvention;

namespace Kemprise.Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            return null;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            ResourceDictionaryLoader.Create().LoadResourceDictionaries(Assembly.GetAssembly(typeof(ShellViewModel)));
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.GetContainer()
                .RegisterTypes(
                    AllClasses.FromAssembliesInBasePath().Where(type => typeof(IErpMethodCall).IsAssignableFrom(type)),
                    WithMappings.FromAllInterfaces, WithName.TypeName, WithLifetime.PerResolve);

            containerRegistry.GetContainer()
                .RegisterTypes(
                    AllClasses.FromLoadedAssemblies().Where(x => typeof(IParentNavigationItem).IsAssignableFrom(x)),
                    WithMappings.FromAllInterfaces, WithName.TypeName, WithLifetime.ContainerControlled);

            containerRegistry.GetContainer()
                .RegisterTypes(
                    AllClasses.FromLoadedAssemblies().Where(x => typeof(IChildNavigationItem).IsAssignableFrom(x)),
                    WithMappings.FromAllInterfaces, WithName.TypeName, WithLifetime.ContainerControlled);

            containerRegistry.GetContainer()
                 .RegisterTypes(
                     AllClasses.FromLoadedAssemblies().Where(x => typeof(IModuleReportDetail).IsAssignableFrom(x)),
                     WithMappings.FromAllInterfaces, WithName.TypeName, WithLifetime.ContainerControlled);

            containerRegistry.GetContainer()
                .RegisterTypes(
                    AllClasses.FromLoadedAssemblies().Where(x => typeof(IQuickAccessView).IsAssignableFrom(x)),
                    WithMappings.FromAllInterfaces, WithName.TypeName, WithLifetime.ContainerControlled);

            containerRegistry.RegisterSingleton<IFlurlClient, FlurlClient>();

            containerRegistry.Register(typeof(IControllerBase), typeof(SecurityController), nameof(SecurityController));

            containerRegistry.Register<IApiRequestManager, ApiRequestManager>();
            containerRegistry.Register<IReportLoaderService, ReportLoaderService>();
            containerRegistry.Register<IDesktopNotificationService, DesktopNotificationService>();
            containerRegistry.Register<ILookupService, LookupService>();
            containerRegistry.Register<IFileService, FileService>();
            containerRegistry.Register<IExcelReaderService, ExcelReaderService>();
            containerRegistry.Register<IWorldService, WorldService>();
        }

        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            base.ConfigureModuleCatalog(moduleCatalog);
            moduleCatalog.AddModule(typeof(SecurityModule));
            moduleCatalog.AddModule(typeof(SalesModule));
            moduleCatalog.AddModule(typeof(ManagementModule));
            moduleCatalog.AddModule(typeof(ProcurementModule));
            moduleCatalog.AddModule(typeof(PlmModule));
            moduleCatalog.AddModule(typeof(InventoryModule));
        }

        protected override void ConfigureViewModelLocator()
        {
            base.ConfigureViewModelLocator();
            ViewModelLocationProvider.SetDefaultViewModelFactory(type => Container.Resolve(type));
        }

        protected override void OnInitialized()
        {
            MaterialPalette.Palette.FontSize = 13;
            MaterialPalette.Palette.FontFamily = new FontFamily("Helvetica");
            MaterialPalette.Palette.AccentNormalColor = (Color)ColorConverter.ConvertFromString("#FF69F0AE");
            MaterialPalette.Palette.AccentHoverColor = (Color)ColorConverter.ConvertFromString("#FFB9F6CA");
            MaterialPalette.Palette.AccentPressedColor = (Color)ColorConverter.ConvertFromString("#FF00E676");
            MaterialPalette.Palette.DividerColor = (Color)ColorConverter.ConvertFromString("#1E000000");
            MaterialPalette.Palette.IconColor = (Color)ColorConverter.ConvertFromString("#FF000000");
            MaterialPalette.Palette.MainColor = (Color)ColorConverter.ConvertFromString("#FFFFFFFF");
            MaterialPalette.Palette.MarkerColor = (Color)ColorConverter.ConvertFromString("#FF000000");
            MaterialPalette.Palette.ValidationColor = (Color)ColorConverter.ConvertFromString("#FFD50000");
            MaterialPalette.Palette.ComplementaryColor = (Color)ColorConverter.ConvertFromString("#FFE0E0E0");
            MaterialPalette.Palette.AlternativeColor = (Color)ColorConverter.ConvertFromString("#FFF5F5F5");
            MaterialPalette.Palette.MarkerInvertedColor = (Color)ColorConverter.ConvertFromString("#FFFFFFFF");
            MaterialPalette.Palette.PrimaryColor = (Color)ColorConverter.ConvertFromString("#FFFAFAFA");
            MaterialPalette.Palette.PrimaryNormalColor = (Color)ColorConverter.ConvertFromString("#FF1EB150");
            MaterialPalette.Palette.PrimaryFocusColor = (Color)ColorConverter.ConvertFromString("#FF00AC4C");
            MaterialPalette.Palette.PrimaryHoverColor = (Color)ColorConverter.ConvertFromString("#FF30E66E");
            MaterialPalette.Palette.PrimaryPressedColor = (Color)ColorConverter.ConvertFromString("#FF263238");
            MaterialPalette.Palette.RippleColor = (Color)ColorConverter.ConvertFromString("#FFFFFFFF");
            MaterialPalette.Palette.ReadOnlyBackgroundColor = (Color)ColorConverter.ConvertFromString("#00FFFFFF");
            MaterialPalette.Palette.ReadOnlyBorderColor = (Color)ColorConverter.ConvertFromString("#FFABABAB");
            MaterialPalette.Palette.DividerSolidColor = (Color)ColorConverter.ConvertFromString("#FFE1E1E1");
            MaterialPalette.Palette.PrimaryOpacity = 0.87;
            MaterialPalette.Palette.SecondaryOpacity = 0.54;
            MaterialPalette.Palette.DisabledOpacity = 0.26;
            MaterialPalette.Palette.DividerOpacity = 0.38;

            var view = Container.Resolve<ShellView>();
            view.Show();

            MainWindow = view.ParentOfType<Window>();
            if (MainWindow != null)
            {
                MainWindow.ShowInTaskbar = true;
                RegionManager.SetRegionManager(MainWindow, Container.Resolve<IRegionManager>());
            }

            RegionManager.UpdateRegions();
            base.OnInitialized();
        }

        protected override void InitializeModules()
        {
            base.InitializeModules();
            var container = Container.GetContainer();
            container.ResolveAll(typeof(IControllerBase));

            var eventAggregator = container.Resolve<IEventAggregator>();
            eventAggregator.GetEvent<RequestUserCredentialsEvent>().Publish();

            var adapterMappings = Container.Resolve<RegionAdapterMappings>();
            adapterMappings.RegisterMapping(typeof(StackPanel), Container.Resolve<StackPanelRegionAdapter>());
            adapterMappings.RegisterMapping(typeof(Grid), Container.Resolve<GridRegionAdapter>());
        }

        private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Unexpected error occured. Please inform IT support." + Environment.NewLine + e.Exception.Message, "Error");
            e.Handled = true;
        }
    }
}