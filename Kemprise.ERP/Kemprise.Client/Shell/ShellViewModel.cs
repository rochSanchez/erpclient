﻿using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Events.Security;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Regions;
using Kemprise.Management.Controller;
using Kemprise.Procurement.Controller;
using Kemprise.Sales.Controller;
using Prism.Commands;
using Prism.Events;
using Prism.Ioc;
using Prism.Regions;
using System;
using System.Linq;
using System.Windows;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.PLM.Controller;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Client.Shell
{
    public class ShellViewModel : NotifyPropertyChangedBase
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly IRegionManager _regionManager;
        private readonly IUnityContainer _container;

        public ShellViewModel()
        {
            _eventAggregator = ContainerLocator.Current.Resolve<IEventAggregator>();
            _regionManager = ContainerLocator.Current.Resolve<IRegionManager>();
            _container = ContainerLocator.Current.Resolve<IUnityContainer>();

            _eventAggregator.GetEvent<LoginSuccessfulEvent>().Subscribe(OnUserLoginSuccessful, ThreadOption.UIThread);
            _eventAggregator.GetEvent<OpenModuleEvent>().Subscribe(OnOpenModule);
            _eventAggregator.GetEvent<ErrorEvent>().Subscribe(OnExceptionCaught);
            _eventAggregator.GetEvent<LogoutUserEvent>().Subscribe(OnLogoutUserEvent);

            GoHomeCommand = new DelegateCommand<object>(GoHomeExecute);
            LogOutCommand = new DelegateCommand(LogOutExecute);

            ErpBackground = new Uri("/Kemprise.Client;component/Images/BGLogo.jpg", UriKind.Relative);
            IsSmallLogoVisible = false;
        }

        public DelegateCommand LogOutCommand { get; set; }
        public DelegateCommand<object> GoHomeCommand { get; set; }

        private void OnUserLoginSuccessful(LoginSuccessfulEventPayload obj)
        {
            var quickAccessView = _container.ResolveAll<IQuickAccessView>().ToList().OrderBy(view => view.Order);
            foreach (var accessView in quickAccessView)
            {
                _regionManager.AddToRegion(RegionNames.QuickAccessRegion, accessView);
            }

            UserName = ApiSession.GetUserName();
        }

        private void OnOpenModule(OpenModuleEventPayload obj)
        {
            ErpBackground = new Uri("/Kemprise.Client;component/Images/BG.jpg", UriKind.Relative);
            IsSmallLogoVisible = true;
        }

        private void OnExceptionCaught(ErrorEventPayload obj)
        {
            var parameters = new DialogParameters
            {
                Content = obj.Message,
                DialogStartupLocation = WindowStartupLocation.CenterOwner,
                Owner = Application.Current.MainWindow
            };

            RadWindow.Alert(parameters);
        }

        private void OnLogoutUserEvent()
        {
            foreach (var view in _regionManager.Regions[RegionNames.MainWorkAreaRegion].ActiveViews)
            {
                _regionManager.Regions[RegionNames.MainWorkAreaRegion].Remove(view);
            }

            foreach (var view in _regionManager.Regions[RegionNames.QuickAccessRegion].ActiveViews)
            {
                _regionManager.Regions[RegionNames.QuickAccessRegion].Remove(view);
            }

            _eventAggregator.GetEvent<RequestUserCredentialsEvent>().Publish();
        }

        private void GoHomeExecute(object obj)
        {
            var mainRegion = _regionManager.Regions[RegionNames.MainWorkAreaRegion];
            if (mainRegion.ActiveViews.Any())
            {
                ErpBackground = new Uri("/Kemprise.Client;component/Images/BGLogo.jpg", UriKind.Relative);
                IsSmallLogoVisible = false;

                _regionManager.Regions[RegionNames.MainWorkAreaRegion].ActiveViews.ForEach(mainRegion.Remove);
            }

            _eventAggregator.GetEvent<DeSelectModuleEvent<ManagementController>>().Publish();
            _eventAggregator.GetEvent<DeSelectModuleEvent<SalesController>>().Publish();
            _eventAggregator.GetEvent<DeSelectModuleEvent<ProcurementController>>().Publish();
            _eventAggregator.GetEvent<DeSelectModuleEvent<PLMController>>().Publish();
        }

        private void LogOutExecute()
        {
            _eventAggregator.GetEvent<LogoutUserEvent>().Publish();
        }

        public Uri ErpBackground
        {
            get { return GetValue(() => ErpBackground); }
            set { SetValue(() => ErpBackground, value); }
        }

        public bool IsSmallLogoVisible
        {
            get { return GetValue(() => IsSmallLogoVisible); }
            set { SetValue(() => IsSmallLogoVisible, value); }
        }

        public string UserName
        {
            get { return GetValue(() => UserName); }
            set { SetValue(() => UserName, value); }
        }
    }
}