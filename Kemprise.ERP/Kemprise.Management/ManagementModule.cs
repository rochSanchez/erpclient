﻿using ERP.WebApi.DTO;
using Kemprise.Infrastructure.Controllers;
using Kemprise.Infrastructure.Modules;
using Kemprise.Management.Controller;
using Kemprise.Management.WorkArea;
using Prism.Ioc;
using Prism.Unity;
using System.Reflection;
using Unity;
using Unity.Lifetime;

namespace Kemprise.Management
{
    public class ManagementModule : ErpModule
    {
        public ManagementModule(IUnityContainer container)
        {
            container.RegisterType(typeof(IControllerBase), typeof(ManagementController),
                nameof(ManagementController), new ContainerControlledLifetimeManager());
        }

        protected override Assembly GetBusinessAssembly()
        {
            return Assembly.GetAssembly(typeof(EntityListItem));
        }

        protected override Assembly GetModuleAssembly()
        {
            return Assembly.GetAssembly(typeof(ManagementModule));
        }

        public override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.GetContainer()
                .RegisterType<object, ManagementWorkAreaView>(typeof(ManagementWorkAreaView).FullName,
                    new ContainerControlledLifetimeManager());
        }
    }
}