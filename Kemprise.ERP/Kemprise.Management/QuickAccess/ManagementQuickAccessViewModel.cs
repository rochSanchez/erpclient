﻿using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Management.Controller;
using Prism.Commands;
using Prism.Events;
using Unity;

namespace Kemprise.Management.QuickAccess
{
    public class ManagementQuickAccessViewModel : NotifyPropertyChangedBase, IQuickAccess
    {
        private readonly IEventAggregator _eventAggregator;

        public ManagementQuickAccessViewModel(IEventAggregator eventAggregator, IUnityContainer container)
        {
            _eventAggregator = eventAggregator;
            eventAggregator.GetEvent<DeSelectModuleEvent<ManagementController>>().Subscribe(OnDeSelectModule);

            OpenModuleCommand = new DelegateCommand(OpenModuleExecute);

            IsVisible = true;
        }

        public DelegateCommand OpenModuleCommand { get; set; }

        private void OpenModuleExecute()
        {
            _eventAggregator.GetEvent<OpenModuleEvent>().Publish(new OpenModuleEventPayload(ErpModules.Management, this));
        }

        private void OnDeSelectModule()
        {
            IsSelected = false;
        }

        public bool IsSelected
        {
            get { return GetValue(() => IsSelected); }
            set { SetValue(() => IsSelected, value); }
        }

        public bool IsVisible
        {
            get { return GetValue(() => IsVisible); }
            set { SetValue(() => IsVisible, value); }
        }
    }
}