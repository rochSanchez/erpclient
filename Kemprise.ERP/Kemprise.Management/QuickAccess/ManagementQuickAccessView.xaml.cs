﻿using Kemprise.Infrastructure.Modules;

namespace Kemprise.Management.QuickAccess
{
    /// <summary>
    /// Interaction logic for ManagementQuickAccessView.xaml
    /// </summary>
    public partial class ManagementQuickAccessView : IQuickAccessView
    {
        public ManagementQuickAccessView()
        {
            InitializeComponent();
        }

        public int Order => 1;
    }
}