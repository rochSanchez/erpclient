﻿using Kemprise.Infrastructure.Controllers;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Events.Security;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Regions;
using Kemprise.Management.WorkArea;
using Prism.Events;
using Prism.Regions;
using Unity;
using Unity.Lifetime;

namespace Kemprise.Management.Controller
{
    public class ManagementController : ControllerBase
    {
        public ManagementController(IEventAggregator eventAggregator, IRegionManager regionManager, IUnityContainer container) :
            base(eventAggregator, regionManager, container)
        {
            eventAggregator.GetEvent<OpenModuleEvent>().Subscribe(OnOpenModule);
            eventAggregator.GetEvent<LogoutUserEvent>().Subscribe(OnLogoutUserEvent);
        }

        private void OnLogoutUserEvent()
        {
            Container.RegisterType<object, ManagementWorkAreaView>(typeof(ManagementWorkAreaView).FullName,
                new ContainerControlledLifetimeManager());
        }

        private void OnOpenModule(OpenModuleEventPayload obj)
        {
            if (obj.ErpModule == ErpModules.Management)
                obj.QuickAccess.IsSelected = true;
            else
            {
                EventAggregator.GetEvent<DeSelectModuleEvent<ManagementController>>().Publish();
                return;
            }

            RegionManager.RequestNavigate(RegionNames.MainWorkAreaRegion, typeof(ManagementWorkAreaView).FullName);
        }
    }
}