﻿using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.ViewModel;
using Unity;

namespace Kemprise.Management
{
    public class ManagementViewModelBase : ErpViewModelBase
    {
        public ManagementViewModelBase(IUnityContainer container, string category) :
            base(container, ErpModules.Management, category)
        {
            // Filters = new ObservableCollection<Filter>();
            // AddFilterCommand = new DelegateCommand(AddFilter, CanAddFilter);
            // FilterCommand = new DelegateCommand(SetFilter);
            // RemoveFilterCommand = new DelegateCommand(RemoveFilter, CanRemoveFilter);
            // ClearAllCommand = new DelegateCommand(ClearAllExecute);
        }

        // public ObservableCollection<Filter> Filters { get; set; }
        // public Dictionary<string, string> Fields { get; set; }
        // public Dictionary<string, Action<string, string>> Criteria { get; set; }
        // public Dictionary<string, string> FilterRegistrations { get; set; } = new Dictionary<string, string>();
        // public DelegateCommand AddFilterCommand { get; set; }
        // public DelegateCommand FilterCommand { get; set; }
        // public DelegateCommand ClearAllCommand { get; set; }
        // public DelegateCommand RemoveFilterCommand { get; set; }

        // public string FilterValue
        // {
        //     get { return GetValue(() => FilterValue); }
        //     set { SetValue(() => FilterValue, value, AddFilterCommand.RaiseCanExecuteChanged); }
        // }
        //
        // public bool IsFilterValueReadOnly
        // {
        //     get { return GetValue(() => IsFilterValueReadOnly); }
        //     set { SetValue(() => IsFilterValueReadOnly, value); }
        // }
        //
        // public Action<string, string> SelectedCriteria
        // {
        //     get { return GetValue(() => SelectedCriteria); }
        //     set { SetValue(() => SelectedCriteria, value, () => { AddFilterCommand.RaiseCanExecuteChanged(); }); }
        // }
        //
        // public object SelectedCriteriaName
        // {
        //     get { return GetValue(() => SelectedCriteriaName); }
        //     set
        //     {
        //         SetValue(() => SelectedCriteriaName, value, () =>
        //         {
        //             var x = SelectedCriteriaName != null
        //                 ? SelectedCriteriaName.ToString().Split(',')[0].Trim().Replace("[", "")
        //                 : "";
        //             if (x == "Is empty" || x == "Is not empty")
        //                 IsFilterValueReadOnly = true;
        //             else
        //                 IsFilterValueReadOnly = false;
        //         });
        //     }
        // }

        // public string SelectedField
        // {
        //     get { return GetValue(() => SelectedField); }
        //     set { SetValue(() => SelectedField, value, AddFilterCommand.RaiseCanExecuteChanged); }
        // }

        // public Filter SelectedFilter
        // {
        //     get { return GetValue(() => SelectedFilter); }
        //     set { SetValue(() => SelectedFilter, value, RemoveFilterCommand.RaiseCanExecuteChanged); }
        // }

        // private void DoesNotContainFilter<T>(string s, string s1) where T : EntityListItem
        // {
        //     CollectionView.View.Filter = o =>
        //     {
        //         if (o == null) return false;
        //         var item = (T)o;
        //         var property = item.GetType().GetProperty(s1);
        //         var value = property?.GetValue(o, null);
        //         if (value == null) return false;
        //         return !value.ToString().Contains(s);
        //     };
        // }

        // private void EndsWithFilter<T>(string s, string s1) where T : EntityListItem
        // {
        //     CollectionView.View.Filter = o =>
        //     {
        //         if (o == null) return false;
        //         var item = (T)o;
        //         var property = item.GetType().GetProperty(s1);
        //         var value = property?.GetValue(o, null);
        //         return value != null && value.ToString().EndsWith(s);
        //     };
        // }
        //
        // private void EqualFilter<T>(string s, string s1) where T : EntityListItem
        // {
        //     CollectionView.View.Filter = o =>
        //     {
        //         if (o == null) return false;
        //         var item = (T)o;
        //         var property = item.GetType().GetProperty(s1);
        //         var propertyValue = property?.GetValue(o, null)?.ToString();
        //         var result = propertyValue == s;
        //         return result;
        //     };
        // }
        //
        // private void GreaterThanFilter<T>(string s, string s1) where T : EntityListItem
        // {
        //     CollectionView.View.Filter = o =>
        //     {
        //         if (o == null) return false;
        //         var item = (T)o;
        //         var property = item.GetType().GetProperty(s1);
        //         var result = int.Parse(s);
        //         var result2 = int.Parse(property.GetValue(o, null)?.ToString());
        //         return result2 > result;
        //     };
        // }

        // private void IsEmptyFilter<T>(string s, string s1) where T : EntityListItem
        // {
        //     CollectionView.View.Filter = o =>
        //     {
        //         if (o == null) return false;
        //         var x = (T)o;
        //         var property = x.GetType().GetProperty(s1);
        //         var value = property?.GetValue(o, null);
        //         return value != null && string.IsNullOrEmpty(value.ToString());
        //     };
        // }
        //
        // private void IsNotEmptyFilter<T>(string s, string s1) where T : EntityListItem
        // {
        //     CollectionView.View.Filter = o =>
        //     {
        //         if (o == null) return false;
        //         var item = (T)o;
        //         var property = item.GetType().GetProperty(s1);
        //         return !string.IsNullOrEmpty(property?.GetValue(o, null)?.ToString());
        //     };
        // }
        //
        // private void LessThanFilter<T>(string s, string s1) where T : EntityListItem
        // {
        //     CollectionView.View.Filter = o =>
        //     {
        //         if (o == null) return false;
        //         var item = (T)o;
        //         var property = item.GetType().GetProperty(s1);
        //         var result = int.Parse(s);
        //         var result2 = int.Parse(property.GetValue(o, null)?.ToString());
        //         return result2 < result;
        //     };
        // }

        // private void LikeFilter<T>(string s, string s1) where T : EntityListItem
        // {
        //     CollectionView.View.Filter = o =>
        //     {
        //         if (o == null) return false;
        //         var item = (T)o;
        //         var property = item.GetType().GetProperty(s1);
        //         return property != null && property.GetValue(o, null).ToString().Contains(s);
        //     };
        // }
        //
        // private void RemovePane(object view)
        // {
        //     var paneToRemove = DocumentPanes.FirstOrDefault(x => x.Content.As<ScrollViewer>().Content == view);
        //     if (paneToRemove != null)
        //         DocumentPanes.Remove(paneToRemove);
        // }
        //
        // private void ClearAllExecute()
        // {
        // }
        //
        // private void StartsWithFilter<T>(string s, string s1) where T : EntityListItem
        // {
        //     CollectionView.View.Filter = o =>
        //     {
        //         if (o == null) return false;
        //         var x = (T)o;
        //         var property = x.GetType().GetProperty(s1);
        //         var value = property?.GetValue(o, null);
        //         return value != null && value.ToString().StartsWith(s);
        //     };
        // }

        // private void AddFilter()
        // {
        //     var x = SelectedCriteriaName.ToString().Split(',')[0].Trim().Replace("[", "");
        //     Filters.Add(new Filter
        //     {
        //         Action = SelectedCriteria,
        //         Text = SelectedField + " " + x + " " + FilterValue,
        //         Value = FilterValue,
        //         Field = SelectedField
        //     });
        //     Clear();
        // }
        //
        // private bool CanAddFilter()
        // {
        //     if (EntityListItems == null || !EntityListItems.Any()) return false;
        //     var x = SelectedCriteriaName != null
        //         ? SelectedCriteriaName.ToString().Split(',')[0].Trim().Replace("[", "")
        //         : "";
        //     bool result;
        //     if (x == "Is empty" || x == "Is not empty")
        //         result = SelectedCriteria == null || SelectedField == null;
        //     else
        //         result = string.IsNullOrEmpty(FilterValue) || SelectedCriteria == null || SelectedField == null;
        //     return !result;
        // }

        // private bool CanRemoveFilter()
        // {
        //     return SelectedFilter != null;
        // }
        //
        // private void Clear()
        // {
        //     FilterValue = null;
        //     SelectedCriteria = null;
        //     SelectedField = null;
        // }
        //
        // private void RemoveFilter()
        // {
        //     var item = Filters.First(x => x.Text == SelectedFilter.Text);
        //     Filters.Remove(item);
        //     CollectionView.View.Filter = o => true;
        //     ClearAllExecute();
        // }
        //
        // private void SetFilter()
        // {
        //     ClearAllExecute();
        //     if (SelectedFilter == null) return;
        //     var filterValue = SelectedFilter.Value;
        //     var fieldValue = SelectedFilter.Field;
        //     SelectedFilter.Action(filterValue, fieldValue);
        // }

        // public void RegisterAndFilter<T>(string propertyName, string value) where T : EntityListItem
        // {
        //     if (FilterRegistrations.ContainsKey(propertyName))
        //         FilterRegistrations[propertyName] = value;
        //     else
        //         FilterRegistrations.Add(propertyName, value);
        //
        //     Filter<T>();
        // }
        //
        // private void Filter<T>() where T : EntityListItem
        // {
        //     CollectionView.View.Filter = obj =>
        //     {
        //         if (obj == null) return false;
        //         var item = obj as T;
        //         var properties = item?.GetType().GetProperties();
        //         var filterCount = FilterRegistrations.Count(x => x.Value != null);
        //         var matches = Enumerable.ToList(
        //             (from valuePair in FilterRegistrations.Where(filter => filter.Value != null)
        //              let firstOrDefault = properties?.FirstOrDefault(x => x.Name == valuePair.Key)
        //              let isDate = firstOrDefault?.GetValue(item) is DateTime
        //              let value = isDate
        //                  ? ((DateTime)firstOrDefault?.GetValue(item)).ToShortDateString()
        //                  : firstOrDefault?.GetValue(item)?.ToString()
        //              where value == valuePair.Value
        //              select firstOrDefault));
        //
        //         return filterCount == matches.Count;
        //     };
        // }
    }

    // public class Filter
    // {
    //     public Action<string, string> Action { get; set; }
    //     public string Field { get; set; }
    //     public string Text { get; set; }
    //     public string Value { get; set; }
    // }
}