﻿using ERP.WebApi.DTO;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.WorkArea;
using Prism.Commands;
using Prism.Events;
using System.Collections.Generic;
using System.Linq;
using Unity;

namespace Kemprise.Management.WorkArea
{
    public class ManagementWorkAreaViewModel : ErpWorkAreaViewModelBase
    {
        public ManagementWorkAreaViewModel(IUnityContainer container, IEventAggregator eventAggregator) :
            base(container, eventAggregator)
        {
            ItemsListDictionary = new Dictionary<string, IEnumerable<EntityListItem>>();

            NavigationItems = container.ResolveAll<IParentNavigationItem>()
                .Where(item => item.Module == ErpModules.Management)
                .OrderBy(item => item.Name);

            eventAggregator.GetEvent<RefreshItemListEvent<ManagementWorkAreaViewModel>>().Subscribe(OnRefreshItemList);
            OpenSettingsCommand = new DelegateCommand(OpenSettingsExecute);
        }

        public DelegateCommand OpenSettingsCommand { get; set; }

        private void OpenSettingsExecute()
        {
            var settings = NavigationItems.FirstOrDefault(x => x.Name == "Settings");
            SelectedNavItem = settings;
        }
    }
}