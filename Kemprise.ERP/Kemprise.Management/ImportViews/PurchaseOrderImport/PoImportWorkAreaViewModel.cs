﻿using Unity;

namespace Kemprise.Management.ImportViews.PurchaseOrderImport
{
    public class PoImportWorkAreaViewModel : ManagementViewModelBase
    {
        public PoImportWorkAreaViewModel(IUnityContainer container) :
            base(container, "PoImport")
        {
        }
    }
}