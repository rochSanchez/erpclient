﻿using Kemprise.Infrastructure.Tools.ExcelReader;

namespace Kemprise.Management.ImportViews.PartImport
{
    public class PartImportData
    {
        [MappingNames("Column0")] public string PartNumber { get; set; }

        [MappingNames("Column1")] public string Name { get; set; }

        [MappingNames("Column2")] public string Source { get; set; }

        [MappingNames("Column3")] public string AssemblyMode { get; set; }

        [MappingNames("Column4")] public string CanBeSold { get; set; }

        [MappingNames("Column5")] public string CanBePurchased { get; set; }

        [MappingNames("Column6")] public string SalesPrice { get; set; }

        [MappingNames("Column7")] public string Cost { get; set; }

        [MappingNames("Column8")] public string Currency { get; set; }

        [MappingNames("Column9")] public string UnitOfMeasurement { get; set; }

        [MappingNames("Column10")] public string Context { get; set; }
    }
}