﻿using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace Kemprise.Management.ImportViews.PartImport.Methods
{
    public class PartImportAddRec : ErpSaveMethodCallBase
    {
        public PartImportAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "PartImportAddRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override ApiControllers Controller => ApiControllers.Import;
        public override string Category => "PartImport";
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var partImportEntity = entity.As<PartImportEntity>();
            partImportEntity.PartImportList.ForEach(x => x.Name = x.Name.Trim());

            var textInfo = new CultureInfo("en-Us", false).TextInfo;
            foreach (var item in partImportEntity.PartImportList)
            {
                item.UnitOfMeasurement = textInfo.ToTitleCase(item.UnitOfMeasurement);
                item.Class = textInfo.ToTitleCase(item.Class);
            }

            var dataToSave = new Dictionary<string, object>
            {
                { "PartImportList", partImportEntity.PartImportList }
            };

            await SaveNewRecordAsync<InquiryAggregate>(MethodName, dataToSave);
        }
    }
}