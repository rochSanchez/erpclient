﻿using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.MethodCalls;
using System.Collections.ObjectModel;

namespace Kemprise.Management.ImportViews.PartImport
{
    public class PartImportEntity : EntityBase
    {
        public PartImportEntity()
        {
            PartImportList = new ObservableCollection<PartDto>();
        }

        public ObservableCollection<PartDto> PartImportList { get; set; }
    }
}