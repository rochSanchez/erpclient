﻿using ERP.WebApi.DTO.DTOs.PLM.Part;
using Kemprise.Infrastructure.Events.Errors;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Tools.ExcelReader;
using Kemprise.Management.ImportViews.PartImport.PartImportViews;
using Microsoft.Win32;
using Prism.Commands;
using System;
using Unity;

namespace Kemprise.Management.ImportViews.PartImport
{
    public sealed class PartImportWorkAreaViewModel : ManagementViewModelBase
    {
        private readonly IExcelReaderService _excelReader;
        private readonly PartImportEntity _entity;

        public PartImportWorkAreaViewModel(IUnityContainer container, IExcelReaderService excelReader) :
            base(container, "PartImport")
        {
            _excelReader = excelReader;

            Entity = new DynamicNotifyPropertyChangedProxy(new PartImportEntity());
            _entity = Entity.GetDto<PartImportEntity>();

            SetView(PartImportViewNames.PartImportMainView);

            ImportCommand = new DelegateCommand(ImportExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshExecute);
            SaveCommand = new DelegateCommand(SaveExecute);
        }

        public DelegateCommand ImportCommand { get; set; }

        private void ImportExecute()
        {
            var dialog = new OpenFileDialog { Filter = "Excel Files|*.xls;*.xlsx" };
            dialog.ShowDialog();
            IsBusy = true;
            try
            {
                var result = _excelReader.Import<PartImportData>(dialog.FileName);
                result.ForEach(item =>
                {
                    _entity.PartImportList.Add(new PartDto
                    {
                        PartNumber = item.PartNumber,
                        Name = item.Name,
                        Source = item.Source,
                        AssemblyMode = item.AssemblyMode,
                        CanBeSold = item.CanBeSold == "True",
                        CanBePurchased = item.CanBePurchased == "True",
                        SalesPrice = Convert.ToDecimal(item.SalesPrice),
                        Cost = Convert.ToDecimal(item.Cost),
                        Currency = item.Currency,
                        UnitOfMeasurement = item.UnitOfMeasurement,
                        Class = item.Context
                    });
                });
            }
            catch (Exception e)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload(e.Message));
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void RefreshExecute(object obj)
        {
            _entity.PartImportList.Clear();
        }

        private async void SaveExecute()
        {
            await SaveRecordAsync("PartImportAddRec");
        }
    }
}