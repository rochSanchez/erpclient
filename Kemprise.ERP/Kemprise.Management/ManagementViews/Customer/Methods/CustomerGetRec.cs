﻿using ERP.WebApi.DTO.DTOs.Management.Customer;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Threading.Tasks;

namespace Kemprise.Management.ManagementViews.Customer.Methods
{
    public class CustomerGetRec : GetMethodCallBase
    {
        public CustomerGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "CustomerGetRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override string Category => ManagementTypes.Customers.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Customer;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var customerDto = await GetRecordAsync<CustomerDto>(MethodName, new object[] { MethodParameter });
            if (customerDto == null) return;

            entity.SetProperty("CustomerDto", new DynamicNotifyPropertyChangedProxy(customerDto));
        }
    }
}