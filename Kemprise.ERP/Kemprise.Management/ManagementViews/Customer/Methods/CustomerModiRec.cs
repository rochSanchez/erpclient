﻿using ERP.WebApi.DTO.DTOs.Management.Customer;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Management.ManagementViews.Customer.Methods
{
    public class CustomerModiRec : ErpSaveMethodCallBase
    {
        public CustomerModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "CustomerModiRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override ApiControllers Controller => ApiControllers.Customer;
        public override string Category => ManagementTypes.Customers.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var customerDto = entity.As<CustomerEntity>().CustomerDto.GetDto<CustomerDto>();

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(CustomerDto), customerDto }
            };

            await SaveNewRecordAsync<CustomerDto>(MethodName, dataToSave);
        }
    }
}