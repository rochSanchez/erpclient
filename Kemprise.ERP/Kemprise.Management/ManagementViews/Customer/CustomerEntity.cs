﻿using ERP.WebApi.DTO.DTOs.Management.Customer;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.Management.ManagementViews.Customer
{
    public class CustomerEntity : EntityBase
    {
        public CustomerEntity()
        {
            CustomerDto = new DynamicNotifyPropertyChangedProxy(new CustomerDto { CustomerType = "Individual" });
        }

        public DynamicNotifyPropertyChangedProxy CustomerDto { get; set; }
    }
}