﻿using ERP.WebApi.DTO.DTOs.Management.Customer;
using Kemprise.Infrastructure.Converters;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Management.ManagementViews.Customer.CustomerViews;
using Kemprise.Management.WorkArea;
using Prism.Commands;
using System.Windows;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Management.ManagementViews.Customer
{
    public sealed class CustomerWorkAreaViewModel : ManagementViewModelBase
    {
        private CustomerEntity _entity;

        public CustomerWorkAreaViewModel(IUnityContainer container) :
            base(container, ManagementTypes.Customers.ToString())
        {
            SetView(CustomerViewNames.CustomerMainView);

            ClearCommand = new DelegateCommand(ClearExecute);
            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            EditCommand = new DelegateCommand(EditExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSaveExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshCustomersExecute);
            RadioButtonIsCheckedCommand = new DelegateCommand<object>(RadioButtonIsCheckedExecute);

            Methods["CustomerGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        private void WhenGetRecDone()
        {
            var customerDto = _entity.CustomerDto.GetDto<CustomerDto>();
            _entity.CustomerDto.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "CustomerType")
                {
                    if (customerDto.CustomerType == "Company")
                    {
                        _entity.CustomerDto.SetProperty("Designation", "");
                        _entity.CustomerDto.SetProperty("MobileNumber", "");
                        _entity.CustomerDto.SetProperty("TaxId", "");
                    }
                    if (customerDto.CustomerType == "Individual")
                    {
                        _entity.CustomerDto.SetProperty("ContactPerson", "");
                        _entity.CustomerDto.SetProperty("OfficeNumber", "");
                        _entity.CustomerDto.SetProperty("Website", "");
                    }
                }

                IsIndividual = customerDto.CustomerType == "Individual";
                SaveCommand.RaiseCanExecuteChanged();
            };

            IsIndividual = customerDto.CustomerType == "Individual";
            SaveCommand.RaiseCanExecuteChanged();
        }

        private void ClearExecute()
        {
            RadWindow.Confirm("This will clear everything. Continue?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.CustomerDto.ClearValues();
                _entity.CustomerDto.SetProperty("CustomerType", "Individual");
            });
        }

        private async void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new CustomerEntity());

            _entity = Entity.GetDto<CustomerEntity>();
            _entity.IsNew = true;

            await OpenItemExecute("");

            IsClearVisible = true;
            SetView(CustomerViewNames.CustomerEditView);
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new CustomerEntity());
            _entity = Entity.GetDto<CustomerEntity>();

            await OpenItemExecute(SelectedItem.Id);

            SetView(CustomerViewNames.CustomerDisplayView);
        }

        private async void EditExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select a Customer",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            Entity = new DynamicNotifyPropertyChangedProxy(new CustomerEntity());
            _entity = Entity.GetDto<CustomerEntity>();

            await OpenItemExecute(SelectedItem.Id);

            IsClearVisible = false;
            SetView(CustomerViewNames.CustomerEditView);
        }

        private void BackExecute()
        {
            SetView(CustomerViewNames.CustomerMainView);
            RefreshExecute();
        }

        private async void SaveExecute()
        {
            var taskToPerform = _entity.IsNew ? "CustomerAddRec" : "CustomerModiRec";
            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSaveExecute()
        {
            var customerDto = _entity.CustomerDto.GetDto<CustomerDto>();

            if (string.IsNullOrEmpty(customerDto.CustomerName) ||
                string.IsNullOrEmpty(customerDto.CustomerAddress) ||
                string.IsNullOrEmpty(customerDto.Country))
                return false;

            if (customerDto.CustomerType == "Company")
                return !string.IsNullOrEmpty(customerDto.ContactPerson) &&
                       !string.IsNullOrEmpty(customerDto.OfficeNumber);

            return true;
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<ManagementWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Customers"));
        }

        private void RefreshCustomersExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        private void RadioButtonIsCheckedExecute(object obj)
        {
            if (!(obj is ObjectParameter param)) return;
            _entity.CustomerDto.SetProperty(param.PropertyName, param.NewValue);
        }

        public bool IsIndividual
        {
            get { return GetValue(() => IsIndividual); }
            set
            {
                SetValue(() => IsIndividual, value);
                IsCompany = !value;
            }
        }

        public bool IsCompany
        {
            get { return GetValue(() => IsCompany); }
            set { SetValue(() => IsCompany, value); }
        }
    }
}