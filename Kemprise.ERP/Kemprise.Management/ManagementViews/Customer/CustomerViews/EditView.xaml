﻿<UserControl x:Class="Kemprise.Management.ManagementViews.Customer.CustomerViews.EditView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:controls="clr-namespace:Kemprise.Infrastructure.Controls;assembly=Kemprise.Infrastructure"
             xmlns:converters="clr-namespace:Kemprise.Infrastructure.Converters;assembly=Kemprise.Infrastructure"
             xmlns:telerik="http://schemas.telerik.com/2008/xaml/presentation">

    <UserControl.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary Source="/Kemprise.Infrastructure;component/Controls/GeneralStyles.xaml" />
            </ResourceDictionary.MergedDictionaries>

            <converters:BoolToVisibilityConverter x:Key="BoolToVisibilityConverter" />
            <converters:RadioButtonIsCheckedConverter x:Key="RadioButtonConverter" />
            <converters:ParameterConverter x:Key="ParameterConverter" />

            <Style BasedOn="{StaticResource GenTextBlock}" TargetType="{x:Type TextBlock}">
                <Setter Property="Width" Value="150" />
            </Style>
            <Style BasedOn="{StaticResource GenRadMaskedTextInput}" TargetType="{x:Type telerik:RadMaskedTextInput}">
                <Setter Property="MaxWidth" Value="300" />
                <Setter Property="MinWidth" Value="300" />
            </Style>
            <Style x:Key="WrapTextBox"
                   BasedOn="{StaticResource GenWrapTextBox}"
                   TargetType="{x:Type TextBox}">
                <Setter Property="MinHeight" Value="100" />
                <Setter Property="MaxHeight" Value="100" />
                <Setter Property="MaxWidth" Value="300" />
                <Setter Property="MinWidth" Value="300" />
            </Style>
        </ResourceDictionary>
    </UserControl.Resources>

    <Grid>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto" />
            <RowDefinition Height="*" />
        </Grid.RowDefinitions>

        <StackPanel Margin="10,0,0,0">
            <StackPanel Orientation="Horizontal">
                <controls:ImageMenuButton ButtonForeground="{StaticResource NameTxtBlockColor}"
                                          Command="{Binding BackCommand}"
                                          Content="Back"
                                          ImageSource="/Kemprise.Client;component/Images/BackIcon.png" />
            </StackPanel>
            <Separator />
        </StackPanel>

        <Grid Grid.Row="1"
              Margin="10,5,10,10"
              Background="White">

            <Grid.Effect>
                <DropShadowEffect Opacity="0.2"
                                  RenderingBias="Quality"
                                  ShadowDepth="4"
                                  Color="Black" />
            </Grid.Effect>

            <Grid Margin="20">
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="Auto" />
                </Grid.ColumnDefinitions>

                <StackPanel>
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="Auto" />
                        </Grid.ColumnDefinitions>

                        <StackPanel Orientation="Horizontal">
                            <Image Width="35" Source="/Kemprise.Client;component/Images/CustomerIcon.png" />
                            <Separator Margin="15,5,15,5" Style="{StaticResource {x:Static ToolBar.SeparatorStyleKey}}" />
                            <TextBlock Style="{StaticResource CategoryTxtBlockStyle}" Text="Customer Information" />
                        </StackPanel>
                    </Grid>
                    <Separator Margin="5,10,5,10" Background="{StaticResource NameTxtBlockColor}" />

                    <StackPanel Margin="0,0,0,20" Orientation="Horizontal">
                        <TextBlock Text="Customer Type" />
                        <RadioButton Margin="0,0,20,0"
                                     Command="{Binding RadioButtonIsCheckedCommand}"
                                     Content="Individual"
                                     GroupName="CustomerType"
                                     IsChecked="{Binding Entity.CustomerDto.CustomerType, Converter={StaticResource RadioButtonConverter}, ConverterParameter='Individual'}">
                            <RadioButton.CommandParameter>
                                <MultiBinding Converter="{StaticResource ParameterConverter}" ConverterParameter="Individual">
                                    <MultiBinding.Bindings>
                                        <Binding Path="GroupName" RelativeSource="{RelativeSource Self}" />
                                    </MultiBinding.Bindings>
                                </MultiBinding>
                            </RadioButton.CommandParameter>
                        </RadioButton>
                        <RadioButton Command="{Binding RadioButtonIsCheckedCommand}"
                                     Content="Company"
                                     GroupName="CustomerType"
                                     IsChecked="{Binding Entity.CustomerDto.CustomerType, Converter={StaticResource RadioButtonConverter}, ConverterParameter='Company'}">
                            <RadioButton.CommandParameter>
                                <MultiBinding Converter="{StaticResource ParameterConverter}" ConverterParameter="Company">
                                    <MultiBinding.Bindings>
                                        <Binding Path="GroupName" RelativeSource="{RelativeSource Self}" />
                                    </MultiBinding.Bindings>
                                </MultiBinding>
                            </RadioButton.CommandParameter>
                        </RadioButton>
                    </StackPanel>

                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width=" Auto" />
                            <ColumnDefinition Width=" Auto" />
                        </Grid.ColumnDefinitions>

                        <StackPanel Margin="0,0,20,0">
                            <StackPanel Orientation="Horizontal">
                                <TextBlock Text="Customer Number" />
                                <telerik:RadMaskedTextInput EmptyContent="Auto Generated"
                                                            IsReadOnly="True"
                                                            Value="{Binding Entity.CustomerDto.CustomerNumber}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock Text="Customer Name" />
                                <telerik:RadMaskedTextInput Value="{Binding Entity.CustomerDto.CustomerName}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock VerticalAlignment="Top" Text="Customer Address" />
                                <TextBox Style="{StaticResource WrapTextBox}" Text="{Binding Entity.CustomerDto.CustomerAddress, UpdateSourceTrigger=PropertyChanged}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock Text="Country" />
                                <telerik:RadComboBox MinWidth="300"
                                                     MaxWidth="300"
                                                     Margin="5,2,5,2"
                                                     telerik:TextSearch.TextPath="Value"
                                                     Cursor="Hand"
                                                     DisplayMemberPath="Value"
                                                     EmptyText="Select"
                                                     ItemsSource="{Binding Countries}"
                                                     SelectedValue="{Binding Entity.CustomerDto.Country, Mode=TwoWay}"
                                                     SelectedValuePath="Value" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock Text="Email Address" />
                                <telerik:RadMaskedTextInput Value="{Binding Entity.CustomerDto.CustomerEmail}" />
                            </StackPanel>
                        </StackPanel>

                        <StackPanel Grid.Column="1">
                            <StackPanel Orientation="Horizontal">
                                <TextBlock Text="Designation" Visibility="{Binding IsIndividual, Converter={StaticResource BoolToVisibilityConverter}}" />
                                <telerik:RadMaskedTextInput Visibility="{Binding IsIndividual, Converter={StaticResource BoolToVisibilityConverter}}" Value="{Binding Entity.CustomerDto.Designation}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock Text="Mobile Number" Visibility="{Binding IsIndividual, Converter={StaticResource BoolToVisibilityConverter}}" />
                                <telerik:RadMaskedTextInput Visibility="{Binding IsIndividual, Converter={StaticResource BoolToVisibilityConverter}}" Value="{Binding Entity.CustomerDto.MobileNumber}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock Text="Tax ID" Visibility="{Binding IsIndividual, Converter={StaticResource BoolToVisibilityConverter}}" />
                                <telerik:RadMaskedTextInput Visibility="{Binding IsIndividual, Converter={StaticResource BoolToVisibilityConverter}}" Value="{Binding Entity.CustomerDto.TaxId}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock Text="Contact Person" Visibility="{Binding IsCompany, Converter={StaticResource BoolToVisibilityConverter}}" />
                                <telerik:RadMaskedTextInput Visibility="{Binding IsCompany, Converter={StaticResource BoolToVisibilityConverter}}" Value="{Binding Entity.CustomerDto.ContactPerson}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock Text="Office Number" Visibility="{Binding IsCompany, Converter={StaticResource BoolToVisibilityConverter}}" />
                                <telerik:RadMaskedTextInput Visibility="{Binding IsCompany, Converter={StaticResource BoolToVisibilityConverter}}" Value="{Binding Entity.CustomerDto.OfficeNumber}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock Text="Website" Visibility="{Binding IsCompany, Converter={StaticResource BoolToVisibilityConverter}}" />
                                <telerik:RadMaskedTextInput Visibility="{Binding IsCompany, Converter={StaticResource BoolToVisibilityConverter}}" Value="{Binding Entity.CustomerDto.Website}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock VerticalAlignment="Top" Text="Comments" />
                                <TextBox Style="{StaticResource WrapTextBox}" Text="{Binding Entity.CustomerDto.Comments}" />
                            </StackPanel>
                        </StackPanel>
                    </Grid>

                    <StackPanel Margin="0,15,0,0"
                                HorizontalAlignment="Right"
                                Orientation="Horizontal">
                        <controls:ErpButton Margin="0,0,5,0"
                                            HorizontalAlignment="Right"
                                            Command="{Binding ClearCommand}"
                                            Content="Clear"
                                            Visibility="{Binding IsClearVisible, Converter={StaticResource BoolToVisibilityConverter}}" />
                        <controls:ErpButton Margin="0,0,5,0"
                                            HorizontalAlignment="Right"
                                            Command="{Binding SaveCommand}"
                                            Content="Submit" />
                    </StackPanel>
                </StackPanel>
            </Grid>
        </Grid>
    </Grid>
</UserControl>