﻿using ERP.WebApi.DTO.DTOs.Management.Supplier;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.Management.ManagementViews.Supplier
{
    public class SupplierEntity : EntityBase
    {
        public SupplierEntity()
        {
            SupplierDto = new DynamicNotifyPropertyChangedProxy(new SupplierDto { SupplierType = "Individual" });
        }

        public DynamicNotifyPropertyChangedProxy SupplierDto { get; set; }
    }
}