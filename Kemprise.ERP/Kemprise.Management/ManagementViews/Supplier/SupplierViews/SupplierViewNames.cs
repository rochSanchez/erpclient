﻿namespace Kemprise.Management.ManagementViews.Supplier.SupplierViews
{
    public class SupplierViewNames
    {
        public const string SupplierMainView = "Supplier Main View";
        public const string SupplierEditView = "Supplier Edit View";
        public const string SupplierDisplayView = "Supplier Display View";
    }
}