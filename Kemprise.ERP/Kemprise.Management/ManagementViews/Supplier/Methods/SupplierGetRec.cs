﻿using ERP.WebApi.DTO.DTOs.Management.Supplier;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Threading.Tasks;

namespace Kemprise.Management.ManagementViews.Supplier.Methods
{
    public class SupplierGetRec : GetMethodCallBase
    {
        public SupplierGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "SupplierGetRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override string Category => ManagementTypes.Suppliers.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Supplier;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var supplierDto = await GetRecordAsync<SupplierDto>(MethodName, new object[] { MethodParameter });
            if (supplierDto == null) return;

            entity.SetProperty("SupplierDto", new DynamicNotifyPropertyChangedProxy(supplierDto));
        }
    }
}