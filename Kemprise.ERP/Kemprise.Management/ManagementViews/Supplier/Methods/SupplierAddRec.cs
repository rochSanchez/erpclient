﻿using ERP.WebApi.DTO.DTOs.Management.Supplier;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Management.ManagementViews.Supplier.Methods
{
    public class SupplierAddRec : ErpSaveMethodCallBase
    {
        public SupplierAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "SupplierAddRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override ApiControllers Controller => ApiControllers.Supplier;
        public override string Category => ManagementTypes.Suppliers.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var supplierDto = entity.As<SupplierEntity>().SupplierDto.GetDto<SupplierDto>();

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(SupplierDto), supplierDto }
            };

            await SaveNewRecordAsync<SupplierDto>(MethodName, dataToSave);
        }
    }
}