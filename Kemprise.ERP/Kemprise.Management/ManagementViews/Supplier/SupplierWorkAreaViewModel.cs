﻿using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Management.Supplier;
using Kemprise.Infrastructure.Converters;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Management.ManagementViews.Supplier.SupplierViews;
using Kemprise.Management.WorkArea;
using Prism.Commands;
using System.Collections.Generic;
using System.Windows;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Management.ManagementViews.Supplier
{
    public sealed class SupplierWorkAreaViewModel : ManagementViewModelBase
    {
        private SupplierEntity _entity;

        public SupplierWorkAreaViewModel(IUnityContainer container) :
            base(container, ManagementTypes.Suppliers.ToString())
        {
            SetView(SupplierViewNames.SupplierMainView);

            ClearCommand = new DelegateCommand(ClearExecute);
            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            EditCommand = new DelegateCommand(EditExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSave);
            RefreshCommand = new DelegateCommand<object>(RefreshSuppliersExecute);
            RadioButtonIsCheckedCommand = new DelegateCommand<object>(RadioButtonIsCheckedExecute);

            ApproveSupplierCommand = new DelegateCommand(ApproveSupplierExecute);

            SelectedFilter = new KeyValuePair<string, string>("ALL", "ALL");
            Methods["SupplierGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        public DelegateCommand ApproveSupplierCommand { get; set; }

        public Dictionary<string, string> SupplierFilters { get; set; } = new Dictionary<string, string>
        {
            {"ALL","ALL" },
            {SupplierStatus.PendingReview, SupplierStatus.PendingReview },
            {SupplierStatus.Approved, SupplierStatus.Approved }
        };

        private void WhenGetRecDone()
        {
            var supplierDto = _entity.SupplierDto.GetDto<SupplierDto>();
            IsApproveButtonVisible = ApiSession.HasRoles(ErpRoles.ApprovePurchaseOrder)
                                     && supplierDto.Status == SupplierStatus.PendingReview;

            _entity.SupplierDto.PropertyChanged += (s, e) => { SaveCommand.RaiseCanExecuteChanged(); };
            SaveCommand.RaiseCanExecuteChanged();
        }

        private void ClearExecute()
        {
            RadWindow.Confirm("This will clear everything. Continue?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.SupplierDto.ClearValues();
                _entity.SupplierDto.SetProperty("SupplierType", "Individual");
            });
        }

        private async void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new SupplierEntity());

            _entity = Entity.GetDto<SupplierEntity>();
            _entity.IsNew = true;

            await OpenItemExecute("");

            IsSaveButtonVisible = true;
            SetView(SupplierViewNames.SupplierEditView);
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new SupplierEntity());
            _entity = Entity.GetDto<SupplierEntity>();

            await OpenItemExecute(SelectedItem.Id);

            var supplierDto = _entity.SupplierDto.GetDto<SupplierDto>();
            var viewToShow = SupplierViewNames.SupplierDisplayView;

            if (ApiSession.HasRoles(ErpRoles.ApprovePurchaseOrder)
                && supplierDto.Status == SupplierStatus.PendingReview)
            {
                IsSaveButtonVisible = false;
                viewToShow = SupplierViewNames.SupplierEditView;
            }

            SetView(viewToShow);
        }

        private async void EditExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select a Supplier",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            Entity = new DynamicNotifyPropertyChangedProxy(new SupplierEntity());
            _entity = Entity.GetDto<SupplierEntity>();

            await OpenItemExecute(SelectedItem.Id);

            var supplierDto = _entity.SupplierDto.GetDto<SupplierDto>();
            var viewToShow = SupplierViewNames.SupplierEditView;

            if (ApiSession.HasRoles(ErpRoles.ApprovePurchaseOrder)
                && supplierDto.Status == SupplierStatus.PendingReview)
                IsSaveButtonVisible = false;
            else
            {
                IsSaveButtonVisible = true;
                IsApproveButtonVisible = false;
            }

            SetView(viewToShow);
        }

        private void BackExecute()
        {
            RefreshExecute();
            SetView(SupplierViewNames.SupplierMainView);
        }

        private async void SaveExecute()
        {
            _entity.SupplierDto.SetProperty("Status",
                ApiSession.HasRoles(ErpRoles.ApprovePurchaseOrder)
                    ? SupplierStatus.Approved
                    : SupplierStatus.PendingReview);

            var taskToPerform = _entity.IsNew ? "SupplierAddRec" : "SupplierModiRec";
            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSave()
        {
            var supplierDto = _entity.SupplierDto.GetDto<SupplierDto>();
            return !string.IsNullOrEmpty(supplierDto.SupplierName)
                   && !string.IsNullOrEmpty(supplierDto.SupplierAddress)
                   && !string.IsNullOrEmpty(supplierDto.Country)
                   && !string.IsNullOrEmpty(supplierDto.ContactPerson)
                   && !string.IsNullOrEmpty(supplierDto.OfficeNumber);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<ManagementWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Suppliers"));
        }

        private void RefreshSuppliersExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        private void RadioButtonIsCheckedExecute(object obj)
        {
            if (!(obj is ObjectParameter param)) return;
            _entity.SupplierDto.SetProperty(param.PropertyName, param.NewValue);
        }

        private async void ApproveSupplierExecute()
        {
            _entity.SupplierDto.SetProperty("Status", SupplierStatus.Approved);

            var saveResult = await SaveRecordAsync("SupplierModiRec");
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        public bool IsApproveButtonVisible
        {
            get { return GetValue(() => IsApproveButtonVisible); }
            set { SetValue(() => IsApproveButtonVisible, value); }
        }

        public bool IsSaveButtonVisible
        {
            get { return GetValue(() => IsSaveButtonVisible); }
            set { SetValue(() => IsSaveButtonVisible, value); }
        }
    }
}