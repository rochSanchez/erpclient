﻿using Kemprise.Infrastructure.Events.Security;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Windows;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Management.ManagementViews.Settings
{
    public class SettingsWorkAreaViewModel : ManagementViewModelBase
    {
        private ResetPasswordWindow _resetPasswordWindow;
        private SettingsEntity _entity;

        public SettingsWorkAreaViewModel(IUnityContainer container) :
            base(container, ManagementTypes.Settings.ToString())
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new SettingsEntity());
            _entity = Entity.GetDto<SettingsEntity>();
            UserName = ApiSession.GetUserName();
            Email = ApiSession.GetEmail();
            UserPhoto = ApiSession.GetUserPhoto();
            ResetPasswordCommand = new DelegateCommand(ResetPasswordExecute);
            SaveNewPasswordCommand = new DelegateCommand(SaveExecute, SaveCanExecute);
            CancelResetCommand = new DelegateCommand(CancelResetExecute);
        }

        private void CancelResetExecute()
        {
            NewPassword = string.Empty;
            ConfirmPassword = string.Empty;
            _resetPasswordWindow.Close();
        }

        private bool SaveCanExecute()
        {
            if (string.IsNullOrEmpty(_entity.NewPassword)) return false;
            if (string.IsNullOrEmpty(_entity.ConfirmPassword)) return false;
            if (_entity.NewPassword != _entity.ConfirmPassword) return false;
            return true;
        }

        private async void SaveExecute()
        {
            var result = await SaveRecordAsync("PasswordReset");
            if (result.OperationResult == OperationResult.Success)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Password reset successful.",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });
                _resetPasswordWindow.Close();
                EventAggregator.GetEvent<LogoutUserEvent>().Publish();
            }
        }

        public DelegateCommand CancelResetCommand { get; set; }
        public DelegateCommand SaveNewPasswordCommand { get; set; }
        public DelegateCommand ResetPasswordCommand { get; set; }

        private void ResetPasswordExecute()
        {
            _resetPasswordWindow = new ResetPasswordWindow
            {
                DataContext = this,
                Owner = Application.Current.MainWindow
            };
            _resetPasswordWindow.ShowDialog();
        }

        public string UserName
        {
            get { return GetValue(() => UserName); }
            set { SetValue(() => UserName, value); }
        }

        public byte[] UserPhoto
        {
            get { return GetValue(() => UserPhoto); }
            set { SetValue(() => UserPhoto, value); }
        }

        public string Email
        {
            get { return GetValue(() => Email); }
            set { SetValue(() => Email, value); }
        }

        public string ConfirmPassword
        {
            get { return GetValue(() => ConfirmPassword); }
            set { SetValue(() => ConfirmPassword, value, () => { _entity.ConfirmPassword = value; }); }
        }

        public string NewPassword
        {
            get { return GetValue(() => NewPassword); }
            set { SetValue(() => NewPassword, value, () => { _entity.NewPassword = value; }); }
        }
    }
}