﻿using ERP.WebApi.DTO.Security.Authentication;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Threading.Tasks;

namespace Kemprise.Management.ManagementViews.Settings.Methods
{
    public class PasswordReset : ErpSaveMethodCallBase
    {
        public PasswordReset(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "PasswordReset";
        public override ErpModules ModuleName => ErpModules.Management;
        public override ApiControllers Controller => ApiControllers.Account;
        public override string Category => ManagementTypes.Settings.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var settingsEntity = entity.As<SettingsEntity>();
            if (settingsEntity == null) return;

            var dataToSave = new PasswordResetRequest
            { NewPassword = settingsEntity.NewPassword, UserName = ApiSession.GetUserName() };

            await UpdateRecordAsync(MethodName, dataToSave);
        }
    }
}