﻿using Kemprise.Infrastructure.MethodCalls;

namespace Kemprise.Management.ManagementViews.Settings
{
    public class SettingsEntity : EntityBase
    {
        public SettingsEntity()
        {
        }

        public string Username { get; set; }
        public string ConfirmPassword { get; set; }
        public string NewPassword { get; set; }
    }
}