﻿using System.Windows;
using Kemprise.Infrastructure.Extensions;

namespace Kemprise.Management.ManagementViews.Settings
{
    /// <summary>
    /// Interaction logic for AddRateWindow.xaml
    /// </summary>
    public partial class ResetPasswordWindow
    {
        public ResetPasswordWindow()
        {
            InitializeComponent();
        }

        private void NewPasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            var vm = DataContext.As<SettingsWorkAreaViewModel>();
            vm.Entity.GetDto<SettingsEntity>().NewPassword = NewPasswordBox.Password;
            vm.SaveNewPasswordCommand.RaiseCanExecuteChanged();
        }

        private void ConfirmPasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            var vm = DataContext.As<SettingsWorkAreaViewModel>();
            vm.Entity.GetDto<SettingsEntity>().ConfirmPassword = ConfirmPasswordBox.Password;
            vm.SaveNewPasswordCommand.RaiseCanExecuteChanged();
        }
    }
}