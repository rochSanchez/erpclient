﻿namespace Kemprise.Management.ManagementViews
{
    public enum ManagementTypes
    {
        Suppliers,
        Customers,
        Employees,
        Currencies,
        Companies,
        Users,
        Settings,
        PartAttribute
    }
}