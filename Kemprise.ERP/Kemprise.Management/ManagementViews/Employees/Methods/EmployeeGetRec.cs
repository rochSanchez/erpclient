﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Management.ManagementViews.Employees.Methods
{
    public class EmployeeGetRec : GetMethodCallBase
    {
        public EmployeeGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "EmployeeGetRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override string Category => ManagementTypes.Employees.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Employee;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var employeeAggregate = await GetRecordAsync<EmployeeAggregate>(MethodName, new object[] { MethodParameter });
            if (employeeAggregate == null) return;

            if (employeeAggregate.EmployeeDto != null)
                entity.SetProperty("EmployeeDto", new DynamicNotifyPropertyChangedProxy(employeeAggregate.EmployeeDto));

            if (employeeAggregate.EmployeeFiles.Any())
            {
                var employeeFiles = new ObservableCollection<FileDto>(employeeAggregate.EmployeeFiles);
                entity.SetProperty("EmployeeFiles", employeeFiles);
            }
        }
    }
}