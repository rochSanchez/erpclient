﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.Management.ManagementViews.Employees.EmployeeViews;
using Kemprise.Management.WorkArea;
using Prism.Commands;
using System.Windows;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Management.ManagementViews.Employees
{
    public sealed class EmployeeWorkAreaViewModel : ManagementViewModelBase
    {
        private readonly IFileService _fileService;
        private EmployeeEntity _entity;

        public EmployeeWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, ManagementTypes.Employees.ToString())
        {
            _fileService = fileService;
            SetView(EmployeeViewNames.EmployeeMainView);

            ClearCommand = new DelegateCommand(ClearExecute);
            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            EditCommand = new DelegateCommand(EditExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSaveExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshEmployeesExecute);

            UploadFileCommand = new DelegateCommand(UploadFileExecute);
            DownloadFileCommand = new DelegateCommand(DownloadFileExecute);

            Methods["EmployeeGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        public DelegateCommand UploadFileCommand { get; set; }
        public DelegateCommand DownloadFileCommand { get; set; }

        private void WhenGetRecDone()
        {
            _entity.EmployeeDto.PropertyChanged += (s, e) => { SaveCommand.RaiseCanExecuteChanged(); };
            SaveCommand.RaiseCanExecuteChanged();
        }

        private void ClearExecute()
        {
            RadWindow.Confirm("This will clear everything. Continue?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.EmployeeDto.ClearValues();
            });
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new EmployeeEntity());
            _entity = Entity.GetDto<EmployeeEntity>();

            await OpenItemExecute(SelectedItem.Id);

            SetView(EmployeeViewNames.EmployeeDisplayView);
        }

        private async void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new EmployeeEntity());

            _entity = Entity.GetDto<EmployeeEntity>();
            _entity.IsNew = true;

            await OpenItemExecute("");

            IsClearVisible = true;
            SetView(EmployeeViewNames.EmployeeEditView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<ManagementWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Employees"));
        }

        private void RefreshEmployeesExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        private async void EditExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select an Employee",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            Entity = new DynamicNotifyPropertyChangedProxy(new EmployeeEntity());
            _entity = Entity.GetDto<EmployeeEntity>();

            await OpenItemExecute(SelectedItem.Id);

            IsClearVisible = false;
            SetView(EmployeeViewNames.EmployeeEditView);
        }

        private void BackExecute()
        {
            SetView(EmployeeViewNames.EmployeeMainView);
            RefreshExecute();
        }

        private async void SaveExecute()
        {
            var taskToPerform = _entity.IsNew ? "EmployeeAddRec" : "EmployeeModiRec";
            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSaveExecute()
        {
            var employeeDto = _entity.EmployeeDto.GetDto<EmployeeDto>();
            return !string.IsNullOrEmpty(employeeDto.Name);
        }

        private void UploadFileExecute()
        {
            _fileService.GetAnyFile(_entity.EmployeeFiles);
        }

        private void DownloadFileExecute()
        {
            if (SelectedFile == null) return;
            _fileService.SaveFile(SelectedFile);
        }

        public FileDto SelectedFile
        {
            get { return GetValue(() => SelectedFile); }
            set { SetValue(() => SelectedFile, value); }
        }
    }
}