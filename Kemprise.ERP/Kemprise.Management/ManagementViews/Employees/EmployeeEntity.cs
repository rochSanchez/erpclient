﻿using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using System.Collections.ObjectModel;

namespace Kemprise.Management.ManagementViews.Employees
{
    public class EmployeeEntity : EntityBase
    {
        public EmployeeEntity()
        {
            EmployeeDto = new DynamicNotifyPropertyChangedProxy(new EmployeeDto());

            EmployeeFiles = new ObservableCollection<FileDto>();
        }

        public DynamicNotifyPropertyChangedProxy EmployeeDto { get; set; }

        public ObservableCollection<FileDto> EmployeeFiles { get; set; }
        public FileDto FileToDelete { get; set; }
    }
}