﻿using ERP.WebApi.DTO.DTOs.Authorization;
using ERP.WebApi.DTO.Security.Authentication;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.Files;
using Kemprise.Management.ManagementViews.UserRoles.UserRolesView;
using Kemprise.Management.WorkArea;
using Prism.Commands;
using System;
using System.Windows;
using System.Windows.Media.Imaging;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Management.ManagementViews.UserRoles
{
    public sealed class UserRolesWorkAreaViewModel : ManagementViewModelBase
    {
        private readonly IFileService _fileService;
        private UserRolesEntity _entity;

        public UserRolesWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, ManagementTypes.Users.ToString())
        {
            _fileService = fileService;
            SetView(UserRolesViewNames.MainView);

            RoleToAssignFiltered = new DelegateCommand(() => _entity.SelectedRoleToAssign = null);
            RoleToRemoveFiltered = new DelegateCommand(() => _entity.SelectedRoleToRemove = null);
            OpenCommand = new DelegateCommand(OpenExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshUsersExecute);

            EditCommand = new DelegateCommand(EditExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSaveExecute);
            AssignRoleCommand = new DelegateCommand(AssignRoleExecute);
            RemoveRoleCommand = new DelegateCommand(RemoveRoleExecute);
            AddUserCommand = new DelegateCommand(AddUserExecute);
            AddUserImageCommand = new DelegateCommand(AddUserImageExecute);
        }

        public DelegateCommand RoleToAssignFiltered { get; set; }
        public DelegateCommand RoleToRemoveFiltered { get; set; }
        public DelegateCommand AddUserCommand { get; set; }
        public DelegateCommand RemoveRoleCommand { get; set; }
        public DelegateCommand AssignRoleCommand { get; set; }
        public DelegateCommand AddUserImageCommand { get; set; }

        private void AddUserImageExecute()
        {
            var result = _fileService.ImageToBytes();
            if (result == null) return;
            _entity.NewUser.SetProperty("UserPhoto", result);
            Image = _fileService.ByteArrayToImage(_entity.NewUser.GetDto<RegistrationRequest>().UserPhoto);
        }

        private void AddUserExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new UserRolesEntity());
            _entity = Entity.GetDto<UserRolesEntity>();

            _entity.NewUser.PropertyChanged += (s, e) => SaveCommand.RaiseCanExecuteChanged();
            Image = new BitmapImage(new Uri(@"/Kemprise.Client;component/Images/DefaultUserImage.png", UriKind.Relative));

            SaveCommand.RaiseCanExecuteChanged();
            SetView(UserRolesViewNames.AddUser);
        }

        private async void RemoveRoleExecute()
        {
            if (_entity.SelectedRoleToRemove == null) return;

            var saveResult = await SaveRecordAsync("RemoveUserFromRole");
            if (saveResult.OperationResult != OperationResult.Success) return;

            await GetRecordAsync("GetUserRoles");
        }

        private async void AssignRoleExecute()
        {
            if (_entity.SelectedRoleToAssign == null) return;

            var saveResult = await SaveRecordAsync("AddUserToRole");
            if (saveResult.OperationResult != OperationResult.Success) return;

            await GetRecordAsync("GetUserRoles");
        }

        private async void EditExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select a User",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            Entity = new DynamicNotifyPropertyChangedProxy(new UserRolesEntity());
            _entity = Entity.GetDto<UserRolesEntity>();

            await OpenItemExecute(SelectedItem.As<UserDto>().UserId);

            SetView(UserRolesViewNames.EditUserRoles);
        }

        private void BackExecute()
        {
            SetView(UserRolesViewNames.MainView);
            RefreshExecute();
        }

        private async void SaveExecute()
        {
            var saveResult = await SaveRecordAsync("Register");
            if (saveResult.OperationResult != OperationResult.Success) return;
            BackExecute();
        }

        private void OpenExecute()
        {
        }

        private bool CanSaveExecute()
        {
            var dto = _entity.NewUser.GetDto<RegistrationRequest>();
            return !string.IsNullOrEmpty(dto.FirstName)
                   && !string.IsNullOrEmpty(dto.LastName)
                   && !string.IsNullOrEmpty(dto.Password)
                   && !string.IsNullOrEmpty(dto.Email);
        }

        private void RefreshUsersExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<ManagementWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "User Roles"));
        }

        public BitmapImage Image
        {
            get { return GetValue(() => Image); }
            set { SetValue(() => Image, value); }
        }
    }
}