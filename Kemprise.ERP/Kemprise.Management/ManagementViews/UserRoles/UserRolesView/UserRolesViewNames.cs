﻿namespace Kemprise.Management.ManagementViews.UserRoles.UserRolesView
{
    public class UserRolesViewNames
    {
        public const string MainView = "Users Main View";
        public static string EditUserRoles =  "Edit User Roles View";
        public static string AddUser = "Add User View";
    }
}