﻿using ERP.WebApi.DTO.Security.Authentication;
using Kemprise.Infrastructure.Extensions;
using System.Windows;

namespace Kemprise.Management.ManagementViews.UserRoles.UserRolesView
{
    /// <summary>
    /// Interaction logic for AddUserView.xaml
    /// </summary>
    public partial class AddUserView
    {
        public AddUserView()
        {
            InitializeComponent();
        }

        private void RadPasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            var vm = DataContext.As<UserRolesWorkAreaViewModel>();
            vm.Entity.GetDto<UserRolesEntity>().NewUser.GetDto<RegistrationRequest>().Password = PasswordBox.Password;
        }
    }
}