﻿using System.Collections.ObjectModel;
using ERP.WebApi.DTO.DTOs.Authorization;
using ERP.WebApi.DTO.Security.Authentication;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.Management.ManagementViews.UserRoles
{
    public class UserRolesEntity : EntityBase
    {
        public UserRolesEntity()
        {
            User = new DynamicNotifyPropertyChangedProxy(new UserRolesDto());
            NewUser = new DynamicNotifyPropertyChangedProxy(new RegistrationRequest());
            UserRoles = new ObservableCollection<UserRole>();
            Roles = new ObservableCollection<UserRole>();
        }

        public UserRole SelectedRoleToRemove { get; set; }
        public UserRole SelectedRoleToAssign { get; set; }

        public DynamicNotifyPropertyChangedProxy User { get; set; }
        public ObservableCollection<UserRole> UserRoles { get; set; }
        public ObservableCollection<UserRole> Roles { get; set; }

        public DynamicNotifyPropertyChangedProxy NewUser { get; set; }
    }
}