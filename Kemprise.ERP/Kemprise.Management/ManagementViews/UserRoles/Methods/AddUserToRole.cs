﻿using ERP.WebApi.DTO.DTOs.Authorization;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Threading.Tasks;

namespace Kemprise.Management.ManagementViews.UserRoles.Methods
{
    public class AddUserToRole : ErpSaveMethodCallBase
    {
        public AddUserToRole(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "AddUserToRole";
        public override ErpModules ModuleName => ErpModules.Management;
        public override ApiControllers Controller => ApiControllers.Account;
        public override string Category => ManagementTypes.Users.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var userEntity = entity.As<UserRolesEntity>();

            var dataToSave = new UserToRoleDto
            {
                RoleName = userEntity.SelectedRoleToAssign.Name,
                UserName = userEntity.User.GetDto<UserDto>().UserName
            };

            await UpdateRecordAsync(MethodName, dataToSave);
        }
    }
}