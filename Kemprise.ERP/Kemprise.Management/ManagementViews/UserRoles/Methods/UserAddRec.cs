﻿using ERP.WebApi.DTO.Security.Authentication;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Management.ManagementViews.UserRoles.Methods
{
    public class UserAddRec : ErpSaveMethodCallBase
    {
        public UserAddRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "Register";
        public override ErpModules ModuleName => ErpModules.Management;
        public override ApiControllers Controller => ApiControllers.Account;
        public override string Category => ManagementTypes.Users.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var dataToSave = entity.As<UserRolesEntity>().NewUser.GetDto<RegistrationRequest>();

            var data = new Dictionary<string, object>
            {
                { nameof(RegistrationRequest), dataToSave }
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}