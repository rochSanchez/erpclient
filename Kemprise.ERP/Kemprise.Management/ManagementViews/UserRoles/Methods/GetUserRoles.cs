﻿using ERP.WebApi.DTO.DTOs.Authorization;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Kemprise.Management.ManagementViews.UserRoles.Methods
{
    public class GetUserRoles : GetMethodCallBase
    {
        public GetUserRoles(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "GetUserRoles";
        public override ErpModules ModuleName => ErpModules.Management;
        public override string Category => ManagementTypes.Users.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Account;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var result = await GetRecordAsync<UserRolesDto>(MethodName, new object[] { MethodParameter });
            entity.SetProperty("User", new DynamicNotifyPropertyChangedProxy(result.User));

            var roles = new ObservableCollection<UserRole>(result.Roles.ToList());
            var userRoles = new ObservableCollection<UserRole>(result.UserRoles.ToList());

            entity.SetProperty("Roles", roles);
            entity.SetProperty("UserRoles", userRoles);
        }
    }
}