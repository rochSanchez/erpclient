﻿using ERP.WebApi.DTO.DTOs.Management.Currency;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Management.ManagementViews.Currency.Methods
{
    public class CurrencyAddRec : ErpSaveMethodCallBase
    {
        public CurrencyAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "CurrencyAddRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override ApiControllers Controller => ApiControllers.Currency;
        public override string Category => ManagementTypes.Currencies.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var currencyDto = entity.As<CurrencyEntity>().CurrencyDto.GetDto<CurrencyDto>();

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(CurrencyDto), currencyDto }
            };

            await SaveNewRecordAsync<CurrencyDto>(MethodName, dataToSave);
        }
    }
}