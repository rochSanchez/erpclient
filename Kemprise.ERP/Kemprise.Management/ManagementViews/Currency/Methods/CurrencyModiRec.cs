﻿using ERP.WebApi.DTO.DTOs.Management.Currency;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kemprise.Management.ManagementViews.Currency.Methods
{
    public class CurrencyModiRec : ErpSaveMethodCallBase
    {
        public CurrencyModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "CurrencyModiRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override ApiControllers Controller => ApiControllers.Currency;
        public override string Category => ManagementTypes.Currencies.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var vendorDto = entity.As<CurrencyEntity>().CurrencyDto.GetDto<CurrencyDto>();

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(CurrencyDto), vendorDto }
            };

            await SaveNewRecordAsync<CurrencyDto>(MethodName, dataToSave);
        }
    }
}