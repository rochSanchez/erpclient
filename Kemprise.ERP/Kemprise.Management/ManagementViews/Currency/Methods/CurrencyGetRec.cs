﻿using ERP.WebApi.DTO.DTOs.Management.Currency;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Infrastructure.Services.APICommon;
using System.Threading.Tasks;

namespace Kemprise.Management.ManagementViews.Currency.Methods
{
    public class CurrencyGetRec : GetMethodCallBase
    {
        public CurrencyGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "CurrencyGetRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override string Category => ManagementTypes.Currencies.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Currency;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var currencyDto = await GetRecordAsync<CurrencyDto>(MethodName, new object[] { MethodParameter });
            if (currencyDto == null) return;

            entity.SetProperty("CurrencyDto", new DynamicNotifyPropertyChangedProxy(currencyDto));
        }
    }
}