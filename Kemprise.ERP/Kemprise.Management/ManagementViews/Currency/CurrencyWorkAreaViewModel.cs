﻿using ERP.WebApi.DTO.DTOs.Management.Currency;
using Kemprise.Infrastructure.Constants;
using Kemprise.Infrastructure.Events.Modules;
using Kemprise.Infrastructure.Extensions;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;
using Kemprise.Management.ManagementViews.Currency.CurrencyViews;
using Kemprise.Management.WorkArea;
using Prism.Commands;
using System.Collections.Generic;
using System.Windows;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Kemprise.Management.ManagementViews.Currency
{
    public sealed class CurrencyWorkAreaViewModel : ManagementViewModelBase
    {
        private CurrencyEntity _entity;

        public CurrencyWorkAreaViewModel(IUnityContainer container) :
            base(container, ManagementTypes.Currencies.ToString())
        {
            SetView(CurrencyViewNames.CurrencyMainView);

            ClearCommand = new DelegateCommand(ClearExecute);
            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            OpenCommand = new DelegateCommand(OpenExecute);
            EditCommand = new DelegateCommand(EditExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSaveExecute);
            RefreshCommand = new DelegateCommand<object>(RefreshCurrenciesExecute);

            WorldCurrencies = WorldService.GetAllCurrencies();

            Methods["CurrencyGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        private void WhenGetRecDone()
        {
            _entity.CurrencyDto.PropertyChanged += (s, e) => { SaveCommand.RaiseCanExecuteChanged(); };
            SaveCommand.RaiseCanExecuteChanged();
        }

        private void ClearExecute()
        {
            RadWindow.Confirm("This will clear everything. Continue?", (s, e) =>
            {
                if (e.DialogResult == false || e.DialogResult == null) return;

                _entity.CurrencyDto.ClearValues();
            });
        }

        private async void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new CurrencyEntity());
            _entity = Entity.GetDto<CurrencyEntity>();

            await OpenItemExecute(SelectedItem.Id);

            SetView(CurrencyViewNames.CurrencyDisplayView);
        }

        private async void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new CurrencyEntity());

            _entity = Entity.GetDto<CurrencyEntity>();
            _entity.IsNew = true;

            await OpenItemExecute("");

            IsClearVisible = true;
            SetView(CurrencyViewNames.CurrencyEditView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent<ManagementWorkAreaViewModel>>()
                .Publish(new RefreshItemListEventPayload(this, "Currencies"));
        }

        private void RefreshCurrenciesExecute(object obj)
        {
            RefreshExecute();
            var gridView = obj as RadGridView;
            gridView?.GroupDescriptors.Clear();
            gridView?.SortDescriptors.Clear();
            gridView?.FilterDescriptors.Clear();
        }

        private async void EditExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select a Currency",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            Entity = new DynamicNotifyPropertyChangedProxy(new CurrencyEntity());
            _entity = Entity.GetDto<CurrencyEntity>();

            await OpenItemExecute(SelectedItem.Id);

            IsClearVisible = false;
            SetView(CurrencyViewNames.CurrencyEditView);
        }

        private void BackExecute()
        {
            SetView(CurrencyViewNames.CurrencyMainView);
            RefreshExecute();
        }

        private async void SaveExecute()
        {
            var taskToPerform = _entity.IsNew ? "CurrencyAddRec" : "CurrencyModiRec";
            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSaveExecute()
        {
            var currencyDto = _entity.CurrencyDto.GetDto<CurrencyDto>();
            return !string.IsNullOrEmpty(currencyDto.Name)
                && !string.IsNullOrEmpty(currencyDto.Code);
        }

        public IReadOnlyList<WorldCurrency> WorldCurrencies
        {
            get { return GetValue(() => WorldCurrencies); }
            set { SetValue(() => WorldCurrencies, value); }
        }

        public WorldCurrency SelectedWorldCurrency
        {
            get { return GetValue(() => SelectedWorldCurrency); }
            set
            {
                SetValue(() => SelectedWorldCurrency, value);
                if (value == null) return;
                _entity.CurrencyDto.SetProperty("Name", value.Name);
                _entity.CurrencyDto.SetProperty("Code", value.Code);
                SelectedWorldCurrency = null;
            }
        }
    }
}