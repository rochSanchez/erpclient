﻿namespace Kemprise.Management.ManagementViews.Currency.CurrencyViews
{
    public class CurrencyViewNames
    {
        public const string CurrencyMainView = "Currency Main View";
        public const string CurrencyEditView = "Currency Edit View";
        public const string CurrencyDisplayView = "Currency Display View";
    }
}