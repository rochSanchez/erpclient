﻿using ERP.WebApi.DTO.DTOs.Management.Currency;
using Kemprise.Infrastructure.MethodCalls;
using Kemprise.Infrastructure.PropertyChangeNotification;

namespace Kemprise.Management.ManagementViews.Currency
{
    public class CurrencyEntity : EntityBase
    {
        public CurrencyEntity()
        {
            CurrencyDto = new DynamicNotifyPropertyChangedProxy(new CurrencyDto());
        }

        public DynamicNotifyPropertyChangedProxy CurrencyDto { get; set; }
    }
}