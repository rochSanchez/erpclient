﻿using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Management.ImportViews.PartImport;
using System;
using Unity;

namespace Kemprise.Management.NavigationItems
{
    public class PartImportNavItem : ParentNavigationItemBase
    {
        private readonly PartImportWorkAreaView _workAreaView;

        public PartImportNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<PartImportWorkAreaView>();

            IsVisible = false;
        }

        public override string Name => "Part Import";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Import.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Management;

        public override object View => _workAreaView;
    }
}