﻿using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Management.ManagementViews.Employees;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.Management.NavigationItems
{
    public class EmployeeNavItem : ParentNavigationItemBase
    {
        private readonly EmployeeWorkAreaView _workAreaView;

        public EmployeeNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<EmployeeWorkAreaView>();

            // IsVisible = ApiSession.HasRole(ErpRoles.SuperUser);
            IsVisible = false;
        }

        public override string Name => "Employees";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Employee.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Management;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<EmployeeListItem>(ApiControllers.Employee, "EmployeeGetRecList");
        }
    }
}