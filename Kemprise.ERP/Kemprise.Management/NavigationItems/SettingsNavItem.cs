﻿using ERP.WebApi.DTO;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Management.ManagementViews.Settings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.Management.NavigationItems
{
    public class SettingsNavItem : ParentNavigationItemBase
    {
        private readonly SettingsWorkAreaView _workAreaView;

        public SettingsNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<SettingsWorkAreaView>();

            IsVisible = false;
        }

        public override string Name => "Settings";

        public override Uri Icon => new Uri("", UriKind.Relative);

        public override ErpModules Module => ErpModules.Management;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await Task.Run(() => new List<EntityListItem>());
        }
    }
}