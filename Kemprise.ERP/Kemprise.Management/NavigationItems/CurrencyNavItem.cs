﻿using ERP.WebApi.DTO;
using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Management.Currency;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Management.ManagementViews.Currency;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.Management.NavigationItems
{
    public class CurrencyNavItem : ParentNavigationItemBase
    {
        private readonly CurrencyWorkAreaView _workAreaView;

        public CurrencyNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<CurrencyWorkAreaView>();

            IsVisible = ApiSession.HasRole(ErpRoles.SuperUser);
        }

        public override string Name => "Currencies";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Currency.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Management;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<CurrencyListItem>(ApiControllers.Currency, "CurrencyGetRecList");
        }
    }
}