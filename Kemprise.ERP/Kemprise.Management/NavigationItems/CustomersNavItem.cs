﻿using ERP.WebApi.DTO;
using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Management.Customer;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Management.ManagementViews.Customer;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.Management.NavigationItems
{
    public class CustomersNavItem : ParentNavigationItemBase
    {
        private readonly CustomerWorkAreaView _workAreaView;

        public CustomersNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<CustomerWorkAreaView>();

            IsVisible = ApiSession.HasRoles(ErpRoles.SalesRoles);
        }

        public override string Name => "Customers";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Customer.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Management;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<CustomerListItem>(ApiControllers.Customer, "CustomerGetRecList");
        }
    }
}