﻿using ERP.WebApi.DTO;
using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Management.Supplier;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Management.ManagementViews.Supplier;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.Management.NavigationItems
{
    public class SupplierNavItem : ParentNavigationItemBase
    {
        private readonly SupplierWorkAreaView _workAreaView;

        public SupplierNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<SupplierWorkAreaView>();

            IsVisible = ApiSession.HasRoles(ErpRoles.CreatePurchaseRequest);
        }

        public override string Name => "Suppliers";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/Supplier.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Management;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<SupplierListItem>(ApiControllers.Supplier, "SupplierGetRecList");
        }
    }
}