﻿using ERP.WebApi.DTO;
using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Authorization;
using Kemprise.Infrastructure.Modules;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Services.APICommon;
using Kemprise.Management.ManagementViews.UserRoles;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace Kemprise.Management.NavigationItems
{
    public class UserRolesNavItem : ParentNavigationItemBase
    {
        private readonly UserRolesWorkAreaView _workAreaView;

        public UserRolesNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<UserRolesWorkAreaView>();

            IsVisible = ApiSession.HasRole(ErpRoles.SuperUser);
        }

        public override string Name => "User Roles";

        public override Uri Icon => new Uri("/Kemprise.Client;component/Images/User.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Management;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            var methodParameter = ApiSession.HasRole(ErpRoles.SuperUser)
                ? RoleCategory.None
                : RoleCategory.Sales;
            return await RequestManager.GetRecordsAsync<UserDto>(ApiControllers.Account, "GetUsers", new object[] { methodParameter });
        }
    }
}