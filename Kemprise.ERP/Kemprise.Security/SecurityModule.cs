﻿using Kemprise.Infrastructure.Modules;
using Kemprise.Security.Controller;
using System.Reflection;
using Unity;

namespace Kemprise.Security
{
    public class SecurityModule : ErpModule
    {
        private readonly IUnityContainer _unityContainer;

        public SecurityModule(IUnityContainer unityContainer)
        {
            _unityContainer = unityContainer;
        }

        protected override Assembly GetBusinessAssembly()
        {
            return Assembly.GetAssembly(typeof(SecurityController));
        }

        protected override Assembly GetModuleAssembly()
        {
            return Assembly.GetAssembly(typeof(SecurityController));
        }
    }
}