﻿using Kemprise.Infrastructure.Controllers;
using Kemprise.Infrastructure.Events.Security;
using Kemprise.Infrastructure.Modules.Navigation;
using Kemprise.Infrastructure.Regions;
using Kemprise.Security.Login;
using Prism.Events;
using Prism.Regions;
using System.Linq;
using Unity;
using Unity.RegistrationByConvention;

namespace Kemprise.Security.Controller
{
    public class SecurityController : ControllerBase
    {
        private UserLoginView _userLoginView;

        public SecurityController(IEventAggregator eventAggregator, IRegionManager regionManager, IUnityContainer container) :
            base(eventAggregator, regionManager, container)
        {
            EventAggregator.GetEvent<RequestUserCredentialsEvent>().Subscribe(OnUserLogin);
            EventAggregator.GetEvent<LoginSuccessfulEvent>().Subscribe(OnUserLoginSuccessful);
            EventAggregator.GetEvent<LogoutUserEvent>().Subscribe(OnLogoutUserEvent);
        }

        private void OnLogoutUserEvent()
        {
            Container.RegisterTypes(AllClasses.FromLoadedAssemblies().Where(x => typeof(IParentNavigationItem).IsAssignableFrom(x)),
                WithMappings.FromAllInterfaces, WithName.TypeName, WithLifetime.ContainerControlled);
            Container.RegisterTypes(AllClasses.FromLoadedAssemblies().Where(x => typeof(IChildNavigationItem).IsAssignableFrom(x)),
                WithMappings.FromAllInterfaces, WithName.TypeName, WithLifetime.ContainerControlled);
        }

        private void OnUserLoginSuccessful(LoginSuccessfulEventPayload obj)
        {
            if (obj.IsSuccessful)
                RegionManager.Regions[RegionNames.DialogRegion].Deactivate(_userLoginView);
        }

        private void OnUserLogin()
        {
            _userLoginView = Container.Resolve<UserLoginView>();
            RegionManager.RegisterViewWithRegion(RegionNames.DialogRegion, () => _userLoginView);
        }
    }
}